@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Detail Bonus')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-credit-card"></i> Detail Bonus
        </h1>
    </div>
    {{--cek CSS ADMIN--}}
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
@stop

@section('content')
    @php($no=1)
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12 center">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        {{--<a href="/admin/report-bonus?start_date={{request('start')}}&end_date={{request('end')}}&status={{request('status')}}" class="btn btn-primary">Kembali</a> <a href="/admin/export-bonus?type={{request('type')}}&start={{request('start')}}&end={{request('end')}}&status={{request('status')}}" class="btn btn-success">Export</a>--}}
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <tr>
                                    <th>No</th>
                                    <th>Member From</th>
                                    <th>Memember To</th>
                                    <th>Tipe Bonus</th>
                                    <th>Deskripsi</th>
                                    <th>Jumlah Bonus</th>
                                    <th>Tanggal Bayar</th>
                                    <th>Tanggal Bonus</th>
                                    <th>Status</th>
                                </tr>
                                @foreach($bonus as $values)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$values->memberFrom->member_name}}</td>
                                        <td>{{$values->memberTo->member_name}}</td>
                                        <td>{{$values->bonus_type}}</td>
                                        <td>{{$values->bonus_desc}}</td>
                                        <td>Rp {{$values->bonus_amount}}</td>
                                        <td>{{$values->paid_at}}</td>
                                        <td>{{$values->created_at}}</td>
                                        <td>{{$values->bonus_status}}</td>
                                    </tr>
                                @endforeach
                            </table>
                            {{$bonus->appends(request()->input())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
