@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Report Polish')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-credit-card"></i> Report Polish
        </h1>
    </div>
@stop

@section('content')
    @php($no = 1)
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12 center">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th width="30" >No</th>
                                    <th width="150">Periode</th>
                                    <th width="40" >Jumlah</th>
                                    <th width="40" >Status</th>
                                    <th width="250"  align="center">Paid Date</th>
                                    <th width="100" align="right">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($report_polish as $data)
                                    {{--                                            @php($month = DateTime::createFromFormat('!m', $data->polis_date))--}}
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{date('M',strtotime($data->polis_date))." ".date('Y',strtotime($data->polis_date))}}</td>
                                        <td>{{$data->total}}</td>

                                        @if($data->fee_status != NULL && $data->paid_date != NULL)
                                            <td>Terbayarkan</td>
                                            <td>{{date('D, d M Y H:i:s',strtotime($data->paid_date))}}</td>
                                        @else
                                            <td>Pending</td>
                                            <td>-</td>
                                        @endif
                                        <td>
                                            <a href="/admin/polish-payments?start_date={{date('Y-m-01',strtotime($data->polis_date))}}&&end_date={{date('Y-m-31',strtotime($data->polis_date))}}"
                                               class="btn-sm btn-primary">Rincian</a>
                                            <a href="export-polish?start_date={{date('Y-m-01',strtotime($data->polis_date))}}&&end_date={{date('Y-m-31',strtotime($data->polis_date))}}    "
                                               class="btn-sm btn-success">Export Excel</a>
                                            @if(Auth::user()->role_id==1)
                                                <a href="/admin/paid-polis/{{date('Ym',strtotime($data->polis_date))}}"
                                                   class="btn-sm btn-success">Paid</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
