@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Report Polish')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-credit-card"></i> Report Bonus
        </h1>
    </div>
    {{--cek CSS ADMIN--}}
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12 center">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <form action="" method="GET">
                                    <div class="form-group col-md-2">
                                        <label>Tanggal awal :</label>
                                        <input type="date" class="form-control" name="start_date"
                                               value="{{request("start_date")}}">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Tanggal akhir :</label>
                                        <input type="date" class="form-control" name="end_date"
                                               value="{{request("end_date")}}">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label></label><br>
                                        <button class="btn btn-primary" type="submit">Cari</button>
                                        <a href="/admin/report-bonus" class="btn btn-default" type="submit">Reset</a>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <!-- small box -->
                                <div class="small-box bg-blue">
                                    <div class="inner">
                                        <h3>Rp {{number_format($generasi,0,',','.')}}</h3>

                                        <p>Total Bonus Generasi</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                    <a href="/admin/detail-report-bonus?status=generasi&start={{request('start_date')}}&end={{request('end_date')}}"
                                       class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col">
                                <!-- small box -->
                                <div class="small-box bg-yellow">
                                    <div class="inner">
                                        <h3>Rp {{number_format($sponsor,0,',','.')}}</h3>

                                        <p>Jumlah Bonus Sponsor</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                    <a href="/admin/detail-report-bonus?type=sponsor&start={{request('start_date')}}&end={{request('end_date')}}"
                                       class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col">
                                <!-- small box -->
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <h3>Rp. {{number_format($unqualified,0,',','.')}}</h3>

                                        <p>Total Bonus Unqualified Member</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                    <a href="/admin/detail-report-bonus?status=unqualified&start={{request('start_date')}}&end={{request('end_date')}}"
                                       class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col">
                                <!-- small box -->
                                <div class="small-box bg-red">
                                    <div class="inner">
                                        <h3>Rp. {{number_format($freeze,0,',','.')}}</h3>

                                        <p>Total Bonus Freeze Member</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                    <a href="/admin/detail-report-bonus?status=freeze&start={{request('start_date')}}&end={{request('end_date')}}"
                                       class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col">
                                <!-- small box -->
                                <div class="small-box bg-aqua">
                                    <div class="inner">
                                        <h3>Rp. {{number_format($total,0,',','.')}}</h3>

                                        <p>Total Bonus Keseluruhan</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                    <a href="/admin/detail-report-bonus?status=total&start={{request('start_date')}}&end={{request('end_date')}}"
                                       class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
