@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Report Polish')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-credit-card"></i> Detail Report Bonus @if(request('type')!=NULL) {{strtoupper(request('type'))}} @else {{strtoupper(request('status'))}} @endif
        </h1>
    </div>
    {{--cek CSS ADMIN--}}
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
@stop

@section('content')
    @php($no=1)
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12 center">
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>Rp {{number_format($new,0,',','.')}}</h3>

                                <p>New</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            {{--<a href="/admin/detail-report-bonus?status=generasi&start={{request('start_date')}}&end={{request('end_date')}}" class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>--}}
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <h3>Rp {{number_format($paid,0,',','.')}}</h3>

                                <p>Paid</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            {{--<a href="/admin/detail-report-bonus?status=generasi&start={{request('start_date')}}&end={{request('end_date')}}" class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>--}}
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>Rp {{number_format($jumlah,0,',','.')}}</h3>

                                <p>Total</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            {{--<a href="/admin/detail-report-bonus?status=generasi&start={{request('start_date')}}&end={{request('end_date')}}" class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>--}}
                        </div>
                    </div>
                </div>
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <a href="/admin/report-bonus?start_date={{request('start')}}&end_date={{request('end')}}&status={{request('status')}}" class="btn btn-primary">Kembali</a> <a href="/admin/export-bonus?type={{request('type')}}&start={{request('start')}}&end={{request('end')}}&status={{request('status')}}" class="btn btn-success">Export</a>
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Member From</th>
                                        <th>Member To</th>
                                        <th>Tipe Bonus</th>
                                        <th>Deskripsi</th>
                                        <th>Jumlah Bonus</th>
                                        <th>Status</th>
                                        <th>Tanggal Bayar</th>
                                        <th>Tanggal Bonus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($bonus as $bonuses)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$bonuses->memberFrom->member_name}}</td>
                                        <td>{{$bonuses->memberTo->member_name}}</td>
                                        <td>{{$bonuses->bonus_type}}</td>
                                        <td>{{$bonuses->bonus_desc}}</td>
                                        <td>{{number_format($bonuses->bonus_amount,0,',','.')}}</td>
                                        <td>{{$bonuses->bonus_status}}</td>
                                        <td>{{$bonuses->paid_at}}</td>
                                        <td>{{$bonuses->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$bonus->appends(request()->input())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
