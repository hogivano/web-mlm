@extends('voyager::master')

@section('page_title')
    Networks
@stop

@section('page_header')
    <h1 class="page-title"><i class="voyager-trees"></i> Pohon Jaringan</h1>
@stop

<style type="text/css">
    .test{
        padding: 10px;
        list-style-type: none;
        float: right;
    }

    .test li{
        float: left;
        margin-left: 20px;
        margin-right: 20px;
    }
</style>

@section('content')
<div class="page-content browse container-fluid">
    <div class="panel panel-bordered" style="height:100%; width:100%; display:table;">
        <div class="body">
            <div class="row mb-0">
                <div class="col-sm-12">
                    <ul class="test">
                        <li><a href="/admin/networks" class="btn btn-default">Ke level 1</a></li>
                        <li><a href="{{$uplineId}}" class="btn btn-primary">Naik 1 level</a></li>
                        <li>
                            <form action="/admin/networks/{{request('username')}}" class="form-horizontal" method="GET">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Menuju</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" placeholder="Masukan Username" id="autocomplete" name="username">
                                        </div>
                                        <div class="col-sm-1" style="margin-top: -6px">
{{--                                            <button class="btn btn-primary">Go</button>--}}
                                            <a href="" class="btn btn-primary go_link">Go</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12">
                <div class='dd'>
                    @if(Request::path() !== 'admin/networks')
                        <li class='dd-item upline_member' >
                            <div class='dd-handle'>
                                <a href='/admin/networks/{{$upline['member_id']}}' style='padding-left: 13px;'>{{ $upline['member_name']  }}
                                    <i style="padding-left: 5px" class="zmdi zmdi-upload"></i>
                                </a>
                                <span class='pull-right'></span>
                            </div>
                        </li>
                        <li class='dd-item current_member'>
                            <div class='dd-handle'>
                                <a href='#' style='padding-left: 13px;'>{{ $current['member_name'] }}
                                </a>
                                <span class='pull-right'></span>
                            </div>
                        </li>
                        <div id="pad-to-right" style="margin-left:30px;">
                            @endif
                            {!! $networks !!}
                            @if(Request::path() !== 'admin/networks')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    {{--        <link rel="stylesheet" href="{{URL::asset('/assets/plugins/bootstrap/css/bootstrap.min.css')}}">--}}
    <style type="text/css">
        .ui-autocomplete{
            z-index: 9999999;
        }
    </style>
    <link rel="stylesheet" href="http://sibayakindonesia.com/assets/css/style-admin.min.css">
<style>
    @import url("https://fonts.googleapis.com/css?family=Comfortaa:300,400,700");

    @font-face {
        font-family: Material-Design-Iconic-Font;
        src: url(http://sibayakindonesia.com/assets/fonts/Material-Design-Iconic-Fontd1f1.woff2?v=2.2.0) format("woff2"), url(http://sibayakindonesia.com/assets/fonts/Material-Design-Iconic-Fontd1f1.woff?v=2.2.0) format("woff"), url(http://sibayakindonesia.com/assets/fonts/Material-Design-Iconic-Fontd1f1.ttf?v=2.2.0) format("truetype")
    }

    .zmdi {
    display: inline-block;
    font: normal normal normal 14px/1 'Material-Design-Iconic-Font';
    font-size: 18px;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale
    }

    .zmdi-eye-off:before {
    content: '\f15b'
    }

    .zmdi-eye:before {
    content: '\f15c'
    }

    .zmdi-eyedropper:before {
    content: '\f15d'
    }
</style>
@stop

@section('javascript')
    {{--<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>--}}
{{--<script src="{{ asset('/') }}assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->--}}
{{--<script src="{{ asset('/') }}assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->--}}
<script src="{{ asset('/') }}assets/plugins/nestable/jquery.nestable.js"></script> <!-- Jquery Nestable -->
{{--<script src="{{ asset('/') }}assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->--}}
<script src="{{ asset('/') }}assets/js/pages/ui/sortable-nestable.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script>
    $(document).ready(function(){
        let pathname = "<?php echo $_SERVER['REQUEST_URI']; ?>"

        let hidden = ".dd-item .dd-list ";
        let strNull =
            "<div class='dd-handle'>\n" +
            "<a style='color:grey;padding-left: 13px;'>\n" +
            "<i class='zmdi zmdi-account' style='padding-right:10px;'></i>KOSONG\n" +
            "</a>\n" +
            "<span class='pull-right'><i class='voyager-eye' style='padding-right: 8px;'></i>  0 Downline</span>\n" +
            "</div>";

        let ulLen = $('.dd-item .dd-list').length;
        for (let i = 0; i < ulLen; i++) {
            let el = '.dd-item .dd-list:eq(' + i + ')';
            let cldLen = $(el).children().length;
            if (cldLen < 5) {
                $(el).last().append(strNull.repeat(5 - cldLen))
            }
        }

        $('.dd .dd-list ' + hidden.repeat({{$levelKedalaman}})).remove();

        if(pathname === '/admin/networks/1'){
            $('.upline_member').hide()
        }

        if(pathname !== '/admin/networks') {
            let cldRoot = $('#pad-to-right').children().length;
            if(cldRoot === 0){
                $('#pad-to-right').append(strNull.repeat(5))
            }

            var thisFirst = $(".dd .dd-list:eq(0)").children().length;
            if(thisFirst < 5 ){
                $('.dd .dd-list:eq(0)').last().append(strNull.repeat(5 - thisFirst))
            }
        }
    });
</script>

    <script type="text/javascript">
        // Single Select
        $("#autocomplete").autocomplete({
            source: function (request, response) {
                // Fetch data
                $.ajax({
                    url: "/admin/get-username",
                    type: 'get',
                    dataType: "json",
                    data: {
                        search: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#autocomplete').val(ui.item.value); // display the selected text
                // $('#selectuser_id').val(ui.item.value); // save selected id to input
                $('.go_link').attr("href","/admin/networks/"+ui.item.member_id); // save selected id to input
                return false;
            }
        });

        $( function() {
        });
        function split( val ) {
            return val.split( /,\s*/ );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }
    </script>
@stop
