@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Withdrawal')
<style type="text/css">
    .test{
        list-style-type: none;
        float: right;
    }
    .test li{
        float: left;
        margin-left: 20px;
    }
</style>
{{--<link rel="stylesheet" href="http://sibayakindonesia.com/assets/css/style-admin.min.css">--}}
@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-credit-card"></i> Withdrawal
        </h1>
    </div>

    <!-- Small boxes (Stat box) -->
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>Rp {{number_format($amount,0,',','.')}}</h3>

                        <p>Jumlah Uang</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>Rp {{number_format($ppn_amount,0,',','.')}}</h3>
                        <p>Jumlah Uang PPN</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
            </div>

            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>Rp {{number_format($pph_amount,0,',','.')}}</h3>
                        <p>Jumlah Uang PPH</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>Rp {{number_format($amount-$ppn_amount-$pph_amount,0,',','.')}}</h3>

                        <p>Total Transfer</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ./col -->
@stop

@section('content')
    @php($no = 1)
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                            @if(count($withdrawal_list) == 0)
                            <div class="alert alert-warning">
                                <p>Saat ini tidak ada withdrawal dengan status pending.</p>
                            </div>

                                @else
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="test">
                                        <li>
                                            @if($pending > 0)
                                                <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-default">Update Status Proses</button>
                                            @elseif($proses > 0)
                                                <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#modal-default">Update Status Done</button>
                                            @elseif($pending == 0 && $proses == 0)
                                            @else
                                            @endif
                                        </li>
                                        <li><a href="export-excel?status={{request('status')}}" class="btn btn-success">Export Excel</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Member</th>
                                            <th>Uang</th>
                                            <th>PPN</th>
                                            <th>Jumlah PPN</th>
                                            <th>PPH</th>
                                            <th>Jumlah PPH</th>
                                            <th>Jumlah Transfer</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($withdrawal_list as $withdrawal_item)
                                            <tr>
                                                <th>{{$no++}}</th>
                                                <th>{{$withdrawal_item->mutasi->member->member_name}}</th>
                                                <th>Rp {{number_format($withdrawal_item->amount,0,',','.')}}</th>
                                                <th>{{$withdrawal_item->ppn}}%</th>
                                                <th>Rp {{number_format($withdrawal_item->ppn_amount,0,',','.')}}</th>
                                                <th>{{$withdrawal_item->pph}}%</th>
                                                <th>Rp {{number_format($withdrawal_item->pph_amount,0,',','.')}}</th>
                                                <th><b>Rp {{number_format(($withdrawal_item->amount-$withdrawal_item->ppn_amount-$withdrawal_item->pph_amount),0,',','.')}}</b></th>
                                                <th>{{$withdrawal_item->withdrawal_status}}</th>
                                                <th>{{date('D, d M Y H:i:s',strtotime($withdrawal_item->created_at))}}</th>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{$withdrawal_list->links()}}
                            </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                @if($pending>0)
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Pemberitahuan</h4>
                    </div>
                    <div class="modal-body">
                        <p>Klik button PROSES jika withdrawal sedang dalam proses pengiriman.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <a href="withdrawal-proses" class="btn btn-primary">Proses</a>
                    </div>
                    @else
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Pemberitahuan</h4>
                    </div>
                    <div class="modal-body">
                        <p>Klik button SELESAI jika proses pengiriman selesai.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <a href="withdrawal-sukses" class="btn btn-success">Selesai</a>
                    </div>
                @endif
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@stop

@section('css')
    {{--cek CSS ADMIN--}}
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/assets/css/skins/_all-skins.min.css">


    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
            {{--<link rel="stylesheet" href="{{URL::asset('/assets/plugins/bootstrap/css/bootstrap.min.css')}}">--}}

@stop

@section('javascript')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- jQuery 3 -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
@stop
