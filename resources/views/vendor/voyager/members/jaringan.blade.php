@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Pohon Jaringan')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            Pohon Jaringan
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <style>
                                /* CSS Tree menu styles */
                                ol.tree {
                                    padding: 0 0 0 30px;
                                    width: 300px;

                                }
                                li {
                                    position: relative;
                                    margin-left: -15px;
                                    list-style: none;
                                }
                                li input {
                                    position: absolute;
                                    left: 0;
                                    margin-left: 0;
                                    opacity: 0;
                                    z-index: 2;
                                    cursor: pointer;
                                    height: 1em;
                                    width: 1em;
                                    top: 0;
                                }
                                li input + ol {
                                    background: url('http://sibayakindonesia.com/img/tree/plus.png') 37px 3px  no-repeat;
                                    margin: -1.95em 0 0 -40px;
                                    padding: 2px;
                                    height: 1.3em;
                                }
                                li input + ol > li {
                                    display: none;
                                    margin-left: -14px !important;
                                    padding-left: 1px;
                                }
                                li label {
                                    background: url('') 15px 6px no-repeat;
                                    cursor: pointer;
                                    display: block;
                                    padding-left: 20px;
                                }
                                li input:checked + ol {
                                    background: url('http://sibayakindonesia.com/img/tree/minus.png') 37px 3px no-repeat;
                                    margin: -1.95em 0 0 -40px;
                                    padding: 1.563em 0 0 80px;
                                    height: auto;
                                }
                                li input:checked + ol > li {
                                    display: block;
                                    margin: 0px 0px 0px 0.125em;
                                }
                                li input:checked + ol > li:last-child {
                                    margin: 8px 0 0.063em;
                                }

                            </style>
                            <?php
                                function createTreeView($parent, $menu) {
                                    $html = "";
                                    if (isset($menu['parents'][$parent])) {
                                        $html .= "<ol class='tree'>";
                                        foreach ($menu['parents'][$parent] as $itemId) {
                                            if(!isset($menu['parents'][$itemId])) {
                                                $html .= "<li><label for='subfolder2'><a href='/admin/members/$itemId'>".$menu['items'][$itemId]['member_name']."</a></label> <input type='checkbox' name='subfolder2'/></li>";
                                            }
                                            if(isset($menu['parents'][$itemId])) {
                                                $html .= "<li><label for='subfolder2'><a href='/admin/members/$itemId'>".$menu['items'][$itemId]['member_name']."</a></label> <input type='checkbox' name='subfolder2'/>";
                                                $html .= createTreeView($itemId, $menu);
                                                $html .= "</li>";
                                            }
                                        }
                                        $html .= "</ol>";
                                    }
                                    return $html;
                                }
                                $menus = array(
                                    'items' => array(),
                                    'parents' => array()
                                );

                            $dataTypeContent = $users;
                            foreach ($dataTypeContent as $items){
                                //      Create current menus item id into array
                                $menus['items'][$items['member_id']] = $items;
                                //      Creates list of all items with children
                                $menus['parents'][$items['member_network']][] = $items['member_id'];
                            }
                            ?>
                            <h2>Pohon jaringan</h2>
                            <?php
                            echo createTreeView(0, $menus);
                            //var_dump($item['member_name']); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

@section('css')

@stop

@section('javascript')
    <!-- DataTables -->
@stop
