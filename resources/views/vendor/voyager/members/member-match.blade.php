@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Matching')
<style type="text/css">
    .test{
        list-style-type: none;
        float: right;
    }
    .test li{
        float: left;
        margin-left: 20px;
    }
</style>
{{--<link rel="stylesheet" href="http://sibayakindonesia.com/assets/css/style-admin.min.css">--}}
@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-credit-card"></i> Matching Member
        </h1>
    </div>
@endsection

@section('content')
    @php($no = 1)
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="GET">
                            <input type="hidden" name="count_member" value="{{request('count_member')}}">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <label>From Left Member</label>
                                    <div class="form-group">
                                        <input type="date" class="form-control members" name="date_from"
                                         value="{{request('date_from')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <label>To Left Member</label>
                                    <div class="form-group">
                                        <input type="date" class="form-control members" name="date_to"
                                         value="{{request('date_to')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label>Bonus Left Member</label>
                                        <select name="bonus" class="form-control">
                                            <option value="">-</option>
                                            <option value="small" @if(request('order')=='small') selected @endif>The Smallest</option>
                                            <option value="most" @if(request('order')=='most') selected @endif>The Most</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <label>From Right Member</label>
                                    <div class="form-group">
                                        <input type="date" class="form-control members" name="date_old_from"
                                            value="{{request('date_old_from')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <label>To Right Member</label>
                                    <div class="form-group">
                                        <input type="date" class="form-control members" name="date_old_to"
                                            value="{{request('date_old_to')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label>Bonus Right Member</label>
                                        <select name="bonus_old" class="form-control">
                                            <option value="">-</option>
                                            <option value="small" @if(request('order')=='small') selected @endif>The Smallest</option>
                                            <option value="most" @if(request('order')=='most') selected @endif>The Most</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-3 padding-3-perc">
                                    <br>
                                    <div class="form-group">
                                        <button id="btn" class="btn btn-primary">Filter</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Body for new Member -->
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <!-- Filter new member -->
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Join Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($date_now = new \DateTime())
                                    @foreach ($new_member as $item)
                                        <tr>
                                            <td>{{$item->member_name}}</td>
                                            <td>{{date('D, d M Y',strtotime($item->member_reg_date))}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div>
                    <div class="center-block" style="text-align: center">
                        <form method="GET">

                            <input type="hidden" name="date_from" value="{{request('date_from')}}">
                            <input type="hidden" name="date_to" value="{{request('date_to')}}">
                            <input type="hidden" name="order" value="{{request('order')}}">

                            <input type="hidden" name="date_old_from" value="{{request('date_old_from')}}">
                            <input type="hidden" name="date_old_to" value="{{request('date_old_to')}}">
                            <input type="hidden" name="order_old" value="{{request('order_old')}}">

                            <input type="number" class="form-control" name="count_member" value="{{request('count_member')}}" min="1">
                            <button class="btn btn-success">Generate</button> <button  type="button" id="btn" class="btn btn-primary" type="button" data-toggle="modal" data-target="#largeModal">Matching</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Join Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($old_member as $items)
                                        <tr>
                                            <td>{{$items->member_name}}</td>
                                            <td>{{date('D, d M Y',strtotime($items->member_reg_date))}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <label>Matching Member</label>
                            <div class="float-right">
                                <a href="/admin/reset-match" class="btn btn-primary">Reset</a>
                            </div>
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Member</th>
                                        <th>Member</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($member_match as $member)
                                        <tr>
                                            <td>{{$member->NewMember->member_name}}</td>
                                            <td>{{$member->OldMember->member_name}}</td>
                                            <td>{{date('D, d M Y',strtotime($member->created_at))}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="largeModalLabel">Matching Member</h4>
                </div>
                <div class="modal-body">
                    Apakah anda yakin untuk mematching data?
                    <form action="matching" method="GET">
                        {{csrf_field()}}
                        <div class="form-group">
                            @foreach($new_member as $new)
                                <input type="hidden" name="new_member[]" value="{{$new->member_id}}">
                            @endforeach
                            @foreach($old_member as $old)
                                <input type="hidden" name="old_member[]" value="{{$old->member_id}}">
                            @endforeach
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary waves-effect">Kirim</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

{{--<button class="btn btn-primary float-right" type="button" data-toggle="modal" data-target="#largeModal">Transfer Token</button>--}}
</div>
@endsection

@section('css')
    {{--cek CSS ADMIN--}}
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/assets/css/skins/_all-skins.min.css">


    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
            {{--<link rel="stylesheet" href="{{URL::asset('/assets/plugins/bootstrap/css/bootstrap.min.css')}}">--}}

@stop

@section('javascript')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- jQuery 3 -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
@stop