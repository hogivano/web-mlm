@extends('frontend/layouts/main')
@section('title', 'Transfer Soul')
@section('title-content', 'Transfer Soul')

@section('container')
        <div class="message" style="margin-top: 20px">
          @include('flash::message')
        </div>
        <div class="card p-3 my-3 card-green">
            <div class="card-body text-white">
                <div class="row">
                    <div class="col">
                        <img src="{{asset('img/soul.png')}}" class="img-thumbnail" style="background: transparent;">
                    </div>
                    <div class="col text-center">
                        <h1>{{ $wallet->soul }}</h1><h2>SOUL</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mt-3">
                        <form method="POST" action="{{ route('membership.soul.transfer.submit') }}" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate">
                          {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-label">USERNAME TO :</label>
                                 <input type="text" class="form-control" name="transaction_to" placeholder="username to" value="{{ old('transaction_to') }}" id="exampleFormControlInput1">
                            </div>
                            <div class="form-group">
                                <label class="control-label">AMOUNT :</label>
                                 <input type="number" min="1" step="1" class="form-control" name="amount" placeholder="quantity" value="{{ old('amount') }}" id="exampleFormControlInput1">
                            </div>
                            <div class="form-group">
                                <label class="control-label">WALLET PASSWORD :</label>
                                 <input type="password" class="form-control" name="wallet_password" placeholder="wallet password" id="exampleFormControlInput1">
                            </div>
                            <div class="d-flex justify-content-center">
                                <button type="submit" id="submit-login-btn" class="btn btn-copy uppercase" style="widows: 200px">
                                    <span>CONFIRM</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="card p-3 my-3 card-green">
            <div class="card-body text-white">
                <h4>Transfer RECORD</h4>
                <hr style="border: 0;
                height: 2px;
                background: #333;
                background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
              @if(sizeof($tokenTransaction) == 0)
              <p class="text-white text-center">Data not available</p>
              @else
              <div class="pg-number mt-3">
                {!! $tokenTransaction !!}
              </div>
              @foreach($tokenTransaction as $i)
              <div class="row mb-2" style="border-bottom: 0.1px solid #828282; font-size: 15px">
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">ID Transaction</label>
                    <p class="text-white">{{ $i->tt_id }}</p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">Status</label>
                    <p class="text-white">
                    {{ $i->transaction_status }}
                    </p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">From</label>
                    <p class="text-white">
                      @if($i->memberFrom)
                      {{ $i->memberFrom->username }}
                      @else
                      Member not found
                      @endif
                    </p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">To</label>
                    <p class="text-white">
                      @if($i->memberTo)
                      {{ $i->memberTo->username }}
                      @else
                      Member not found
                      @endif
                    </p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">Amount</label>
                    <p class="text-white">
                    {{ $i->token_amount }}
                    </p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">Date</label>
                    <p class="text-white">
                      {{ date_format($i->created_at, 'd-m-Y H:i') }}
                    </p>
                  </div>
                </div>
              </div>
              @endforeach
              @endif
            </div>
        </div>
@endsection
@section('script')
<script>
$(document).ready( function () {
  $('.table').DataTable({
    "searching": false
  });
} );
</script>
@endsection
