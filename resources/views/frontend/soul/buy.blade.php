@extends('frontend/layouts/main')
@section('title', 'Buy Soul')
@section('title-content', 'Buy Soul')

@section('container')
        <div class="message" style="margin-top: 20px">
          @include('flash::message')
        </div>
        <div class="card p-3 my-3 card-green">
            <div class="card-body text-white">
                <div class="row">
                    <div class="col">
                        <img src="{{asset('img/soul.png')}}" class="img-thumbnail" style="background: transparent;">
                    </div>
                    <div class="col text-center">
                        <h1>{{ $wallet->soul }}</h1><h2>SOUL</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mt-3">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('membership.soul.buy.submit') }}" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate">
                          {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-label">QUANTITY :</label>
                                <select id="select-quantity" class="form-control" required name="quantity">
                                  <option value="">Select Quantity</option>
                                  <option value="100">100</option>
                                  <option value="250">250</option>
                                  <option value="500">500</option>
                                  <option value="1000">1000</option>
                                  <option value="2500">2500</option>
                                  <option value="5000">5000</option>
                                  <option value="0">Custom</option>
                                </select>
                                 <!-- <input type="number" step="1" min="1" class="form-control" name="quantity" required placeholder="quantity" id="exampleFormControlInput1"> -->
                            </div>
                            <div class="form-group" id="input-custom-quantity">
                            </div>
                            <div class="form-group">
                                <label class="control-label">WALLET PASSWORD :</label>
                                 <input type="password" class="form-control" name="wallet_password" required placeholder="wallet password" id="exampleFormControlInput1">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Proof of Payment</label>
                                 <input type="file" name="proof_of_payment" required class="form-control" id="exampleFormControlInput1">
                            </div>
                            <div class="d-flex justify-content-center">
                                <button type="submit" id="submit-login-btn" class="btn btn-copy uppercase" style="widows: 200px">
                                    <span>CONFIRM</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="card p-3 my-3 card-green">
            <div class="card-body text-white">
                <h4>Buy RECORD</h4>
                <hr style="border: 0;
                height: 2px;
                background: #333;
                background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
              @if(sizeof($tokenTransaction) == 0)
              <p class="text-white text-center">Data not available</p>
              @else
              <div class="pg-number mt-3">
                {!! $tokenTransaction !!}
              </div>
              @foreach($tokenTransaction as $i)
              <div class="row mb-2" style="border-bottom: 0.1px solid #828282; font-size: 15px">
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">ID Transaction</label>
                    <p class="text-white">{{ $i->tt_id }}</p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">Quantity</label>
                    <p class="text-white">
                      {{ $i->token_amount }}
                    </p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">Status</label>
                    <p class="text-white">
                      {{ $i->transaction_status }}
                    </p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">Date</label>
                    <p class="text-white">
                      {{ date_format($i->created_at, 'd-m-Y H:i') }}
                    </p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label class="text-secondary">Proof of Payment</label>
                    @if($i->proof_of_payment)
                    <button data-toggle="modal" data-target="#modalProofofPayment{{$i->tt_id}}" class="btn btn-link p-0">Show</button>
                    @else
                    Not Uploaded
                    @endif
                  </div>
                </div>
              </div>
              <div class="modal fade" id="modalProofofPayment{{$i->tt_id}}" tabindex="-1" role="dialog" aria-labelledby="labelProofofPayment{{$i->tt_id}}" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-black" id="labelProofofPayment{{$i->tt_id}}">Proof of Payment</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <img src="{{ route('membership.soul.proof_of_payment', $i->tt_id) }}" class="w-100" alt="">
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              @endif
            </div>
        </div>
@endsection
@section('script')

<script>
$(document).ready( function () {
  $('.table').DataTable({
    "searching": false
  });


  $('#select-quantity').on('change', function(){
    if($(this).val() == '0'){
      $("#input-custom-quantity").html(
        '<label class="control-label">Custom Quantity :</label>' +
        '<input class="form-control" name="custom_quantity" required placeholder="quantity" type="number" step="1" min="10" value="">'
      );
    } else {
      $("#input-custom-quantity").html('');
    }
  })
} );
</script>
@endsection
