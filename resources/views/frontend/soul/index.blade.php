@extends('frontend/layouts/main')

@section('title', 'Soul')
@section('title-content', 'Soul')

@section('container')
    <div class="card p-3 my-3 card-green" >
        <div class="card-body row text-white">
            <style>
                .carousel-caption {
                    position: relative;
                    left: 0;
                    top: 0;
                }
                @media screen and (min-width: 350px){
                    .carousel-caption {
                        display: block;
                        padding: 5px;
                        width:100%;
                        margin: 0 auto;
                    }
                    .carousel-caption .h6{
                        margin: 0 auto;
                    }
                }
            </style>

            <div class="bd-example" style="margin:0 auto;">
                <div id="carouselExampleCaptions2" class="carousel slide" data-ride="carousel" style="width:200px;">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{asset('img/soul.png')}}" class="d-block w-100" alt="...">
                            <a href="{{ route('membership.soul.buy') }}" class="carousel-caption d-none d-lg-block d-xl-bloc btn btn-copy text-white font-weight-bold mb-4 mt-4" style="text-decoration: none;">
                                <span class="h6">BUY SOUL</span>
                            </a>
                            <a href="{{ route('membership.soul.buy') }}" class="carousel-caption d-lg-none d-xl-none btn btn-copy text-white font-weight-bold mb-4 mt-4" style="text-decoration: none;">
                                <span class="h6">BUY SOUL</span>
                            </a>
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('img/soul.png')}}" class="d-block w-100" alt="...">
                            <a href="{{ route('membership.soul.buyback') }}" class="carousel-caption d-none d-lg-block d-xl-bloc btn btn-copy text-white font-weight-bold mb-4 mt-4" style="text-decoration: none;">
                                <span class="h6">BUYBACK SOUL</span>
                            </a>
                            <a href="{{ route('membership.soul.buyback') }}" class="carousel-caption d-lg-none d-xl-none btn btn-copy text-white font-weight-bold mb-4 mt-4" style="text-decoration: none;">
                                <span class="h6">BUYBACK SOUL</span>
                            </a>
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('img/soul.png')}}" class="d-block w-100" alt="...">
                            <a href="{{ route('membership.soul.transfer') }}" class="carousel-caption d-none d-md-block btn btn-copy text-white font-weight-bold mb-4 mt-4" style="text-decoration: none;">
                                <span class="h6">TRANSFER SOUL</span>
                            </a>
                            <a href="{{ route('membership.soul.transfer') }}" class="carousel-caption d-lg-none d-xl-none btn btn-copy text-white font-weight-bold mb-4 mt-4" style="text-decoration: none;">
                                <span class="h6">TRANSFER SOUL</span>
                            </a>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
@endsection
