@extends('frontend/layouts/main')

@section('title', 'Home')
@section('link')
<style>
  html>body .modal-backdrop {
    display: none !important;
  }

  .message {
    position: absolute;
    width: 100%;
    z-index: 999;
  }

  .img-auto {
    border: 5px solid white;
    object-fit: cover;
    width: 200px;
    height: 200px;
    border-radius: 50%;
    object-position: 0 0;
  }

  .img-auto-uncheck {
    border: 5px solid white;
    object-fit: cover;
    width: 200px;
    height: 200px;
    border-radius: 50%;
    object-position: 0 0;
  }

  .img-auto:hover {
    cursor: pointer;
  }

  .img-check {
    border: 5px solid red;
    -webkit-box-shadow: 0px 0px 50px 0px rgba(228,41,242,1);
    -moz-box-shadow: 0px 0px 50px 0px rgba(228,41,242,1);
    box-shadow: 0px 0px 50px 0px rgba(228,41,242,1);
  }

  .circle-div {
    width: 200px;
    height: 200px;
    border-radius: 50%;
    border: 10px solid #ff2e3c;
    box-shadow: 0 0 50px red;
    display: table;
  }
</style>
@endsection
@section('container')
@if(Agent::isMobile())
<div style="height: 100vh; width: 100%; position: relative">
  <div class="message">
    @include('flash::message')
  </div>

  <div class="header mb-3 mx-auto" style="width: 370px; position: absolute; top: 0; left: 0; right: 0">
    <button type="button" data-toggle="modal" data-target="#modalAutoHire" class="btn btn-copy w-100" name="button">Quick Order</button>
  </div>
  <div class="modal fade" id="modalAutoHire" tabindex="-1" role="dialog" aria-labelledby="labelAutoHire" aria-hidden="true">
    <div class="modal-dialog" role="document" style="margin-left: 20px">
      <div class="modal-content card-green" style="background-color: rgb(18, 18, 18, .8)">
        <div class="modal-header">
          <h5 class="modal-title text-white" id="labelAutoHire">Quick Order</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" id="form-auto" action="{{ route('membership.auto_hire') }}">
            {{ csrf_field() }}
            <?php $soul = 0; ?>
            @foreach($data as $i)
            <div class="mb-3 pb-4" style="border-bottom: .5px solid white;">
              <div class="img w-100 text-center" style="position: relative">
                  @if(sizeof($i->charactersOrder) == 1)
                  <img class="mx-auto img-auto-uncheck" src="{{ route('membership.character.picture', $i->character_id) }}" style="object-fit: cover; width: 200px; height: 200px;border-radius: 50%;" alt="Card image ca" >

                  @elseif(sizeof($i->charactersSeller) == 1)
                  <img class="mx-auto img-auto-uncheck" src="{{ route('membership.character.picture', $i->character_id) }}" style="object-fit: cover; width: 200px; height: 200px;border-radius: 50%;" alt="Card image ca" >

                  @elseif(sizeof($i->charactersOrder) == 0)
                  <img class="mx-auto img-auto img-check" src="{{ route('membership.character.picture', $i->character_id) }}" style="object-fit: cover; width: 200px; height: 200px;border-radius: 50%;" alt="Card image ca" >
                  <?php $soul = $soul + $i->soul; ?>
                  <input type="text" name="id[]" hidden value="{{ $i->character_id }}">
                  <input type="checkbox" class="checked-box" hidden data-soul="{{$i->soul}}" style="font-size: 15px !important; position: absolute; top: 35%; right: 30%; width: 20px; height: 20px" checked name="checkbox[{{$i->character_id}}]">
                  @else
                  <img class="mx-auto img-auto-uncheck" src="{{ route('membership.character.picture', $i->character_id) }}" style="object-fit: cover; width: 200px; height: 200px;border-radius: 50%;" alt="Card image ca" >
                  @endif
                <p class="text-white text-center mt-2 mb-1">{{ $i->name }}</p>
                <h5 class="text-white text-center form-check-label">
                  @if(sizeof($i->charactersOrder) == 1)
                  <span class="text-secondary">Requested</span>
                  @elseif(sizeof($i->charactersSeller) == 1)
                  <span class="text-secondary">On Proccess</span>
                  @elseif(sizeof($i->charactersOrder) == 0)
                  <b>{{ $i->soul }} Soul</b>
                  @endif
                </h5>
              </div>
            </div>
            @endforeach
          </form>
          <div class="text-white">
            <p style="font-size: 30px;" class="text-center">
              Soul : <b id="soul-info">{{ $soul }}</b>
            </p>
            <button type="submit" form="form-auto" class="btn btn-copy w-100">Order</button>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
     <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
      <div class="carousel-inner wh-100">
        @for($i = 0; $i < sizeof($data); $i++)
        @if($i == 0)
        <div class="carousel-item active wh-100" style="position: relative;">
        @else
        <div class="carousel-item wh-100" style="position: relative;">
        @endif
            <img src="{{ route('membership.character.picture', $data[$i]->character_id) }}" style="object-fit: cover;" class="wh-100" alt="Card image ca" >
                    <div style="position: absolute; z-index: 999;z-index: 999;bottom: 30px;left: 30px;">
                        <span class="h4 card-title mt-2 text-success">{{ ($i+1) . '. ' . $data[$i]->name }} </span>
                        <div class="d-flex my-2">
                          <p class="text-success align-self-center mb-0">
                              <small>({{ $data[$i]->status }})</small>
                          </p>
                          @if(sizeof($data[$i]->charactersOrder) == 1)
                            <button class="btn font-weight-bold border-0 mx-2 btn-secondary" disabled style="padding: 5px 10px!important">
                              Ordering <i class="fas fa-angle-double-right"></i>
                            </button>
                          @elseif(sizeof($data[$i]->charactersSeller) == 1)
                          <button class="btn font-weight-bold border-0 mx-2 btn-secondary" disabled style="padding: 5px 10px!important">
                            On Proccess <i class="fas fa-angle-double-right"></i>
                          </button>
                          @elseif(sizeof($data[$i]->charactersOrder) == 0)
                            <button class="btn font-weight-bold border-0 mx-2 btn-copy" data-toggle="modal" data-target="#modalBuy{{$data[$i]->character_id}}" style="padding: 5px 10px!important">
                              ORDER <i class="fas fa-angle-double-right"></i>
                            </button>
                          @endif
                        </div>
                        <table border="0" class="text-success">
                            <tr >
                                <td>PROFIT</td>
                                <td> : </td>
                                <td class="text-success">{{ $data[$i]->profit_for . '% - ' . $data[$i]->profit_to . '%' }} </td>
                            </tr>
                            <tr>
                                <td>PRICE</td>
                                <td> : </td>
                                <td class="text-success">{{ $data[$i]->price_for . ' - ' . $data[$i]->price_to }} </td>
                            </tr>
                            <tr>
                                <td>S O U L</td>
                                <td> : </td>
                                <td class="text-success">{{ $data[$i]->soul }}</td>
                            </tr>
                            <tr>
                                <td class="text-success">TIME</td>
                                <td> : </td>
                                <td class="text-success">{{ date('H:i', strtotime($data[$i]->time)) }}</td>
                            </tr>
                        </table>
                    </div>
        </div>
        <div class="modal fade" id="modalBuy{{$data[$i]->character_id}}" tabindex="-1" role="dialog" aria-labelledby="modalBuy{{$data[$i]->character_id}}" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order for {{ $data[$i]->name }} (Soul: {{ $data[$i]->soul }})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Are you sure order this character?
                <form method="post" action="{{ route('membership.character.order.submit') }}" id="formBuyStatus{{$data[$i]->character_id}}">
                  {{ csrf_field() }}
                  <input type="text" hidden name="character_id" value="{{ $data[$i]->character_id }}">
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" form="formBuyStatus{{$data[$i]->character_id}}" class="btn btn-primary">Yes</button>
              </div>
            </div>
          </div>
        </div>
        @endfor
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</div>
<div id="floating-btn" class="d-flex justify-content-center align-items-center">
        <i class="fas fa-dna mx-auto"></i>
</div>
<div class="menu-floating" style="display: none">
    <div class="menu-item d-flex justify-content-center align-items-center" style="">
      <a href="{{ route('membership.home') }}" class="text-white">
        <i class="fab fa-asymmetrik mx-auto"></i>
      </a>
    </div>
    <div class="menu-item d-flex justify-content-center align-items-center" style="">
      <a href="{{ route('membership.hire') }}" class="text-white">
        <i class="fas fa-space-shuttle mx-auto"></i>
      </a>
    </div>
    <div class="menu-item d-flex justify-content-center align-items-center" style="">
      <a href="{{ route('membership.profile') }}" class="text-white">
        <i class="fas fa-user-astronaut mx-auto"></i>
      </a>
    </div>
</div>
@else
<div class="container owl-carousel d-flex justify-content-center" style="z-index: 9999">
  <div style="width: 400px; position: relative; margin: auto">
    <div class="message">
      @include('flash::message')
    </div>
    <div class="header mb-3 mx-auto text-center" style="width: 370px; position: absolute; top: 0; left: 0; right: 0">
      <button type="button" data-toggle="modal" data-target="#modalAutoHire" class="btn btn-copy w-100" name="button">Quick Order</button>
    </div>
    <div class="modal fade" id="modalAutoHire" tabindex="-1" role="dialog" aria-labelledby="labelAutoHire" aria-hidden="true">
      <div class="modal-dialog" role="document" style="margin-left: 20px">
        <div class="modal-content card-green" style="background-color: rgb(18, 18, 18, .8)">
          <div class="modal-header">
            <h5 class="modal-title text-white" id="labelAutoHire">Quick Order</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" id="form-auto" action="{{ route('membership.auto_hire') }}">
              {{ csrf_field() }}
              <?php $soul = 0; ?>
              @foreach($data as $i)
              <div class="mb-3 pb-4" style="border-bottom: .5px solid white;">
                <div class="img w-100" style="position: relative">
                    @if(sizeof($i->charactersOrder) == 1)
                    <img class="mx-auto img-auto-uncheck" src="{{ route('membership.character.picture', $i->character_id) }}" style="object-fit: cover; width: 200px; height: 200px;border-radius: 50%;" alt="Card image ca" >

                    @elseif(sizeof($i->charactersSeller) == 1)
                    <img class="mx-auto img-auto-uncheck" src="{{ route('membership.character.picture', $i->character_id) }}" style="object-fit: cover; width: 200px; height: 200px;border-radius: 50%;" alt="Card image ca" >

                    @elseif(sizeof($i->charactersOrder) == 0)
                    <img class="mx-auto img-auto img-check" src="{{ route('membership.character.picture', $i->character_id) }}" style="object-fit: cover; width: 200px; height: 200px;border-radius: 50%;" alt="Card image ca" >
                    <?php $soul = $soul + $i->soul; ?>
                    <input type="text" name="id[]" hidden value="{{ $i->character_id }}">
                    <input type="checkbox" class="checked-box" hidden data-soul="{{$i->soul}}" style="font-size: 15px !important; position: absolute; top: 35%; right: 30%; width: 20px; height: 20px" checked name="checkbox[{{$i->character_id}}]">
                    @else
                    <img class="mx-auto img-auto-uncheck" src="{{ route('membership.character.picture', $i->character_id) }}" style="object-fit: cover; width: 200px; height: 200px;border-radius: 50%;" alt="Card image ca" >
                    @endif
                  <p class="text-white text-center mt-2 mb-1">{{ $i->name }}</p>
                  <h5 class="text-white text-center form-check-label">
                    @if(sizeof($i->charactersOrder) == 1)
                    <span class="text-secondary">Requested</span>
                    @elseif(sizeof($i->charactersSeller) == 1)
                    <span class="text-secondary">On Proccess</span>
                    @elseif(sizeof($i->charactersOrder) == 0)
                    <b>{{ $i->soul }} Soul</b>
                    @endif
                  </h5>
                </div>
              </div>
              @endforeach
            </form>
            <div class="text-white">
              <p style="font-size: 30px;" class="text-center">
                Soul : <b id="soul-info">{{ $soul }}</b>
              </p>
              <button type="submit" form="form-auto" class="btn btn-copy w-100">Order</button>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
       <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
         <div id="floating-btn" class="d-flex justify-content-center align-items-center">
                 <i class="fas fa-dna mx-auto"></i>
         </div>
         <div class="menu-floating" style="display: none">
             <div class="menu-item d-flex justify-content-center align-items-center" style="">
               <a href="{{ route('membership.home') }}" class="text-white">
                 <i class="fab fa-asymmetrik mx-auto"></i>
               </a>
             </div>
             <div class="menu-item d-flex justify-content-center align-items-center" style="">
               <a href="{{ route('membership.hire') }}" class="text-white">
                 <i class="fas fa-space-shuttle mx-auto"></i>
               </a>
             </div>
             <div class="menu-item d-flex justify-content-center align-items-center" style="">
               <a href="{{ route('membership.profile') }}" class="text-white">
                 <i class="fas fa-user-astronaut mx-auto"></i>
               </a>
             </div>
         </div>
        <div class="carousel-inner wh-100">
          @for($i = 0; $i < sizeof($data); $i++)
          @if($i == 0)
          <div class="carousel-item active wh-100" style="position: relative;">
          @else
          <div class="carousel-item wh-100" style="position: relative;">
          @endif
              <img src="{{ route('membership.character.picture', $data[$i]->character_id) }}" style="object-fit: cover;" class="wh-100" alt="Card image ca" >
                      <div style="position: absolute; z-index: 999;z-index: 999;bottom: 30px;left: 30px;">
                          <span class="h4 card-title mt-2 text-success">{{ ($i+1) . '. ' . $data[$i]->name }} </span>
                          <div class="d-flex my-2">
                            <p class="text-success align-self-center mb-0">
                                <small>({{ $data[$i]->status }})</small>
                            </p>
                            @if(sizeof($data[$i]->charactersOrder) == 1)
                              <button class="btn font-weight-bold border-0 mx-2 btn-secondary" disabled style="padding: 5px 10px!important">
                                Ordering <i class="fas fa-angle-double-right"></i>
                              </button>
                            @elseif(sizeof($data[$i]->charactersSeller) == 1)
                            <button class="btn font-weight-bold border-0 mx-2 btn-secondary" disabled style="padding: 5px 10px!important">
                              On Proccess <i class="fas fa-angle-double-right"></i>
                            </button>
                            @elseif(sizeof($data[$i]->charactersOrder) == 0)
                              <button class="btn font-weight-bold border-0 mx-2 btn-copy" data-toggle="modal" data-target="#modalBuy{{$data[$i]->character_id}}" style="padding: 5px 10px!important">
                                ORDER <i class="fas fa-angle-double-right"></i>
                              </button>
                            @endif
                          </div>
                          <table border="0" class="text-success">
                              <tr >
                                  <td>PROFIT</td>
                                  <td> : </td>
                                  <td class="text-success">{{ $data[$i]->profit_for . '% - ' . $data[$i]->profit_to . '%' }} </td>
                              </tr>
                              <tr>
                                  <td>PRICE</td>
                                  <td> : </td>
                                  <td class="text-success">{{ $data[$i]->price_for . ' - ' . $data[$i]->price_to }} </td>
                              </tr>
                              <tr>
                                  <td>S O U L</td>
                                  <td> : </td>
                                  <td class="text-success">{{ $data[$i]->soul }}</td>
                              </tr>
                              <tr>
                                  <td class="text-success">TIME</td>
                                  <td> : </td>
                                  <td class="text-success">{{ date('H:i', strtotime($data[$i]->time)) }}</td>
                              </tr>
                          </table>
                      </div>
          </div>
          <div class="modal fade" id="modalBuy{{$data[$i]->character_id}}" tabindex="-1" role="dialog" aria-labelledby="modalBuy{{$data[$i]->character_id}}" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Order for {{ $data[$i]->name }} (Soul: {{ $data[$i]->soul }})</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  Are you sure order this character?
                  <form method="post" action="{{ route('membership.character.order.submit') }}" id="formBuyStatus{{$data[$i]->character_id}}">
                    {{ csrf_field() }}
                    <input type="text" hidden name="character_id" value="{{ $data[$i]->character_id }}">
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" form="formBuyStatus{{$data[$i]->character_id}}" class="btn btn-primary">Yes</button>
                </div>
              </div>
            </div>
          </div>
          @endfor
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
@section('script')
    <script>
        $(document).ready(function(){
          $(".owl-carousel").owlCarousel();
          $(".btn-copy").on("click", function(){
            console.log("okeedi");
            $(".modal-backdrop").remove();
          });

          $(".checked-box").on("click", function(){
            console.log("clicked");
            if( $(this).is(':checked') ){
              var total = Number($("#soul-info").text()) + $(this).data("soul");
              $("#soul-info").html(total);
            } else {
              var total = Number($("#soul-info").text()) - $(this).data("soul");
              $("#soul-info").html(total);
            }
          });

          $(".img-auto").on('click', function(){
            if($(this).hasClass('img-check')){
              var siblings = $(this).siblings(".checked-box");
              $(siblings).prop("checked", false);
              var total = Number($("#soul-info").text()) - $(siblings).data("soul");
              $("#soul-info").html(total);
              $(this).removeClass('img-check');
            } else {
              var siblings = $(this).siblings(".checked-box");
              $(siblings).prop("checked", true);
              var total = Number($("#soul-info").text()) + $(siblings).data("soul");
              $("#soul-info").html(total);

              $(this).addClass('img-check');
            }
          });
        });
    </script>
@endsection
