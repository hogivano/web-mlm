@extends('frontend/layouts/main')

@section('title', 'Home - Laravel')

@section('container')
<div class="main-container" style="margin-bottom: 250px">
    <h3 class="text-center text-white p-2 bg-warning">INVENTORY CENTER</h3>
    <div class="card p-3 m-3" style="background: rgba(254, 193, 7, .45);">
        <div class="card-body row text-white">
            <style>.carousel-caption {
                position: relative;
                left: 0;
                top: 0;
            }</style>

            <div class="bd-example" style="margin:0 auto;">
                <div id="carouselExampleCaptions2" class="carousel slide" data-ride="carousel" style="width:250px;">
                    <ol class="carousel-indicators my-4">
                        <li data-target="#carouselExampleCaptions2" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleCaptions2" data-slide-to="1"></li>
                        <li data-target="#carouselExampleCaptions2" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{asset('img/gold.png')}}" class="d-block w-100" alt="...">
                            <a href="{{ url('buy-gold') }}" class="carousel-caption d-none d-md-block bg-warning text-white font-weight-bold mb-4" style="text-decoration: none;">
                                <span class="h6">QR CODE</span>
                            </a>
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('img/gold.png')}}" class="d-block w-100" alt="...">
                            <a href="{{ url('transfer-gold') }}" class="carousel-caption d-none d-md-block bg-warning text-white font-weight-bold mb-4" style="text-decoration: none;">
                                <span class="h6">MEMBER ACCOUNT</span>
                            </a>
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('img/gold.png')}}" class="d-block w-100" alt="...">
                            <a href="{{ url('transfer-gold') }}" class="carousel-caption d-none d-md-block bg-warning text-white font-weight-bold mb-4" style="text-decoration: none;">
                                <span class="h6">CHILD ACCOUNT</span>
                            </a>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleCaptions2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('script')
<script src="js/user_coree7db.js?id=aabc5d2a8b59e2a33fa0" type="text/javascript"></script>
<script>
    +function ($) {
        $(document).ready(function () {
            $('.loader').remove();
            $('.main-container').css('display', '');
        });
    }(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $('#login-form').makeAjaxForm({
                submitBtn: '#submit-login-btn',
                afterSuccessFunction: function (resp) {
                    location.href='index.html';
                },
            });
        });
    }(jQuery));
</script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">

    if(("standalone" in window.navigator) && window.navigator.standalone){

        var noddy, remotes = false;

        document.addEventListener('click', function(event) {

            noddy = event.target;

            while(noddy.nodeName !== "A" && noddy.nodeName !== "HTML") {
                noddy = noddy.parentNode;
            }

            if('href' in noddy && noddy.href.indexOf('http') !== -1 && (noddy.href.indexOf(document.location.host) !== -1 || remotes))
            {
                if (!noddy.hasAttribute("data-toggle")) {
                    event.preventDefault();
                    document.location.href = noddy.href;
                }
            }

        },false);
    }
</script>
<script>
    +function ($) {
        $(document).ready(function () {

        });
    }(jQuery);
</script>
<script>
    if (typeof navigator.serviceWorker !== 'undefined') {
        navigator.serviceWorker.register('service-worker.js', { scope: '/' })
        .then(function (registration) {
            console.log('Service worker registered successfully');
        }).catch(function (e) {
            console.error('Error during service worker registration:', e);
        });
    }
</script>
@endsection
