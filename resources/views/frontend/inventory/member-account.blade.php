@extends('frontend/layouts/main')

@section('title', 'Registrasi Member')

@section('container')
<div class="main-container">
    <div>
        <img src="{{ asset('img/logo.png') }}" class="img-fluid">
    </div>
    <div class="ct1 auth-layout mt-3">
        <div>
            <div id="body-alert-container">
            </div>
            <div class="m-3">
                <p class="white-title text-center">
                    Member Account
                </p>
                <div class="p-1">
                    <form method="POST" action="" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate"><input name="_token" type="hidden" value="AXDoXw9Rn8fGHinKTrGDo5KYkyN0uY022XTxJrtE">
                        <div class="form-group">
                            <label class="control-label">Introducer :</label>
                            <input class="form-control" name="username_introducer" placeholder="username/email introducer" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Username :</label>
                            <input class="form-control" name="username" placeholder="username" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email :</label>
                            <input class="form-control" name="email" placeholder="example@example.com" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Country :</label>
                            <select name="country" class="custom-select form-control">
                              <option selected>Select Country</option>
                              <option value="1">Indonesia</option>
                              <option value="2">Malaysia</option>
                              <option value="3">Singapura</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">No.Telp :</label>
                            <input type="text" placeholder="081xxxx" class="form-control" name="no_telp">
                        </div>
                        <div class="form-group">
                            <label class="control-label">PASSWORD :</label>
                            <input class="form-control" placeholder="password" name="password" type="password" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">PASSWORD CONFIRMATION :</label>
                            <input class="form-control" placeholder="konfirmasi password" name="confirmation_password" type="password" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">WALLET PASSWORD :</label>
                            <input class="form-control" name="wallet_password" placeholder="wallet password" type="password" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">WALLET PASSWORD CONFIRMATION :</label>
                            <input class="form-control" name="confirmation_wallet_password" placeholder="konfirmation wallet password" type="password" value="">
                        </div>
                        <div class="text-center text-white"> Sudah punya akun ? <a href="{{ route('membership.login') }}" style="color:#fff;font-weight: bold;">LOGIN</a></div>
                        <div class="form-group">
                            <button type="button" id="submit-login-btn" class="btn btn-warning btn-block uppercase" style="background: rgba(255, 199, 31, 1);color:#fff;border:none;">
                                <span>CONFIRM</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- begin::Global Config(global config for global JS sciprts) -->
<script src="js/user_coree7db.js?id=aabc5d2a8b59e2a33fa0" type="text/javascript"></script>
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $('#login-form').makeAjaxForm({
                submitBtn: '#submit-login-btn',
                afterSuccessFunction: function (resp) {
                    location.href='index.html';
                },
            });
        });
    }(jQuery));
</script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">

    if(("standalone" in window.navigator) && window.navigator.standalone){

        var noddy, remotes = false;

        document.addEventListener('click', function(event) {

            noddy = event.target;

            while(noddy.nodeName !== "A" && noddy.nodeName !== "HTML") {
                noddy = noddy.parentNode;
            }

            if('href' in noddy && noddy.href.indexOf('http') !== -1 && (noddy.href.indexOf(document.location.host) !== -1 || remotes))
            {
                if (!noddy.hasAttribute("data-toggle")) {
                    event.preventDefault();
                    document.location.href = noddy.href;
                }
            }

        },false);
    }
</script>
<script>
    +function ($) {
        $(document).ready(function () {

        });
    }(jQuery);
</script>


















<script>
    if (typeof navigator.serviceWorker !== 'undefined') {
        navigator.serviceWorker.register('service-worker.js', { scope: '/' })
        .then(function (registration) {
            console.log('Service worker registered successfully');
        }).catch(function (e) {
            console.error('Error during service worker registration:', e);
        });
    }
</script>
@endsection
