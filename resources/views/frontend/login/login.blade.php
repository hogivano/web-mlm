@extends('frontend/layouts/home')

@section('title', 'Login')
@section('link')
<style>
  html > body {
    background-image: url('/img/bg-black.png');
  }
</style>
@endsection
@section('container')
<div class="main-container" style="display: table; padding-top: 50px">
    <div class="card auth-layout card-black w-100" style="display: table-cell; vertical-align: text-bottom;">
      <img src="{{ asset('img/logo-new.png') }}" class="w-100" alt="">
        <div>
            <div class="mx-3 mb-3">
                <p class="white-title text-center">
                    Login
                </p>
                <div class="p-1">
                    <form method="POST" action="{{route('membership.login')}}" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label">USERNAME :</label>
                            <input class="form-control" name="member_username" placeholder="username" type="text" required value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">PASSWORD :</label>
                            <input class="form-control" name="member_password" placeholder="password" type="password" required value="">
                        </div>
                        <div class="form-group text-right">
                            <a href="{{ route('forget-password') }}" class="link" style="color:#fff;font-weight:bold;text-decoration: none;">FORGET PASSWORD?</a>
                        </div>
                        <div class="form-group">
                            <label class="kt-checkbox" style="font-weight: bold;">
                                <input type="checkbox" name="remember" value="1"> REMEMBER ME
                                <span></span>
                            </label>
                        </div>
                        <div class="text-center text-white"> I don't have account ? <a href="{{ route('membership.register') }}" style="color:#fff;font-weight: bold;">REGISTER NOW</a></div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-copy uppercase text-center" style="width: 200px">
                                <span>LOGIN NOW</span>
                            </button>
                        </div>
                        <div class="form-group">
                          @if(Session::has('error'))
                              <div class="alert alert-danger">
                                  {{Session::get('error')}}
                              </div>
                          @endif
                          @if($errors->first())
                              <div class="alert alert-danger">
                                  {{$errors->first()}}
                              </div>
                          @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
