@extends('frontend/layouts/main')

@section('title', 'Bank Account')
@section('title-content', 'Bank Account')

@section('container')
  <div class="message" style="margin-top: 20px">
    @include('flash::message')
  </div>
  <div class="ct1 auth-layout mt-3 card-green">
    <div>
        <div id="body-alert-container">
        </div>
        <div class="m-3">
            <p class="white-title text-center">
                Bank Account
            </p>
            <div class="p-1">
                <form method="POST" action="{{route('membership.bank.submit')}}" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label">Bank :</label>
                        <input class="form-control" name="bank" placeholder="bank" required type="text" value="{{ $data->bank }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Holder Name :</label>
                        <input class="form-control" name="bank_account" required placeholder="holder name" type="text" value="{{ $data->bank_account }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Bank Number :</label>
                        <input class="form-control" name="bank_number" required placeholder="bank number" type="text" value="{{ $data->bank_number }}">
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" id="submit-login-btn" class="btn btn-copy uppercase text-center" style="width: 200px;">
                            <span>SAVE</span>
                        </button>
                    </div>
                    <div class="form-group">
                      @if(Session::has('error'))
                          <div class="alert alert-danger">
                              {{Session::get('error')}}
                          </div>
                      @endif
                      @if($errors->first())
                          <div class="alert alert-danger">
                              {{$errors->first()}}
                          </div>
                      @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
