@extends('frontend/layouts/main')

@section('title', 'Security')
@section('title-content', 'Security')

@section('container')
  <div class="message" style="margin-top: 20px">
    @include('flash::message')
  </div>
  <div class="ct1 auth-layout mt-3 card-green">
    <div>
        <div id="body-alert-container">
        </div>
        <div class="m-3">
            <p class="white-title text-center">
                Password Account
            </p>
            <div class="p-1">
                <form method="POST" action="{{route('membership.security.account.submit')}}" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label">Old Password :</label>
                        <input class="form-control" name="old_password" placeholder="old password" required type="password" value="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">New Password :</label>
                        <input class="form-control" name="new_password" placeholder="new password" required type="password" value="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Confirmation Password :</label>
                        <input class="form-control" name="confirmation_new_password" required placeholder="confirmation password" type="password" value="">
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" id="submit-login-btn" class="btn btn-copy uppercase text-center" style="width: 200px;">
                            <span>SAVE</span>
                        </button>
                    </div>
                    <div class="form-group">
                      @if(Session::has('error'))
                          <div class="alert alert-danger">
                              {{Session::get('error')}}
                          </div>
                      @endif
                      @if($errors->first())
                          <div class="alert alert-danger">
                              {{$errors->first()}}
                          </div>
                      @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
  <div class="ct1 auth-layout mt-3 card-green">
    <div>
        <div id="body-alert-container">
        </div>
        <div class="m-3">
            <p class="white-title text-center">
                Wallet Password
            </p>
            <div class="p-1">
                <form method="POST" action="{{route('membership.security.wallet.submit')}}" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label">Old Wallet Password :</label>
                        <input class="form-control" name="old_wallet_password" placeholder="old wallet password" required type="password" value="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">New Wallet Password :</label>
                        <input class="form-control" name="new_wallet_password" placeholder="new wallet password" required type="password" value="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Confirmation Wallet Password :</label>
                        <input class="form-control" name="confirmation_new_wallet_password" required placeholder="confirmation wallet password" type="password" value="">
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" id="submit-login-btn" class="btn btn-copy uppercase text-center" style="width: 200px;">
                            <span>SAVE</span>
                        </button>
                    </div>
                    <div class="form-group">
                      @if(Session::has('error'))
                          <div class="alert alert-danger">
                              {{Session::get('error')}}
                          </div>
                      @endif
                      @if($errors->first())
                          <div class="alert alert-danger">
                              {{$errors->first()}}
                          </div>
                      @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
@endsection
