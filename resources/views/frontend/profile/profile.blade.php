@extends('frontend/layouts/main')

@section('title', 'Profile')
@section('title-content', 'Information')
@section('container')
        <div class="card p-3 my-3 card-green" >
            <div class="card-body row text-white">
                <div class="col-4"><img src="{{asset('img/user.png')}}" width="80" class="rounded-circle"></div>
                <div class="col-8">
                    <h4>Hi, {{ $user->username }} </h4>
                    <span style="font-size: 20px;">
                      @if($user->parent_id == null || $user->parent_id == 0 || !$user->parent_id)
                      Account : Main
                      @else
                      Account : Sub
                      @endif
                    </span><br/>
                    <span> Status : {{ $user->status }} </span>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                  <a href="{{ route('membership.mind') }}">
                    <div class="card text-center me-menu-top" style="border: none">
                        <div class="card-body padding-5">
                          <img src="{{ asset('img/mind.png') }}" class="w-100" style="object-fit: contain;" alt="">
                            <hr style="border: 0;
                            height: 1px;
                            background: #333;
                            background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                              <p>${{ $wallet->reality }}</p>
                            <span style="font-size: 18px">Mind</span>
                        </div>
                    </div>
                  </a>
                </div>
                <div class="col-4">
                  <a href="{{ route('membership.soul') }}">
                    <div class="card text-center me-menu-top" style="border: none">
                      <div class="card-body padding-5">
                        <img src="{{ asset('img/soul.png') }}" class="w-100" style="object-fit: contain;" alt="">
                          <hr style="border: 0;
                            height: 1px;
                            background: #333;
                            background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                        <p>${{ $wallet->soul }}</p>
                        <span style="font-size: 18px">Soul</span>
                      </div>
                    </div>
                  </a>
                </div>
                <div class="col-4">
                    <a href="{{ route('membership.power') }}">
                      <div class="card text-center me-menu-top" style="border: none">
                          <div class="card-body padding-5">
                              <img src="{{ asset('img/power.png') }}" class="w-100" style="object-fit: contain;" alt="">
                                <hr style="border: 0;
                                  height: 1px;
                                  background: #333;
                                  background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                                  background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                                  background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                                  background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                              <p>${{ $wallet->power }}</p>
                              <span style="font-size: 18px">Power</span>
                          </div>
                      </div>
                    </a>
                </div>
            </div>
            <div class="row mt-4">
              <div class="col-4">
                  <a href="#" class="card me-menu-top" style="border: none">
                    <div class="card-body padding-5 text-center">
                        <img src="{{ asset('img/time.png') }}" class="w-100" style="object-fit: contain;" alt="">
                          <hr style="border: 0;
                            height: 1px;
                            background: #333;
                            background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                        <p>0</p>
                        <span style="font-size: 18px">Time</span>
                    </div>
                  </a>
              </div>
                <div class="col-4">
                    <a href="#" class="card me-menu-top" style="border: none">
                      <div class="card-body padding-5 text-center">
                          <img src="{{ asset('img/space.png') }}" class="w-100" style="object-fit: contain;" alt="">
                            <hr style="border: 0;
                              height: 1px;
                              background: #333;
                              background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                              background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                              background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                              background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                          <p>0</p>
                          <span style="font-size: 18px">Space</span>
                      </div>
                    </a>
                </div>
                  <div class="col-4">
                      <a href="#" class="card me-menu-top" style="border: none">
                        <div class="card-body padding-5 text-center">
                            <img src="{{ asset('img/reality.png') }}" class="w-100" style="object-fit: contain;" alt="">
                              <hr style="border: 0;
                                height: 1px;
                                background: #333;
                                background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                                background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                                background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                                background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                            <p>0</p>
                            <span style="font-size: 18px">Reality</span>
                        </div>
                      </a>
                  </div>
                  <div class="col-4 mt-4">
                      <a href="{{ route('membership.sub_account') }}" class="card card-green me-menu-bottom">
                          <div class="card-body text-center padding-5">
                              <span class="fas fa-users" style="font-size: 45px;"></span>
                              <span>Multiplayer</span>
                          </div>
                      </a>
                  </div>
                <div class="col-4 mt-4">
                    <a href="{{ route('membership.invite') }}" class="card card-green me-menu-bottom">
                        <div class="card-body text-center padding-5">
                            <span class="fas fa-handshake" style="font-size: 45px;"></span>
                            <span>Invite Player</span>
                        </div>
                    </a>
                </div>
                <div class="col-4 mt-4">
                    <a href="{{ route('membership.security') }}" class="card card-green me-menu-bottom">
                        <div class="card-body text-center padding-5">
                            <span class="fas fa-shield-alt pt-2" style="font-size: 45px;"></span>
                            <span>Security</span>
                        </div>
                    </a>
                </div>
                <div class="col-4 mt-4">
                    <a href="{{ route('membership.bank') }}" class="card card-green me-menu-bottom">
                        <div class="card-body text-center padding-5">
                            <span class="fas fa-university" style="font-size: 45px;"></span><br/>
                            <span>Bank Details</span>
                        </div>
                    </a>
                </div>
                <div class="col-4 mt-4">
                    <a href="" class="card card-green me-menu-bottom">
                        <div class="card-body text-center padding-5">
                            <span class="fas fa-users" style="font-size: 45px;"></span>
                            <span>Group Player</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
@endsection
@section('container')
    <div class="main-container" style="margin-bottom:150px;">
        <h3 class="text-center text-white p-2 bg-warning">Multitasking</h3>
        <div class="card p-3 m-3" style="background: rgba(254, 193, 7, .45);">
            <div class="card-body row text-white">
                <div class="col-4"><img src="{{asset('img/user.png')}}" width="80" class="rounded-circle"></div>
                <div class="col-8">
                    <h4> Name </h4>
                    <span> Status : PENDING </span><br/>
                    <span> Tier : NORMAL </span>
                </div>
            </div>
            <div class="row">
              <div class="col-4">
                  <div class="card text-center" style="border-color:#ccc;font-size:12px;">
                      <div class="card-body">
                          <span>$0.00</span>
                          <hr style="border: 0;
                          height: 1px;
                          background: #333;
                          background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                          background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                          background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                          background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                          <span>Character Reward</span>
                      </div>
                  </div>
              </div>
                <div class="col-4">
                    <div class="card text-center" style="border-color:#ccc;font-size:12px;">
                        <div class="card-body">
                            <span>$0.00</span>
                            <hr style="border: 0;
                            height: 1px;
                            background: #333;
                            background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                            <a href="{{ route('membership.inventory') }}"><span>Gold Bar</span></a>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card text-center" style="border-color:#ccc;font-size:12px;">
                        <div class="card-body">
                            <span>$0.00</span>
                            <hr style="border: 0;
                            height: 1px;
                            background: #333;
                            background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                            <span>Equipment</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card p-3 m-3" style="background: rgba(254, 193, 7, .45);font-size: 15px;">
            <div class="row">
                <div class="col-4">
                    <a href="{{ route('membership.inventory') }}" class="card bg-light" style="border-color:#ccc;text-decoration: none;">
                        <div class="card-body text-center">
                            <span class="fas fa-users" style="font-size: 45px;"></span>
                            <span>Inventory Center</span>
                        </div>
                    </a>
                </div>
                <div class="col-4">
                    <a href="{{ route('membership.soul.transfer') }}" class="card bg-light" style="border-color:#ccc;text-decoration: none;">
                        <div class="card-body text-center">
                            <span class="fas fa-handshake" style="font-size: 45px;"></span>
                            <span>Trade Center</span>
                        </div>
                    </a>
                </div>
                <div class="col-4">
                    <a href="{{ route('membership.security') }}" class="card bg-light" style="border-color:#ccc;text-decoration: none;">
                        <div class="card-body text-center">
                            <span class="fas fa-shield-alt pt-2" style="font-size: 45px;"></span>
                            <span>Security</span>
                        </div>
                    </a>
                </div>
                <div class="col-4 mt-2">
                    <a href="{{ route('membership.bank') }}" class="card bg-light" style="border-color:#ccc;text-decoration: none;">
                        <div class="card-body text-center">
                            <span class="fas fa-university" style="font-size: 45px;"></span><br/>
                            <span>Bank Details</span>
                        </div>
                    </a>
                </div>
                <div class="col-4 mt-2">
                    <a href="" class="card bg-light" style="border-color:#ccc;text-decoration: none;">
                        <div class="card-body text-center">
                            <span class="fas fa-users" style="font-size: 45px;"></span>
                            <span>Group</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
