@extends('frontend/layouts/home')
@section('title', 'register')
@section('link')
<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/noppa/text-security/master/dist/text-security.css">
<style>
  input [type="tel"] {
       font-family: "text-security-disc" !important;
       -webkit-text-security: disc !important;
   }
   html > body {
               background-image: url('/img/bg-black.png');
             }
</style>
@endsection
@section('container')
<div class="main-container m-3" style="margin-bottom:150px;">
  <div class="message" style="margin-top: 20px">
    @include('flash::message')
  </div>
    <div class="auth-layout mt-3 card-green w-100" style="padding: 10px 20px;">
        <div>
            <div id="body-alert-container">
            </div>
            <div class="my-3" id="term">
                <p class="white-title text-center">
                    Register
                </p>
                <div class="p-1">
                  <div class="w-100 px-2 py-3" style="height: 400px; background-color: white; overflow-y: scroll" disabled>
                    {!! $setting->value !!}
                  </div>
                  <div class="form-check my-2">
                    <input type="checkbox" checked class="form-check-input" id="check-term">
                    <!-- <label class="form-check-label text-white" for="exampleCheck1">I have acknowledged all the term and condition of F&F gaming instruction and will accept all the risk taken by own.</label> -->
                    <label class="form-check-label text-white" for="exampleCheck1">
                      Accept term and condition
                    </label>
                  </div>
                  <div class="text-center w-100">
                    <button id="btn-term" class="btn btn-copy uppercase text-center" style="width: 200px;">
                      <span>Continue</span>
                    </button>
                  </div>
                </div>
            </div>
            <div class="my-3 d-none" id="account">
                <p class="white-title text-center">
                    Member Account
                </p>
                <div class="p-1">
                  <form method="POST" action="{{ route('membership.register.account.submit') }}" accept-charset="UTF-8" role="form" id="login-form" novalidate="novalidate">
                    <input name="_token" type="hidden" value="AXDoXw9Rn8fGHinKTrGDo5KYkyN0uY022XTxJrtE">
                      {{ csrf_field() }}
                      <div class="form-group">
                          <label class="control-label">Introducer :</label>
                          @if(request()->get('usr'))
                          <input class="form-control" name="name_introducer" placeholder="username introducer" disabled type="text" value="{{ request()->get('usr') }}">
                          <input type="text" name="username_introducer" class="form-control" value="{{ request()->get('usr') }}" hidden>
                          @else
                          <input class="form-control" required name="username_introducer" placeholder="username introducer" type="text" value="{{ old('username_introducer') }}">
                          @endif
                      </div>
                      <div class="form-group">
                          <label class="control-label">Username :</label>
                          <input class="form-control" required name="username" placeholder="username" type="text" value="{{ old('username') }}">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Email :</label>
                          <input class="form-control" required name="email" placeholder="example@example.com" type="text" value="{{ old('email') }}">
                      </div>
                      <div class="form-group">
                          <label class="control-label">PASSWORD :</label>
                          <input class="form-control" required placeholder="password" name="password" type="password" value="">
                      </div>
                      <div class="form-group">
                          <label class="control-label">PASSWORD CONFIRMATION :</label>
                          <input class="form-control" required placeholder="confirmation password" name="password_confirmation" type="password" value="">
                      </div>
                      <div class="form-group">
                          <label class="control-label">WALLET PASSWORD :</label>
                          <input class="form-control" required name="wallet_password" placeholder="wallet password" type="password" value="">
                      </div>
                      <div class="form-group">
                          <label class="control-label">WALLET PASSWORD CONFIRMATION :</label>
                          <input class="form-control" required name="wallet_password_confirmation" placeholder="confirmation wallet password" type="password" value="">
                      </div>
                      <div class="text-center text-white"> Already have a account? <a href="{{ route('membership.login') }}" style="color:#fff;font-weight: bold;">LOGIN</a></div>
                      <div class="d-flex justify-content-center">
                          <button type="submit" id="submit-login-btn" class="btn btn-copy uppercase text-center" style="width: 200px;">
                              <span>REGISTER</span>
                          </button>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function(){
    $('#btn-term').on('click', function(){
      if($("#check-term").is(':checked')){
        $("#term").remove();
        $("#account").removeClass('d-none');
      }
    });
  });
</script>
@endsection
