@extends('frontend/layouts/home')

@section('title', 'Complete Account')
@section('link')
<style>
  html > body {
    background-image: url('/img/bg-black.png');
  }
</style>
@endsection
@section('container')
<div class="main-container" style="margin-bottom:150px;">
    <div class="d-flex justify-content-start">
        <button class="btn text-white" onclick="window.history.back();" style="box-shadow: inset 0 0 25px #ffbf00;color:#848e96;"><i class="fas fa-chevron-left"></i></button>
    </div>
    <div class="card-green auth-layout mt-3">
        <div class="my-3 py-2 py-3">
            <p class="white-title text-center text-success" style="font-size:40px;">
                90%
            </p>
            <div class="p-1 white-title text-center text-success">
                <h3>Pending Verification</h3>
            </div>
            <div class="information text-center">
              <?php
              $user = Auth::guard('member')->user();
               ?>
               @if($user->parent_id != null || $user->parent_id != 0)
               <p class="text-danger text-center mb-0">Please transfer min 10 soul with your main account for activate this account.</p>
               <button type="button" class="btn btn-copy mt-3" data-toggle="modal" data-target="#loginModal" name="button">Main Account</button>
               <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title text-black" id="modalLabel">Login To Main Account</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                       </button>
                     </div>
                     <div class="modal-body">
                       <form id="formLogin" class="" method="post" action="{{ route('membership.sub_account.change_account') }}">
                         {{ csrf_field() }}
                         <input type="text" name="id" value="{{ $user->parent_id }}" hidden>
                         <div class="form-group text-black text-left">
                           <label for="">Your Current Password</label>
                           <input type="password" name="password" class="form-control" required value="">
                         </div>
                       </form>
                     </div>
                     <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                       <button type="submit" form="formLogin" class="btn btn-primary">Submit</button>
                     </div>
                   </div>
                 </div>
               </div>
               @endif
            </div>
        </div>
    </div>
</div>
@endsection
