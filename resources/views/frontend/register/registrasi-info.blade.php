@extends('frontend/layouts/home')
@section('title', 'register')
@section('link')
<style>
  html > body {
    background-image: url('/img/bg-black.png');
  }
</style>
@endsection
@section('container')

<div class="main-container" style="margin-bottom:150px;">
    <div class="d-flex justify-content-start">
        <button class="btn text-white" onclick="window.history.back();" style="box-shadow: inset 0 0 25px #ffbf00;color:#848e96;"><i class="fas fa-chevron-left"></i></button>
    </div>
    <div class="message" style="margin-top: 20px">
      @include('flash::message')
    </div>
    <div class="auth-layout mt-3 card-green w-100" style="padding: 10px 20px;">
        <div>
            <div class="my-3">
                <p class="white-title text-center">
                    Information
                </p>
                <div class="p-1">
                  <form method="POST" action="{{ route('membership.register.information.submit') }}" accept-charset="UTF-8" role="form" id="login-form">
                    {{ csrf_field() }}
                      <div class="form-group">
                          <label class="control-label">Fullname :</label>
                          <input class="form-control" name="full_name" required type="text" placeholder="full name" value="{{ old('full_name') }}">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Wechat ID :</label>
                          <input class="form-control" name="wechat_id" type="text" placeholder="wechat id" value="">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Country :</label>
                          <select id="select-country" name="country" required class="custom-select form-control">
                              <option selected value="">Select Country</option>
                              @foreach($country as $i)
                              <option value="{{ $i->id }}" data-code="{{$i->code_number}}" data-bank="{{$i->bank}}">{{ $i->name }}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Number Phone :</label>
                          <div class="d-flex">
                            <select id="select-code_number" class="form-control d-inline w-auto" name="code_number" required>
                              <option value="">Select</option>
                            </select>
                            <input class="form-control" name="number_phone" type="number" min="1" placeholder="number phone" value="{{ old('number_phone') }}">
                          </div>
                          <small class="text-secondary">Code number auto input if select country</small>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Gender :</label>
                          <select name="gender" required class="custom-select form-control">
                              <option selected value="">Select Gender</option>
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                              <option value="decline_to_state">Decline to State</option>
                              <option value="custom">Custom</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Bank :</label>
                          <select id="select-bank" name="bank" required class="custom-select form-control">
                              <option selected>Select Bank</option>
                          </select>
                          <small class="text-secondary">Showing list bank after the select country</small>
                      </div>
                      <div class="form-group" id="input-custom-bank">

                      </div>
                      <div class="form-group">
                          <label class="control-label">Bank Account Number :</label>
                          <input class="form-control" name="bank_number" required placeholder="bank number" type="number" value="{{ old('bank_number') }}">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Bank Account Holder Name :</label>
                          <input class="form-control" name="bank_account" required placeholder="bank account" type="text" value="{{ old('bank_account') }}">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Swift Code :</label>
                          <input class="form-control" name="swift_code" placeholder="swift code" type="text" value="{{ old('swift_code') }}">
                      </div>
                      <div class="d-flex justify-content-center">
                          <button type="submit" id="submit-login-btn" class="btn btn-copy uppercase" style="width: 200px">
                              <span>SUBMIT</span>
                          </button>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function(){
    $('#select-country').on('change', function(){
      if ($(this).val() != ""){
        var code = $(this).find(':selected').data('code');
        var bank = $(this).find(':selected').data('bank');
        console.log(bank);
        $("#select-code_number").html(
          '<option selected value="'+ code +'">+'+ code +'</option>'
        );
        $("#select-bank").html('<option selected value="">Select Bank</option>');
        bank.forEach(function(item, index){
          $("#select-bank").append('<option value="'+ item['name'].toLowerCase() +'">'+ item['name'].toUpperCase() +'</option>');
        })
        $("#select-bank").append('<option value="custom" >Custom</option>');
      } else {
        $("#select-code_number").html(
          '<option selected value="">Select</option>'
        );
        $("#select-bank").html('<option selected value="">Select Bank</option>');
        $("#select-bank").append('<option value="custom" >Custom</option>');
        $("#input-custom-bank").html('');
      }
    });

    $('#select-bank').on('change', function(){
      if($(this).val() == 'custom'){
        $("#input-custom-bank").html(
          '<label class="control-label">Custom Bank :</label>' +
          '<input class="form-control" name="bank_name" required placeholder="bank name" type="text" value="">'
        );
      } else {
        $("#input-custom-bank").html('');
      }
    });
  });
</script>
@endsection
