@extends('frontend/layouts/main')

@section('title', 'Time')
@section('title-content', 'Time')

@section('link')
<style>
  html>body .modal-backdrop {
    display: none !important;
  }

  .owl-carousel .owl-item {
    width: 100% !important;
  }

  .owl-carousel .owl-stage {
    width: 100% !important;
  }

  .message {
    position: absolute;
    width: 100%;
    z-index: 999;
  }

  #tabHire .nav-item .nav-link {
      color: white;
  }

  #tabHire .nav-item .nav-link.active{
    background-color: transparent;
    color: green;
    border: none;
    border-bottom: 1px solid green;
  }

  #tabHire .nav-item .nav-link:hover {
    border: none;
    color: green;
  }

  #tabHire .nav-item .nav-link.active:hover {
    color: green;
    background-color: transparent;
    border: 1px solid green;
  }

  .pg-number {
    font-size: 12px;
  }
</style>
@endsection
@section('container')
<div class="container" style="z-index: 9999; margin-top: 30px;">
@if(Agent::isMobile())
  <div class="content" style="margin: auto;position: relative;">
@else
  <div class="content" style="width: 400px;margin: auto;position: relative;">
@endif
<div class="message">
  @include('flash::message')
</div>
    <div class="section-your-item">
      <h4 class="text-white mb-4">On Sale</h4>
      @if(sizeof($charactersSeller) == 0)
      <p class="text-secondary mt-3 text-center">No data available</p>
      @else
      <div class="owl-carousel w-100 pb-2" id="owlSeller" style="border-bottom: 1px solid white">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
            <div class="carousel-inner w-100">
              @for($i = 0; $i < sizeof($charactersSeller); $i++)
                <div class="carousel-item w-100" style="position: relative;">
                    <img src="{{ route('membership.character.picture', $charactersSeller[$i]->character_id) }}" style="object-fit: cover;width: 200px; height: 200px; border-radius: 50%;" class="mx-auto" alt="Card image ca" >
                      <div class="mt-3">
                        <h4 class="text-center text-success">
                          @if($charactersSeller[$i]->character)
                          {{ $charactersSeller[$i]->character->name }}
                          @else
                          Not Found
                          @endif
                        </h4>
                      </div>
                      <div class="d-block mt-3">
                        <h6 class="text-center" style="color: black; background-color: white; padding: 10px 15px;">{{ $charactersSeller[$i]->status }}</h6>
                        @if($charactersSeller[$i]->charactersTransactions)
                        <p class="text-white text-left">Capital Price : {{ number_format($charactersSeller[$i]->price) }}</p>
                        <p class="text-white text-left mb-1">Price of Sale : {{ number_format($charactersSeller[$i]->charactersTransactions->price_of_sale) }}</p>
                        <p class="text-white text-left">Price of Sale (USD) : -</p>
                        <p class="text-white text-left">Profit : {{ number_format($charactersSeller[$i]->charactersTransactions->profit) }}</p>
                        @else
                        <p class="text-white text-left mb-1">Price : {{ number_format($charactersSeller[$i]->price) }}</p>
                        <p class="text-white text-left">Price (USD) : -</p>
                        @endif
                        @if($charactersSeller[$i]->status == 'ready')
                          @if($charactersSeller[$i]->prediction_price)
                          <p class="text-white text-left mb-1">Prediction Sale : {{ number_format($charactersSeller[$i]->prediction_price) }}</p>
                          <p class="text-white text-left">Prediction Sale (USD) : -</p>
                          @endif
                        <h4 class="text-white mt-2 time_remind" id="time_remain{{$charactersSeller[$i]->id}}" data-start-sale="{{ $charactersSeller[$i]->start_of_sale }}" data-end-sale="{{ $charactersSeller[$i]->end_of_sale }}">Time remaining: </h4>
                        @endif
                        @if($charactersSeller[$i]->charactersTransactions)
                        <div class="py-2">
                          <p class="text-white mb-1 text-center">You got a buyer</p>
                          @if($charactersSeller[$i]->charactersTransactions->charactersOrder)
                            @if($charactersSeller[$i]->charactersTransactions->charactersOrder->member)
                            <div class="text-center">
                              <button type="button" name="button" data-toggle="modal" data-target="#modalSeller{{$charactersSeller[$i]->id}}" class="btn btn-link text-center">Detail Buyer</button>
                            </div>
                            @else
                            <div class="text-center">
                              <p class="mb-0 text-secondary">Sorry, detail buyer not found please contact admin</p>
                            </div>
                            @endif
                          @if($charactersSeller[$i]->charactersTransactions->proof_of_payment == null)
                          {{ $charactersSeller[$i]->charactersTransactions }}
                          <br>
                          The buyer has not made payment
                          @else
                          <div class="form-group text-left mt-1">
                            <label class="@if($charactersSeller[$i]->charactersTransactions->status == 'reject') text-warning @else text-secondary @endif">Buyer Payment</label>
                            <img class="w-100" style="object-fit:cover" src="{{ route('membership.character.payment.show', $charactersSeller[$i]->charactersTransactions->id) }}" alt="">
                          </div>
                          @endif
                          @if($charactersSeller[$i]->charactersTransactions->status == 'reject')
                          <div class="form-group">
                            <span class="text-danger">Buyer payment rejected, please contact buyer for upload again.</span>
                          </div>
                          @endif
                          <div class="form-status">
                            <form method="post" action="{{ route('membership.character.set_status') }}">
                              {{ csrf_field() }}
                              <div class="form-group">
                                <label class="text-white">Set Proof</label>
                                <input type="text" name="id" value="{{ $charactersSeller[$i]->charactersTransactions->id }}" hidden>
                                <select class="form-control" name="status">
                                  <option value="success">Accept</option>
                                  <option value="reject">Reject</option>
                                </select>
                              </div>
                              <button type="submit" class="btn btn-copy" name="button">Submit</button>
                            </form>
                          </div>
                          @endif
                        </div>
                        @endif
                      </div>
                </div>
              @endfor
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
      <!-- modal for informastion detail seller -->
      @for($i = 0; $i < sizeof($charactersSeller); $i++)
        @if($charactersSeller[$i]->charactersTransactions)
          @if($charactersSeller[$i]->charactersTransactions->charactersOrder)
          <div class="modal fade" style="z-index: 999999" id="modalSeller{{$charactersSeller[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelSeller{{$charactersSeller[$i]->id}}" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabelSeller{{$charactersOrder[$i]->id}}">Info Buyer @if($charactersSeller[$i]->charactersTransactions->charactersOrder->member)
                    {{ $charactersSeller[$i]->charactersTransactions->charactersOrder->member->username }}@endif</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="biodata-seller">
                    <h5>
                      <b>Info Detail</b>
                    </h5>
                    @if($charactersSeller[$i]->charactersTransactions->charactersOrder->member)
                      <div class="my-3" style="border-bottom: 1px solid grey">
                        <p>Username : {{ $charactersSeller[$i]->charactersTransactions->charactersOrder->member->username }}</p>
                        <?php
                        $infoUser = Auth::user();
                         ?>
                         @if($infoUser->parent_id == null || $infoUser->parent_id == 0 || !$infoUser->parent_id)
                          @if($charactersSeller[$i]->charactersTransactions->charactersOrder->member->parent_id == $infoUser->id)
                          <p>Account : Your Sub Account</p>
                          @endif
                         @elseif($charactersSeller[$i]->charactersTransactions->charactersOrder->member->parent_id == $infoUser->parent_id)
                          <p>Account : Sub Account</p>
                         @elseif($charactersSeller[$i]->charactersTransactions->charactersOrder->member->id == $infoUser->parent_id)
                          <p>Account : Your Main Account</p>
                         @endif
                        @if($charactersSeller[$i]->charactersTransactions->charactersOrder->member->memberData)
                          <p>Number Phone : +{{ $charactersSeller[$i]->charactersTransactions->charactersOrder->member->memberData->number_phone }}
                          </p>
                          <p>Wechat Id : {{ $charactersSeller[$i]->charactersTransactions->charactersOrder->member->memberData->wechat_id }}</p>
                        @else
                        <p class="text-danger">Data Buyer not found</p>
                        @endif
                      </div>
                    @else
                    <p class="text-danger">Data Buyer not found</p>
                    @endif
                  </div>
                  <div class="bank-account-seller">
                    <h5>
                      <b>Bank Account</b>
                    </h5>
                    @if($charactersSeller[$i]->charactersTransactions->charactersOrder->member)
                      @if($charactersSeller[$i]->charactersTransactions->charactersOrder->member->memberData)
                      <div class="my-3" style="border-bottom: 1px solid grey">
                        <p>Bank : {{ $charactersSeller[$i]->charactersTransactions->charactersOrder->member->memberData->bank }}</p>
                        <p>Holder Name : {{ $charactersSeller[$i]->charactersTransactions->charactersOrder->member->memberData->bank_account }}</p>
                        <p>Bank Number : {{ $charactersSeller[$i]->charactersTransactions->charactersOrder->member->memberData->bank_number }}</p>
                      </div>
                      @else
                      <p class="text-danger">Data Buyer not found</p>
                      @endif
                    @else
                    <p class="text-danger">Data Buyer not found</p>
                    @endif
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          @endif
        @endif
      @endfor
      @endif
    </div>
    <div class="section-order-item pt-4">
      <h4 class="text-white mb-4">Request Order</h4>
      <?php $countOrder = 0; ?>
      @if(sizeof($charactersOrder) == 0)
      <p class="text-secondary mt-3 text-center">No data available</p>
      @else
      <div class="owl-carousel w-100" id="owlOrder">
        <div id="carouselExampleControls2" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
            <div class="carousel-inner w-100">
              @for($i = 0; $i < sizeof($charactersOrder); $i++)
                @if($charactersOrder[$i]->status == 'request')
                  <?php $countOrder = $countOrder + 1; ?>
                  <div class="carousel-item w-100" style="position: relative;">
                    <img src="{{ route('membership.character.picture', $charactersOrder[$i]->character_id) }}" style="object-fit: cover;width: 200px; height: 200px; border-radius: 50%;" class="mx-auto" alt="Card image ca" >
                    <div class="mt-3">
                      <h4 class="text-center text-success">
                        @if($charactersOrder[$i]->character)
                        {{ $charactersOrder[$i]->character->name }}
                        @else
                        Not Found
                        @endif
                      </h4>
                    </div>
                    <div class="d-block mt-3">
                      <h6 class="text-center" style="color: black; background-color: white; padding: 10px 15px;">{{ $charactersOrder[$i]->status }}</h6>
                    </div>
                  </div>
                <!-- check charactersorder transaction -->
                @elseif($charactersOrder[$i]->charactersTransactions)
                  <!-- status order start -->
                  @if($charactersOrder[$i]->charactersTransactions->status == 'on_payment' || $charactersOrder[$i]->charactersTransactions->status == 'reject')
                    <?php $countOrder = $countOrder + 1; ?>
                    <div class="carousel-item w-100" style="position: relative;">
                      <img src="{{ route('membership.character.picture', $charactersOrder[$i]->character_id) }}" style="object-fit: cover;width: 200px; height: 200px; border-radius: 50%;" class="mx-auto" alt="Card image ca" >
                      <div class="mt-3">
                        <h4 class="text-center text-success">
                          @if($charactersOrder[$i]->character)
                          {{ $charactersOrder[$i]->character->name }}
                          @else
                          Not Found
                          @endif
                        </h4>
                      </div>
                      <div class="d-block mt-3">
                        <h6 class="text-center" style="color: black; background-color: white; padding: 10px 15px;">{{ 'gained' }}</h6>
                      </div>
                      <div class="detail-payment">
                        <p class="text-secondary text-center">
                          <small>you must pay to get this item</small>
                        </p>
                        @if($charactersOrder[$i]->charactersTransactions->characters_seller_id == 0)
                          @if(sizeof($bankCompany) == 0)
                          <p class="text-secondary mt-2">Contact your admin for detail transfer this item</p>
                          @else
                          <button type="button" name="button" data-toggle="modal" data-target="#modalRequestCompany" class="btn btn-link">Detail Bank Transfer</button>
                          <div class="detail-payment">
                            <form method="post" enctype="multipart/form-data" class="px-4 mt-3" action="{{ route('membership.character.payment.submit') }}">
                              {{ csrf_field() }}
                              <p class="text-secondary text-left mb-1">Price : <b>{{ number_format($charactersOrder[$i]->charactersTransactions->price_of_sale) }}</b></p>
                              <p class="text-secondary text-left">Price (USD) : <b>-</b></p>
                              <input type="text" hidden name="characters_transactions_id" value="{{ $charactersOrder[$i]->charactersTransactions->id }}">
                              @if($charactersOrder[$i]->charactersTransactions->proof_of_payment !== null)
                              <div class="form-group text-left">
                                <label class="@if($charactersOrder[$i]->charactersTransactions->status == 'reject') text-warning @else text-white @endif">Your Payment</label>
                                <img class="w-100" style="object-fit:cover;" src="{{ route('membership.character.payment.show', $charactersOrder[$i]->charactersTransactions->id) }}" alt="">
                              </div>
                              @endif
                              <div class="form-group text-left">
                                <label class="text-white">Transfer Payment</label>
                                <select class="form-control" name="method_payment" required>
                                  <option value="">Select bank</option>
                                  @foreach($bankCompany as $bank)
                                    @if($bank->bank)
                                    <option value="{{ $bank->bank->name }}">{{ $bank->bank->name }}</option>
                                    @endif
                                  @endforeach
                                </select>
                              </div>
                              @if($charactersOrder[$i]->charactersTransactions->status == 'reject')
                              <div class="form-group text-left">
                                <span class="text-danger">Sorry, your payment rejected please upload again.</span>
                              </div>
                              @endif
                              <div class="form-group text-left">
                                <label class="text-white">Payment Deadline</label>
                                <p class="text-secondary">{{ date('d-m-Y H:i', strtotime($charactersOrder[$i]->charactersTransactions->end_of_payment)) }}</p>
                              </div>
                              <div class="form-group text-left">
                                <label class="text-white">Upload Payment</label>
                                <input type="file" name="proof_of_payment" value="" required>
                              </div>
                              <div class="form-group text-left">
                                <button type="submit" class="btn btn-copy" name="button">Submit</button>
                              </div>
                            </form>
                          </div>
                          @endif
                        @else
                          @if($charactersOrder[$i]->charactersTransactions->charactersSeller)
                          <div class="text-center">
                            <button type="button" name="button" data-toggle="modal" data-target="#modalRequestSeller{{$charactersOrder[$i]->id}}" class="btn btn-link text-center">Detail Seller</button>
                          </div>
                          <div class="detail-payment pb-3">
                            <form method="post" enctype="multipart/form-data" class="px-4 mt-3" action="{{ route('membership.character.payment.submit') }}">
                              {{ csrf_field() }}
                              <p class="text-secondary mb-1">Price : <b>{{ number_format($charactersOrder[$i]->charactersTransactions->price_of_sale) }}</b></p>
                              <p class="text-secondary">Price (USD) : <b>-</b></p>
                              <input type="text" hidden name="characters_transactions_id" value="{{ $charactersOrder[$i]->charactersTransactions->id }}">
                              @if($charactersOrder[$i]->charactersTransactions->proof_of_payment !== null)
                              <div class="form-group text-left">
                                <label class="@if($charactersOrder[$i]->charactersTransactions->status == 'reject') text-warning @else text-white @endif">Your Payment</label>
                                <img class="w-100" style="object-fit:cover;" src="{{ route('membership.character.payment.show', $charactersOrder[$i]->charactersTransactions->id) }}" alt="">
                              </div>
                              @endif
                              @if($charactersOrder[$i]->charactersTransactions->status == 'reject')
                              <div class="form-group">
                                <span class="text-danger">Sorry, your payment rejected please upload again.</span>
                              </div>
                              @endif
                              <div class="form-group">
                                <label class="text-white">Payment Deadline</label>
                                <p class="text-secondary">{{ date('d-m-Y H:i', strtotime($charactersOrder[$i]->charactersTransactions->end_of_payment)) }}</p>
                              </div>
                              <div class="form-group text-left">
                                <label class="text-white">Upload Payment</label>
                                <input type="file" class="text-control" name="proof_of_payment" value="" required>
                              </div>
                              <button type="submit" class="btn btn-copy" name="button">Submit</button>
                            </form>
                          </div>
                          @else
                          <p class="text-secondary">You must contact your seller</p>
                          @endif
                        @endif
                      </div>
                    </div>
                  @endif
                @endif
              @endfor
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
      @if($countOrder == 0)
      <p class="text-secondary mt-3 text-center">No data available</p>
      @endif
        <!-- modal for informastion detail seller -->
        @for($i = 0; $i < sizeof($charactersOrder); $i++)
          @if($charactersOrder[$i]->charactersTransactions)
            @if($charactersOrder[$i]->charactersTransactions->charactersSeller)
            <div class="modal fade" style="z-index: 999999" id="modalRequestSeller{{$charactersOrder[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelRequestSeller{{$charactersOrder[$i]->id}}" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelRequestSeller{{$charactersOrder[$i]->id}}">Info Seller @if($charactersOrder[$i]->charactersTransactions->charactersSeller->member)
                      {{ $charactersOrder[$i]->charactersTransactions->charactersSeller->member->username }}@endif</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="biodata-seller">
                      <h5>
                        <b>Info Detail</b>
                      </h5>
                      @if($charactersOrder[$i]->charactersTransactions->charactersSeller->member)
                        <div class="my-3" style="border-bottom: 1px solid grey">
                          <p>Username : {{ $charactersOrder[$i]->charactersTransactions->charactersSeller->member->username }}</p>
                          <?php
                          $infoUser = Auth::user();
                           ?>
                           @if($infoUser->parent_id == null || $infoUser->parent_id == 0 || !$infoUser->parent_id)
                            @if($charactersOrder[$i]->charactersTransactions->charactersSeller->member->parent_id == $infoUser->id)
                            <p>Account : Your Sub Account</p>
                            @endif
                           @elseif($charactersOrder[$i]->charactersTransactions->charactersSeller->member->parent_id == $infoUser->parent_id)
                            <p>Account : Sub Account</p>
                           @elseif($charactersOrder[$i]->charactersTransactions->charactersSeller->member->id == $infoUser->parent_id)
                            <p>Account : Your Main Account</p>
                           @endif
                          @if($charactersOrder[$i]->charactersTransactions->charactersSeller->member->memberData)
                            <p>Number Phone : +{{ $charactersOrder[$i]->charactersTransactions->charactersSeller->member->memberData->number_phone }}
                            </p>
                            <p>Wechat Id : {{ $charactersOrder[$i]->charactersTransactions->charactersSeller->member->memberData->wechat_id }}</p>
                          @else
                          <p class="text-danger">Data seller not found</p>
                          @endif
                        </div>
                      @else
                      <p class="text-danger">Data seller not found</p>
                      @endif
                    </div>
                    <div class="bank-account-seller">
                      <h5>
                        <b>Bank Account</b>
                      </h5>
                      @if($charactersOrder[$i]->charactersTransactions->charactersSeller->member)
                        @if($charactersOrder[$i]->charactersTransactions->charactersSeller->member->memberData)
                        <div class="my-3" style="border-bottom: 1px solid grey">
                          <p>Bank : {{ $charactersOrder[$i]->charactersTransactions->charactersSeller->member->memberData->bank }}</p>
                          <p>Holder Name : {{ $charactersOrder[$i]->charactersTransactions->charactersSeller->member->memberData->bank_account }}</p>
                          <p>Bank Number : {{ $charactersOrder[$i]->charactersTransactions->charactersSeller->member->memberData->bank_number }}</p>
                        </div>
                        @else
                        <p class="text-danger">Data seller not found</p>
                        @endif
                      @else
                      <p class="text-danger">Data seller not found</p>
                      @endif
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
            @endif
          @endif
        @endfor
        <div class="modal fade" id="modalRequestCompany" tabindex="-1" role="dialog" aria-labelledby="modalRequestCompany" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">List bank transfer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                @foreach($bankCompany as $bank)
                <div class="my-3" style="border-bottom: 1px solid grey">
                  @if($bank->bank)
                  <p>Bank : {{ $bank->bank->name }}</p>
                  @endif
                  <p>Holder Name : {{ $bank->bank_account }}</p>
                  <p>Bank Number : {{ $bank->bank_number }}</p>
                </div>
                @endforeach
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      @endif
    </div>
    <div class="mt-3 card card-green">
      <div class="card-header">
        <ul class="nav nav-tabs" id="tabHire" role="tablist" style="border-bottom: none">
          <li class="nav-item">
            <a class="nav-link active" id="buyin-tab" data-toggle="tab" href="#buyin" role="tab" aria-controls="buyin" aria-selected="true">Buy In</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="sellout-tab" data-toggle="tab" href="#sellout" role="tab" aria-controls="sellout" aria-selected="false">Sell Out</a>
          </li>
        </ul>
      </div>
      <div class="card-body">
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active px-2" id="buyin" role="tabpanel" aria-labelledby="buyin-tab">
            @if(sizeof($historyOrder) == 0)
            <p class="text-white text-center">Data not available</p>
            @else
            @foreach($historyOrder as $i)
            <div class="row mb-2" style="border-bottom: 0.1px solid #828282; font-size: 15px">
              <div class="col-6">
                <div class="form-group">
                  <label class="text-secondary">ID Order</label>
                  <p class="text-white">{{ $i->id }}</p>
                </div>
                <div class="form-group">
                  <label class="text-secondary">Character</label>
                  <p class="text-white">
                    @if($i->character)
                    {{ $i->character->name }}
                    @else
                    <span class="text-danger">Not Found</span>
                    @endif
                  </p>
                </div>
                <div class="form-group">
                  <label class="text-secondary">Price</label>
                  <p class="text-white">
                    @if($i->character)
                    {{ number_format($i->character->price_for) }} - {{ number_format($i->character->price_to) }}
                    @else
                    0
                    @endif
                  </p>
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label class="text-secondary">Order Time</label>
                  <p class="text-white">
                    {{ date('d-m-Y H:i', strtotime($i->date_order)) }}
                  </p>
                </div>
                <div class="form-group">
                  <label class="text-secondary">Status</label>
                  <p class="@if($i->status == 'success') text-white @elseif($i->status == 'request') text-warning @else text-danger @endif">
                    {{ $i->status }}
                  </p>
                </div>
              </div>
            </div>
            @endforeach
            <div class="pg-number mt-3">
              {!! $historyOrder !!}
            </div>
            @endif
          </div>
          <div class="tab-pane fade px-2" id="sellout" role="tabpanel" aria-labelledby="sellout-tab">
            @if(sizeof($soldSeller) == 0)
            <p class="text-white text-center">Data not available</p>
            @else
            @foreach($soldSeller as $i)
            <div class="row mb-2" style="border-bottom: 0.1px solid #828282; font-size: 15px">
              <div class="col-6">
                <div class="form-group">
                  <label class="text-secondary">ID Transaction</label>
                  <p class="text-white">
                    @if($i->charactersTransactions)
                      {{$i->charactersTransactions->id}}
                    @else
                    <span class="text-warning">In Sales</span>
                    @endif
                  </p>
                </div>
                <div class="form-group">
                  <label class="text-secondary">Character</label>
                  <p class="text-white">
                    @if($i->character)
                    {{ $i->character->name }}
                    @else
                    <span class="text-danger">Not Found</span>
                    @endif
                  </p>
                </div>
                <div class="form-group">
                  <label class="text-secondary">Buyer</label>
                  <p class="text-white">
                    @if($i->charactersTransactions)
                      @if($i->charactersTransactions->charactersOrder)
                        @if($i->charactersTransactions->charactersOrder->member)
                        {{ $i->charactersTransactions->charactersOrder->member->username }}
                        @else
                        <span class="text-danger">Not Found</span>
                        @endif
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    @else
                    <span class="text-warning">In Sales</span>
                    @endif
                  </p>
                </div>
                <div class="form-group">
                  <label class="text-secondary">Price of Sale</label>
                  <p class="text-white">
                    @if($i->charactersTransactions)
                    {{ number_format($i->charactersTransactions->price_of_sale) }}
                    @else
                    <span class="text-warning">In Sales</span>
                    @endif
                  </p>
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label class="text-secondary">Date Sold</label>
                  <p class="text-white">
                    {{ date('d/m/y H:i', strtotime($i->date_of_sale)) }}
                  </p>
                </div>
                <div class="form-group">
                  <label class="text-secondary">Status</label>
                  <p class="@if($i->status == 'ready') text-white @elseif($i->status == 'on_payment') text-warning @else text-white @endif">
                    {{ $i->status }}
                  </p>
                </div>
              </div>
              <div class="col-12 mb-3">
                <div class="d-flex">
                  @if($i->charactersTransactions)
                    @if($i->charactersTransactions->charactersOrder)
                      @if($i->charactersTransactions->charactersOrder->member)
                      <button type="button" class="btn btn-copy btn-sm" data-toggle="modal" data-target="#modalDetailBuyer{{$i->id}}" name="button">Info Buyer</button>
                      @endif
                    @endif
                  <button type="button" data-toggle="modal" data-target="#modalDetailSell{{$i->id}}" class="btn btn-copy btn-sm mx-2" name="button">Detail</button>
                  @endif
                </div>
              </div>
            </div>
            @if($i->charactersTransactions)
              @if($i->charactersTransactions->charactersOrder)
                @if($i->charactersTransactions->charactersOrder->member)
                <div class="modal fade" id="modalDetailBuyer{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="modalLabelBuyer{{$i->id}}" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content card-green" style="background-color: black">
                      <div class="modal-body">
                        <div class="form-group">
                          <label class="text-secondary">Username</label>
                          <p class="text-white">{{ $i->charactersTransactions->charactersOrder->member->username }}</p>
                        </div>
                        <div class="form-group">
                          <label class="text-secondary">Bank</label>
                          @if($i->charactersTransactions->charactersOrder->member->memberData)
                          <p class="text-white">{{ $i->charactersTransactions->charactersOrder->member->memberData->bank }}</p>
                          @else
                          <p class="text-danger">Bank no set. Please contact admin</p>
                          @endif
                        </div>
                        <div class="form-group">
                          <label class="text-secondary">Bank Holder Name</label>
                          @if($i->charactersTransactions->charactersOrder->member->memberData)
                          <p class="text-white">{{ $i->charactersTransactions->charactersOrder->member->memberData->bank_account }}</p>
                          @else
                          <p class="text-danger">Bank no set. Please contact admin</p>
                          @endif
                        </div>
                        <div class="form-group">
                          <label class="text-secondary">Bank Number</label>
                          @if($i->charactersTransactions->charactersOrder->member->memberData)
                          <p class="text-white">{{ $i->charactersTransactions->charactersOrder->member->memberData->bank_number }}</p>
                          @else
                          <p class="text-danger">Bank no set. Please contact admin</p>
                          @endif
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                @endif
              @endif
                <div class="modal fade" id="modalDetailSell{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="modalLabelSell{{$i->id}}" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content card-green" style="background-color: black">
                      <div class="modal-body">
                        <div class="form-group">
                          <label class="text-secondary">Price of Capital</label>
                          <p class="text-white mb-0">{{ number_format($i->price) }}</p>
                        </div>
                        <div class="form-group">
                          <label class="text-secondary">Price of Capital (USD)</label>
                          <p class="text-white">-</p>
                        </div>
                        <div class="form-group">
                          <label class="text-secondary">Profit</label>
                          <p class="text-white mb-0">{{ number_format($i->charactersTransactions->profit) }}</p>
                        </div>
                        <div class="form-group">
                          <label class="text-secondary">Profit (USD)</label>
                          <p class="text-white">-</p>
                        </div>
                        <div class="form-group">
                          <label class="text-secondary">Price of Sale</label>
                          <p class="text-white mb-0">{{ number_format($i->charactersTransactions->price_of_sale) }}</p>
                        </div>
                        <div class="form-group">
                          <label class="text-secondary">Price of Sale (USD)</label>
                          <p class="text-white">-</p>
                        </div>
                        <div class="form-group">
                          <label class="text-secondary">Date of Sale</label>
                          <p class="text-white">
                            {{ date('d/m/y H:i', strtotime($i->start_of_sale)) }} - {{ date('d/m/y H:i', strtotime($i->end_of_sale)) }}
                          </p>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
            @endif
            @endforeach
            <div class="pg-number mt-3">
              {!! $soldSeller !!}
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-duration-format/2.3.2/moment-duration-format.js"></script>
    <script>
        $(document).ready(function(){
          $("#owlOrder").owlCarousel();
          $("#owlOrder .carousel-item").first().addClass('active');
          $("#owlSeller").owlCarousel();
          $("#owlSeller .carousel-item").first().addClass('active');
          $(".btn-copy").on("click", function(){
            $(".modal-backdrop").remove();
          });
          $('.table').DataTable({
            "searching": false
          });

          var array = [];
          function timer(id, value){
            setInterval(function(){
              // duration = moment.duration(endTime, 'minutes');
              // console.log("duration format : " + duration);
              var now = moment();
              var calculate = value - moment();
              let timeRemaining = moment.duration(calculate).format('d [day] h:m:s');
              if(calculate > 0){
                $(id).html("On Sale : " + timeRemaining);
              } else {
                $(id).html("Loading...");
              }
            }, 1000);
          }
          $('.time_remind').each(function(e){
            let now = moment();
            var startTime = Date.parse($(this).data('start-sale'));
            var endTime = Date.parse($(this).data('end-sale'));

            console.log(now + ' ' + endTime);
            if (now >= endTime){
              $(this).html("Loading...");
            } else if (now >= startTime){
              var id = $(this);
              timer(this, endTime);
            } else {
              $(this).html("Start Sale : " + $(this).data('start-sale'));
            }
          });
        });
    </script>
@endsection
