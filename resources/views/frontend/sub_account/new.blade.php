@extends('frontend/layouts/main')

@section('title', 'Add Sub Account')
@section('title-content', 'Add Sub Account')

@section('link')
<style>
  html>body .modal-backdrop {
    display: none !important;
  }

  .owl-carousel .owl-item {
    width: 100% !important;
  }

  .owl-carousel .owl-stage {
    width: 100% !important;
  }

  #tabHire .nav-item .nav-link {
      color: white;
  }

  #tabHire .nav-item .nav-link.active{
    background-color: transparent;
    color: green;
    border: none;
    border-bottom: 1px solid green;
  }

  #tabHire .nav-item .nav-link:hover {
    border: none;
    color: green;
  }

  #tabHire .nav-item .nav-link.active:hover {
    color: green;
    background-color: transparent;
    border: 1px solid green;
  }

  .pg-number {
    font-size: 12px;
  }
</style>
@endsection
@section('container')
<div class="container main-container" style="z-index: 9999; margin-top: 30px;">
@if(Agent::isMobile())
  <div class="content" style="margin: auto;position: relative;">
@else
  <div class="content" style="width: 400px;margin: auto;position: relative;">
@endif
<div class="message mt-3">
  @include('flash::message')
</div>
    <div class="mt-3 auth-layout card-green">
      <div class="card-body">
        <form method="post" action="{{ route('membership.sub_account.create') }}">
          {{ csrf_field() }}
          <div class="form-group">
              <label class="control-label">Username :</label>
              <input class="form-control" required name="username" placeholder="username" type="text" value="{{ old('username') }}">
          </div>
          <div class="form-group">
            <label class="control-label">Password :</label>
            <input class="form-control" required name="password" placeholder="current password account" type="password">
          </div>
          <div class="form-group text-center">
            <button type="submit" class="btn btn-copy" name="button">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-duration-format/2.3.2/moment-duration-format.js"></script>
    <script>
        $(document).ready(function(){
          $(".owl-carousel").owlCarousel();
          $(".btn-copy").on("click", function(){
            $(".modal-backdrop").remove();
          });
          $('.table').DataTable({
            "searching": false
          });

          var array = [];
          function timer(id, value){
            setInterval(function(){
              // duration = moment.duration(endTime, 'minutes');
              // console.log("duration format : " + duration);
              var now = moment();
              var calculate = value - moment();
              let timeRemaining = moment.duration(calculate).format('d [day] h:m:s');
              if(timeRemaining > 0){
                $(id).html("On Sale : " + timeRemaining);
              } else {
                $(id).html("Loading...");
              }
            }, 1000);
          }
          $('.time_remind').each(function(e){
            let now = moment();
            var startTime = Date.parse($(this).data('startSale'));
            var endTime = Date.parse($(this).data('endSale'));

            if (now >= endTime){
              $(this).html("Loading...");
            } else if (now >= startTime){
              var id = $(this);
              timer(this, endTime);
            } else {
              $(this).html("Start Sale : " + $(this).data('startSale'));
            }
          });
        });
    </script>
@endsection
