@extends('frontend/layouts/main')

@section('title', 'Sub Account')
@section('title-content', 'Sub Account')

@section('link')
<style>
  html>body .modal-backdrop {
    display: none !important;
  }

  .owl-carousel .owl-item {
    width: 100% !important;
  }

  .owl-carousel .owl-stage {
    width: 100% !important;
  }

  #tabHire .nav-item .nav-link {
      color: white;
  }

  #tabHire .nav-item .nav-link.active{
    background-color: transparent;
    color: green;
    border: none;
    border-bottom: 1px solid green;
  }

  #tabHire .nav-item .nav-link:hover {
    border: none;
    color: green;
  }

  #tabHire .nav-item .nav-link.active:hover {
    color: green;
    background-color: transparent;
    border: 1px solid green;
  }

  .pg-number {
    font-size: 12px;
  }
</style>
@endsection
@section('container')
<div class="container" style="z-index: 9999; margin-top: 30px;">
@if(Agent::isMobile())
  <div class="content" style="margin: auto;position: relative;">
@else
  <div class="content" style="width: 400px;margin: auto;position: relative;">
@endif
<div class="message mt-3">
  @include('flash::message')
</div>
<div class="add-button">
  <a href="{{ route('membership.sub_account.new') }}" class="btn btn-link text-primary">
    + Add
  </a>
</div>
    <div class="mt-3 card card-green">
      <div class="card-body">
        @if(sizeof($data) == 0)
        <p class="text-white text-center">Data not available</p>
        @else
        @foreach($data as $i)
        <div class="row mb-2" style="border-bottom: 0.1px solid #828282; font-size: 15px">
          <div class="col-4">
            <div class="form-group">
              <label class="text-secondary">Username</label>
              <p class="text-white">
                {{ $i->username }}
              </p>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label class="text-secondary">Status</label>
              <p class="text-white">
                {{ $i->status }}
              </p>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label class="text-secondary">Account</label>
              @if($i->parent_id == null || $i->parent_id == 0 || !$i->parent_id)
              <p class="text-primary">
                Main
              </p>
              @else
              <p class="text-white">
                Sub
              </p>
              @endif
            </div>
          </div>
          @if($i->wallet)
          <div class="col-4">
            <div class="form-group">
              <label class="text-secondary">Reality</label>
              <p class="text-white">
                {{ $i->wallet->reality }}
              </p>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label class="text-secondary">Soul</label>
              <p class="text-white">
                {{ $i->wallet->soul }}
              </p>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label class="text-secondary">Power</label>
              <p class="text-white">
                {{ $i->wallet->power }}
              </p>
            </div>
          </div>
          @endif
          <div class="col-12">
            <div class="form-group">
              <label class="text-secondary">Email</label>
              <p class="text-white">
                @if($i->email)
                {{ $i->email }}
                @else
                Not Set
                @endif
              </p>
            </div>
            <div class="d-flex">
              <div class="form-group">
                <label class="text-secondary">Action</label>
                @if(Auth::guard('member')->user()->id == $i->id)
                <p class="text-white">
                  <b>Current</b>
                </p>
                @else
                <div class="d-flex">
                  <form class="d-inline" onsubmit="return confirm('Do you really want to login to {{$i->username}}?');" method="post" action="{{ route('membership.sub_account.change_account') }}">
                    {{ csrf_field() }}
                    <input type="text" name="id" value="{{ $i->id }}" hidden>
                    <button type="submit" class="btn btn-copy" name="button">Login</button>
                  </form>
                  <a href="{{ route('membership.soul.transfer') }}" class="btn btn-link">Transfer Soul</a>
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
        @endforeach
        @endif
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-duration-format/2.3.2/moment-duration-format.js"></script>
    <script>
        $(document).ready(function(){
          $(".owl-carousel").owlCarousel();
          $(".btn-copy").on("click", function(){
            $(".modal-backdrop").remove();
          });
          $('.table').DataTable({
            "searching": false
          });

          var array = [];
          function timer(id, value){
            setInterval(function(){
              // duration = moment.duration(endTime, 'minutes');
              // console.log("duration format : " + duration);
              var now = moment();
              var calculate = value - moment();
              let timeRemaining = moment.duration(calculate).format('d [day] h:m:s');
              if(timeRemaining > 0){
                $(id).html("On Sale : " + timeRemaining);
              } else {
                $(id).html("Loading...");
              }
            }, 1000);
          }
          $('.time_remind').each(function(e){
            let now = moment();
            var startTime = Date.parse($(this).data('startSale'));
            var endTime = Date.parse($(this).data('endSale'));

            if (now >= endTime){
              $(this).html("Loading...");
            } else if (now >= startTime){
              var id = $(this);
              timer(this, endTime);
            } else {
              $(this).html("Start Sale : " + $(this).data('startSale'));
            }
          });
        });
    </script>
@endsection
