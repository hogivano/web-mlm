@extends('frontend/layouts/main')

@section('title', 'Invite')
@section('title-content', 'Invite')
@section('link')
<style>
  .input-referal:hover {
    cursor: pointer;
  }

  .carousel-control-next, .carousel-control-prev {
    bottom: 0 !important;
    top: none !important;
  }
</style>
@endsection
@section('container')
        <div class="card p-3 m-3 card-green">
            <div class="card-body row text-white">
                <style>
                    .carousel-caption {
                        position: relative;
                        left: 0;
                        top: 0;
                    }

                    @media screen and (min-width: 350px){
                        .carousel-caption {
                            display: block;
                            padding: 5px;
                            width:100%;
                            margin: 0 auto;
                        }
                        .carousel-caption .h6{
                            margin: 0 auto;
                        }
                    }
                </style>
                <?php $user = Auth::guard()->user(); ?>
                @if($user->parent_id == null || $user->parent_id == 0 || !$user->parent_id)
                <div class="bd-example" style="margin:0 auto;">
                  <div class="mb-3" style="text-align:center">
                    {!! QrCode::size(200)->backgroundColor(255, 255, 255)->generate(route("referal", ["usr" => $username])) !!}
                  </div>
                  <div class="w-100 mb-2 text-center">
                    <button type="button" name="button" onclick="copyToClipboard()" style="font-size: 12px" class="btn-copy text-center mb-1">Copy</button>
                    <input class="w-100 input-referal" id="input-referal" type="text" name="" style="background-color: transparent; color:white" value="{{ route('referal', ['usr' => $username]) }}">
                  </div>
                    <a href="{{ route('referal', ['usr' => $username]) }}" class="carousel-caption d-none d-lg-block d-xl-block btn btn-copy text-white font-weight-bold mb-4" style="text-decoration: none;">
                        <span class="h6">QR CODE</span>
                    </a>
                </div>
                @else
                <p class="text-center text-danger">Sorry qrcode cannot generate in sub account</p>
                @endif
            </div>
        </div>
@endsection
@section('script')
<script>
  function copyClipboard(element){
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
  }

    function copyToClipboard() {
      var copyText = document.getElementById("input-referal");

      /* Select the text field */
      copyText.select();
      //$('#affiliate').select();
    	  document.execCommand('copy');
    	  iosCopyToClipboard(copyText)
    	alert('copy to clipboard');

    	}

    function iosCopyToClipboard(el) {
        var oldContentEditable = el.contentEditable,
            oldReadOnly = el.readOnly,
            range = document.createRange();

        el.contentEditable = true;
        el.readOnly = false;
        range.selectNodeContents(el);

        var s = window.getSelection();
        s.removeAllRanges();
        s.addRange(range);

        el.setSelectionRange(0, 999999); // A big number, to cover anything that could be inside the element.

        el.contentEditable = oldContentEditable;
        el.readOnly = oldReadOnly;

        document.execCommand('copy');
    }
</script>
@endsection
