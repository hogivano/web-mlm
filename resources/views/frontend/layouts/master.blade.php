!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="AXDoXw9Rn8fGHinKTrGDo5KYkyN0uY022XTxJrtE" />
    <meta content=" " name="description" />
    <meta content=" " name="author" />
    <!--begin::Fonts -->
    <link rel="apple-touch-icon" href="">
    <link rel="shortcut icon" href="">
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="<?=asset('css/user_coreceff.css?id=468f82d64786fe1f5902');?>">
    <style>
        html > body {
            background-image: url('{{ asset("img/bg2.jpg") }}');
            background-position: center center;
            background-attachment: fixed;
            background-repeat: no-repeat;
            background-size: cover;
            min-height: 100vh;
            font-family: 'Roboto', sans-serif;
        }
        .loader {
            position: relative;
            width: 100vw;
            height: 100vh;
            background: #000;
        }
        .loader > div {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -116px;
            margin-left: -75px;
        }
        .loader > div > img {
            width: 150px;
            clear: both;
        }
        .loader > img.logo {
            width: 100%;
            position: absolute;
            margin: 100px auto 0 auto;
            text-align: center;
            max-width: var(--max-width);
            transform: translate(-50%, 0%);
        }
        .loader > img.characters {
            width: 100%;
            max-width: var(--max-width);
            position: absolute;
            margin: 0 auto;
            text-align: center;
            bottom: 0;
            transform: translate(-50%, 0%);
        }
    </style>
    <script src="manup.min.js" type="text/javascript"></script>
    @yield('style')
</head>
@yield('content')
@yield('script')
