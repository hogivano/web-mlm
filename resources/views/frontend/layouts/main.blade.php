<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Membership - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content=" " name="description" />
    <meta content=" " name="author" />
    <link rel="stylesheet" href="<?=asset('css/user_coreceff.css?id=468f82d64786fe1f5902');?>">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet" id="owl-carousel-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" rel="stylesheet" id="owl-carousel-theme-css">
    <link href="https://fonts.googleapis.com/css2?family=Audiowide&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/18.1.3/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/18.1.3/css/dx.light.css" />
    <script type="text/javascript" src="https://cdn3.devexpress.com/jslib/18.1.3/js/dx.all.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link href="{{ asset('css/style-new.css') }}" rel="stylesheet" id="responsive-css">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" id="responsive-css">
    <style>
        #floating-btn {
            z-index: 9;
            width: 70px;
            height: 70px;
            border-radius: 50%;
            position: fixed;
            border:2px solid white;
            box-shadow: 2px 2px 20px white;
            background-color: transparent !important;
            right: 20px;
            bottom: 20px;
            padding: 3px 10px;
        }
        #floating-btn i {
            font-size: 30px;
            color: white;
        }
        #floating-btn:hover{
            cursor: pointer;
        }

        .menu-floating {
            position: fixed; bottom: 90px; right: 30px; z-index: 9
        }

        .menu-floating .menu-item {
            margin: 10px 0;
            height: 50px;
            width: 50px;
            border-radius: 50%;
            border:2px solid white;
            box-shadow: 2px 2px 20px white;
            background-color: transparent !important;
            color: white;
        }

        .menu-floating .menu-item i {
            font-size: 30px;
        }
    </style>
    @yield('link')
</head>
<body>
    <!-- <div class="loader">
        <img src="img/logoav1.png" class="logo">
        <div>
            <br>
            <img src="img/loading.gif">
        </div>
        <img src="img/char.png" class="characters">
    </div> -->
    <!-- Floating Action Button like Google Material -->
    @if(!strpos(request()->path(), 'home'))
    <div id="floating-btn" class="d-flex justify-content-center align-items-center">
            <i class="fas fa-dna mx-auto"></i>
    </div>
    <div class="menu-floating" style="display: none">
        <div class="menu-item d-flex justify-content-center align-items-center" style="">
          <a href="{{ route('membership.home') }}" class="text-white">
            <i class="fab fa-asymmetrik mx-auto"></i>
          </a>
        </div>
        <div class="menu-item d-flex justify-content-center align-items-center" style="">
          <a href="{{ route('membership.hire') }}" class="text-white">
            <i class="fas fa-space-shuttle mx-auto"></i>
          </a>
        </div>
        <div class="menu-item d-flex justify-content-center align-items-center" style="">
          <a href="{{ route('membership.profile') }}" class="text-white">
            <i class="fas fa-user-astronaut mx-auto"></i>
          </a>
        </div>
    </div>
    <div class="main-container m-3" style="margin-bottom:250px !important;">
        <h3 class="text-center text-white p-2 card-green">@yield('title-content')</h3>
        <div class="d-flex justify-content-between px-1">
            <button class="btn text-white" onclick="window.history.back();" style="box-shadow: inset 0 0 25px #ff0800;color:#848e96; font-size: 20px !important;"><i class="fas fa-chevron-left"></i></button>
            <button class="btn text-white" id="menuProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="box-shadow: inset 0 0 25px #ff0800;color:#848e96; border-radius: 50%; font-size: 20px !important;"><i class="fa fa-user" aria-hidden="true"></i></button>
            <div class="dropdown-menu" style="box-shadow: inset 0 0 25px #ff0800;color:white; border-radius: 20px; min-width: 20px !important; " aria-labelledby="menuProfile">
              <a class="dropdown-item text-white text-center" href="{{ route('membership.logout') }}">Logout</a>
            </div>
        </div>
        @yield('container')
    </div>
    @else
    @yield('container')
    @endif
    <script>
    </script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('dist/mfb.js') }}"></script>
    <script src="{{ asset('js/velocity.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#floating-btn').on('click', function(){
              var element = $('.menu-floating');
              if (element.is(':visible')) {
                element.velocity({
                  opacity: 0,
                  scale: 0
                }, {
                  display: 'none'
                }, {
                  duration: 100
                },'slideDown');
              } else {
                element.velocity({
                  opacity: 1,
                  scale: 1
                }, {
                  display: 'block'
                }, {
                  duration: 300
                },'slideUp');
              }
            });
        });
    </script>
@yield('script')
</body>
</html>
