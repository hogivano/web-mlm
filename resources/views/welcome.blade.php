<div class="section-block">
    <div class="container">
        <div class="section-heading center-holder">
            <h3 class="bold"> <span class="italic libre-baskerville primary-color">Berita Terbaru</span>
            </h3>
            <div class="section-heading-line"></div>
            {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <br>incididunt ut--}}
            {{--labore et dolore magna aliqua.</p>--}}
        </div>
        <div class="row mt-30">
            @if($new!=null)
                <div class="col-md-6 col-sm-12 col-12">
                    <div class="blog-news">
                        <div class="blog-news-img"><img src="{{url("storage/".$new->image)}}" alt="img"></div>
                        <div class="blog-news-text">
                            <h4><a href="{{url("detail/".$new->slug)}}">{{$new->title}}</a></h4>
                            <p><?= get_snippet($new->excerpt,20) ?> ...</p>
                            <div class="blog-news-comment">
                                <p>Posted </p><span>{{date("d M Y",strtotime($new->created_at))}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="col-md-6 col-sm-12 col-12">
                @if($news!=null)
                    @foreach($news as $n)
                        <div class="blog-news">
                            <div class="row">
                                <div class="col-md-5 col-sm-5 col-12">
                                    <div class="blog-news-left-img"><img src="{{url("storage/".$n->image)}}" alt="img">
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-7 col-12">
                                    <div class="blog-news-left-text">
                                        <h4><a href="{{url("detail/".$n->slug)}}">{{$n->title}}</a></h4>
                                        <p><?= get_snippet($n->excerpt,9) ?> ...</p>
                                        <div class="blog-news-comment">
                                            <p>Posted </p><span>{{date("d M Y",strtotime($n->created_at))}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

        </div>
    </div>
</div>