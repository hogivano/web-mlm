@extends('admin.layouts.app', ['activePage' => 'member', 'titlePage' => __('Member List')])
@section('title', 'List Member')
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="message mt-2">
      @include('flash::message')
    </div>
    <div class="d-flex mt-2">
      <a href="#">
          <span class="material-icons">
            add
          </span>
          New Member
      </a>
    </div>
    <div class="row mt-2">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">New Member</h4>
            <p class="card-category"> Need action for new member</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID
                  </th>
                  <th>
                    Username
                  </th>
                  <th>
                    Email
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @foreach($newMember as $i)
                  <tr>
                    <td>
                      {{ $i->id }}
                    </td>
                    <td>
                      {{ $i->username }}
                    </td>
                    <td>
                      {{ $i->email }}
                    </td>
                    <td>
                      {{ $i->status }}
                    </td>
                    <td class="d-flex">
                      <form method="post" class="d-inline" action="{{ route('admin.member.set_status') }}">
                        {{ csrf_field() }}
                        <input type="text" hidden name="id" value="{{ $i->id }}">
                        <input type="text" hidden name="status" value="active">
                        <button type="submit" onclick="return confirm('Are you sure you active this member?');" class="mr-2 text-success btn-link btn p-0">
                          <span class="material-icons">
                            check
                          </span>
                        </button>
                      </form>
                      <a href="#" class="mr-2">
                        <span class="material-icons">
                          info
                        </span>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              {!! $newMember !!}
            </div>
          </div>
        </div>
      </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">All Member</h4>
              <p class="card-category"> Here is all member registered</p>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      ID
                    </th>
                    <th>
                      Name
                    </th>
                    <th>
                      Username
                    </th>
                    <th>
                      Email
                    </th>
                    <th>
                      Status
                    </th>
                    <th>
                      Action
                    </th>
                  </thead>
                  <tbody>
                    @foreach($data as $i)
                    <tr>
                      <td>
                        {{ $i->id }}
                      </td>
                      <td>
                        @if($i->memberData)
                        {{ $i->memberData->full_name }}
                        @else
                        Not Set
                        @endif
                      </td>
                      <td>
                        {{ $i->username }}
                      </td>
                      <td>
                        {{ $i->email }}
                      </td>
                      <td>
                        @if($i->memberData)
                        {{ $i->status }}
                        @else
                        <strong class="text-warning">Not Set Member Data</strong>
                        @endif
                      </td>
                      <td class="d-flex">
                        <button href="#" class="mr-2 btn btn-link text-primary p-0">
                            status
                        </button>
                        <a href="#" class="mr-2 text-info">
                          <span class="material-icons">
                            edit
                          </span>
                        </a>
                        <a href="#" class="mr-2 text-secondary">
                          <span class="material-icons">
                            info
                          </span>
                        </a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $data !!}
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
