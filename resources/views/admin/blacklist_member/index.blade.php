@extends('admin.layouts.app', ['activePage' => 'blacklist_member', 'titlePage' => __('Blacklist Member')])
@section('title', 'Blacklist Member')
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="message mt-2">
      @include('flash::message')
    </div>
    <div class="d-flex mt-2">
      <a href="{{ route('admin.blacklist_member.new') }}">
          <span class="material-icons">
            add
          </span>
          New Member Blacklist
      </a>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Blacklist Member</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      ID
                    </th>
                    <th>
                      Member
                    </th>
                    <th>
                      Blacklist
                    </th>
                    <th>
                      Status
                    </th>
                    <th>
                      Date Created
                    </th>
                    <th>
                      Action
                    </th>
                  </thead>
                  <tbody>
                    @foreach($data as $i)
                    <tr>
                      <td>
                        {{ $i->id }}
                      </td>
                      <td>
                        @if($i->member)
                        {{ $i->member->username }}
                        @else
                        <span class="text-danger">Not Found</span>
                        @endif
                      </td>
                      <td>
                        @if($i->blacklistStatus)
                        <span style="color: <?php echo $i->blacklistStatus->color; ?>">
                          {{ $i->blacklistStatus->name }}
                        </span>
                        @else
                        <span class="text-danger">Not Found</span>
                        @endif
                      </td>
                      <td>
                        @if($i->status == 'on')
                        <span class="text-success">Active</span>
                        @else
                        <span class="text-danger">Non-Active</span>
                        @endif
                      </td>
                      <td>
                        {{ date('d-m-Y H:i', strtotime($i->created_at)) }}
                      </td>
                      <td class="d-flex">
                        <button type="button" data-toggle="modal" data-target="#modalStatus" class="btn btn-link p-0 btn-primary" name="button">
                          Status
                        </button>
                        <div class="modal fade" id="modalStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Change Active or Non Active</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form id="form-status" class="" action="{{ route('admin.blacklist_member.change_status') }}" method="post">
                                  {{ csrf_field() }}
                                  <input type="text" name="id" value="{{ $i->id }}" hidden>
                                  <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status" required>
                                      <option value="">Select status</option>
                                      <option value="on">Active</option>
                                      <option value="off">Non Active</option>
                                    </select>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" form="form-status" class="btn btn-primary">Save</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <a href="#" class="mr-2 text-danger mx-2">
                          <span class="material-icons">
                            delete
                          </span>
                        </a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
