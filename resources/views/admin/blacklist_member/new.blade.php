@extends('admin.layouts.app', ['activePage' => 'blacklist_member', 'titlePage' => __('New Blacklist Member')])
@section('title')
New Blacklist Member
@endsection
@section('link')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="message mt-2">
        @include('flash::message')
      </div>
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('admin.blacklist_member.create') }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('New Blacklist Member') }}</h4>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="list-input mt-3">
                  <div class="form-group{{ $errors->has('member_id') ? ' has-danger' : '' }}">
                    <label>Member</label>
                    <select class="member-select form-control{{ $errors->has('member_id') ? ' is-invalid' : '' }}" name="member_id" id="input-member_id" required="true" aria-required="true">
                      <option value="">Select member</option>
                      @foreach($member as $i)
                      <option value="{{ $i->id }}">{{ $i->username }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('name'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('blacklist_status_id') ? ' has-danger' : '' }}">
                    <label>Status Blacklist</label>
                    <select class="form-control{{ $errors->has('blacklist_status_id') ? ' is-invalid' : '' }}" name="blacklist_status_id" id="input-blacklist_status_id" required="true" aria-required="true">
                      <option value="">Select status</option>
                      @foreach($blacklistStatus as $i)
                      <option value="{{ $i->id }}">{{ $i->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group{{ $errors->has('message') ? ' has-danger' : '' }}">
                    <label>Messagess</label>
                    <input class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" id="input-message" type="text" value="{{ old('message') }}" placeholder="custom message for member" aria-required="true"/>
                    @if ($errors->has('message'))
                      <span id="message-error" class="error text-danger" for="input-message">{{ $errors->first('message') }}</span>
                    @endif
                  </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
$(document).ready(function() {
  $('.member-select').select2();
});
</script>
@endsection
