<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="{{ url('/') }}" class="simple-text logo-normal">
      {{ __('MLM') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('admin.dashboard') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'member' || $activePage == 'member' || $activePage == 'blacklist_member') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#member" aria-expanded="false">
          <i>
            <span class="material-icons">group</span>
          </i>
          <p>{{ __('Members') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse fade" id="member">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'member' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('admin.member') }}">
                <span class="sidebar-mini material-icons" style="text-transform: none">person</span>
                <span class="sidebar-normal">{{ __('List Member') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'network' ? ' active' : '' }}">
              <a class="nav-link" href="">
                <span class="sidebar-mini material-icons" style="text-transform: none">mediation</span>
                <span class="sidebar-normal"> {{ __('Network') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'blacklist_member' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('admin.blacklist_member') }}">
                <span class="sidebar-mini material-icons" style="text-transform: none">person_add_disabled</span>
                <span class="sidebar-normal"> {{ __('Blacklist Member') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item{{ $activePage == 'character' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('admin.character.') }}">
          <i class="material-icons">storefront</i>
            <p>{{ __('Character') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'soul' || $activePage == 'reality' || $activePage == 'power') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#wallet" aria-expanded="false">
          <i>
            <span class="material-icons">payments</span>
          </i>
          <p>{{ __('Wallet') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse fade" id="wallet">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'member' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('admin.member') }}">
                <span class="sidebar-mini material-icons" style="text-transform: none">person</span>
                <span class="sidebar-normal">{{ __('Reality') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'soul' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('admin.soul') }}">
                <span class="sidebar-mini material-icons" style="text-transform: none">mediation</span>
                <span class="sidebar-normal"> {{ __('Soul') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'network' ? ' active' : '' }}">
              <a class="nav-link" href="">
                <span class="sidebar-mini material-icons" style="text-transform: none">mediation</span>
                <span class="sidebar-normal"> {{ __('Power') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item {{ $activePage == 'bank company' || $activePage == 'bank' || $activePage == 'country' || $activePage == 'blacklist_status' || $activePage == 'setting' ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#setting" aria-expanded="false">
          <i>
            <span class="material-icons">build</span>
          </i>
          <p>{{ __('Setting') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse fade" id="setting">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'bank' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('admin.bank') }}">
                <span class="sidebar-mini material-icons" style="text-transform: none">account_balance</span>
                <span class="sidebar-normal">{{ __('Bank Name') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'bank company' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('admin.bank_company') }}">
                <span class="sidebar-mini material-icons" style="text-transform: none">account_balance_wallet</span>
                <span class="sidebar-normal">{{ __('Bank Company') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'country' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('admin.country') }}">
                <span class="sidebar-mini material-icons" style="text-transform: none">flag</span>
                <span class="sidebar-normal">{{ __('Country') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'blacklist_status' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('admin.blacklist_status') }}">
                <span class="sidebar-mini material-icons" style="text-transform: none">block</span>
                <span class="sidebar-normal">{{ __('Blacklist Status') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'setting' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('admin.setting') }}">
                <span class="sidebar-mini material-icons" style="text-transform: none">build</span>
                <span class="sidebar-normal">{{ __('Setting') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>
