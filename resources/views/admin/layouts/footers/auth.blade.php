<footer class="footer">
  <div class="container-fluid">
    <nav class="float-left">
      <ul>
        <li>
          <a href="https://hogivano.web.id">
              {{ __('hogivano') }}
          </a>
        </li>
      </ul>
    </nav>
  </div>
</footer>
