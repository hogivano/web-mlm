@extends('admin.layouts.app', ['activePage' => 'country', 'titlePage' => __('New Country')])
@section('title')
New Country
@endsection
@section('link')
<script src="https://cdn.tiny.cloud/1/0vbceq09oc6btb6lq7hoh4iannz8s2zovz3qk29lfvmz6jua/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endsection
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('admin.country.create') }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('New Country') }}</h4>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="list-input mt-3">
                  <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <label>Name</label>
                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" value="{{ old('name') }}" required="true" aria-required="true"/>
                    @if ($errors->has('name'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('value') ? ' has-danger' : '' }}">
                    <label>Value/Description</label>
                    <textarea>
                    </textarea>
                    @if ($errors->has('value'))
                      <span id="value-error" class="error text-danger" for="input-value">{{ $errors->first('value') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('time_zone') ? ' has-danger' : '' }}">
                    <label>Time Zone</label>
                    <input class="form-control{{ $errors->has('time_zone') ? ' is-invalid' : '' }}" name="time_zone" id="input-time_zone" placeholder="ex: UTC+7" type="text" value="{{ old('time_zone') }}" aria-required="true"/>
                    @if ($errors->has('time_zone'))
                      <span id="time_zone-error" class="error text-danger" for="input-time_zone">{{ $errors->first('time_zone') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('code_number') ? ' has-danger' : '' }}">
                    <label>Code Number</label>
                    <input class="form-control{{ $errors->has('code_number') ? ' is-invalid' : '' }}" name="code_number" id="input-code_number" placeholder="ex:62" type="number" value="{{ old('code_number') }}" required="true" aria-required="true"/>
                    <small>Just input number don't use +</small>
                    @if ($errors->has('code_number'))
                      <span id="code_number-error" class="error text-danger" for="input-code_number">{{ $errors->first('code_number') }}</span>
                    @endif
                  </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
</script>
@endsection
