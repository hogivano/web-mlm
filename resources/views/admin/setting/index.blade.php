@extends('admin.layouts.app', ['activePage' => 'setting', 'titlePage' => __('Setting App')])
@section('title', 'Setting App')
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="message mt-2">
      @include('flash::message')
    </div>
    <div class="row mt-2">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">All Setting</h4>
            <p class="card-category"> Here is all setting in apps</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    Key
                  </th>
                  <th>
                    Display Name
                  </th>
                  <th>
                    Value/Description
                  </th>
                  <th>
                    Details
                  </th>
                  <th>
                    Type
                  </th>
                  <th>
                    Order
                  </th>
                  <th>
                    Group
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @foreach($data as $i)
                  <tr>
                    <td>
                      {{ $i->key }}
                    </td>
                    <td>
                      {{ $i->display_name }}
                    </td>
                    <td>
                      {!! $i->value !!}
                    </td>
                    <td>
                      {{ $i->details }}
                    </td>
                    <td>
                      {{ $i->type }}
                    </td>
                    <td>
                      {{ $i->order }}
                    </td>
                    <td>
                      {{ $i->group }}
                    </td>
                    <td class="d-flex">
                      <a href="{{ route('admin.setting.edit', $i->id) }}" class="mr-2 text-info">
                        <span class="material-icons">
                          edit
                        </span>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
