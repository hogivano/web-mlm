@extends('admin.layouts.app', ['activePage' => 'setting', 'titlePage' => __('Edit Setting')])
@section('title')
Edit Setting
@endsection
@section('link')
<script src="https://cdn.tiny.cloud/1/0vbceq09oc6btb6lq7hoh4iannz8s2zovz3qk29lfvmz6jua/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endsection
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('admin.setting.update', $data->id) }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Edit {{ $data->display_name }}</h4>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="list-input mt-3">
                  <div class="form-group{{ $errors->has('value') ? ' has-danger' : '' }}">
                    <label>Value/Description</label>
                    <textarea name="value">
                      {!! old('value', $data->value) !!}
                    </textarea>
                    @if ($errors->has('value'))
                      <span id="value-error" class="error text-danger" for="input-value">{{ $errors->first('value') }}</span>
                    @endif
                  </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
</script>
@endsection
