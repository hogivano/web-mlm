@extends('admin.layouts.app', ['activePage' => 'blacklist_status', 'titlePage' => __('Blacklist Status')])
@section('title', 'Blacklist Status')
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="message mt-2">
      @include('flash::message')
    </div>
    <div class="d-flex mt-2">
      <a href="{{ route('admin.blacklist_status.new') }}">
          <span class="material-icons">
            add
          </span>
          New Blacklist
      </a>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Blacklist Status</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      ID
                    </th>
                    <th>
                      Name
                    </th>
                    <th>
                      Color
                    </th>
                    <th>
                      Total Blacklist
                    </th>
                    <th>
                      Total Unblacklist
                    </th>
                    <th>
                      Action
                    </th>
                  </thead>
                  <tbody>
                    @foreach($data as $i)
                    <tr>
                      <td>
                        {{ $i->id }}
                      </td>
                      <td>
                        {{ $i->name }}
                      </td>
                      <td>
                        {{ $i->color }}
                      </td>
                      <td class="d-flex">
                        <a href="{{ route('admin.blacklist_status.edit', $i->id) }}" class="mr-2 text-info">
                          <span class="material-icons">
                            edit
                          </span>
                        </a>
                        <form class="d-inline" onsubmit="return confirm('Do you really want to delete the status?');" action="{{ route('admin.blacklist_status.delete') }}" method="post">
                          {{ csrf_field() }}
                          <input type="text" name="id" value="{{ $i->id }}" hidden>
                          <button type="submit" name="button" class="btn-link btn btn-danger p-0">
                            <span class="material-icons">
                              delete
                            </span>
                          </button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
