@extends('admin.layouts.app', ['activePage' => 'blacklist_status', 'titlePage' => __('New Blacklist Status')])
@section('title')
New Blacklist Status
@endsection
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="message mt-2">
        @include('flash::message')
      </div>
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('admin.blacklist_status.create') }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('New Status') }}</h4>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="list-input mt-3">
                  <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <label>Name</label>
                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" value="{{ old('name') }}" placeholder="ex: blacklist 1" required="true" aria-required="true"/>
                    @if ($errors->has('name'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('color') ? ' has-danger' : '' }}">
                    <label>Color</label>
                    <input class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }}" name="color" id="input-color" placeholder="ex: #ffffff" type="text" value="{{ old('color') }}" required="true" aria-required="true"/>
                    <small>Format color in Hex (#xxxxxx)</small>
                    @if ($errors->has('color'))
                      <span id="color-error" class="error text-danger" for="input-color">{{ $errors->first('color') }}</span>
                    @endif
                  </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
