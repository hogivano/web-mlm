@extends('admin.layouts.app', ['activePage' => 'bank company', 'titlePage' => __('List Bank Company')])
@section('title', 'List Bank Company')
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="message mt-2">
      @include('flash::message')
    </div>
    <div class="d-flex mt-2">
      <a href="{{ route('admin.bank_company.new') }}">
          <span class="material-icons">
            add
          </span>
          New Bank Company
      </a>
    </div>
    <div class="row mt-2">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">All Bank Company</h4>
            <p class="card-category"> Here is bank company for transaction member</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    Bank
                  </th>
                  <th>
                    Bank Account
                  </th>
                  <th>
                    Bank Number
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @foreach($data as $i)
                  <tr>
                    <td>
                      @if($i->bank)
                      {{ strtoupper($i->bank->name) }}
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    </td>
                    <td>
                      {{ $i->bank_account }}
                    </td>
                    <td>
                      {{ $i->bank_number }}
                    </td>
                    <td class="d-flex">
                      <a href="{{ route('admin.bank_company.edit', $i->id) }}" class="mr-2 text-info">
                        <span class="material-icons">
                          edit
                        </span>
                      </a>
                      <form method="post" class="d-inline" action="{{ route('admin.bank_company.delete') }}">
                        {{ csrf_field() }}
                        <input type="text" hidden name="id" value="{{ $i->id }}">
                        <input type="text" hidden name="status" value="active">
                        <button type="submit" onclick="return confirm('Are you sure you delete it?');" class="mr-2 text-danger btn-link btn p-0">
                          <span class="material-icons">
                            delete
                          </span>
                        </button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
