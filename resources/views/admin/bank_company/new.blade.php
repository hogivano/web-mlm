@extends('admin.layouts.app', ['activePage' => 'bank company', 'titlePage' => __('New Bank Company')])
@section('title')
New Bank Company
@endsection
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('admin.bank_company.create') }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('New Bank Company') }}</h4>
                <p class="card-category">{{ __('Data information') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="list-input mt-3">
                  <div class="form-group{{ $errors->has('bank_id') ? ' has-danger' : '' }}">
                    <label>Bank Name</label>
                    <select class="form-control" required name="bank_id">
                      <option value="">select bank name</option>
                      @foreach($bank as $i)
                      <option value="{{ $i->id }}" <?php if(old('bank_id') == $i->id) echo ' selected'; ?>>{{ strtoupper($i->name) }}</option>
                      @endforeach
                    </select>
                    <small>If you want add new bank name. <a href="{{ route('admin.bank.new') }}">Click This</a></small>
                    @if ($errors->has('bank_id'))
                      <span id="bank_id-error" class="error text-danger" for="input-bank_id">{{ $errors->first('bank_id') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('bank_account') ? ' has-danger' : '' }}">
                    <label>Bank Holder Name</label>
                    <input class="form-control{{ $errors->has('bank_account') ? ' is-invalid' : '' }}" required name="bank_account" id="input-bank_account" type="text" value="{{ old('bank_account') }}" aria-required="true"/>
                    @if ($errors->has('bank_account'))
                      <span id="bank_account-error" class="error text-danger" for="input-bank_account">{{ $errors->first('bank_account') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('bank_number') ? ' has-danger' : '' }}">
                    <label>Bank Number</label>
                    <input class="form-control{{ $errors->has('bank_number') ? ' is-invalid' : '' }}" required name="bank_number" id="input-bank_number" type="number" value="{{ old('bank_number') }}" aria-required="true"/>
                    @if ($errors->has('bank_number'))
                      <span id="bank_number-error" class="error text-danger" for="input-bank_number">{{ $errors->first('bank_number') }}</span>
                    @endif
                  </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
