@extends('admin.layouts.app', ['activePage' => 'character', 'titlePage' => __('Detail Characters Transaction')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('admin.character.create') }}" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Detail Transaction') }}</h4>
                <p class="card-category">{{ __('Data information') }}</p>
              </div>
              <div class="card-body">
                <div class="row" style="border-bottom: 1px solid #c7c7c7">
                  <div class="col-sm-12 col-md-6">
                    <h4>
                      <b>Information Buyer</b>
                    </h4>
                    <div class="form-group">
                      <label>Buyer</label>
                      <p>
                        @if($data->charactersOrder)
                          @if($data->charactersOrder->member)
                          <a href="{{ route('admin.member.show', $data->charactersOrder->member->id) }}">
                            {{ $data->charactersOrder->member->username }}
                          </a>
                          @else
                          <span class="text-danger">Buyer Not Found</span>
                          @endif
                        @else
                        <span class="text-danger">In Sales</span>
                        @endif
                      </p>
                    </div>
                    <div class="form-group">
                      <label>Date Order Buyer</label>
                      <p>
                        @if($data->charactersOrder)
                          {{ date('d-m-Y H:i:s', strtotime($data->charactersOrder->date_order)) }}
                        @else
                        <span class="text-danger">In Sales</span>
                        @endif
                      </p>
                    </div>
                    <div class="form-group">
                      <label>Prediction Price For Buyer</label>
                      <p>
                        @if($data->prediction_price_for_buyer)
                          {{ $data->prediction_price_for_buyer }}
                        @else
                        <span class="text-danger">Not Set</span>
                        @endif
                      </p>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <h4>
                      <b>Information Seller</b>
                    </h4>
                    <div class="form-group">
                      <label>Seller</label>
                      <p>
                        @if($data->charactersSeller)
                          @if($data->charactersSeller->member)
                          <a href="{{ route('admin.member.show', $data->charactersSeller->member->id) }}">
                            {{ $data->charactersSeller->member->username }}
                          </a>
                          @else
                          <span class="text-danger">Seller Not Found</span>
                          @endif
                        @elseif($data->characters_seller_id == null || $data->characters_seller_id == 0)
                          <span class="text-danger">Not Set, because from Company</span>
                        @else
                          <span class="text-danger">Not Found</span>
                        @endif
                      </p>
                    </div>
                    <div class="form-group">
                      <label>Date of Sale</label>
                      <p>
                        @if($data->charactersSeller)
                          {{ date('d-m-Y H:i:s', strtotime($data->charactersSeller->start_of_sale)) . ' - ' . date('d-m-Y H:i:s', strtotime($data->charactersSeller->end_of_sale)) }}
                        @elseif($data->characters_seller_id == null || $data->characters_seller_id == 0)
                        <span class="text-danger">Not Set, because from Company</span>
                        @else
                        <span class="text-danger">Not Found</span>
                        @endif
                      </p>
                    </div>
                    <div class="form-group">
                      <label>Sold Date</label>
                      <p>
                        @if($data->charactersSeller)
                          @if($data->charactersSeller)
                          {{ date('d-m-Y H:i:s', strtotime($data->charactersSeller->date_of_sale)) }}
                          @else
                          <span class="text-warning">In Sales</span>
                          @endif
                        @elseif($data->characters_seller_id == null || $data->characters_seller_id == 0)
                        <span class="text-danger">Not Set, because from Company</span>
                        @else
                        <span class="text-danger">Not Found</span>
                        @endif
                      </p>
                    </div>
                    <div class="form-group">
                      <label>Capital Seller</label>
                      <p>
                        @if($data->charactersSeller)
                          {{ number_format($data->charactersSeller->price) }}
                        @elseif($data->characters_seller_id == null || $data->characters_seller_id == 0)
                          <span class="text-danger">Not Set, because from Company</span>
                        @else
                          <span class="text-danger">Not Found</span>
                        @endif
                      </p>
                    </div>
                    <div class="form-group">
                      <label>Profit For Seller</label>
                      <p>
                        @if($data->profit)
                        {{ number_format($data->profit) }}
                        @else
                        <span class="text-danger">Not Set</span>
                        @endif
                      </p>
                    </div>
                    <div class="form-group">
                      <label>Price of Sale Seller</label>
                      <p>
                        @if($data->price_of_sale)
                        {{ number_format($data->price_of_sale) }}
                        @else
                        <span class="text-danger">Not Set</span>
                        @endif
                      </p>
                    </div>
                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-12">
                    <h4>
                      <b>Information Transaction</b>
                    </h4>
                    <div class="form-group">
                      <label>Start of Payment</label>
                      <p>
                        @if($data->start_of_payment)
                        {{ date('d-m-Y H:i:s', strtotime($data->start_of_payment)) }}
                        @else
                        <span class="text-danger">Not Set</span>
                        @endif
                      </p>
                    </div>
                    <div class="form-group">
                      <label>End of Payment</label>
                      <p>
                        @if($data->end_of_payment)
                        {{ date('d-m-Y H:i:s', strtotime($data->end_of_payment)) }}
                        @else
                        <span class="text-danger">Not Set</span>
                        @endif
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <a href="{{ route('admin.character.transaction.edit', $data->id) }}" class="btn btn-primary">{{ __('Edit Transaction') }}</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
