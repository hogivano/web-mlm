@extends('admin.layouts.app', ['activePage' => 'character', 'titlePage' => __('Character List')])
@section('title', 'List Character')
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="message" style="margin-top: 20px">
      @include('flash::message')
    </div>
    <div class="d-flex">
      <a href="{{ route('admin.character.new') }}">
          <span class="material-icons">
            add
          </span>
          New Character
      </a>
    </div>
    <div class="row mt-2">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Character Table</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    Sequence
                  </th>
                  <th>
                    Title
                  </th>
                  <th>
                    Profit
                  </th>
                  <th>
                    Price
                  </th>
                  <th>
                    Soul
                  </th>
                  <th>
                    Time Order
                  </th>
                  <th>
                    Time
                  </th>
                  <th>
                    Time Of Sale
                  </th>
                  <th>
                    Is Sequence
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @for($i = 0; $i < sizeof($data); $i++)
                  <tr>
                    <td>
                      {{ $i + 1 }}
                    </td>
                    <td>
                      {{ $data[$i]->name }}
                    </td>
                    <td>
                      {{ $data[$i]->profit_for }}% - {{ $data[$i]->profit_to }}%
                    </td>
                    <td>
                      {{ $data[$i]->price_for }} - {{ $data[$i]->price_to }}
                    </td>
                    <td>
                      {{ $data[$i]->soul }}
                    </td>
                    <td>
                      {{ date('H:i', strtotime($data[$i]->start_order_time)) }} - {{ date('H:i', strtotime($data[$i]->end_order_time)) }}
                    </td>
                    <td>
                      {{ date('H:i', strtotime($data[$i]->time)) }}
                    </td>
                    <td>
                      @if ( $data[$i]->length_of_sale == null)
                      not set
                      @else
                      {{ $data[$i]->length_of_sale }} day
                      @endif
                    </td>
                    <td>
                      @if($data[$i]->is_sequence == 1)
                      Yes
                      @else
                      No
                      @endif
                    </td>
                    <td>
                      {{ $data[$i]->status }}
                    </td>
                    <td class="d-flex">
                      <a href="{{ route('admin.character.show', $data[$i]->character_id) }}" class="mr-2">
                        <span class="material-icons">
                          info
                        </span>
                      </a>
                      <a href="{{ route('admin.character.edit', $data[$i]->character_id) }}" class="mr-2">
                        <span class="material-icons text-info">
                          edit
                        </span>
                      </a>
                      <form method="post" class="d-inline" action="{{ route('admin.character.delete') }}">
                        {{ csrf_field() }}
                        <input type="text" hidden name="id" value="{{ $data[$i]->character_id }}">
                        <button type="submit" onclick="return confirm('Are you sure you delete this character?');" class="mr-2 text-danger btn-link btn p-0">
                          <span class="material-icons">
                            delete
                          </span>
                        </button>
                      </form>
                    </td>
                  </tr>
                  @endfor
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Recent Booking Order</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID Transaction
                  </th>
                  <th>
                    Character
                  </th>
                  <th>
                    Member
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Date Order
                  </th>
                </thead>
                <tbody>
                  @foreach($charactersOrder as $i)
                  <tr>
                    <td>
                      {{ $i->id }}
                    </td>
                    <td>
                      @if($i->character)
                      {{ $i->character->name }}
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    </td>
                    <td>
                      {{ $i->member->username }}
                    </td>
                    <td>
                      {{ $i->status }}
                    </td>
                    <td>
                      {{ $i->date_order }}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Seller Character</h4>
            <small>Just information list character from the seller on ready to sale</small>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID Seller
                  </th>
                  <th>
                    Character
                  </th>
                  <th>
                    Member
                  </th>
                  <th>
                    Date of Sale
                  </th>
                  <th>
                    Price
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @foreach($charactersSeller as $i)
                  <tr>
                    <td>
                      {{ $i->id }}
                    </td>
                    <td>
                      @if($i->character)
                      {{ $i->character->name }}
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    </td>
                    <td>
                      @if($i->member)
                      {{ $i->member->username }}
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    </td>
                    <td>
                      {{ $i->start_of_sale . ' - ' . $i->end_of_sale }}
                    </td>
                    <td>
                      {{ $i->price }}
                    </td>
                    <td>
                      {{ $i->status }}
                    </td>
                    <td>
                      <a href="#">
                        Split
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Histories Transaction</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID Transaction
                  </th>
                  <th>
                    Character
                  </th>
                  <th>
                    Buyer
                  </th>
                  <th>
                    Seller
                  </th>
                  <th>
                    Soul
                  </th>
                  <th>
                    Price
                  </th>
                  <th>
                    Profit
                  </th>
                  <th>
                    Proof of Payment
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Date
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @foreach($charactersTransactions as $i)
                  <tr>
                    <td>
                      {{ $i->id }}
                    </td>
                    <td>
                      @if($i->character)
                      {{ $i->character->name }}
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    </td>
                    <td>
                      @if($i->charactersOrder)
                        @if($i->charactersOrder->member)
                        {{ $i->charactersOrder->member->username }}
                        @else
                        <span class="text-danger">Username Not Found</span>
                        @endif
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    </td>
                    <td>
                      @if($i->charactersSeller)
                        @if($i->charactersSeller->member)
                        {{ $i->charactersSeller->member->username }}
                        @else
                        <span class="text-danger">Seller Not Found</span>
                        @endif
                      @else
                        @if($i->characters_seller_id == 0)
                        <span class="text-primary">Company</span>
                        @else
                        <span class="text-danger">Not Found</span>
                        @endif
                      @endif
                    </td>
                    <td>
                      {{ $i->soul }}
                    </td>
                    <td>
                      {{ $i->price_of_sale }}
                    </td>
                    <td>
                      {{ $i->profit }}
                    </td>
                    <td>
                      @if($i->proof_of_payment != null)
                      <button data-toggle="modal" data-target="#modalProofofPayment{{$i->id}}" class="btn btn-link p-0 text-primary">Show</button>
                      <div class="modal fade" id="modalProofofPayment{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="labelProofofPayment{{$i->id}}" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title text-black" id="labelProofofPayment{{$i->id}}">Proof of Payment</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <img src="{{ route('admin.character.transaction.payment.show', $i->id) }}" class="w-100" alt="">
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      @else
                      Not Uploaded
                      @endif
                    </td>
                    <td>
                      {{ $i->status }}
                    </td>
                    <td>
                      {{ date('d-m-Y H:i', strtotime($i->updated_at)) }}
                    </td>
                    <td>
                      <div class="d-flex">
                        <a href="#" class="mr-2">
                          <span class="material-icons">
                            info
                          </span>
                        </a>
                        @if($i->status != 'success')
                        <button href="" class="btn btn-link p-0 text-primary" data-target="#modalStatus{{$i->id}}" data-toggle="modal">
                          status
                        </button>
                        @endif
                      </div>
                    </td>
                  </tr>
                  @if($i->status != 'success')
                  <div class="modal fade" id="modalStatus{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="modalStatus{{$i->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Status for Transaction ({{$i->id}})</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form method="post" action="{{ route('admin.character.transaction.set_status') }}" id="formTransactionStatus{{$i->id}}">
                            {{ csrf_field() }}
                            <input type="text" hidden name="id" value="{{ $i->id }}">
                            <div class="form-group">
                              <label>Set Status</label>
                              <select class="form-control" required name="status">
                                <option value="">Select status</option>
                                <option value="success">Accept</option>
                                <option value="reject">Reject</option>
                                <option value="failed">Failed</option>
                              </select>
                              <small class="text-danger">If you want cancel order & transaction select "failed"</small>
                            </div>
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" form="formTransactionStatus{{$i->id}}" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
