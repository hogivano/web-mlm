<?php
  $characterName = $data->name;
?>
@extends('admin.layouts.app', ['activePage' => 'character', 'titlePage' => __('Character ' . $data->name)])
@section('title')
{{ 'Character '. $data->name }}
@endsection
@section('link')
<style>
  .detail p {
    font-size: 18px;
    margin-bottom: 10px;
  }
</style>
@endsection
@section('content')
<?php
$base_url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'];
$url = $base_url . $_SERVER["REQUEST_URI"];
 ?>
<div class="content">
  <div class="container-fluid">
    <div class="message" style="margin-top: 20px">
      @include('flash::message')
    </div>
    <div class="row mt-2">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Detail</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-4 col-sm-12">
                <img src="{{ route('admin.character.picture.show', $data->character_id) }}" class="w-100" style="object-fit: cover" alt="{{ $data->name }}">
              </div>
              <div class="col-md-8 col-sm-12 d-flex align-items-center justify-content-center">
                <div class="detail">
                  <p>Name : <b>{{ $data->name }}</b> </p>
                  <p>Price : {{ number_format($data->price_for) }} - {{ number_format($data->price_to) }}</p>
                  <p>Profit : {{ $data->profit_for }}% - {{ $data->profit_to }}%</p>
                  <p>Soul : {{ $data->soul }}</p>
                  <p>Start Order : {{ date('H:i', strtotime($data->start_order_time)) }}</p>
                  <p>End Order : {{ date('H:i', strtotime($data->end_order_time)) }}</p>
                  <p>Length of Sale : {{ $data->length_of_sale }} Day</p>
                  <p>Time of Announcement : {{ date('H:i', strtotime($data->time)) }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Order Table</h4>
          </div>
          <div class="card-body">
            <div class="form-filter py-2">
              <form id="form-order">
                <div class="row">
                  <div class="col-md-2">
                      <div class="form-group">
                        <label>Search</label>
                        <input type="text" class="form-control" name="search_order" value="{{ old('search_order') }}">
                      </div>
                  </div>
                  <div class="col-md-2">
                      <div class="form-group">
                        <label>Start Date</label>
                        <input type="date" class="form-control" name="start_date_order" value="{{ old('start_date_order') }}">
                      </div>
                  </div>
                  <div class="col-md-2">
                      <div class="form-group">
                        <label>End Date</label>
                        <input type="date" placeholder="dd-mm-yyyy" class="form-control" name="end_date_order" value="{{ old('end_date_order') }}">
                      </div>
                  </div>
                  <div class="col-md-2 text-left">
                    <button type="submit" id="filter-order" class="btn btn-outline-primary" name="button">Filter</button>
                  </div>
                  <div class="col-md-4 text-left">
                    <div class="dropdown">
                      <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Excel
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" id="export-order" data-toggle="modal" data-target="#modalExportExcelOrder" href="#">Export</a>
                        <a class="dropdown-item" id="import-order" data-toggle="modal" data-target="#modalImportExcelOrder" href="#">Import</a>
                        <a class="dropdown-item" href="{{ route('admin.character.show.download_template_order') }}">Download Template</a>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal fade" id="modalImportExcelOrder" tabindex="-1" role="dialog" aria-labelledby="modalImportExcelLabelOrder" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modalImportExcelLabelOrder">Upload File</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form method="post" id="form-import-order" enctype="multipart/form-data" action="{{ route('admin.character.show.import-order', $data->character_id) }}">
                      {{ csrf_field() }}
                      <div class="">
                        <label class="text-warning">Format file must (.xls/xlsx)</label>
                        <input type="file" class="form-control" required name="file_excel" value="">
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" form="form-import-order" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal fade" id="modalExportExcelOrder" tabindex="-1" role="dialog" aria-labelledby="modalExportExcelLabelOrder" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modalExportExcelLabelOrder">Export Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form method="post" id="form-export-order" action="{{ route('admin.character.show.export-order', $data->character_id) }}">
                      {{ csrf_field() }}
                      <h6 for="">Get data</h6>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label for="">Start Date</label>
                            <input class="form-control" type="date" required name="start_date" value="">
                          </div>
                          <div class="col-md-6">
                            <label for="">End Date</label>
                            <input class="form-control" type="date" required name="end_date" value="">
                          </div>
                        </div>
                      </div>
                      <small class="text-danger">Note : export must between one day</small>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" form="form-export-order" class="btn btn-primary">Export</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID Transaction
                  </th>
                  <th>
                    Member
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Date Order
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @foreach($charactersOrder as $i)
                  <tr>
                    <td>
                      {{ $i->id }}
                    </td>
                    <td>
                      @if($i->member)
                        @if(sizeof($i->member->blacklistMember) > 0)
                        <span style="color: <?php echo $i->member->blacklistMember[0]->blacklistStatus->color; ?>">
                          {{ $i->member->username }}
                        </span>
                        @else
                        {{ $i->member->username }}
                        @endif
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    </td>
                    <td class="@if($i->status == 'success') text-success @elseif($i->status == 'unsuccess') text-danger @elseif($i->status == 'cancel') text-warning @endif">
                      {{ $i->status }}
                    </td>
                    <td>
                      {{ date_format(date_create($i->date_order), "d-m-Y H:i") }}
                    </td>
                    <td>
                      @if($i->member && $i->status == 'request')
                      <button type="button" data-toggle="modal" data-target="#modalStatusOrder{{$i->id}}" class="btn btn-link p-0 btn-primary">Status</button>
                      @endif
                    </td>
                  </tr>
                  @if($i->member)
                  <div class="modal fade" id="modalStatusOrder{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="modalLabelStatusOrder{{$i->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalLabelStatusOrder{{$i->id}}">Change Status {{ $i->member->username }}</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form method="post" id="form-status-order" action="{{ route('admin.character.show.order.set_status') }}">
                            {{ csrf_field() }}
                            <input type="text" hidden name="id" value="{{ $i->id }}">
                            <input type="text" hidden name="member_id" value="{{ $i->member->id }}">
                            <div class="form-group">
                              <label for="">Status</label>
                              <select class="form-control" name="status">
                                <option value="">Select status</option>
                                <option value="unsucceess">Unsucceess</option>
                                <option value="cancel">Cancel</option>
                              </select>
                            </div>
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" form="form-status-order" class="btn btn-primary">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endif
                  @endforeach
                </tbody>
              </table>
            </div>
            <div class="d-flex">
              {!! $charactersOrder !!}
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Seller Table</h4>
          </div>
          <div class="card-body">
              <div class="form-filter py-2">
                <form id="form-seller">
                  <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                          <label>Search</label>
                          <input type="text" class="form-control" name="search_seller" value="{{ old('search_seller') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                          <label>Start Date</label>
                          <input type="date" class="form-control" name="start_date_seller" value="{{ old('start_date_seller') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                          <label>End Date</label>
                          <input type="date" class="form-control" name="end_date_seller" value="{{ old('end_date_seller') }}">
                        </div>
                    </div>
                    <div class="col-md-2 text-right">
                      <button type="submit" id="filter-seller" class="btn btn-outline-primary" name="button">Filter</button>
                    </div>
                    <div class="col-md-2 text-right">
                      <button type="button" id="export-seller" data-toggle="modal" data-target="#modalExportExcelSeller" class="btn btn-primary" name="button">Export Excel</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal fade" id="modalExportExcelSeller" tabindex="-1" role="dialog" aria-labelledby="modalExportExcelLabelSeller" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="modalExportExcelLabelSeller">Export Seller</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form method="post" id="form-export-seller" action="{{ route('admin.character.show.export-seller', $data->character_id) }}">
                        {{ csrf_field() }}
                        <h6 for="">Get data</h6>
                        <div class="form-group">
                          <label for="">By Status</label>
                          <select class="form-control" name="status">
                            <option value="">Select status</option>
                            <option value="ready">Ready</option>
                            <option value="sold">Sold</option>
                            <option value="on_payment">On Payment</option>
                            <option value="expired">Expired</option>
                          </select>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" form="form-export-seller" class="btn btn-primary">Export</button>
                    </div>
                  </div>
                </div>
              </div>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID Transaction
                  </th>
                  <th>
                    Member
                  </th>
                  <th>
                    Date of Sale
                  </th>
                  <th>
                    Sold date
                  </th>
                  <th>
                    Capital Price
                  </th>
                  <th>
                    Prediction of Sale
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @foreach($charactersSeller as $i)
                  <tr>
                    <td>
                      {{ $i->id }}
                    </td>
                    <td>
                      @if($i->member)
                      {{ $i->member->username }}
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    </td>
                    <td>
                      {{ date('d-m-Y H:i:s', strtotime($i->start_of_sale)) . ' - ' . date('d-m-Y H:i:s', strtotime($i->end_of_sale)) }}
                    </td>
                    <td>
                      @if($i->date_of_sale)
                      {{ date('d-m-Y H:i:s', strtotime($i->date_of_sale)) }}
                      @else
                      On Going
                      @endif
                    </td>
                    <td>
                      {{ $i->price }}
                    </td>
                    <td>
                      @if($i->prediction_price)
                      {{ $i->prediction_price }}
                      @else
                      <span class="text-danger">Not Set</span>
                      @endif
                    </td>
                    <td>
                      {{ $i->status }}
                    </td>
                    <td>
                      <a href="#">
                        Split
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <div class="d-flex">
              {!! $charactersSeller !!}
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Transaction Table</h4>
          </div>
          <div class="card-body">
              <div class="form-filter py-2">
                <form id="form-transaction">
                  <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                          <label>Search</label>
                          <input type="text" class="form-control" name="search_transaction" value="{{ old('search_transaction') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                          <label>Start Date</label>
                          <input type="date" class="form-control" name="start_date_transaction" value="{{ old('start_date_transaction') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                          <label>End Date</label>
                          <input type="date" class="form-control" name="end_date_transaction" value="{{ old('end_date_transaction') }}">
                        </div>
                    </div>
                    <div class="col-md-2 text-right">
                      <button type="submit" id="filter-transaction" class="btn btn-outline-primary" name="button">Filter</button>
                    </div>
                    <div class="col-md-2 text-right">
                      <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Excel
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" id="import-transaction" data-toggle="modal" data-target="#modalImportExcelTransaction" href="#">Import</a>
                          <a class="dropdown-item" href="{{ route('admin.character.show.download_template_transaction') }}">Download Template</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal fade" id="modalImportExcelTransaction" tabindex="-1" role="dialog" aria-labelledby="modalImportExcelLabelTransaction" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="modalImportExcelLabelTransaction">Upload File</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form method="post" id="form-import-transaction" enctype="multipart/form-data" action="{{ route('admin.character.show.import-transaction', $data->character_id) }}">
                        {{ csrf_field() }}
                        <div class="">
                          <label for="">Support Sub Account</label>
                          <div class="form-check">
                            <input type="checkbox" class="" name="sub_account" id="exampleCheck1">
                            <label class="ml-2" for="exampleCheck1">On</label>
                          </div>
                        </div>
                        <div class="">
                          <label class="text-warning">Format file must (.xls/xlsx)</label>
                          <input type="file" class="form-control" required name="file_excel" value="">
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" form="form-import-transaction" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      ID Transaction
                    </th>
                    <th>
                      Member Buyer
                    </th>
                    <th>
                      Member Seller
                    </th>
                    <th>
                      Price Of Sale
                    </th>
                    <th>
                      Profit
                    </th>
                    <th>
                      Proof_of_payment
                    </th>
                    <th>
                      Status
                    </th>
                    <th>
                      Action
                    </th>
                  </thead>
                  <tbody>
                    @foreach($charactersTransactions as $i)
                    <tr>
                      <td>
                        {{ $i->id }}
                      </td>
                      <td>
                        @if($i->charactersOrder)
                          @if($i->charactersOrder->member)
                            @if(sizeof($i->charactersOrder->member->blacklistMember) > 0)
                            <span style="color: <?php echo $i->charactersOrder->member->blacklistMember[0]->color; ?>">
                              {{ $i->charactersOrder->member->username }}
                            </span>
                            @else
                            {{ $i->charactersOrder->member->username }}
                            @endif
                          @else
                          <span class="text-danger">Not Found</span>
                          @endif
                        @else
                        <span class="text-danger">Not Found</span>
                        @endif
                      </td>
                      <td>
                        @if($i->charactersSeller)
                        {{ $i->charactersSeller->member->username }}
                        @else
                        <span class="text-primary">Company</span>
                        @endif
                      </td>
                      <td>
                        {{ $i->price_of_sale }}
                      </td>
                      <td>
                        {{ $i->profit }}
                      </td>
                      <td>
                        @if($i->proof_of_payment)
                        <button data-toggle="modal" data-target="#modalProofofPayment{{$i->id}}" class="btn btn-link p-0 text-primary">Show</button>
                        <div class="modal fade" id="modalProofofPayment{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="labelProofofPayment{{$i->id}}" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title text-black" id="labelProofofPayment{{$i->id}}">Proof of Payment</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <img src="{{ route('admin.character.transaction.payment.show', $i->id) }}" class="w-100" alt="">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        @else
                        <span class="text-danger">Not Uploaded</span>
                        @endif
                      </td>
                      <td>
                        @if($i->status == 'success')
                        <span class="text-success">
                          {{ $i->status }}
                        </span>
                        @elseif($i->status == 'reject')
                        <span class="text-warning">
                          reject payment
                        </span>
                        @elseif($i->status == 'failed')
                        <span class="text-danger">
                          {{ $i->status }}
                        </span>
                        @else
                        {{ $i->status }}
                        @endif
                      </td>
                      <td>
                        <a href="{{ route('admin.character.transaction.show', $i->id) }}" class="mr-2">
                          <span class="material-icons">
                            info
                          </span>
                        </a>
                        <a href="{{ route('admin.character.transaction.edit', $i->id) }}" class="mr-2">
                          <span class="material-icons text-info">
                            edit
                          </span>
                        </a>
                        @if($i->status != 'success' && $i->status != 'failed')
                        <button href="" class="btn btn-link p-0 text-primary" data-target="#modalStatus{{$i->id}}" data-toggle="modal">
                          status
                        </button>
                        @endif
                      </td>
                    </tr>
                    @if($i->status != 'success' && $i->status != 'failed')
                    <div class="modal fade" id="modalStatus{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="modalStatus{{$i->id}}" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Status for Transaction ({{$i->id}})</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <form method="post" action="{{ route('admin.character.transaction.set_status') }}" id="formTransactionStatus{{$i->id}}">
                              {{ csrf_field() }}
                              <input type="text" hidden name="id" value="{{ $i->id }}">
                              <div class="form-group">
                                <label>Set Status</label>
                                <select class="form-control" required name="status">
                                  <option value="">Select status</option>
                                  <option value="success">Accept</option>
                                  <option value="reject">Reject Payment</option>
                                  <option value="failed">Failed</option>
                                </select>
                                <small class="text-danger">If you want cancel order & transaction select "failed"</small>
                              </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" form="formTransactionStatus{{$i->id}}" class="btn btn-primary">Save changes</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
              <div class="d-flex">
                {!! $charactersTransactions !!}
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function(){
    $('#form-order').on('submit',function(e){
      e.preventDefault();
      var formData=$(this).serialize();
      console.log(formData);
      var fullUrl = window.location.href;
      var finalUrl = '/'
      if (fullUrl.includes("?")){
        finalUrl = fullUrl+"&"+formData;
      } else {
        finalUrl = fullUrl+"?"+formData;
      }
      finalUrl = removeDuplicate(finalUrl);
      window.location.href = finalUrl;
      // alert(finalUrl);
    });

    $('#form-seller').on('submit',function(e){
      e.preventDefault();
      var formData=$(this).serialize();
      console.log(formData);
      var fullUrl = window.location.href;
      var finalUrl = '/'
      if (fullUrl.includes("?")){
        finalUrl = fullUrl+"&"+formData;
      } else {
        finalUrl = fullUrl+"?"+formData;
      }
      finalUrl = removeDuplicate(finalUrl);
      window.location.href = finalUrl;
      // alert(finalUrl);
    });
  });

  function removeDuplicate(url) {
    url = decodeURIComponent(url);                  // decode the url, %5B becomes [ and ,%5D becomes ]. Hence the url becomes  [domain]/find.php?cat=1&subcats[]=1&subcats[]=2
    var query = url.split('?')[1];
    if (typeof query === "undefined") {
      url = url;
    } else {
      var parts = query.split('&');                   // split the query into parts: cat=1, subcats[]=1, subcats[]=2
      var params = {};
      for (var i = 0; i < parts.length; i++) {
        var nv = parts[i].split('=');
        if (!nv[0]) continue;
        var value = nv[1] || true;
        if (params[nv[0]] && params[nv[0]].indexOf(value)) {
          params[nv[0]].push(value);
        } else {
          params[nv[0]] = [value];
        }
      }

      url = url.split('?')[0] + '?';
      var keys = Object.keys(params);
      for (var i = 0; i < keys.length; i++) {
        console.log(params[keys[i]]);
        url += keys[i] + '=' + params[keys[i]][params[keys[i]].length-1];

        if (i !== keys.length - 1) url += '&';
      }
    }
    return url;
  }
</script>
@endsection
