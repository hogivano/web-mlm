@extends('admin.layouts.app', ['activePage' => 'character', 'titlePage' => __('Edit Character')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('admin.character.update', $data->character_id) }}" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Edit Character') }}</h4>
                <p class="card-category">{{ __('Data information') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Below Character') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('below_character') ? ' has-danger' : '' }}">
                      <select class="form-control{{ $errors->has('below_character') ? ' is-invalid' : '' }}" name="below_character">
                        <option value="">Select positition character</option>
                        @foreach($character as $i)
                          @if($i->character_id != $data->character_id)
                          <option value="{{ $i->character_id }}"<?php if($i->characters_number < $data->characters_number) {echo ' selected';} ?> >{{ $i->name }}</option>
                          @endif
                        @endforeach
                      </select>
                      @if ($errors->has('below_character'))
                        <span id="below_character-error" class="error text-danger" for="input-below_character">{{ $errors->first('below_character') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('name character') }}" value="{{ old('name', $data->name) }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Soul') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('soul') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('soul') ? ' is-invalid' : '' }}" name="soul" id="input-soul" type="number" step="0.01" min="1" placeholder="{{ __('soul') }}" value="{{ old('soul', $data->soul) }}" required />
                      @if ($errors->has('soul'))
                        <span id="soul-error" class="error text-danger" for="input-soul">{{ $errors->first('soul') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-12">
                    <div class="row">
                      <label class="col-sm-4 col-form-label">{{ __('Price For') }}</label>
                      <div class="col-sm-8">
                        <div class="form-group{{ $errors->has('price_for') ? ' has-danger' : '' }}">
                          <input class="form-control{{ $errors->has('price_for') ? ' is-invalid' : '' }}" name="price_for" id="input-price_for" type="number" placeholder="{{ __('price for') }}" value="{{ old('price_for', $data->price_for) }}" required />
                          @if ($errors->has('price_for'))
                            <span id="price_for-error" class="error text-danger" for="input-price_for">{{ $errors->first('price_for') }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="row">
                      <label class="col-sm-4 col-form-label">{{ __('Price To') }}</label>
                      <div class="col-sm-8">
                        <div class="form-group{{ $errors->has('price_to') ? ' has-danger' : '' }}">
                          <input class="form-control{{ $errors->has('price_to') ? ' is-invalid' : '' }}" name="price_to" id="input-price_to" type="number" placeholder="{{ __('price to') }}" value="{{ old('price_to', $data->price_to) }}" required />
                          @if ($errors->has('price_to'))
                            <span id="price_to-error" class="error text-danger" for="input-price_to">{{ $errors->first('price_to') }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-12">
                    <div class="row">
                      <label class="col-sm-4 col-form-label">{{ __('Profit For (%)') }}</label>
                      <div class="col-sm-8">
                        <div class="form-group{{ $errors->has('profit_for') ? ' has-danger' : '' }}">
                          <input class="form-control{{ $errors->has('profit_for') ? ' is-invalid' : '' }}" name="profit_for" id="input-profit_for" type="number" placeholder="{{ __('profit for') }}" value="{{ old('profit_for', $data->profit_for) }}" required />
                          @if ($errors->has('profit_for'))
                            <span id="profit_for-error" class="error text-danger" for="input-profit_for">{{ $errors->first('profit_for') }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="row">
                      <label class="col-sm-4 col-form-label">{{ __('Profit To (%)') }}</label>
                      <div class="col-sm-8">
                        <div class="form-group{{ $errors->has('profit_to') ? ' has-danger' : '' }}">
                          <input class="form-control{{ $errors->has('profit_to') ? ' is-invalid' : '' }}" name="profit_to" id="input-profit_to" type="number" placeholder="{{ __('profit to') }}" value="{{ old('profit_to', $data->profit_to) }}" required />
                          @if ($errors->has('profit_to'))
                            <span id="profit_to-error" class="error text-danger" for="input-profit_to">{{ $errors->first('profit_to') }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-12">
                    <div class="row">
                      <label class="col-sm-4 col-form-label">{{ __('Start Order Time') }}</label>
                      <div class="col-sm-8">
                        <div class="form-group{{ $errors->has('start_order_time') ? ' has-danger' : '' }}">
                          <input class="form-control{{ $errors->has('start_order_time') ? ' is-invalid' : '' }}" name="start_order_time" id="input-start_order_time" type="time" placeholder="{{ __('start order time') }}" value="{{ old('start_order_time', $data->start_order_time) }}" required />
                          @if ($errors->has('start_order_time'))
                            <span id="start_order_time-error" class="error text-danger" for="input-start_order_time">{{ $errors->first('start_order_time') }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="row">
                      <label class="col-sm-4 col-form-label">{{ __('End Order Time') }}</label>
                      <div class="col-sm-8">
                        <div class="form-group{{ $errors->has('end_order_time') ? ' has-danger' : '' }}">
                          <input class="form-control{{ $errors->has('end_order_time') ? ' is-invalid' : '' }}" name="end_order_time" id="input-end_order_time" type="time" placeholder="{{ __('end order time') }}" value="{{ old('end_order_time', $data->end_order_time) }}" required />
                          @if ($errors->has('end_order_time'))
                            <span id="end_order_time-error" class="error text-danger" for="input-end_order_time">{{ $errors->first('end_order_time') }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6 col-sm-12">
                    <div class="row">
                      <label class="col-sm-4 col-form-label">{{ __('Time of Announcement') }}</label>
                      <div class="col-sm-8">
                        <div class="form-group{{ $errors->has('time') ? ' has-danger' : '' }}">
                          <input class="form-control{{ $errors->has('time') ? ' is-invalid' : '' }}" name="time" id="input-time" type="time" placeholder="{{ __('time') }}" value="{{ old('time', $data->time) }}" required />
                          @if ($errors->has('time'))
                            <span id="time-error" class="error text-danger" for="input-time">{{ $errors->first('time') }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="row py-2">
                      <label class="col-4">
                        {{ __('Set in sequence?') }}
                      </label>
                      <div class="col-8">
                        <input class="" type="checkbox" <?php if($data->is_sequence == 1) echo ' checked ' ; ?> name="is_sequence" id="defaultCheck1">
                        <label class="" for="defaultCheck1">
                          Yes
                        </label>
                      </div>
                    </div>
                    @if ($errors->has('is_sequence'))
                      <span id="is_sequence-error" class="error text-danger" for="input-is_sequence">{{ $errors->first('is_sequence') }}</span>
                    @endif
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Time of Announcement') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('time') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('time') ? ' is-invalid' : '' }}" name="time" id="input-time" type="time" placeholder="{{ __('time') }}" value="{{ old('time', $data->time) }}" required />
                      @if ($errors->has('time'))
                        <span id="time-error" class="error text-danger" for="input-time">{{ $errors->first('time') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Length of Sale (day)') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('length_of_sale') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('length_of_sale') ? ' is-invalid' : '' }}" name="length_of_sale" id="input-length_of_sale" type="number" placeholder="{{ __('length of sale') }}" value="{{ old('length_of_sale', $data->length_of_sale) }}" required />
                      @if ($errors->has('length_of_sale'))
                        <span id="length_of_sale-error" class="error text-danger" for="input-length_of_sale">{{ $errors->first('length_of_sale') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <img src="{{ route('admin.character.picture.show', $data->character_id) }}" style="width: 250px; object-fit: cover;" alt="">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('New Picture') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('picture') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('picture') ? ' is-invalid' : '' }}" name="picture" id="input-picture" type="file" placeholder="{{ __('picture') }}" value="{{ old('picture') }}" style="opacity: 1 !important; position: initial !important;"/>
                      @if ($errors->has('picture'))
                        <span id="picture-error" class="error text-danger" for="input-picture">{{ $errors->first('picture') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
