@extends('admin.layouts.app', ['activePage' => 'bank', 'titlePage' => __('Edit Bank')])
@section('title')
Edit Bank
@endsection
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('admin.bank.update', $data->id) }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Edit Bank') }}</h4>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="list-input mt-3">
                  <div class="form-group{{ $errors->has('country_id') ? ' has-danger' : '' }}">
                    <label>Country</label>
                    <select class="form-control" name="country_id" required>
                      <option value="">Select country</option>
                      @foreach($country as $i)
                      <option value="{{ $i->id }}" <?php if(old('country_id', $data->country_id) == $i->id) echo ' selected'; ?>>{{ $i->name }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('country_id'))
                      <span id="country_id-error" class="error text-danger" for="input-country_id">{{ $errors->first('country_id') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <label>Name</label>
                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" placeholder="name bank" type="text" value="{{ old('name', $data->name) }}" required="true" aria-required="true"/>
                    @if ($errors->has('name'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                    @endif
                  </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
