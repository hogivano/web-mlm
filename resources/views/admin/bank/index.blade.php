@extends('admin.layouts.app', ['activePage' => 'bank', 'titlePage' => __('List Bank')])
@section('title', 'List Bank')
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="message mt-2">
      @include('flash::message')
    </div>
    <div class="d-flex mt-2">
      <a href="{{ route('admin.bank.new') }}">
          <span class="material-icons">
            add
          </span>
          New Bank
      </a>
    </div>
    <div class="row mt-2">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">All Bank</h4>
            <p class="card-category"> Here is bank for list in register</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    Bank
                  </th>
                  <th>
                    Country
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @foreach($data as $i)
                  <tr>
                    <td>
                      {{ $i->name }}
                    </td>
                    <td>
                      @if($i->country)
                      {{ $i->country->name }}
                      @else
                      <span class="text-danger">Not Found</span>
                      @endif
                    </td>
                    <td class="d-flex">
                      <a href="{{ route('admin.bank.edit', $i->id) }}" class="mr-2 text-info">
                        <span class="material-icons">
                          edit
                        </span>
                      </a>
                      <form method="post" class="d-inline" action="{{ route('admin.bank.delete') }}">
                        {{ csrf_field() }}
                        <input type="text" hidden name="id" value="{{ $i->id }}">
                        <input type="text" hidden name="status" value="active">
                        <button type="submit" onclick="return confirm('Are you sure you delete it?');" class="mr-2 text-danger btn-link btn p-0">
                          <span class="material-icons">
                            delete
                          </span>
                        </button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
