@extends('admin.layouts.app', ['activePage' => 'soul', 'titlePage' => __('Soul Transaction')])
@section('title', 'Soul Transaction')
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="message" style="margin-top: 20px">
      @include('flash::message')
    </div>
    <div class="row mt-2">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Request Buy & Buy Back</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID Transaction
                  </th>
                  <th>
                    Member
                  </th>
                  <th>
                    Type
                  </th>
                  <th>
                    Amount
                  </th>
                  <th>
                    Payment
                  </th>
                  <th>
                    Date
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                  @foreach($requestBuy as $i)
                  <tr>
                    <td>
                      {{ $i->tt_id }}
                    </td>
                    <td>
                      {{ $i->memberTo->username }}
                    </td>
                    <td>
                      {{ $i->transaction_type }}
                    </td>
                    <td>
                      {{ $i->token_amount }}
                    </td>
                    <td>
                      @if($i->proof_of_payment == null)
                        @if($i->transaction_type == 'buyback')
                        <button type="button" data-toggle="modal" data-target="#modalUploadBuyBack{{$i->tt_id}}" class="btn btn-primary btn-link p-0" name="button">Upload</button>
                        @else
                        Not set
                        @endif
                      @else
                      <button data-toggle="modal" data-target="#modalProofofPayment{{$i->tt_id}}" class="btn btn-link p-0 text-primary">Show</button>
                      <div class="modal fade" id="modalProofofPayment{{$i->tt_id}}" tabindex="-1" role="dialog" aria-labelledby="labelProofofPayment{{$i->tt_id}}" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title text-black" id="labelProofofPayment{{$i->tt_id}}">Proof of Payment</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <img src="{{ route('admin.soul.image.proof_of_payment', $i->tt_id) }}" class="w-100" alt="">
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endif
                    </td>
                    <td>
                      {{ date_format($i->created_at,  'd-m-Y H:i') }}
                    </td>
                    <td class="text-primary">
                      <button type="button" data-toggle="modal" data-target="#modalBuy{{$i->tt_id}}" class="p-0 btn btn-link text-primary" name="button">
                        status
                      </button>
                    </td>
                  </tr>
                  <div class="modal fade" id="modalUploadBuyBack{{$i->tt_id}}" tabindex="-1" role="dialog" aria-labelledby="modalUploadBuyBack{{$i->tt_id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalUploadBuyBack{{$i->tt_id}}">Upload payment for buyback {{ $i->memberTo->username }}</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <p>Soul Amount: <b>{{ $i->token_amount }}</b></p>
                          <form method="post" enctype="multipart/form-data" action="{{ route('admin.soul.proof_of_payment.upload') }}" id="formUploadBuyback{{$i->tt_id}}">
                            {{ csrf_field() }}
                            <input type="file" class="form-control" name="proof_of_payment" required value="">
                            <input type="text" hidden name="tt_id" value="{{ $i->tt_id }}">
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" form="formUploadBuyback{{$i->tt_id}}" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal fade" id="modalBuy{{$i->tt_id}}" tabindex="-1" role="dialog" aria-labelledby="modalBuy{{$i->tt_id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Status for {{ $i->memberTo->username }}</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form method="post" action="{{ route('admin.soul.buy.set_status') }}" id="formBuyStatus{{$i->tt_id}}">
                            {{ csrf_field() }}
                            <input type="text" hidden name="transaction_type" value="{{ $i->transaction_type }}">
                            <input type="text" hidden name="tt_id" value="{{ $i->tt_id }}">
                            <div class="form-group">
                              <select class="form-control" required name="status">
                                <option value="">Select status</option>
                                <option value="success">Accept</option>
                                <option value="reject">Reject</option>
                              </select>
                            </div>
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" form="formBuyStatus{{$i->tt_id}}" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Histories Buy & BuyBack</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID Transaction
                  </th>
                  <th>
                    Member
                  </th>
                  <th>
                    Type
                  </th>
                  <th>
                    Amount
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Date
                  </th>
                </thead>
                <tbody>
                  @foreach($historiesBuy as $i)
                  <tr>
                    <td>
                      {{ $i->tt_id }}
                    </td>
                    <td>
                      {{ $i->memberTo->username }}
                    </td>
                    <td>
                      {{ $i->transaction_type }}
                    </td>
                    <td>
                      {{ $i->token_amount }}
                    </td>
                    <td>
                      {{ $i->transaction_status }}
                    </td>
                    <td>
                      {{ date_format($i->created_at,  'd-m-Y H:i') }}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <div class="d-flex">
              {!! $historiesBuy !!}
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Histories Transfer</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID Transaction
                  </th>
                  <th>
                    From
                  </th>
                  <th>
                    To
                  </th>
                  <th>
                    Amount
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Date
                  </th>
                </thead>
                <tbody>
                  @foreach($historiesTransfer as $i)
                  <tr>
                    <td>
                      {{ $i->tt_id }}
                    </td>
                    <td>
                      {{ $i->memberFrom->username }}
                    </td>
                    <td>
                      {{ $i->memberTo->username }}
                    </td>
                    <td>
                      {{ $i->token_amount }}
                    </td>
                    <td>
                      {{ $i->transaction_status }}
                    </td>
                    <td>
                      {{ date_format($i->created_at,  'd-m-Y H:i') }}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <div class="d-flex">
              {!! $historiesTransfer !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
@endsection
