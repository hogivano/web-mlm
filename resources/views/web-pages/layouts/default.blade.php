<!DOCTYPE html>
<html lang="zxx">
    @include('web-pages.includes.head')

<body>
    @include('web-pages.includes.header')

    @yield('content')

    @include('web-pages.includes.footer')
</body>

</html>