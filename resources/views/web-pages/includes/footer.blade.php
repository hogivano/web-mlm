<footer class="footer-style-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <h3>Tentang Kami</h3>
                <div class="mt-20">
                    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut--}}
                    {{--labore et dolore magna aliqua.</p>--}}
                    {!! setting('beranda.tentang_kami') !!}
                </div>
                <ul class="footer-style-1-social-links">
                    <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                    <li><a href="#"><i class="fab fa-skype"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <h3>Rekening Perusahaan</h3>
                <ul class="footer-style-1-contact-info">
                    @if(setting('footer.no_rek1'))
                        <li style="padding: 0;"><span><?=html_entity_decode(setting('footer.no_rek1'))?></span></li>
                    @endif
                    @if(setting('footer.no_rek2'))
                        <li style="padding: 0;"><span><?=html_entity_decode(setting('footer.no_rek2'))?></span></li>
                    @endif
                    @if(setting('footer.no_rek3'))
                        <li style="padding: 0;"><span>{{setting('footer.no_rek3')}}</span></li>
                    @endif
                    @if(setting('footer.no_rek4'))
                        <li style="padding: 0;"><span>{{setting('footer.no_rek4')}}</span></li>
                    @endif
                    @if(setting('footer.no_rek5'))
                        <li style="padding: 0;"><span>{{setting('footer.no_rek5')}}</span></li>
                    @endif
                    @if(setting('footer.npwp'))
                        <li style="padding: 0;"><span><?=html_entity_decode(setting('footer.npwp'))?></span></li>
                    @endif
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <h3>Kontak Kami</h3>
                <ul class="footer-style-1-contact-info">
                    <li><i class="fa fa-phone"></i> <span>{{setting('footer.no_telepon')}}</span></li>
                    <li><i class="fa fa-envelope-open"></i> <span>{{setting('footer.email')}}</span></li>
                    <li><i class="fa fa-map-marker-alt"></i> <span>{{setting('footer.alamat')}}</span></li>
                    {{--                        <li><i class="fa fa-phone-square"></i> <span>+62 0274 800088</span></li>--}}
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 col-12">
                <h3>Link</h3>
                <ul class="footer-style-1-links">
                    <li><a href="{{url("/profile")}}">Tentang Kami</a></li>
                    <li><a href="{{url("/insurance")}}">Produk</a></li>
                    <li><a href="{{url("/marketing-plan")}}">Marketing Plan</a></li>
                    {{--<li><a href="{{url('/blog-news')}}">Berita</a></li>--}}
                    <li><a href="{{url('/membership')}}">Member Area</a></li>
                    {{--                        <li><a href="{{url("/contact-us")}}">Hubungi Kami</a></li>--}}
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-style-1-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-12">
                    <h5>YOP 2019</h5>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                    <ul class="footer-style-1-bar-links">
                        <li><a href="{{url('/membership')}}">Member Area</a></li>
                        <li><a href="{{url("/profile")}}">Tentang Kami</a></li>
                        {{--                            <li><a href="{{url("/contact-us")}}">Hubungi Kami</a></li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {{TawkTo::widgetCode()}}
</footer><a href="#" class="scroll-to-top"><i class="fas fa-chevron-up"></i></a>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5d26e2757a48df6da243fe97/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>

<script src="{{ URL::asset('js2/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js2/plugins.js') }}"></script>
<script src="{{ URL::asset('js2/navigation.js') }}"></script>
<script src="{{ URL::asset('js2/navigation.fixed.js') }}"></script>
{{--    <script src="{{ URL::asset('js/map.js') }}"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>--}}
<script src="{{ URL::asset('js2/main.js') }}"></script>
