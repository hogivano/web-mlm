<head>
<title>MLM V5</title>
<meta charset="UTF-8">
<link rel="shortcut icon" href="/img/logos/favicon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="{!! asset('/css/bootstrap.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('/css/themify-icons.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('/css/fontawesome-all.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('/css/icomoon.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('/css/plugins.css') !!}">
<link rel="stylesheet" href="{!! asset('/css/animate.css') !!}">
<link rel="stylesheet" href="{!! asset('/css/owl.carousel.css') !!}">
<link rel="stylesheet" href="{!! asset('/css/slider.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('/css/navigation.css') !!}" id="navigation_menu">
<link rel="stylesheet" type="text/css" href="{!! asset('/css/default.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('/css/styles.css') !!}" id="main_styles">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900"
    rel="stylesheet">
</head>
