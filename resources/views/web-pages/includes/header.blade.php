<header id="nav-transparent">
    <nav id="navigation4" class="container navigation">
        <div class="nav-header"><a class="nav-brand" href="{{url("/")}}"><p style="color: white;">MLM V1</p>
                <p id="main_logo">MLM V5</p></a>
            <div class="nav-toggle"></div>
        </div>
        <div class="nav-menus-wrapper">
            <ul class="nav-menu align-to-right">
                <li><a href="{{url("/")}}">Beranda</a></li>
                <li><a href="{{url("/profile")}}">Tentang Kami</a></li>
                <li><a href="{{url("insurance")}}">Produk</a></li>
                <li><a href="{{url("/marketing-plan")}}">Marketing Plan</a></li>
                {{--<li><a href="{{url('/blog-news')}}">Berita</a></li>--}}
                {{--<li><a href="{{url('/stockist')}}">Stockist</a></li>--}}
                {{--<li><a href="{{url('/album')}}">Galeri</a></li>--}}
                <li><a href="{{url('/membership/home')}}">Member Area</a></li>
            </ul>
        </div>
    </nav>
</header>
