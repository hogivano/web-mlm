<div class="blog-post-right">
                        <div id="search-input">
                            <div class="input-group"> <input type="text" class="form-control input-sn"
                                    placeholder="Search..." /> <span class="input-group-btn"><button
                                        class="btn btn-info btn-md" type="button"><i class="fa fa-search"></i></button>
                                </span> </div>
                        </div>
                        <h4>Categories</h4>
                        <div class="blog-post-categories mt-20">
                            <ul>
                                <li><a href="#">Business<span>25</span></a></li>
                                <li><a href="#">Resources<span>06</span></a></li>
                                <li><a href="#">Life Style<span>09</span></a></li>
                                <li><a href="#">Graphic Design<span>10</span></a></li>
                                <li><a href="#">Web Designer<span>36</span></a></li>
                                <li><a href="#">User Interface<span>17</span></a></li>
                                <li><a href="#">Right Style<span>09</span></a></li>
                                <li><a href="#">Videos<span>06</span></a></li>
                            </ul>
                        </div>
                        <h4>Recent-post</h4>
                        <div class="recent-post mt-20">
                            <div class="recent-post-info">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-12 pr-0"> <img src="img/content/blog/t-1.jpg"
                                            alt="img"> </div>
                                    <div class="col-md-8 col-sm-8 col-12">
                                        <h3><a href="#">Four ways to cheer yourself up on Blue Monday!</a></h3>
                                        <h6>May 21, 2018</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="recent-post-info">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-12 pr-0"> <img src="img/content/blog/t-2.jpg"
                                            alt="img"> </div>
                                    <div class="col-md-8 col-sm-8 col-12">
                                        <h3><a href="#">How to Organize Your Office for Maximum?</a></h3>
                                        <h6>May 21, 2018</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="recent-post-info">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-12 pr-0"> <img src="img/content/blog/t-3.jpg"
                                            alt="img"> </div>
                                    <div class="col-md-8 col-sm-8 col-12">
                                        <h3><a href="#">Best Messaging Apps for Business 2018</a></h3>
                                        <h6>May 21, 2018</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="recent-post-info">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-12 pr-0"> <img src="img/content/blog/t-4.jpg"
                                            alt="img"> </div>
                                    <div class="col-md-8 col-sm-8 col-12">
                                        <h3><a href="#">Four ways to cheer yourself up on Blue Monday!</a></h3>
                                        <h6>May 21, 2018</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4>Tags</h4>
                        <div class="recent-post-tags mt-20"> <a href="#"
                                class="dark-button button-tag semi-rounded">Business</a> <a href="#"
                                class="dark-button button-tag semi-rounded">Design</a> <a href="#"
                                class="dark-button button-tag semi-rounded">Deweloper</a> <a href="#"
                                class="dark-button button-tag semi-rounded">Design</a> <a href="#"
                                class="dark-button button-tag semi-rounded">Business</a> <a href="#"
                                class="dark-button button-tag semi-rounded">Design</a> <a href="#"
                                class="dark-button button-tag semi-rounded">Deweloper</a> <a href="#"
                                class="dark-button button-tag semi-rounded">Business</a> <a href="#"
                                class="dark-button button-tag semi-rounded">Design</a> <a href="#"
                                class="dark-button button-tag semi-rounded">Design</a> </div>
                        <h4>Archive</h4>
                        <div class="archive-box clearfix mt-20">
                            <ul>
                                <li><a href="#">July 2018</a><span>12</span></li>
                                <li><a href="#">June 2018</a><span>05</span></li>
                                <li><a href="#">May 2018</a><span>08</span></li>
                                <li><a href="#">April 2018</a><span>10</span></li>
                                <li><a href="#">March 2018</a><span>21</span></li>
                                <li><a href="#">February 2018</a><span>09</span></li>
                                <li><a href="#">January 2019</a><span>07</span></li>
                            </ul>
                        </div>
                        <h4>Share Links</h4>
                        <div class="blog-post-follow mt-20">
                            <ul>
                                <li class="social-facebook"><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                                <li class="social-youtube"><a href="#"><i class="fab fa-youtube-square"></i></a></li>
                                <li class="social-twitter"><a href="#"><i class="fab fa-twitter-square"></i></a></li>
                                <li class="social-pinterest"><a href="#"><i class="fab fa-pinterest-square"></i></a>
                                </li>
                                <li class="social-behance"><a href="#"><i class="fab fa-behance-square"></i></a></li>
                            </ul>
                        </div>
                    </div>