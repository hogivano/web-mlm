@extends('web-pages.layouts.default')
@section('content')
    <div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6"
         style="background-image: url({{url("/storage/".$data->image)}});">
        <div class="container"><h1>Berita</h1>
            <ul>
                <li><a href="{{url("/")}}">Beranda</a></li>
                <li><a href="{{url("/blog-news")}}">Berita</a></li>
            </ul>
        </div>
    </div>
    <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-xs-12 offset-md-1">
                    <div class="blog-list center-holder"><img src="{{url("/storage/".$data->image)}}" alt="img"><h4><a href="#">{{$data->title}}</a></h4>
                        <ul class="blog-list-info">
                            <li><i class="far fa-calendar-alt"></i><span>{{ date("d M Y", strtotime($data->created_at)) }}</span></li>
                        </ul>
                        <div class="left-holder">
                            <blockquote>
                                <?=$data->excerpt?>
                            </blockquote>
                            <?=$data->body?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop