@extends('web-pages.layouts.default')
@section('content')
    <style>
        .blog-grid-3-text-me{
            padding: 14px;
            font-size: 14px;
        }
        .blog-grid-3-text-me h4{
            font-size: 16px;
            font-weight: 600;
        }
        .blog-grid-3-text-me .lia{
            margin-right: 8px;
            font-size: 14px;
            color: #999;
        }
        .blog-grid-3-text-me .lib{
            margin-right: 10px;
            font-size: 14px;
            color: #999;
        }
        .blog-grid-3-text-me ul{
            padding-top: 3px;
        }
        .blog-grid-3-text-me li{
            font-size: 14px;
        }

    </style>

<div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6"
     style="background-image: url('img/content/bgs/bg1.jpg');">
    <div class="container">
        <h1>Stockist</h1>
    </div>
</div>
<div class="section-block">
    <div class="container">
        {{--<div class="small-heading center-holder mb-40"><span class="uppercase">Daftar Stockist</span></div>--}}
        <div class="row">
            <div class="col-md-10 col-sm-10 col-12">
                <h3 class="bold">Daftar Stockist Kami</h3>
                <p>Untuk informasi dan pembelian token dapat menghubungi <em>stockist</em> terdekat di kota anda.</p>
            </div>
            @foreach($stockists as $st)
            <div class="col-md-3 col-sm-6 col-12">
                <div class="blog-grid-3">
                    <div class="blog-grid-3-text-me">
                        <h4>{{$st['stockist_name']}}</h4>
                        <ul>
                            <li><i class="fa fa-home lib"></i>{{$st['stockist_city']}}</li>
                            <li><i class="fa fa-phone lib"></i>{{$st['stockist_phone']}}</li>
                            <li><i class="fa fa-credit-card lib"></i>{{$st['stockist_bank_number_1']}} ({{$st['stockist_bank_name_1']}})</li>
                            @if($st['stockist_bank_number_2'])
                                <li><i class="fa fa-credit-card lib"></i>{{$st['stockist_bank_number_2']}} ({{$st['stockist_bank_name_2']}})</li>
                            @endif
                            @if($st['stockist_bank_number_3'])
                                <li><i class="fa fa-credit-card lib"></i>{{$st['stockist_bank_number_3']}} ({{$st['stockist_bank_name_3']}})</li>
                            @endif
                            @if($st['stockist_bank_number_4'])
                                <li><i class="fa fa-credit-card lib"></i>{{$st['stockist_bank_number_4']}} ({{$st['stockist_bank_name_4']}})</li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
                    {{--<div class="col-md-3 col-sm-6 col-12">--}}
                        {{--<div class="contact-box-place-office">--}}
                            {{--<i class="icon-building"></i>--}}
                            {{--<h4>London Office</h4>--}}
                            {{--<h6>Str. Sheram USA / 113</h6>--}}
                            {{--<p>There are many variations of passages of Lorem load Ipsum available.</p>--}}
                            {{--<ul>--}}
                                {{--<li><i class="fas fa-user"></i>{{$st['stockist_name']}}</li>--}}
                                {{--<li><i class="fas fa-home"></i>{{ $st['stockist_city'] }}</li>--}}
                                {{--<li><i class="fas fa-phone"></i>{{ $st['stockist_phone'] }}</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
            @endforeach
        </div>
    </div>
</div>
@stop