@extends('web-pages.layouts.default')
@section('content')
    <div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6"
         style="background-image: url({{url("img/content/bgs/bg1.jpg")}});">
        <div class="container"><h1>Hubungi Kami</h1>
            <ul>
                <li><a href="{{url("/")}}">Beranda</a></li>
                <li><a href="#">Halaman</a></li>
                <li><a href="#">Hubungi Kami</a></li>
            </ul>
        </div>
    </div>
    <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12">
                    <div class="contact-box-data">
                        <div class="section-heading left-holder"><h5 class="bold">Hubungi Kami</h5>
                            <div class="section-heading-line"></div>
                        </div>
                        <div class="contact-box-place clearfix">
                            <div class="contact-box-icon"><i class="ti-map-alt"></i></div>
                            <div class="contact-box-text"><h5>Office</h5>
                                <p>Co Hive, Hartono Mall, Yogyakarta</p></div>
                        </div>
                        <div class="contact-box-place clearfix">
                            <div class="contact-box-icon"><i class="ti-mobile"></i></div>
                            <div class="contact-box-text"><h5>Phone</h5>
                                <p>(+123) 123-456-789</p></div>
                        </div>
                        <div class="contact-box-place clearfix">
                            <div class="contact-box-icon"><i class="ti-email"></i></div>
                            <div class="contact-box-text"><h5>Email</h5>
                                <p>info@sibayakindonesia.com</p></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 col-12">
                    <div class="contact-box-4">
                        <div class="section-heading left-holder mt-15"><h5 class="bold">Kirim pesan</h5>
                            <div class="section-heading-line"></div>
                        </div>
                        <form class="primary-form mt-20">
                            <div class="row">
                                <div class="col-md-12"><textarea name="message" placeholder="Pesan"></textarea></div>
                                <div class="col-md-4 col-sm-4 col-xs-12"><input type="text" name="name" placeholder="Nama">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12"><input type="email" name="email"
                                                                                placeholder="E-mail"></div>
                                <div class="col-md-4 col-sm-4 col-xs-12"><input type="email" name="email"
                                                                                placeholder="Phone"></div>
                                <div class="col-md-12 mt-10 mb-30">
                                    <button type="submit" class="primary-button button-sm semi-rounded">Kirim Pesan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
