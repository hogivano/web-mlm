@extends('web-pages.layouts.default')
@section('content')
    <style>
        li{
            list-style-type : square;
        }

        .feature-block-2{
            height: 320px;
        }
    </style>
<div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6"
        style="background-image: url('/img/content/bgs/bg1.jpg');">
        <div class="container">
            <h1>Produk</h1>
{{--            <h4>Powered By PT Asuransi Tokio Marine Indonesia</h4>--}}
        </div>
    </div>
    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder"><small>We're thinking big</small>
                        <h2>Tentang Produk</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    <div class="text-content-big center-holder mt-25">
                        {!! setting('produk.produk_tentang') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        <h2>Smart Solution</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    <div class="text-content-big center-holder mt-25">
                        {!! setting('produk.smart_solution') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        <h2>Jaminan</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    <div class="text-content-big center-holder mt-25">
                        <p> sesuai permintaan.</p>
{{--                        {!! setting('produk.smart_solution') !!}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-block section-sm">
        <div class="container">
            <div class="section-heading center-holder">
                <h2>Misi dan Tujuan Perusahaan Gabungan</h2>
                <div class="section-heading-line"></div>
                {!! setting('produk.misi_tujuan') !!}
                {{--<p>Sebagai perusahaan asuransi terkemuka, kami berusaha untuk memenuhi kebutuhan finansial utama nasabah.</p>--}}
            </div>
            <div class="row mt-40">
                <div class="col-md-4 col-12">
                    <div class="feature-block-2 feature-bordered feature-shadow feature-bg inner-40 center-holder"><i
                            class="icon-shield-1"></i>
                        <h4>Perlindungan dan Keamanan</h4>
                        {!! setting('produk.perlindungan_keamanan') !!}
                        {{--<p>Memberikan kontribusi kepada masyarakat dan perkembangan ekonomi dengan menyediakan “perlindungan dan keamanan” kepada nasabah kami.</p>--}}
{{--                        <a href="#">Learn More <i class="fas fa-chevron-right"></i></a>--}}
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="feature-block-2 feature-bordered feature-shadow feature-bg inner-40 center-holder"><i
                            class="icon-target2"></i>
                        <h4>Kepercayaan</h4>
                        {!! setting('produk.kepercayaan') !!}
                        {{--<p>Melaksanakan semua kegiatan usaha dengan baik untuk mendapatkan dan mempertahankan kepercayaan dan dukungan dari nasabah kami.</p>--}}
                        <br>
{{--                        <a href="#">Learn More <i class="fas fa-chevron-right"></i></a>--}}
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="feature-block-2 feature-bordered feature-shadow feature-bg inner-40 center-holder"><i
                            class="icon-compass2"></i>
                        <h4>Inovatif</h4>
                        {!! setting('produk.inovasi') !!}
                        {{--<p>Menjadi perusahaan yang inovatif terhadap perubahan.</p>--}}
                        <br><br><br>
                        {{--<a href="#">Learn More <i class="fas fa-chevron-right"></i></a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        <h2>Uang Pertanggungjawaban</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    <ul>
                        {{--<li>Kematian dan cacat permanen (AD & PD) karena kecelakaan berdasarkan skala--}}
                            {{--continental = Rp 50.000.000 s/d maxl Rp 600.000.000.</li>--}}
                       {{--<li> Biaya pengobatan karena kecelakaan (medex) 10% dari AD & PD Rp 5.000.000,---}}
                        {{--s/d maksimal Rp 60.000.000.</li>--}}
                        {!! setting('produk.uang_pertanggungjawaban') !!}
                    </ul>
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>

    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        <h2>Perluasan Jaminan</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    <ul>
                        {{--<li>Kematian dan cacat permanen (AD & PD) karena kecelakaan berdasarkan skala--}}
                        {{--continental = Rp 50.000.000 s/d maxl Rp 600.000.000.</li>--}}
                        {{--<li> Biaya pengobatan karena kecelakaan (medex) 10% dari AD & PD Rp 5.000.000,---}}
                        {{--s/d maksimal Rp 60.000.000.</li>--}}
                        {!! setting('produk.perluasan-jaminan') !!}
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        <h2>Beberapa Pengecualian</h2>
                        <div class="section-heading-line"></div>
                    </div>
{{--                    <ul>--}}
{{--                        <li>Perang yang dinyatakan atau tidak dinyatakan atau tindakan perang, invansi,--}}
{{--                            musuh asing, perang saudara, pemberontakan, revolusi, militer, atau perebutan--}}
{{--                            kekuasaan.</li>--}}
{{--                        <li>Setiap tindakan illegal, yang disengaja atau disengaja dari pemegang polis atau--}}
{{--                            tertanggung ketika sehat ingatan atau tidak, bunuh  diri, cedera yang ditimbulkan--}}
{{--                            sendiri, perjanjian bunuh diri, atau perjanjian percobaan apapun melaluinya,--}}
{{--                            pembunuhan atau  penyerangan karena di provokasi.</li>--}}
{{--                        <li>Tertanggung bertindak sebagai petugas penegak hu--}}
{{--                        kum, tenaga medis darurat atau pemadam kebakaran, petugas pertahanan sipil--}}
{{--                        atau petugas militer dari negara atau otoritas internasional, baik sebagai layanan--}}
{{--                        penuh waktu atau sebagai relawan.</li>--}}
{{--                        <li>Tertanggung terlibat dalam setiap kontes kecepatan atau balapan (selain berjalan--}}
{{--                            kaki) atau kompetisi professional atau olah raga professional.</li>--}}
{{--                        <li>Setiap kondisi yang hasil dari atau merupakan komplikasi kehamilan, persalinan,--}}
{{--                            keguguran (kecuali keguguran karena kecelakaan) atau aborsi, intoksikasi alcohol--}}
{{--                            atau obat-obatan yang tidak diresepkan oleh seorang Dokter.</li>--}}
{{--                        <li>Tertanggung terlibat dalam penerbangan, selain sebagai penumpang yang--}}
{{--                        membayar biaya perjalanan, naik dan turun dari setiap pesawat sayap tetap dan--}}
{{--                        atau helicopter.</li>--}}
{{--                        <li>Turut serta dalam segala jenis kontes kecepatan.</li>--}}
{{--                    </ul>--}}
                    {!! setting('produk.pengecualian') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        <h2>Kondisi</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    {!! setting('produk.kondisi') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        <h2>Jenis Pekerjaan Tertanggung, Kecuali :</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    {!! setting('produk.pekerjaan-kecuali') !!}
                </div>
            </div>
        </div>
    </div>

@stop
