<?php
function get_snippet( $str, $wordCount = 10 ) {
    return implode(
        '',
        array_slice(
            preg_split(
                '/([\s,\.;\?\!]+)/',
                $str,
                $wordCount*2+1,
                PREG_SPLIT_DELIM_CAPTURE
            ),
            0,
            $wordCount*2-1
        )
    );
}

?>
@extends('web-pages.layouts.default')
@section('content')
    <div class="main-video-section">
        <div class="video-area" id="video-area" style="position: relative;">
            <div class="player isMuted" id="main-video-play"
                 data-property="{videoURL:'https://www.youtube.com/watch?time_continue=1&v=eJxA7s9si4A', containment:'#video-area', showControls:false, autoPlay:true, zoom:0, loop:true, mute:true, startAt:0, opacity:1, quality:'low',}"
                 style="display: none; position: relative;"></div>
            <div class="primary-overlay-80" style="position: relative;">
                <div class="main-video-content full-height" style="min-height: 515px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                                <div class="white-color">
                                    <h2 class="bold">MLM V5</h2>
                                    <div class="mt-25">
                                        <h6>Pemasaran Asuransi Melalui Sistem Pemasaran bonus berjenjang/MLM.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-block">
        <div class="container">
            <div class="section-heading center-holder">
                {{--<small>Just make your wish</small>--}}
                <h2 class="bold"> PROGRAM PEMASARAN </h2>
                    {{--<span--}}
                         {{--class="italic libre-baskerville primary-color">rational managers.</span></h2>--}}
                <div class="section-heading-line"></div>
                {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt <br>ut--}}
                    {{--labore et dolore magna aliqua. Ut enim ad minim veniam.</p>--}}
                <p>{!! setting('beranda.beranda_program_pemasaran') !!}.<br></p>
            </div>
            <div class="row mt-40">
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="feature-block inner-5 center-holder"><i class="icon-shield-1"></i>
                        <h4>Proteksi Terlengkap</h4>
                        <p>{!! setting('beranda.proteksi_terlengkap') !!}</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="feature-block inner-5 center-holder"><i class="icon-cooperate"></i>
                        <h4>Premi Terjangkau</h4>
                        {!! setting('beranda.premi_terjangkau') !!}
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-12">
                    <div class="feature-block inner-5 center-holder"><i class="icon-wallet2"></i>
                        <h4>Potensi Income</h4>
                        <p>{!! setting('beranda.potensi_income') !!} </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-block-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-6 col-12"><img src="img/content/marketing/home2.jpg"
                        class="rounded-border shadow-primary mb-10" alt="img"></div>
                <div class="col-md-7 col-sm-6 col-12">
                    <div class="pl-45-md">
                        <div class="section-heading left-holder">
                            <h3 class="bold">Biaya Premi Murah</h3>
                            <div class="section-heading-line"></div>
                        </div>

                        <div class="text-content">{!! setting('beranda.biaya_premi_murah') !!}</div>
{{--                        <ul class="primary-list">--}}
{{--                            <li><i class="fa fa-check-square"></i>Meninggal dunia karena kecelakaan Rp 50.000.000,- s/d Rp 600.000.000.</li>--}}
{{--                            <li><i class="fa fa-check-square"></i>Cacat tetap karena kecelakaan Rp 50.000.000,- s/d Rp 600.000.000.</li>--}}
{{--                            <li><i class="fa fa-check-square"></i>Pengobatan karena kecelakaan Rp 5.000.000,- per kejadian s/d Rp 60.000.000.</li>--}}
{{--                            <li><i class="fa fa-check-square"></i>Santunan pemakaman Rp 5.000.000.</li>--}}
{{--                        </ul>--}}
                    </div>
                </div>
            </div>
            <div class="row mt-80">
                <div class="col-md76 col-sm-7 col-12">
                    <div class="pr-45-md">
                        <div class="section-heading left-holder mt-20">
                            <h3 class="bold">Keunggulan Lain</h3>
                            <div class="section-heading-line"></div>
                        </div>
                        <div class="text-content">{!! setting('beranda.keunggulan_lain') !!}</div>
{{--                        <ul class="primary-list">--}}
{{--                            <li><i class="fa fa-check-square"></i>Perlindungan 24 jam/ hari dan berlaku di seluruh dunia.</li>--}}
{{--                            <li><i class="fa fa-check-square"></i>Termasuk kecelakaan adalah keracunan makanan dan keracunan asap kendaraan.</li>--}}
{{--                            <li><i class="fa fa-check-square"></i>Dan berbagai perluasan pelindungan lain yang akan dijelaskan secara rinci di setiap polis.</li>--}}
{{--                        </ul>--}}
                        {{--<div class="mt-15"><a href="#" class="primary-button button-sm">Learn More</a></div>--}}
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-12"><img src="img/content/marketing/home3.jpg"
                        class="rounded-border shadow-primary mb-10" alt="img"></div>
            </div>
        </div>
    </div>


@stop