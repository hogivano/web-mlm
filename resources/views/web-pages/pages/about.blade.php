@extends('web-pages.layouts.default')
@section('content')
<div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6"
     style="background-image: url('/img/content/bgs/bg1.jpg');">
    <div class="container"><h1>Tentang Kami</h1>
        {{--<ul>--}}
            {{--<li><a href="#">Home</a></li>--}}
            {{--<li><a href="#">Elements</a></li>--}}
            {{--<li><a href="#">Dropcaps</a></li>--}}
        {{--</ul>--}}
    </div>
</div>
<div class="section-block section-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                <div class="small-heading center-holder mb-40"><span class="uppercase"><h2>SEJARAH</h2></span></div>
                <div class="dropcap-1 dropcap-sm text-content-big">
                    {{--<p>A Lorem ipsum dolor sit amet, consectetur--}}
                        {{--adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim--}}
                        {{--veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. Duis aute--}}
                        {{--irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur.--}}
                        {{--Excepteur sint occaecat cupidatat nonproident, sunt in culpa qui officia deserunt mollit anim id est--}}
                        {{--laborum.</p>--}}
                    {!! setting('tentang-kami.sejarah') !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-block-grey section-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                <div class="small-heading center-holder mb-40"><span class="uppercase"><h2>VISI</h2></span></div>
                <blockquote class="bg-white">
                    <h6 class="center" style="text-align: center">{!! setting('tentang-kami.about_visi') !!}</h6>
                </blockquote>
                <div class="small-heading center-holder mb-40"><span class="uppercase"><h2>MISI</h2></span></div>
                <blockquote class="bg-white">
                    <h6  style="text-align: center">{!! setting('tentang-kami.about_misi') !!}</h6>
                </blockquote>
                {{--<div class="dropcap-1 dropcap-sm text-content-big">--}}
                    {{--<p>B Lorem ipsum dolor sit amet, consectetur--}}
                        {{--adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim--}}
                        {{--veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. Duis aute--}}
                        {{--irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur.--}}
                        {{--Excepteur sint occaecat cupidatat nonproident, sunt in culpa qui officia deserunt mollit anim id est--}}
                        {{--laborum.</p>--}}
                    {{--<p class="center" style="color: #000;text-align: center ">Pembentukan kewirausahan Indonesia menuju kesejahteraan anggota.</p>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>

<div class="section-block section-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                <div class="small-heading center-holder mb-40"><span class="uppercase"><h2>PROGRAM PEMASARAN</h2></span></div>
                <div class="text-content-big" style="font-size: 15px;font-weight: 500;color: #999;line-height: 190%;margin-bottom: 15px;">
                    {!! setting('tentang-kami.program_pemasaran') !!}
                    {{--<p style="color: #000; text-align: justify" class="text-black">Smart Solution akan memasarkan produk asuransi kecelakaan dari PT Tokio Marine Insurance Group dan telah mengadakan perjanjian kerjasama eksklusif berkenaan dengan pemasaran produk asuransi yang dimaksudkan. Metode yang dipilih untuk dipakai dalam pemasarannya adalah menggunakan system Jaringan Pemasaran (Network Marketing). System ini memiliki beberapa nilai positif dari sisi pemasaran anatara lain, mengefesiensikan operasional kantor, memberikan penghasilan tambahan bagi nasabah, meminimalisir gagal bayar premi, kecepatan penyebaran pemasaran dan lainnya.--}}
                    {{--<br/>--}}
                    {{--Dengan biaya premi murah yang hanya Rp 150.000,- per tahun nasabah akan mendapatkan perlindungan sebagai berikut :--}}
                    {{--<ul class="primary-list">--}}
                        {{--<li style="margin: 0px;"><i class="fa fa-check-square"></i>Meninggal dunia karena kecelakaan Rp 50.000.000,- s/d Rp 600.000.000.</li>--}}
                        {{--<li style="margin: 0px;"><i class="fa fa-check-square"></i>Cacat tetap karena kecelakaan Rp 50.000.000,- s/d Rp 600.000.000.</li>--}}
                        {{--<li style="margin: 0px;"><i class="fa fa-check-square"></i>Pengobatan karena kecelakaan Rp 5.000.000,- per kejadian s/d Rp 60.000.000.</li>--}}
                        {{--<li style="margin: 0px;"><i class="fa fa-check-square"></i>Santunan pemakaman Rp 5.000.000.</li>--}}
                    {{--</ul>--}}
                    {{--<br>--}}
                    {{--<div class="text-content">--}}
                        {{--<p style="color: #000; text-align: justify">Selain nilai pertanggungan yang lebih lengkap dibandingkan produk asuransi kecelakaan diri dari perusahaan lain dengan nilai premi yang sama, ada keunggulan yang tidak ada ditempat lain, yang hal ini akan bisa menambah minat beli calon nasabah yaitu : </p>--}}
                    {{--</div>--}}
                    {{--<ul class="primary-list">--}}
                        {{--<li style="margin: 0px;"><i class="fa fa-check-square"></i>Perlindungan 24 jam/ hari dan berlaku di seluruh dunia.</li>--}}
                        {{--<li style="margin: 0px;"><i class="fa fa-check-square"></i>Termasuk kecelakaan adalah keracunan makanan dan keracunan asap kendaraan.</li>--}}
                        {{--<li style="margin: 0px;"><i class="fa fa-check-square"></i>Dan berbagai perluasan pelindungan lain yang akan dijelaskan secara rinci di setiap polis.</li>--}}
                    {{--</ul>--}}
                    {{--</p>--}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
