@extends('web-pages.layouts.default')
@section('content')
    <div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6"
         style="background-image: url('{{url('/')}}/img/content/bgs/bg1.jpg');">
        <div class="container">
            <h1>{{$title}}</h1>
        </div>
    </div>
{{--    <div class="section-block">--}}
{{--        <div class="container">--}}
{{--            <div class="section-heading center-holder">--}}
{{--                <h3 class="bold">PT Sibayak Indonesia <span class="italic libre-baskerville primary-color">Gallery</span></h3>--}}
{{--                <div class="section-heading-line"></div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="section-block">
        <div class="container">
            <div class="grid-gallery-4col gallery-style-1 clearfix">
                @foreach($gallery as $gal)
                <?php
                    $link_cropped = substr($gal['photo'],0,-4).'-cropped.jpg';
                ?>
                <div class="gallery-item"><img src="{{url('/')}}/storage/{{substr($gal['photo'],0,-4).'-cropped.jpg'}}" title="image" alt="img">
                    <div class="gallery-overlay">
                        <div class="gallery-content">
                            <div class="gallery-content-inner">
                                <a class="image-popup-gallery" href="{{url('/')}}/storage/{{$gal['photo']}}">
                                    <i class="ti-zoom-in"></i>
                                </a>
{{--                                <a href="#">--}}
{{--                                    <i class="ti-gallery"></i>--}}
{{--                                </a>--}}
                            </div>
                        </div>

                    </div>
                    <p>{{$gal['short_desc']}}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
