@extends('web-pages.layouts.default')
@section('content')
    <style>
        .feature-block-2{
            height: 280px;
        }
    </style>
    <div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6"
        style="background-image: url('/img/content/bgs/bg1.jpg');">
        <div class="container">
            <h1>Marketing Plan</h1>
            {{--<ul>--}}
                {{--<li><a href="#">Home</a></li>--}}
                {{--<li><a href="#">Pages</a></li>--}}
                {{--<li><a href="#">About us minimal style</a></li>--}}
            {{--</ul>--}}
        </div>
    </div>
    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        {{--<small>We're thinking big</small>--}}
                        {{--<h2 class="bold">Delivering results <span class="italic libre-baskerville">that--}}
                                {{--endure</span><br>Technology consultants to the world</h2>--}}
                        {{--<div class="section-heading-line"></div>--}}
                        <h2>Program Pemasaran</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    <div class="text-content-big center-holder mt-25">
                        {{--<p>Smart Solution akan memasarkan produk asuransi kecelakaan dari PT Tokio Marine--}}
                            {{--Insurance Group dan telah mengadakan perjanjian kerjasama eksklusif berkenaan dengan--}}
                            {{--pemasaran produk asuransi yang dimaksudkan.--}}
                            {{--Metode yang dipilih untuk dipakai dalam pemasarannya adalah menggunakan system--}}
                            {{--Jaringan Pemasaran (Network Marketing). System ini memiliki beberapa nilai positif dari--}}

                            {{--sisi pemasaran anatara lain, mengefesiensikan operasional kantor, memberikan--}}
                            {{--penghasilan tambahan bagi nasabah, meminimalisir gagal bayar premi, kecepatan--}}
                            {{--penyebaran pemasaran dan lainnya.--}}
                            {{--</p>--}}
                        {!! setting('marketing-plan.pemasaran') !!}
                    </div>
                </div>
            </div>
{{--            <div class="center-holder mt-10"><a href="#" class="button-md primary-button">More About Us</a></div>--}}
        </div>
    </div>

    <div class="section-block section-sm">
        <div class="container">
            <div class="section-heading center-holder">
                <h2>Jaminan</h2>
            <div class="section-heading-line"></div>
                {{--<p>Menjamin tertanggung selama 24 jam dan di seluruh dunia dari cedera tubuh karena--}}
                    {{--kecelakaan yang tidak disengaja yang semata-mata, secara langsung dan independent--}}
                    {{--dari setiap penyebab lain dari kecelakaan dan disebabkan oleh factor eksternal,--}}
                    {{--bersifat kekerasan dan terlihat, yang mengakibatkan kematian atau cacat permanen--}}
                    {{--atau biaya pengobatan dari tertanggung.</p>--}}
                {!! setting('marketing-plan.jaminan') !!}
            </div>
            <div class="row mt-40">
                <div class="col-md-4 col-sm-4 col-12">
                    <div class="feature-block-2 feature-bordered feature-shadow feature-bg inner-40 center-holder"><i
                            class="icon-shield-1"></i>
                        <h4>Batas Wilayah</h4>
                        {{--<p>seluruh wilayah Republik Indonesia dan di seluruh dunia dalam hal tertanggung--}}
                        {{--berpergian untuk perjalanan bisnis dan atau liburan.</p>--}}
                        {!!  setting('marketing-plan.batas_wilayah') !!}<br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <div class="feature-block-2 feature-bordered feature-shadow feature-bg inner-40 center-holder"><i
                            class="icon-target2"></i>
                        <h4>Usia kepesertaan</h4>
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut--}}
                            {{--labore .</p><a href="#">Learn More <i class="fas fa-chevron-right"></i></a>--}}
                    {{--<p>Batas usia : Minimum 18 tahun dan Maksimum sampai dengan 65 tahun.--}}
                        {!! setting('marketing-plan.usia') !!}
                        <br><br><br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <div class="feature-block-2 feature-bordered feature-shadow feature-bg inner-40 center-holder"><i
                            class="icon-compass2"></i>
                        <h4>Jenis Usaha</h4>
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut--}}
                            {{--labore .</p><a href="#">Learn More <i class="fas fa-chevron-right"></i></a>--}}
                    {{--<p>Pemasaran Asuransi Melalui Sistem Pemasaran bonus berjenjang/MLM.--}}
                        {!! setting('marketing-plan.jenis_usaha') !!}
                        <br><br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        <h2>Keuntungan Penjualan Langsung</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    <div class="text-content-big center-holder mt-25">
                        <div class="row pt-5">
                            <div class="col-12">
                                <img src="http://sibayakindonesia.com/storage/{{ setting('marketing-plan.keuntungan-penjualan-img') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-block section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="section-heading center-holder">
                        <h2>Bonus Level</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    <div class="text-content-big center-holder mt-25">
                        <div class="row pt-3">
                            <div class="col-12">
                                <img src="http://sibayakindonesia.com/storage/{{ setting('marketing-plan.bonus-level-img') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-block-parallax jarallax" data-jarallax data-speed="0.9">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-10 col-12">
                    <div class="section-heading left-holder">
                        <h2>Ketentuan Program</h2>
                        <div class="section-heading-line"></div>
                    </div>
                    {{--<div class="text-content-big mt-10"><p>Dalam menjalankan program pada bisnis ini, peserta diwajibkan mentaati syarat dan ketentuan yang telah ditetapkan oleh perusahaan. Berikut ketentuanya:</p></div>--}}
                    {{--<ul class="primary-list mt-30">--}}
                        {{--<li><i class="fa fa-check-circle"></i>Pembayaran premi dilakukan setiap tanggal 05 & 20 per bulan</li>--}}
                        {{--<li><i class="fa fa-check-circle"></i>Periode Tanggal 01-15 = pembayaran pada tanggal 20</li>--}}
                        {{--<li><i class="fa fa-check-circle"></i>Periode Tanggal 16-30 = pembayaran pada tanggal 05</li>--}}
                        {{--<li><i class="fa fa-check-circle"></i>Rekrut 3 berhak atas bonus 3 level</li>--}}
                        {{--<li><i class="fa fa-check-circle"></i>Rekrut 4 berhak atas bonus 5 level</li>--}}
                        {{--<li><i class="fa fa-check-circle"></i>Rekrut 5 berhak atas bonus 8 level</li>--}}
                    {{--</ul>--}}
                    {!! setting('marketing-plan.program1') !!}
                </div>
                <div class="col-md-6 col-sm-10 col-12">
                    <div class="section-heading left-holder"><h3 class="bold">&nbsp;</h3>
                        <div class="section-heading-line" style="background: white !important"></div>
                    </div>
                    <ul class="primary-list mt-30">
                        {{--<li><i class="fa fa-check-circle"></i>Member diberikan waktu selama 2 bulan (60 hari) sejak bergabung untuk bisa--}}
                            {{--mengajak minimal 3 member baru. Jika tidak dapat memenuhi persyaratan diatas,--}}
                            {{--otomatis akan dipindahkan namanya dari pohon jaringan ke daftar nasabah.--}}
                            {{--Member tersebut masih berhak mendapatkan proteksi dari polis yang dibelinya,--}}
                            {{--tetapi kehilangan hak atas titik usahanya. Apabila suatu saat member tersebut ingin--}}
                            {{--aktif kembali, maka harus mengulangi lagi dari awal.</li>--}}
                        {{--<li><i class="fa fa-check-circle"></i>Member diberi kesempatan untuk cuti premi maksimal 2 bulan / 2x berturut-turut.--}}
                            {{--Apabila member di bulan ke 3 tidak melakukan pembelian ulang, maka ketentuan--}}
                            {{--poin 2 akan diberlakukan terhadap member tersebut.</li>--}}
                        {!! setting('marketing-plan.program2') !!}
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="action-box action-box-md white-bg center-holder-xs pt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-12">
                    <h3 class="bold">Download Marketing Plan Document</h3>
{{--                    {{ json_decode(setting('marketing-plan.upload-doc-marketing-plan'))[0]->download_link}}--}}
                    <p>Untuk keterangan lebih lengkap mengenai Marketing Plan, silakan unduh dokumen dengan klik tombol download berikut.</p>
                </div>
                {{--<div class="col-md-2 col-sm-2 col-12 right-holder center-holder-xs"><a href="http://sibayakindonesia.com/storage/{{ json_decode(setting('marketing-plan.upload-doc-marketing-plan'))[0]->download_link}}" class="button-md primary-button-bordered">Download</a></div>--}}
            </div>
            <div class="row">
                <div class="col-md-10 col-sm-10 col-12">
                    <h3 class="bold">Download Formulir Pendaftaran</h3>
{{--                    {{ json_decode(setting('marketing-plan.upload-doc-marketing-plan'))[0]->download_link}}--}}
                    <p>Bila ingin mendaftarkan diri, silakan unduh dokumen dengan klik tombol download berikut.</p>
                </div>
                {{--@if(storage_path(setting('marketing-plan.upload-doc-form-register'))!="/home/sbyindo8/mlm-sibayak/storage")--}}
                {{--<div class="col-md-2 col-sm-2 col-12 right-holder center-holder-xs"><a href="http://sibayakindonesia.com/storage/{{ json_decode(setting('marketing-plan.upload-doc-form-register'))[0]->download_link}}" class="button-md primary-button-bordered">Download</a></div>--}}
                {{--@endif--}}
            </div>
            <div class="row">
                <div class="col-md-10 col-sm-10 col-12">
                    <h3 class="bold">Download Kode Etik</h3>
{{--                    {{ json_decode(setting('marketing-plan.upload-doc-marketing-plan'))[0]->download_link}}--}}
                    <p>Untuk keterangan lebih lengkap mengenai Kode Etik, silakan unduh dokumen dengan klik tombol download berikut.</p>
                </div>
                {{--@if(storage_path(setting('marketing-plan.kode-etik'))!="/home/sbyindo8/mlm-sibayak/storage")--}}
                    {{--<div class="col-md-2 col-sm-2 col-12 right-holder center-holder-xs"><a href="http://sibayakindonesia.com/storage/{{ json_decode(setting('marketing-plan.kode-etik'))[0]->download_link}}" class="button-md primary-button-bordered">Download</a></div>--}}
                {{--@endif--}}
            </div>
        </div>
    </div>
@stop
