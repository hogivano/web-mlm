@extends('web-pages.layouts.default')
@section('content')
    <div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6"
         style="background-image: url({{url("img/content/bgs/bg1.jpg")}});">>
        <div class="container">
            <h1>Berita</h1>
            {{--<ul>--}}
                {{--<li><a href="{{url("/")}}">Beranda</a></li>--}}
                {{--<li><a href="#">Berita</a></li>--}}
            {{--</ul>--}}
        </div>
    </div>
    <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="row mt--15">
                        @foreach($posts as $post)

                        <div class="col-md-6 col-sm-6 col-12">
                            <div class="blog-grid-2">
                                <div class="blog-grid-img"><img src="storage/{{$post->image}}" alt="img"></div>
                                <div class="blog-grid-text"><h4>{{$post->title}} ...</h4>
                                    <ul>
                                        <li><i class="fa fa-calendar"></i>{{date('d M Y', strtotime($post->created_at))}}</li>
                                    </ul>
                                    <p>{{$post->excerpt}}</p>
                                    <div class="blog-grid-2-button left-holder"><a href="{{url("/detail/".$post->slug)}}">Baca Selengkapnya <i
                                                    class="fa fa-chevron-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
