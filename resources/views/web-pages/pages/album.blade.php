@extends('web-pages.layouts.default')
@section('content')
    <div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6"
         style="background-image: url('/img/content/bgs/bg1.jpg');">
        <div class="container">
            <h1>Galeri Foto</h1>
        </div>
    </div>
    <div class="section-block">
        <div class="container">
{{--            <div class="section-heading center-holder">--}}
{{--                <h3 class="bold">PT Sibayak Indonesia <span class="italic libre-baskerville primary-color">Gallery</span></h3>--}}
{{--                <div class="section-heading-line"></div>--}}
{{--                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt--}}
{{--                    <br>ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>--}}
{{--            </div>--}}
            <div class="row mt-50">
                @foreach($albums as $album)
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="service-block-2">
                        <a href="{{url('/')}}/gallery/{{$album['album_slug']}}">
                            <img src="{{url('/')}}/storage/{{$album['photo']}}" alt="img">
                        </a>
                        <div class="service-block-2-inner">
                            <h4><a href="{{ url('/') }}/gallery/{{$album['album_slug']}}"><?php echo ucfirst($album['album_name']) ?></a></h4>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
