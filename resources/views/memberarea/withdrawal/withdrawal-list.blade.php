@extends('memberarea.master.masterblade')

@section('title','Withdrawal')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Withdrawal</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Withdrawal</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card w_data_1">
                            <div class="body" style="background-color: orange;color: white;">
                                {{--<div class="w_icon blue"><i class="zmdi zmdi-card"></i></div>--}}
                                <h4 class="mt-3">Rp {{number_format($pending,0,',','.')}}</h4>
                                <span>Pending</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card w_data_1">
                            <div class="body" style="background-color: #00A8FF;color: white;">
                                {{--<div class="w_icon dark"><i class="zmdi zmdi-card"></i></div>--}}
                                <h4 class="mt-3">Rp {{number_format($process,0,',','.')}}</h4>
                                <span>Proses</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card w_data_1">
                            <div class="body" style="background-color: red;color: white;">
                                {{--<div class="w_icon indigo"><i class="zmdi zmdi-balance-wallet"></i></div>--}}
                                <h4 class="mt-3">Rp {{number_format($cancel,0,',','.')}}</h4>
                                <span>Cancel</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card w_data_1">
                            <div class="body" style="background-color: limegreen; color: white;">
                                {{--<div class="w_icon indigo"><i class="zmdi zmdi-balance-wallet"></i></div>--}}
                                <h4 class="mt-3">Rp {{number_format($done,0,',','.')}}</h4>
                                <span>Done</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Jumlah</th>
                                            <th>PPN</th>
                                            <th>Jumlah PPN</th>
                                            <th>PPH</th>
                                            <th>Jumlah PPH</th>
                                            <th>Jumlah Diterima</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Done</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($withdrawal_list as $withdrawal_item)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td>Rp {{number_format($withdrawal_item->amount,0,',','.')}}</td>
                                                    <td>{{$withdrawal_item->ppn}}%</td>
                                                    <td>Rp {{number_format($withdrawal_item->ppn_amount,0,',','.')}}</td>
                                                    <td>{{$withdrawal_item->pph}}%</td>
                                                    <td>Rp {{number_format($withdrawal_item->pph_amount,0,',','.')}}</td>
                                                    <td>Rp {{number_format($withdrawal_item->amount - $withdrawal_item->ppn_amount - $withdrawal_item->pph_amount,0,',','.')}}</td>
                                                    <td>
                                                        @if($withdrawal_item->withdrawal_status === 'pending')
                                                            <span class="badge badge-warning">{{$withdrawal_item->withdrawal_status}}</span>
                                                            @elseif($withdrawal_item->withdrawal_status === 'process')
                                                            <span class="badge badge-primary">{{$withdrawal_item->withdrawal_status}}</span>
                                                            @else
                                                            <span class="badge badge-success">{{$withdrawal_item->withdrawal_status}}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{date('D, d M Y H:i:s',strtotime($withdrawal_item->created_at))}}</td>
                                                    <td>{{date('D, d M Y H:i:s',strtotime($withdrawal_item->done_at))}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$withdrawal_list->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>





@endsection
