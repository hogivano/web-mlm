@extends('memberarea.master.masterblade')

@section('title','TokenTransaction')
@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2>Gold Bar</h2>
                        {{--<span class="float-md-right">Jumlah TokenTransaction <h3>{{($token_list->jml_to - $token_list->jml_from)}}</h3></span>--}}
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Gold Bar</li>
                        </ul>
                        {{--<div class="float-md-right">--}}
                            {{--Jumlah TokenTransaction <h3>{{($token_list->jml_to - $token_list->jml_from)}}</h3>--}}
                        {{--</div>--}}
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                        <div class="col-lg-3 col-md-6 col-sm-6 float-right">
                            <div class="card state_w1">
                                <div class="body">
                                    <center>
                                        <span>Jumlah Gold bar</span>
                                        <br>
                                        <span style="font-size: 30px">{{($sisa_membership)}}</span>
                                    </center>

                                    {{--<div class="sparkline" data-type="bar" data-width="97%" data-height="55px" data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#FFC107">5,2,3,7,6,4,8,1</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="body">
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                        {{Session::get('success')}}
                                    </div>
                                @endif
                                <form method="POST" action="/membership/input-goldbar">
                                    @csrf
                                    {{--{{csrf_token()}}--}}
                                    <div class="row clearfix" >
                                        <div class="col-lg-3 col-md-3 col-sm-4 form-control-label">
                                            <label for="autocomplete">Tujuan</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8">
                                            <div class="form-group ui-widget">
                                                <input type="text" name="transaction_to" id="autocomplete" class="form-control members" placeholder="Enter username">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix" >
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-control-label">
                                            <label>Jumlah</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8">
                                            <div class="form-group">
                                                <input type="number" min="1" class="form-control amounts" name="jml_amount" placeholder="Masukan jumlah point">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix" hidden>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-control-label">
                                            <label>Tipe</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8">
                                            <div class="form-group">
                                                <select class="form-control" id="token_types" name="type">
                                                    <option value="membership">Membership</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix" >
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-control-label">
                                            <label>Password</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password" placeholder="Masukan Password Anda">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix" >
                                        <button type="button" id="btn" class="btn btn-primary" data-toggle="modal" data-target="#largeModal">Kirim</button>
                                    </div>
                                    {{-- Modals --}}
                                    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="title" id="largeModalLabel">Konfirmasi Transfer Token</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table table-responsive">
                                                        <table class="table table-striped">
                                                            <tr>
                                                                <td>To Member</td>
                                                                <td>:</td>
                                                                <td class="member"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Amount</td>
                                                                <td>:</td>
                                                                <td class="amount"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Token Type</td>
                                                                <td>:</td>
                                                                <td id="token_type"></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-primary waves-effect">Kirim</button>
                                                        <button class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Dari</th>
                                            <th>Untuk</th>
                                            <th>Jenis Transaksi</th>
                                            <th style="width: 25px;">Jumlah</th>
                                            <th>Status</th>
                                            <th>Tipe Token</th>
                                            <th>Tanggal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $value)
                                            {{--                                                    @php(array_pop($data))--}}
                                            <tr>
                                                <td>{{$no++}}</td>
                                                @if($value->transaction_from == 0)
                                                    <td>Admin</td>
                                                @else
                                                    <td>{{$value->memberFrom->member_username}}</td>
                                                @endif
                                                <td>{{$value->MemberTo->member_name}}</td>
                                                <td>{{$value->transaction_type}}</td>
                                                @if($value->transaction_type === 'registrasi')
                                                    <td><span class="float-right">-{{$value->token_amount}}</span> </td>
                                                @else
                                                    <td><span class="float-right">{{$value->token_amount}}</span> </td>
                                                @endif
                                                <td>{{$value->transaction_status}}</td>
                                                <td>{{$value->token_type}}</td>
                                                <td>{{date("D, d M Y h:i:s",strtotime($value->created_at))}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{$data->appends(request()->input())->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>--}}

    <script>
        $("#btn").click(function () {
            $(".member").html($(".members").val());
            $("#token_type").html($("#token_types").val());
            $(".amount").html($(".amounts").val() + " Poin");

            // var bilangan = $(".amounts").val();
            //
            // var	number_string = bilangan.toString(),
            //     sisa 	= number_string.length % 3,
            //     rupiah 	= number_string.substr(0, sisa),
            //     ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
            //
            // if (ribuan) {
            //     separator = sisa ? '.' : '';
            //     rupiah += separator + ribuan.join('.');
            // }
            // $(".amount").html("Rp "+rupiah);
        });
    </script>

    <script type="text/javascript">
        // Single Select
        $("#autocomplete").autocomplete({
            source: function (request, response) {
                // Fetch data
                $.ajax({
                    url: "/membership/get-username",
                    type: 'get',
                    dataType: "json",
                    data: {
                        search: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#autocomplete').val(ui.item.value); // display the selected text
                // $('#selectuser_id').val(ui.item.value); // save selected id to input
                return false;
            }
        });

        $( function() {
        });
        function split( val ) {
            return val.split( /,\s*/ );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }
    </script>


@endsection
