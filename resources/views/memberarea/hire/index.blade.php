@extends('memberarea.master.masterblade')

@section('title','Profile')
@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Hire</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Camps</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                {{--<h3>Jumlah TokenTransaction : <b>{{($token_list->jml_to - $token_list->jml_from)}}</b></h3>--}}
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Order ID</th>
                                            <th>Character</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Order Time</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{-- @foreach($polish_list as $polish_item)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>Rp {{number_format($polish_item->nominal,0,',','.')}}</td>
                                                <td>{{date('M Y',strtotime($polish_item->polis_date))}}</td>
                                                <td>{{$polish_item->pp_status}}</td>
                                                <td>{{date('D, d M Y H:i:s',strtotime($polish_item->created_at))}}</td>

                                                <td>
                                                    @if ($polish_item->file_polish !== "")
                                                        <a href="http://sibayakindonesia.com/storage/{{ json_decode($polish_item->file_polish)[0]->download_link}}" class="btn btn-sm"><i class="zmdi zmdi-download"></i> Unduh</a>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>

                                        @endforeach --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endsection