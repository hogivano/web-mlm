@extends('memberarea.master.masterblade')

@section('title','Tambah Member')

@section('content')

    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Pohon Jaringan</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Tambah Member</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong><i class="zmdi zmdi-money"></i> Tambah</strong> Member</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="center col-lg-12 col-md-12">
                                        @if(Session::has('error'))
                                                <div class="alert alert-danger">
                                                    {{Session::get('error')}}
                                                </div>
                                            @endif
                                        <form action="/membership/input-member" method="post" autocomplete="off"  >
                                            {{csrf_field()}}
                                            <input type="hidden" name="upline_id" value="{{$upline_id}}">
                                            <input type="hidden" name="kaki" value="{{$kaki}}">
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Sponsor
                                                        <input type="text" class="form-control" name="leader" value="{{Session::get('name')}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Upline
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" name="upline" value="{{$member_item->member_name}}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Username
                                                        <input autocomplete="false" type="text" class="form-control no-resize" name="username" placeholder="Username" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Nama Member
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" name="name" placeholder="Nama Member" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Tempat Lahir
                                                        <input type="text" class="form-control no-resize" name="member_pob" placeholder="Isikan Tempat Lahir Member" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Tanggal Lahir
                                                        <input type="date" class="form-control" name="date" placeholder="Start Date" required/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    Gender
                                                    <div class="form-group">
                                                        <select class="select form-control" name="gender" required>
                                                            <option>Pria</option>
                                                            <option>Wanita</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Pekerjaan Member
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" name="member_work" placeholder="Pekerjaan Member" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Pewaris Member
                                                        <input type="text" class="form-control no-resize" name="member_waris" placeholder="Nama Pewaris Member" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Member Hubungan
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" name="member_hub" placeholder="Nama Hubungan Pewaris Member" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Negara
                                                        <input type="text" class="form-control no-resize" name="country" value="Indonesia" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Provinsi
                                                    <div class="form-group">
                                                        <select name="province" class="form-control" id="province">
                                                            <option class="form-control" value="">Select Province</option>
                                                            @foreach($data as $value)
                                                                <option value="{{$value->propinsi_id}}">{{$value->propinsi_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Kota
                                                        <select name="city" class="form-control" id="city">
                                                            <option class="form-control" value="">Select City</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Alamat
                                                    <div class="form-group">
                                                        <textarea name="alamat" placeholder="Masukan Alamat" class="form-control" required></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Postal Code
                                                        <input type="number" class="form-control" min="0" name="postal_code" placeholder="Masukan Kode Post" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Identitas Member
                                                    <div class="form-group">
                                                        <select class="select form-control" name="member_identity" required>
                                                            <option value="ktp">KTP</option>
                                                            <option value="sim">SIM</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Nomor Identitas
                                                        <input type="number" class="form-control no-resize" name="number_identity" placeholder="Nomor Identitas" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Nomor Telepon
                                                    <div class="form-group">
                                                        <input type="number" class="form-control no-resize" name="no_hp" placeholder="No HP" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Email
                                                        <input type="email" class="form-control no-resize" name="email" placeholder="Email" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Password
                                                    <div class="form-group">
                                                        <input type="password" class="form-control no-resize" name="password" placeholder="Password" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Konfirmasi Password
                                                        <input type="password" class="form-control no-resize" name="confirm_password" placeholder="Confirm Password" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Member Bank
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" name="member_bank" placeholder="Isikan jenis Bank Member" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Nomor Rekening
                                                        <div class="form-group">
                                                            <input type="text" class="form-control no-resize" name="member_bank_number" placeholder="Isikan Nomor Rekening" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Kode Voucher
                                                    <div class="form-group">
                                                        <input type="number" class="form-control no-resize" name="code_voucher" placeholder="Kode Voucher" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Member Bank Name
                                                        <input type="text" class="form-control no-resize" name="member_bank_account" placeholder="Isikan Nama Bank Member" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Kode Pin
                                                    <input type="text" class="form-control no-resize" name="code_pin" placeholder="Kode PIN" required>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Member Bank Branch
                                                        <input type="text" class="form-control no-resize" name="member_bank_branch" placeholder="Isikan Daerah saat pendaftaran Bank" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-line">
                                                    <button class="btn btn-success">Register</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#province').on('change', function() {
                var provinceID = $(this).val();
                if(provinceID) {
                    $.ajax({
                        url: '/membership/get-city-list/'+encodeURI(provinceID),
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                            $('#city').empty();
                            $.each(data, function(key, value) {
                                $('#city').append('<option value="'+ value.city_id +'">'+ value.city_name +'</option>');
                            });
                        }
                    });
                }else{
                    $('#city').empty();
                }
            });
        });
    </script>
@endsection
