@extends('memberarea.master.masterblade')

@section('title','Tambah Member')

@section('content')

    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Pohon Jaringan</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/home"><i class="zmdi zmdi-home"></i> Home</a></li>
                            <li class="breadcrumb-item active">Pohon Jaringan</li>
                            <li class="breadcrumb-item active">Tambah Member</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                    class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                <div class="row clearfix">
                                    <div class="center col-lg-12 col-md-12">
                                        <div class="alert alert-warning">
                                            <span>Pastikan posisi upline dan kaki sesuai yang anda inginkan,
                                            saat ini berada di <b style="color:red">posisi upline {{$member_item->member_name}} dan kaki {{$kaki}}</b>,
                                            Setelah pengisian data selesai <b>DATA TIDAK DAPAT DI UBAH KEMBALI</b>.</span>
                                        </div>
                                        <form action="/membership/register-member" method="post" autocomplete="off" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <input type="hidden" name="upline_id" value="{{$upline_id}}">
                                            <input type="hidden" name="kaki" value="{{$kaki}}">
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Sponsor
                                                        <input type="text" class="form-control" name="leader"
                                                               value="{{Auth::guard('member')->user()->member_username}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Upline
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" name="upline"
                                                               value="{{$member_item->member_name}}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Username
                                                        <input autocomplete="false" type="text" value="{{ old('username') }}"
                                                               class="form-control no-resize" name="username"
                                                               placeholder="Username" >
                                                        <span class="badge">* Username tidak boleh mengandung spasi.</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Nama Member
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" name="name" value="{{ old('name') }}"
                                                               placeholder="Nama Member" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Tempat Lahir
                                                        <input type="text" class="form-control no-resize" value="{{ old('member_pob') }}"
                                                               name="member_pob"
                                                               placeholder="Isikan Tempat Lahir Member" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Tanggal Lahir
                                                        <input type="date" class="form-control" name="dob" value="{{ old('dob') }}"
                                                               placeholder="Start Date" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    Jenis Kelamin
                                                    <div class="form-group">
                                                        <select class="select form-control" name="gender" >
                                                            <option {{ (old("gender") == "Pria" ? "selected":"") }}>Pria</option>
                                                            <option {{ (old("gender") == "Wanita" ? "selected":"") }}>Wanita</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Pekerjaan Member
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" value="{{ old('member_work') }}"
                                                               name="member_work" placeholder="Pekerjaan Member"
                                                               >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Pewaris Member
                                                        <input type="text" class="form-control no-resize" value="{{ old('member_waris') }}"
                                                               name="member_waris" placeholder="Nama Pewaris Member"
                                                               >
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Member Hubungan
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" value="{{ old('member_hub') }}"
                                                               name="member_hub"
                                                               placeholder="Nama Hubungan Pewaris Member" >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Negara
                                                        <input type="text" class="form-control no-resize" name="country"
                                                               value="Indonesia" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Provinsi
                                                    <div class="form-group">
                                                        <select name="province" class="form-control" id="province">
                                                            <option class="form-control" value="">Select Province
                                                            </option>
                                                            @foreach($data as $value)
                                                                <option value="{{$value->propinsi_id}}"  {{ (old("province") == $value->propinsi_id ? "selected":"") }}>{{$value->propinsi_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Kota
                                                        <select name="city" class="form-control" id="city" data="{{old("city")}}">
                                                            <option class="form-control" value="">Select City</option>
                                                            @if(Session::has('cities'))
                                                                @foreach(Session::get('cities') as $value)
                                                                    <option value="{{$value->city_id}}"  {{ (old("city") == $value->city_id ? "selected":"") }}>{{$value->city_name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Alamat
                                                    <div class="form-group">
                                                        <textarea name="alamat" placeholder="Masukan Alamat" 
                                                                  class="form-control" >{{ old('alamat') }}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Kode Pos
                                                        <input type="number" class="form-control" min="0" value="{{ old('postal_code') }}"
                                                               name="postal_code" placeholder="Masukan Kode Pos"
                                                               >
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Identitas Member
                                                    <div class="form-group">
                                                        <select class="select form-control" name="member_identity"
                                                                >
                                                            <option value="ktp"  {{ (old("member_identity") == "ktp" ? "selected":"") }}>KTP</option>
{{--                                                            <option value="sim" {{ (old("member_identity") == "sim" ? "selected":"") }}>SIM</option>--}}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Nomor Identitas
                                                        <input type="number" class="form-control no-resize" value="{{ old('number_identity') }}"
                                                               name="number_identity" placeholder="Nomor Identitas"
                                                               >
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Upload KTP
                                                        <input type="file" class="form-control no-resize" value="{{ old('ktp_picture') }}"
                                                               name="ktp_picture" >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Email
                                                        <input type="email" class="form-control no-resize" name="email" value="{{ old('email') }}"
                                                               placeholder="Email" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Nomor HP
                                                    <div class="form-group">
                                                        <input type="number" class="form-control no-resize" name="no_hp" value="{{ old('no_hp') }}"
                                                               placeholder="No HP" >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-5">
                                                    Password
                                                    <div class="form-group">
                                                        <input type="password" class="form-control no-resize"
                                                               name="password" placeholder="Password" id="myInput">
                                                    </div>
                                                </div>
                                                <div class="col-md-1 input-group-append">
                                                    <span class="input-group-text field-icon" style="height: 50%; margin-top: 40%;margin-left: -40%;"><button type="button" onclick="myFunction()" class="btn btn-default" style="color:grey; background: transparent;border-color: transparent"><i class="zmdi zmdi-eye form-control-feedback"></i></></span>
                                                </div>

                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        Konfirmasi Password
                                                        <input type="password" class="form-control no-resize"
                                                               name="password_confirmation" placeholder="Confirm Password"
                                                               id="konfirmasi">
                                                    </div>
                                                </div>
                                                <div class="col-md-1 input-group-append">
                                                    <span class="input-group-text field-icon" style="height: 50%; margin-top: 40%;margin-left: -40%;"><button type="button" onclick="Mykonfirmasi()" class="btn btn-default" style="color:grey; background: transparent;border-color: transparent"><i class="zmdi zmdi-eye form-control-feedback"></i></></span>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    Jenis Member
                                                    <div class="form-group">
                                                        <select class="select form-control" name="member_type">
                                                            <option value="member"  {{ (old("member_type") == "member" ? "selected":"") }}>Member Bisnis</option>
                                                            {{--                                                            <option value="nasabah" {{ (old("member_type") == "nasabah" ? "selected":"") }}>Nasabah Asuransi</option>--}}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Nomor Rekening
                                                        <div class="form-group">
                                                            <input type="text" class="form-control no-resize" value="{{ old('member_bank_number') }}"
                                                                   name="member_bank_number"
                                                                   placeholder="Isikan Nomor Rekening" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Nama Akun Bank
                                                        <input type="text" class="form-control no-resize" value="{{ old('member_bank_account') }}"
                                                               name="member_bank_account"
                                                               placeholder="Isikan Nama Bank Member" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Nama Bank
                                                    <div class="form-group">
                                                        <input type="text" class="form-control no-resize" value="{{ old('member_bank') }}"
                                                               name="member_bank" placeholder="Isikan nama Bank Member"
                                                        >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        Cabang Bank
                                                        <input type="text" class="form-control no-resize" value="{{ old('member_bank_branch') }}"
                                                               name="member_bank_branch"
                                                               placeholder="Cabang Bank"
                                                        >
                                                    </div>
                                                </div>

                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        Password Sponsor
                                                        <input type="password" class="form-control no-resize"
                                                               name="sponsor_password" placeholder="Password Anda"
                                                               id="sponsor">
                                                    </div>
                                                </div>
                                                <div class="col-md-1 input-group-append">
                                                    <span class="input-group-text field-icon" style="height: 50%; margin-top: 40%;margin-left: -40%;"><button type="button" onclick="Sponsor()" class="btn btn-default" style="color:grey; background: transparent;border-color: transparent"><i class="zmdi zmdi-eye form-control-feedback"></i></></span>
                                                </div>
                                            </div>


                                            <div class="form-group ">
                                                <div class="form-line">
                                                    <button type="submit" name="register" class="btn btn-success">Register</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        function myFunction() {
            var x = document.getElementById("myInput");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        function Mykonfirmasi() {
            var y = document.getElementById("konfirmasi");
            if (y.type === "password") {
                y.type = "text";
            } else {
                y.type = "password";
            }
        }

        function Sponsor() {
            var z = document.getElementById("sponsor");
            if (z.type === "password") {
                z.type = "text";
            } else {
                z.type = "password";
            }
        }
        // $(".toggle-password").click(function() {
        //
        //     $(this).toggleClass("fa-eye fa-eye-slash");
        //     var input = $($(this).attr("toggle"));
        //     if (input.attr("type") == "password") {
        //         input.attr("type", "text");
        //     } else {
        //         input.attr("type", "password");
        //     }
        // });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#province').on('change', function () {
                var provinceID = $(this).val();
                if (provinceID) {
                    $.ajax({
                        url: '/membership/get-city-list/' + encodeURI(provinceID),
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('#city').empty();
                            $.each(data, function (key, value) {
                                $('#city').append('<option value="' + value.city_id + '">' + value.city_name + '</option>');
                            });
                            $('#city').selectpicker('refresh');
                        }
                    });
                } else {
                    $('#city').empty();
                }
            });
        });
    </script>
@endsection
