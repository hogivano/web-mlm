@extends('memberarea.master.masterblade')

@section('title','Profile')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Top Up</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Top Up</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong><i class="zmdi zmdi-money"></i> Top Up</strong></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <form class="form-horizontal" action="/membership/proses-input-topup" method="POST">
                                    {{csrf_field()}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label for="email_address_2">Bank Tujuan</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="{{$bank_item->bank_name}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label for="email_address_2">Dari Bank</label>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-2">
                                            <div class="form-group">
                                                <select name="bank_name" class="form-control" required>
                                                    <option>BCA</option>
                                                    <option>BNI</option>
                                                    <option>Mandiri</option>
                                                    <option>BRI</option>
                                                    <option>Sinarmas</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-2">
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="bank_number" min="0" placeholder="Masukan No Rek" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-2">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="bank_account" placeholder="Masukan A/N Rek" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label for="email_address_2">Jumlah Premi</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <select name="jumlah_premi" class="form-control" required>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    <option>6</option>
                                                    <option>7</option>
                                                    <option>8</option>
                                                    <option>9</option>
                                                    <option>10</option>
                                                    <option>11</option>
                                                    <option>12</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label for="email_address_2">Jumlah Nominal</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <input type="number" class="form-control" min="1" name="jumlah_nominal" placeholder="Masukan Jumlah nominal" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-8 offset-sm-2">
                                            <button class="btn btn-raised btn-primary btn-round waves-effect">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>





@endsection
