<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Test</title>
    <link rel="stylesheet" href="asset/css/styleae52.css?v=5">

    <link href="fontsawesome/css/all.css" rel="stylesheet"> <!--load all styles -->
    <script defer src="fontsawesome/js/all.js"></script> <!--load all styles -->
    <link href="fontsawesome/css/fontawesome.css" rel="stylesheet">
    <link href="fontsawesome/css/brands.css" rel="stylesheet">
    <link href="fontsawesome/css/solid.css" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <link rel="apple-touch-icon" sizes="180x180" href="asset/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="asset/img/favicon.png" sizes="32x32">
    <link rel="shortcut icon" href="asses/img/favicon.png">
</head>

<body class="bg-white">

    @include('memberarea.masterbase.sidebar')

    @yield('content')
  <!-- * App Sidebar -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="asset/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="asset/js/lib/popper.min.js"></script>
    <script src="asset/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    {{-- <script src="unpkg.com/ionicons%405.0.0/dist/ionicons.js"></script> --}}
    <!-- Owl Carousel -->
    <script src="asset/js/plugins/owl-carousel/owl.carousel.min.js"></script>
</body>
</html>