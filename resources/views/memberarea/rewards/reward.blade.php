@extends('memberarea.master.masterblade')

@section('title','Reward History')

@section('content')
    <style>
        a:hover{
            color: #0b0b0b;
        }
        .text {
            position:relative;
            bottom:30px;
            left:0px;
            visibility:hidden;
        }
        .test-widget{
            margin-left: 75%;
            margin-right: 2%;
        }
    </style>
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Reward History</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Reward History</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="test-widget">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                        <div class="card">
                            <div class="body" style="background: rgba(111,66,193,0.5) !important; color: white">
                                <span>Status Pencapaian <a href="#" data-toggle="tooltip" title="Perbandingan antara Kaki downline terbanyak dengan Jumlah kaki downline lainnya."><span class="zmdi zmdi-help-outline"></span></a>
                                {{--<input type="text" class="knob" value="42" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#00adef" readonly>--}}
                                <div class="d-flex bd-highlight text-center mt-4">
                                    <div class="flex-fill bd-highlight">
                                        <small>Kaki Terbanyak</small>
                                        <h5 class="mb-0">{{$downline}}</h5>
                                    </div>
                                    <div class="flex-fill bd-highlight">
                                        <small>Jumlah Kaki Lain</small>
                                        <h5 class="mb-0">{{$sum}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Reward</th>
                                            <th>Traget</th>
                                            <th>Status</th>
                                            <th>Complete Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($reward_list as $reward_item)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td>{{$reward_item->reward_name}}</td>
                                                    <td>{{$reward_item->reward_target}}</td>
                                                        @if(!isset($reward_item->rewardId[0]))
                                                            <td align="center">-</td>
                                                            <td align="center">-</td>
                                                        @else
                                                            <td>
                                                                @if($reward_item->rewardId[0]->rh_status === 'waiting')
                                                                    <span class="badge badge-warning">{{$reward_item->rewardId[0]->rh_status}}</span>
                                                                @elseif($reward_history->rh_status === 'completed')
                                                                    <span class="badge badge-primary">{{$reward_item->rewardId[0]->rh_status}}</span>
                                                                @else
                                                                    <span class="badge badge-success">{{$reward_item->rewardId[0]->rh_status}}</span>
                                                                @endif
                                                            </td>
                                                            <td>{{$reward_item->rewardId[0]->created_at}}</td>
                                                        @endif
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <p class="text">text</p>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>



@endsection
