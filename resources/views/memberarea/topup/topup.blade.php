@extends('memberarea.master.masterblade')

@section('title','Profile')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Top Up</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Top Up</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                        {{Session::get('success')}}
                                    </div>
                                @endif
                                @if($topup_count > 0)
                                    <div class="alert alert-danger">
                                        Maaf saat ini anda tidak dapat melakukan topup dikarenakan topup anda sebelumnya sedang kami proses, terimakasih.
                                    </div>
                                @endif
                                <form class="form-horizontal" action="/membership/proses-input-topup" method="POST" enctype="multipart/form-data">
                                {{--<form class="form-horizontal" action="#" id="form_id" method="" enctype="multipart/form-data">--}}
                                    {{csrf_field()}}
                                    {{--<input type="hidden" name="member_id" value="{{Auth::guard('member')->user()->member_id}}">--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label for="email_address_2">Bank Tujuan</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <select name="bank" class="form-control" id="bank" required>
                                                    @foreach($bank_list as $bank_item)
                                                        <option value="{{$bank_item->bank_id}}">{{$bank_item->bank_name}} - {{$bank_item->bank_number}} - {{$bank_item->bank_account}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label for="email_address_2">Dari Bank</label>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                            <div class="form-group">
                                                <select name="bank_name" class="form-control" id="bank_name" required>
                                                    <option>BCA</option>
                                                    <option>BNI</option>
                                                    <option>Mandiri</option>
                                                    <option>BRI</option>
                                                    <option>Sinarmas</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-2">
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="bank_number" min="0" id="norek" placeholder="Masukan No Rek" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-2">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="bank_account" id="bank_account" placeholder="Masukan A/N Rek" required>
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="row clearfix">--}}
                                        {{--<div class="col-lg-2 col-md-2 col-sm-4 form-control-label">--}}
                                            {{--<label for="email_address_2">Jumlah Premi</label>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-lg-8 col-md-8 col-sm-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select name="jumlah_premi" class="form-control" id="jml_premi" required>--}}
                                                    {{--<option value="1">1 Bulan</option>--}}
                                                    {{--<option value="2">2 Bulan</option>--}}
                                                    {{--<option value="3">3 Bulan</option>--}}
                                                    {{--<option value="4">4 Bulan</option>--}}
                                                    {{--<option value="5">5 Bulan</option>--}}
                                                    {{--<option value="6">6 Bulan</option>--}}
                                                    {{--<option value="7">7 Bulan</option>--}}
                                                    {{--<option value="8">8 Bulan</option>--}}
                                                    {{--<option value="9">9 Bulan</option>--}}
                                                    {{--<option value="10">10 Bulan</option>--}}
                                                    {{--<option value="11">11 Bulan</option>--}}
                                                    {{--<option value="12">12 Bulan</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label for="email_address_2">Jumlah Nominal</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <input type="number" class="form-control" min="1" name="jumlah_nominal" id="nominal" placeholder="Masukan Jumlah nominal"  required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label for="email_address_2">Bukti Transfer</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <input type="file" class="form-control" min="1" id="bukti_transfer" name="bukti_transfer" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-8 offset-sm-2">
                                            @if($topup_count==0)
                                            <button type="button" id="btn" class="btn btn-primary waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">Sinpan</button>
                                            {{--<button id="btn" class="btn btn-default">Simpan</button>--}}
                                                @endif
                                        </div>
                                    </div>
                                {{--</form>--}}

                                <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="title" id="largeModalLabel">Konfirmasi Top Up</h4>
                                            </div>
                                            <div class="modal-body">
                                                    {{csrf_field()}}
                                                    {{--<div hidden>--}}
                                                        {{--<input type="text" name="bank" class="bank_id">--}}
                                                        {{--<input type="text" name="member_id" value="{{Session::get('user_item')->member_id}}">--}}
                                                        {{--<input type="text" class="bank_names" name="bank_name" value="">--}}
                                                        {{--<input type="text" class="bank_accounts" name="bank_account" value="">--}}
                                                        {{--<input type="text" class="bank_numbers" name="bank_number" value="">--}}
                                                        {{--<input type="text" class="jumlah_premis" name="jumlah_premi" value="">--}}
                                                        {{--<input type="text" class="jumlah_nominals" name="jumlah_nominal" value="">--}}
                                                        {{--<input type="file" class="bukti_transfer" name="gambar" value="">--}}
                                                     {{--</div>--}}
                                                    <div class="table table-responsive">
                                                        <table class="table table-striped">
                                                            <tr>
                                                                <td>To Bank</td>
                                                                <td>:</td>
                                                                <td class="banks"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>From Bank</td>
                                                                <td>:</td>
                                                                <td class="bank_names1"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>No. Rek</td>
                                                                <td>:</td>
                                                                <td class="noreks"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Atas Nama</td>
                                                                <td>:</td>
                                                                <td class="an"></td>
                                                            </tr>
                                                            {{--<tr>--}}
                                                                {{--<td>Jumlah Premi</td>--}}
                                                                {{--<td>:</td>--}}
                                                                {{--<td class="premis"></td>--}}
                                                            {{--</tr>--}}
                                                            <tr>
                                                                <td>Jumlah Nominal</td>
                                                                <td>:</td>
                                                                <td class="nominals"> </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-primary waves-effect">Topup</button>
                                                        <button class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $("#btn").click(function() {
        var bank = $("#bank").val();
        var jml_premi_text = $("#jml_premi option:selected").text();
        var jml_premi_val = $("#jml_premi option:selected").val();


        //input value
        $(".bank_names").val($("#bank_name").val());
        $(".bank_id").val($("#bank option:selected").val());
        $(".bank_accounts").val($("#bank_account").val());
        $(".bank_numbers").val($("#norek").val());
        // $(".jumlah_premis").val($("#jml_premi").val());
        $(".jumlah_nominals").val($("#nominal").val());
        //Value

            $(".banks").html($("#bank option:selected").text());
            $(".bank_names1").html($("#bank_name").val());
            $(".noreks").html($("#norek").val());
            $(".an").html($("#bank_account").val());
            // $(".premis").html($("#jml_premi option:selected").text());
            $(".bukti_tranfers").val($("#bukti_transfer").val());
            //Rupiah
            var bilangan = $("#nominal").val();

            var	number_string = bilangan.toString(),
                sisa 	= number_string.length % 3,
                rupiah 	= number_string.substr(0, sisa),
                ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            $(".nominals").html("Rp "+rupiah);


    // $(".banks").val($("#bank option:selected").val());


    // alert($(".bank_names").val());
    // if (validation()) // Calling validation function.
    // {
    // $("form[name='form_name']").submit(); // Form Submission
    // alert(" Bank : " + bank + " n Bank Name : " + bank_name + " n Form name : " + $("form[name='form_name']").attr('name') + "nn Form Submitted Successfully......");
    // }
     });
    </script>


@endsection
