@extends('memberarea.master.masterblade')

@section('title','Top Up History')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Top Up History</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Top Up History</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Bank Name</th>
                                            <th>No Rekening</th>
                                            <th>Nominal</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Detail</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($topup_list as $topup_item)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td>{{$topup_item->topup_bank_from}}</td>
                                                    <td>{{$topup_item->topup_rek_no_from}}</td>
                                                    <td>Rp. {{number_format($topup_item->topup_nominal,0,',','.')}}</td>
                                                    <td>{{date("D, d M Y",strtotime($topup_item->create_at))}}</td>
                                                    <td>
                                                        @if($topup_item->topup_status === 'new')
                                                            <span class="badge badge-primary">{{$topup_item->topup_status}}</span>
                                                        @elseif($topup_item->topup_status === 'process')
                                                            <span class="badge badge-warning">{{$topup_item->topup_status}}</span>
                                                        @elseif($topup_item->topup_status === 'done')
                                                            <span class="badge badge-success">{{$topup_item->topup_status}}</span>
                                                        @else
                                                            <span class="badge badge-danger">{{$topup_item->topup_status}}</span>
                                                        @endif
                                                    </td>
                                                    <td><a href="/membership/detail-history-topup/{{$topup_item->topup_id}}"><span class="zmdi zmdi-eye"></span> </a> </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>





@endsection
