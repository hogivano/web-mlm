@extends('memberarea.master.masterblade')

@section('title','Profile')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Top Up History</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Top Up History</li>
                            <li class="breadcrumb-item active">Detail</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responve">
                                    <table class="table table-striped">
                                        <tr>
                                            <td>Nama Bank Tujuan</td>
                                            <td>:</td>
                                            <td>{{$topup->Bank->bank_name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Rekening Bank Tujuan</td>
                                            <td>:</td>
                                            <td>{{$topup->Bank->bank_number}}</td>
                                        </tr>
                                        <tr>
                                            <td>Pemilik Bank Tujuan</td>
                                            <td>:</td>
                                            <td>{{$topup->Bank->bank_account}}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Bank Anda</td>
                                            <td>:</td>
                                            <td>{{$topup->topup_bank_from}}</td>
                                        </tr>
                                        <tr>
                                            <td>Rekening Bank Anda</td>
                                            <td>:</td>
                                            <td>{{$topup->topup_rek_no_from}}</td>
                                        </tr>
                                        <tr>
                                            <td>Atas Nama</td>
                                            <td>:</td>
                                            <td>{{$topup->topup_name_from}}</td>
                                        </tr>
                                        <tr>
                                            <td>Nominal</td>
                                            <td>:</td>
                                            <td>{{$topup->topup_nominal}}</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>:</td>
                                            <td>{{$topup->topup_status}}</td>
                                        </tr>
                                        <tr>
                                            <td>Bukti Transfer</td>
                                            <td>:</td>
                                            <td><button type="button" data-toggle="modal" data-target="#largeModal"><img src="/img/topup/{{$topup->bukti_transfer}}" width="450px" height="200px"/></button></td>
                                        </tr>
                                    </table>
                                    {{--<a id="single_image" href="/img/topup/{{$topup->bukti_transfer}}"><img src="/img/topup/{{$topup->bukti_transfer}}" alt=""/></a>--}}
                                    {{--<a id="inline" href="#data">This shows content of element who has id="data"</a>--}}

                                    {{--<div style="display:none"><div id="data">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div></div>--}}
                                    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="title" id="largeModalLabel">Bukti Transfer</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <center><img src="/img/topup/{{$topup->bukti_transfer}}" width="900px" height="400px"/></center>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <script type="text/javascript">
        $(document).ready(function() {

            /* This is basic - uses default settings */

            $("a#single_image").fancybox({
                'transitionIn'	:	'elastic',
                'transitionOut'	:	'elastic',
                'speedIn'		:	600,
                'speedOut'		:	200,
                'overlayShow'	:	false
            });


            /* Using custom settings */

            $("a#inline").fancybox({
                'transitionIn'	:	'elastic',
                'transitionOut'	:	'elastic',
                'speedIn'		:	600,
                'speedOut'		:	200,
                'overlayShow'	:	false
                'hideOnContentClick': true
            });

            /* Apply fancybox to multiple items */

            $("a.group").fancybox({
                'transitionIn'	:	'elastic',
                'transitionOut'	:	'elastic',
                'speedIn'		:	600,
                'speedOut'		:	200,
                'overlayShow'	:	false
            });

        });
    </script>

@endsection
