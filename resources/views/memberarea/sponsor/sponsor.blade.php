@extends('memberarea.master.masterblade')

@section('title','Sponsor Ship')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Sponsorship</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> Home</a></li>
                            <li class="breadcrumb-item active">Sponsorship</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                        {{Session::get('success')}}
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                <form method="GET">
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <input type="keyword" class="form-control" name="keyword" value="{{request('keyword')}}" placeholder="Keyword">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <select class="form-control" name="type">
                                                    <option value="">-</option>
                                                    <option value="member" @if(request('type') === 'member') selected @endif>Member</option>
                                                    <option value="nasabah" @if(request('type') === 'nasabah') selected @endif>Nasabah</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <button class="btn btn-raised btn-primary btn-round waves-effect m-l-20">Search</button> <a href="/membership/sponsor-ship" class="btn btn-raised btn-default btn-round waves-effect m-l-20">Reset</a>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Member Name</th>
                                            <th>Date</th>
                                            <th>Tipe</th>
                                            <th>Status</th>
                                            <th>Detail</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($sponsor_list as $sponsor_item)
                                            <tr>
                                                <th scope="row">{{$no++}}</th>
                                                <td>{{$sponsor_item->member_name}}</td>
                                                <td>{{date('D, d M Y',strtotime($sponsor_item->created_at))}}</td>
                                                <td>{{$sponsor_item->member_type}}</td>
                                                <td>@if($sponsor_item->member_status === 'active')
                                                        <span class="badge badge-success">{{$sponsor_item->member_status}}</span>
                                                        @else
                                                        <span class="badge badge-danger">{{$sponsor_item->member_status}}</span>
                                                    @endif
                                                </td>
                                                <td><a href="/membership/detail-member/{{$sponsor_item->member_id}}"><span class="zmdi zmdi-eye"></span> </a> </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>





@endsection
