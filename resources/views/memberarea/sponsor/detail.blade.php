@extends('memberarea.master.masterblade')

@section('title','Detail Member')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Sponsorship</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Sponsorship</li>
                            <li class="breadcrumb-item active">Detail</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <center>@if($member_item->member_photo != NULL)
                                        <img src="/img/profile/{{$member_item->member_photo}}" width="" height=""/>
                                    @else
                                        <img src="/img/avatar-member.png" width="150px" height="175px"/>
                                    @endif
                                </center><br>
                                <div class="table-responve">
                                    <table class="table table-striped">
                                        <tr>
                                            <td>Sponsor</td>
                                            <td>:</td>
                                            <td>{{$member_item->sponsor}}</td>
                                        </tr>
                                        <tr>
                                            <td>Upline</td>
                                            <td>:</td>
                                            <td>{{$member_item->upline}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Name</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Username</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_username}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Email</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_email}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Waris</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_waris}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Waris Hubungan</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_hub}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Phone</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_mobile}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Gender</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_gender}}</td>
                                        </tr>
                                        <tr>
                                            <td>TTL</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_pob}} / {{date('D, d M Y',strtotime($member_item->member_dob))}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Work</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_work}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Address</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_address}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Country</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_country}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Bank</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_bank}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Bank Account</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_bank_account}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Bank Number</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_bank_number}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member Bank Branch</td>
                                            <td>:</td>
                                            <td>{{$member_item->member_bank_branch}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>





@endsection
