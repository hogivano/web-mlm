@extends('memberarea.master.masterblade')

@section('title','Profile')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Profile</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Profile</li>
                            <li class="breadcrumb-item active">Edit Profile</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                @if(Session::has('error'))
                                        <div class="alert alert-danger">
                                            {{Session::get('error')}}
                                        </div>
                                    @endif
                                    <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        <input type="hidden" name="member_photos" value="{{$member_item->member_photo}}">
                                        {{--<input type="hidden" name="member_date" value="{{$member_item->member_dob}}">--}}
{{--                                        <input type="hidden" name="member_id" value="{{$member_item->member_id}}">--}}
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Member Photo</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row clearfix">
                                                    <div class="card">
                                                        <div class="body">
                                                            <input type="file" id="dropify-event" name="member_photo" data-default-file="{{url("img/profile/".$member_item->member_photo)}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Nama Member</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_name" value="{{$member_item->member_name}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Username</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_username" value="{{$member_item->member_username}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Email</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" name="member_email" value="{{$member_item->member_email}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Member Waris</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_waris" value="{{$member_item->member_waris}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Member Hubungan</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_hub" value="{{$member_item->member_hub}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">No. HP</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="number" class="form-control" name="member_mobile" min="0" value="{{$member_item->member_mobile}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">

                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Jenis Kelamin</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <select name="gender" class="form-control" required>
                                                        <option @if($member_item->member_gender === 'Pria') selected @endif>Pria</option>
                                                        <option @if($member_item->member_gender === 'Wanita') selected @endif>Wanita</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Pekerjaan</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_work" value="{{$member_item->member_work}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Nama Bank</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_bank" value="{{$member_item->member_bank}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Atas Nama</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_bank_account" value="{{$member_item->member_bank_account}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Nomor Rekening</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_bank_number" value="{{$member_item->member_bank_number}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Bank Branch</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_bank_branch" value="{{$member_item->member_bank_branch}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">TTL</label>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-2">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="member_pob" value="{{$member_item->member_pob}}" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-2">
                                                <div class="form-group">
                                                    <input type="date" class="form-control" name="member_dob" value="{{$member_item->member_dob}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Alamat</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="member_address" required>{{$member_item->member_address}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Provinsi</label>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <select name="province" id="prov" class="form-control" data-live-search="true" required>
                                                        @foreach($province as $prov)
                                                            <option data-city="{{json_encode($prov->cities)}}" value="{{$prov->propinsi_id}}" @if($prov->propinsi_id == $member_item->province_id ) selected @endif>{{$prov->propinsi_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Kota</label>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group ">
                                                    <select name="city" id="city" class="form-control " data-live-search="true" required>
                                                        @foreach($cities as $prov)
                                                            <option value="{{$prov->city_id}}" @if($prov->city_id == $member_item->city_id ) selected @endif>{{$prov->city_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                <label for="email_address_2">Kode Pos</label>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input type="text" onkeypress="return isNumberKey(event)"   class="form-control" name="postal_code" minlength="5" maxlength="5" value="{{$member_item->member_postcode}}" required>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="row clearfix">--}}
{{--                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">--}}
{{--                                                <label for="email_address_2">Member Type</label>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-lg-8 col-md-8 col-sm-6">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <select name="member_type" class="form-control" required>--}}
{{--                                                        <option @if($member_item->member_type === 'leader') selected @endif value="leader">Leader</option>--}}
{{--                                                        <option @if($member_item->member_type === 'member') selected @endif value="member">Member</option>--}}
{{--                                                    </select>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="row clearfix">--}}
{{--                                            <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">--}}
{{--                                                <label for="email_address_2">Member Status</label>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-lg-8 col-md-8 col-sm-6">--}}
{{--                                                <select name="member_status" class="form-control" required>--}}
{{--                                                    <option @if($member_item->member_status === 'nonactive') selected @endif value="nonactive">Nonactive</option>--}}
{{--                                                    <option @if($member_item->member_status === 'active') selected @endif value="active">Active</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="row clearfix">
                                            <div class="col-sm-8 offset-sm-2">
                                                <button class="btn btn-raised btn-primary btn-round waves-effect" type="submit">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@section("javascript")

    <link rel="stylesheet" href="{{url('assets/plugins/dropify/css/dropify.min.css')}}">
    <script src="{{url('assets/plugins/dropify/js/dropify.min.js')}}"></script>
    <script type="text/javascript">
        $("#prov").on('change', function(e){
            var city = $('option:selected', this).attr('data-city');
            var city_id = {{$member_item->city_id }};
            var prov = JSON.parse(city);
            $("#city").children().remove().end();
            for (i=0;i<prov.length;i++) {
                if (city_id==prov[i]["city_id"]) {
                    $("#city").append(new Option(prov[i]["city_name"], prov[i]["city_id"],true,true));
                }else {
                    $("#city").append(new Option(prov[i]["city_name"], prov[i]["city_id"]));
                }
            }
            $("#city").selectpicker("refresh");
            console.log(this.value,
                this.options[this.selectedIndex].value,
                $(this).find("option:selected").val(),);
        });
        $(function() {
            "use strict";
            $('.dropify').dropify();

            var drEvent = $('#dropify-event').dropify();
            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });
        });
    </script>
@endsection
