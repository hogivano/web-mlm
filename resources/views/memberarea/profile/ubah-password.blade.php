@extends('memberarea.master.masterblade')

@section('title','Ubah Password')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Profile</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Profile</li>
                            <li class="breadcrumb-item active">Ubah Password</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="card">
                            <div class="body">
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                <form class="form-horizontal" method="POST" action="/membership/proses-update-password">
                                    {{csrf_field()}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label>Password Lama</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password_lama">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label>Password</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                            <label>Konfirmasi Password</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password_confirmation">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-8 offset-sm-2">
                                            <button class="btn btn-raised btn-primary btn-round waves-effect">Ubah</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @endsection
