@extends('memberarea.master.masterblade')

@section('title','Pohon Jaringan')

@section('content')
    <style>
        .test{
            float: right;
            list-style-type: none;
        }
        .test li{
            float: left;
            margin-left: 20px;
        }

        .search{
            padding: 10px;
            list-style-type: none;
            float: right;
        }

        .search li{
            float: left;
            margin-left: 20px;
            margin-right: 20px;
        }
    </style>
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Pohon Jaringan</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Pohon Jaringan</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                    class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card ">
                            @if(Session::has('success'))
                                <div class="alert alert-success">
                                    {{Session::get('success')}}
                                </div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger">
                                    {{Session::get('error')}}
                                </div>
                            @endif
                            <div class="body">
                                <div class="row mb-0">
                                    <div class="col-sm-12">
                                        <ul class="search">
                                            <li><a href="/membership/pohon-jaringan" class="btn btn-default">Ke level 1</a></li>
                                            <li><a href="/membership/pohon-jaringan/{{$network->upline_id}}" class="btn btn-primary">Naik 1 level</a></li>
                                            <li>
                                                <form action="/membership/search-network" class="form-horizontal" method="POST">
                                                    {{csrf_field()}}
                                                    <div class="row clearfix">
                                                        <div class="col-lg-10 col-md-10 col-sm-8">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Masukan Username" name="username">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                                            <button class="btn btn-primary">Go</button>
                                                        </div>
                                                    </div>
                                                    {{--<div class="box-body">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<div class="col-sm-8">--}}
                                                                {{--<input type="text" class="form-control" placeholder="Masukan Username" name="username">--}}
                                                                {{--<button class="btn btn-primary">Go</button>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="icon-section">

                                    <ol class="dd-list">
                                        @if ($network->upline_id != -1)
                                            <li class="dd-item" data-id="1">
                                                <div class="dd-handle">
                                                    @if ($network->networkUpline->network_level < $me->network_level)
                                                        <span class="ti-user"> </span><span
                                                                class="icon-name"> {{$upline->member_username}}</span>
{{--                                                        <span class="float-right">{{$upline->downline_count}} Downline</span>--}}
                                                    @else
                                                        <a href="{{$upline->member_id}}">
                                                            <i class="ti-user"> </i>
                                                            <span class="icon-name"> {{$upline->member_name}}</span>
                                                        </a>
                                                        <span class="float-right">{{$upline->downline_count}} Downline</span>
                                                    @endif
                                                </div>

                                            </li>
                                        @endif

                                        <li class="dd-item" data-id="2">
                                            <div class="dd-handle">
                                                <i class="ti-user"> </i>
                                                <span class="icon-name"> {{$network->member_name}}</span>
                                                <ul class="test">
                                                    <li><a href="#" class="float-right detail" data-toggle="modal"
                                                           data-target="#largeModalss"
                                                           data-id="{{$network->member_id}}"><i
                                                                    class="zmdi zmdi-eye"></i> </a></li>
                                                    <li>
                                                        <span class="float-right">{{$network->downline_count}} Downline</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <ol class="dd-list">

                                                @if($network->member_type === 'leader')
                                                    @php($downlineCount = 10)
                                                @else
                                                    @php($downlineCount = 5)
                                                @endif
                                                @for ($i = 0; $i < $downlineCount; $i++)
                                                    {{--cek apakah kaki terisi--}}

                                                    <?php $curNetwork = $network->downlines->where('kaki', ($i + 1))->first(); ?>

                                                    @if(!empty($curNetwork))


                                                        <li class="dd-item" data-id="2">
                                                            <div class="dd-handle">

                                                                @if ($curNetwork->member_id == 0 )
                                                                    @if ($curNetwork->sponsor_id == $me->member_id)
                                                                        <a href="/membership/registrasi/{{$network->member_id.'/'.($i+1)}}">
                                                                            <i class="ti-angle-double-right"></i>
                                                                            <span class="icon-name"> Proses
                                                                                registrasi
                                                                                oleh {{$curNetwork->sponsor->member_name}} </span>
                                                                        </a>
                                                                    @else
                                                                        <i class="ti-angle-double-right"> </i>
                                                                        <span class="icon-name"> Proses
                                                                            registrasi
                                                                            oleh {{$curNetwork->sponsor->member_name}}</span>
                                                                    @endif
                                                                @else
                                                                    <a href="/membership/pohon-jaringan/{{$curNetwork->member_id}}">
                                                                        <i class="ti-user"> </i>
                                                                        <span class="icon-name"> {{$curNetwork->member_name}}</span>
                                                                    </a>
                                                                @endif
                                                                <ul class="test">
                                                                    <li><a href="#" class="float-right detail"
                                                                           data-toggle="modal"
                                                                           data-target="#largeModalss"
                                                                           data-id="{{$curNetwork->member_id}}"><i
                                                                                    class="zmdi zmdi-eye"></i></a></li>
                                                                    <li><span class="float-right">{{$curNetwork->downline_count}} Downline</span>
                                                                    </li>
                                                                </ul>


                                                            </div>

                                                        </li>
                                                        @if ($curNetwork->member_id != 0)
                                                            <ol class="dd-list">
                                                                @if($curNetwork->member_type === 'leader')
                                                                    @php($downlineCount1 = 10)
                                                                @else
                                                                    @php($downlineCount1 = 5)
                                                                @endif
                                                                @for ($x = 0; $x < $downlineCount1; $x++)
                                                                    <?php $curNetwork2 = $curNetwork->downlines->where('kaki', ($x + 1))->first();?>

                                                                    <li class="dd-item">
                                                                        <div class="dd-handle">
                                                                            @if(!empty($curNetwork2))

                                                                                @if ($curNetwork2->member_id == 0)
                                                                                    @if ($curNetwork2->sponsor_id == $me->member_id)
                                                                                        <a href="/membership/registrasi/{{$curNetwork->member_id.'/'.($x + 1)}}">
                                                                                            <i class="ti-angle-double-right"></i>
                                                                                            <span class="icon-name"> Proses
                                                                                                registrasi
                                                                                                oleh {{$curNetwork2->sponsor->member_name}}</span>
                                                                                        </a>
                                                                                    @else
                                                                                        <i class="ti-angle-double-right"> </i>
                                                                                        <span class="icon-name"> Proses
                                                                                            registrasi
                                                                                            oleh {{$curNetwork2->sponsor->member_name}}</span>
                                                                                    @endif
                                                                                    <span class="float-right">{{$curNetwork2->downline_count}} Downline</span>

                                                                                @else
                                                                                    <a href="/membership/pohon-jaringan/{{$curNetwork2->member_id}}">
                                                                                        <i class="ti-user"> </i>
                                                                                        <span class="icon-name"> {{$curNetwork2->member_name}}</span>
                                                                                    </a>
                                                                                    <ul class="test">
                                                                                        <li>
                                                                                            <a class="float-right detail"
                                                                                               data-toggle="modal"
                                                                                               id="data"
                                                                                               data-target="#largeModalss"
                                                                                               href="#"
                                                                                               data-id="{{$curNetwork2->member_id}}"><i
                                                                                                        class="zmdi zmdi-eye"></i>
                                                                                            </a></li>
                                                                                        <li><span class="float-right">{{$curNetwork2->downline_count}} Downline</span>
                                                                                        </li>
                                                                                    </ul>


                                                                                @endif

                                                                            @else
                                                                                <a href="/membership/registrasi/{{$curNetwork->member_id.'/'.($x + 1)}}">
                                                                                    <i class="ti-pencil-alt"> </i>
                                                                                    <span class="icon-name"> Register Member </span>
                                                                                </a>

                                                                        </div>
                                                                    </li>
                                                                    @endif
                                                                @endfor
                                                            </ol>
                                                        @endif
                                                    @else
                                                        <li class="dd-item">
                                                            <div class="dd-handle">

                                                                {{--@if($network->sponsor_id == Session::get('user_item')->member_id && $network->member_id == $network->upline_id && $network->kaki == ($i+1))--}}
                                                                {{--<span>Member Sedang dalam proses</span>--}}
                                                                {{--@else--}}
                                                                <a href="/membership/registrasi/{{$network->member_id.'/'.($i+1)}}">
                                                                    <i class="ti-pencil-alt"> </i>
                                                                    <span class="icon-name"> Register Member </span></a>
                                                                {{--@endif--}}
                                                            </div>

                                                        </li>
                                                    @endif
                                                @endfor
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Modals --}}
    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="largeModalLabel">Detail Member</h4>
                </div>
                <div class="modal-body">
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td>Jumlah yang disponsori</td>
                                <td>:</td>
                                <td class="aaaa"></td>
                            </tr>
                            <tr>
                                <td>Jumlah Downline</td>
                                <td>:</td>
                                <td class="bbbb"></td>
                            </tr>
                            <tr>
                                <td>Polis Terakhir</td>
                                <td>:</td>
                                <td class="cccc">-</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            $j(".detail").on('click', function () {
                var memberId = $(this).attr('data-id');
                if (memberId > 0) {
                    $.ajax({
                        url: '/membership/member-detail/' + memberId,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {

                            $j('.aaaa').html(data.network_sponsor_count);
                            $j('.bbbb').html(data.downline_count);
                            if (data.member.last_premi.length > 0) {
                                $j('.cccc').html(data.member.last_premi[0].fpolish_date);
                            }else{
                                $j('.cccc').html("-");
                            }
                            // var polishDate = new Date(data.member.last_premi.created_at);
                            // var month = ["January", "February", "March", "April", "May", "June",
                            //     "July", "August", "September", "October", "November", "December"][polishDate.getMonth()];
                            // var day = polishDate.getDate();
                            // var year = polishDate.getFullYear();
                            // $j('.cccc').html(day + " " + month + " " + year);

                            // $('#city').empty();
                            // $.each(data, function (key, value) {
                            //     $('#city').append('<option value="' + value.city_id + '">' + value.city_name + '</option>');
                            // });
                            $("#largeModal").modal()
                        }
                    });
                }
            });

        });
    </script>

    {{--<script type="text/javascript">--}}
        {{--// Single Select--}}
        {{--$("#autocomplete").autocomplete({--}}
            {{--source: function (request, response) {--}}
                {{--// Fetch data--}}
                {{--$.ajax({--}}
                    {{--url: "/membership/get-network",--}}
                    {{--type: 'get',--}}
                    {{--dataType: "json",--}}
                    {{--data: {--}}
                        {{--search: request.term--}}
                    {{--},--}}
                    {{--success: function (data) {--}}
                        {{--response(data);--}}
                    {{--}--}}
                {{--});--}}
            {{--},--}}
            {{--select: function (event, ui) {--}}
                {{--// Set selection--}}
                {{--$('#autocomplete').val(ui.item.value); // display the selected text--}}
                {{--// $('#selectuser_id').val(ui.item.value); // save selected id to input--}}
                {{--$('.go_link').attr("href","/membership/pohon-jaringan/"+ui.item.member_id); // save selected id to input--}}
                {{--return false;--}}
            {{--}--}}
        {{--});--}}

        {{--$( function() {--}}
        {{--});--}}
        {{--function split( val ) {--}}
            {{--return val.split( /,\s*/ );--}}
        {{--}--}}
        {{--function extractLast( term ) {--}}
            {{--return split( term ).pop();--}}
        {{--}--}}
    {{--</script>--}}
@endsection
