@extends('memberarea.master.masterblade')

@section('title','Pohon Jaringan')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Pohon Jaringan</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Pohon Jaringan</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                    class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong><i class="zmdi zmdi-money"></i> Pohon</strong> Jaringan</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <ul>
                                    <li>
                                        <i class="zmdi zmdi-account"> {{$network->member_name}} </i>
                                        <ul>

                                            @if($network->member_type === 'leader')
                                                @php($downlineCount = 10)
                                            @else
                                                @php($downlineCount = 5)
                                            @endif
                                            @for ($i = 0; $i < $downlineCount; $i++)
                                                {{--cek apakah kaki terisi--}}
                                                @if(!empty($network->downlines[$i]))
                                                    <li>
                                                        <i class="zmdi zmdi-account"> {{$network->downlines[$i]->member_name}}</i>
                                                    </li>

                                                    <ul>
                                                        @if($network->downlines[$i]->member_type === 'leader')
                                                            @php($downlineCount1 = 10)
                                                        @else
                                                            @php($downlineCount1 = 5)
                                                        @endif
                                                        @for ($x = 0; $x < $downlineCount1; $x++)
                                                            @if(!empty($network->downlines[$i]->downlines[$x]))
                                                                <li>
                                                                    <i class="zmdi zmdi-account"> {{$network->downlines[$i]->downlines[$x]->member_name}}</i>
                                                                </li>
                                                            @else
                                                                <li>
                                                                    <a href="/membership/insert-member/{{$network->downlines[$i]->member_id.'/'.($x + 1)}}">Register
                                                                        Member</a>
                                                                </li>
                                                            @endif
                                                        @endfor
                                                    </ul>
                                                @else
                                                    <li>
                                                        <a href="/membership/insert-member/{{$network->member_id.'/'.($i+1)}}">Register
                                                            Member</a>
                                                    </li>
                                                @endif
                                            @endfor
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
