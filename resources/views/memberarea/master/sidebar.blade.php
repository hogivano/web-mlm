<!-- Left Sidebar -->

@if(Auth::guard('member')->user()->member_type == 'member')
<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="{{url("membership/home")}}"><span class="m-l-10">Member Area</span></a>
    </div>
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
{{--                    <a class="image" href="{{url("/membership/profile")}}"><img src="{{URL::asset("img/profile/".Auth::guard('member')->user()->member_photo)}}" class="img" alt="User"></a>--}}
                    <div class="detail">
                        <h4 class="center no-padding" style="margin: 0px;"><a class="image" href="{{url("/membership/profile")}}">{{Auth::guard('member')->user()->member_name}}</a></h4>
{{--                        <h5 class=" no-padding" style="margin: 0px;">{{strtoupper(Auth::guard('member')->user()->member_type)}}</h5>--}}
                    </div>
                </div>
            </li>
            <li class="{{ (Request::url()== url("/membership/home") ) ? "active open" : "" }}"><a href="/membership/home"><i class="zmdi zmdi-home"></i><span>Dashboard </span></a></li>
            <li class="{{ (Request::url()== url("/membership/hire") ) ? "active open" : "" }}"><a href="/membership/hire"><i class="zmdi zmdi-money"></i><span>Hire</span></a></li>
            {{--<li><a href="/membership/news"><i class="zmdi zmdi-archive"></i><span>News</span></a></li>--}}
            <li class="{{ (Request::url()==url("/membership/profile") ) ? "active open" : "" }}"><a href="/membership/profile"><i class="zmdi zmdi-account"></i><span>Profile</span></a></li>
            <li class="{{ (Request::url()==url("/membership/goldbar" )) ? "active open" : "" }}"><a href="/membership/goldbar"><i class="zmdi zmdi-money"></i><span>Gold Bar</span></a></li>
            {{--<li class="{{ (Request::url()==url("/membership/beli-polis" )) ? "active open" : "" }}"><a href="/membership/beli-polis"><i class="zmdi zmdi-graphic-eq"></i><span>Polis</span></a></li>--}}
            <li class="{{ (Request::url()==url("/membership/beli-polis" )
                    || Request::url()==url("/membership/polis" )) ? "active open" : "" }}"><a href="#" class="menu-toggle"><i class="zmdi zmdi-graphic-eq"></i><span>Transaksi</span></a>
                <ul class="ml-menu">
                    <li class="{{ (Request::url()==url("/membership/buyin") ) ? "active" : "" }}"><a href="/membership/buyin">Buy In</a></li>
                    <li class="{{ (Request::url()==url("/membership/sellout" )) ? "active" : "" }}"><a href="/membership/sellout">Sell Out</a></li>

                </ul>
            </li>
{{--            <li class="{{ (Request::url()==url("/membership/polis" )) ? "active open" : "" }}"><a href="/membership/polis"><i class="zmdi zmdi-graphic-eq"></i><span>Polis</span></a></li>--}}
            {{-- <li class="{{ (Request::url()==url("/membership/sponsor-ship") ) ? "active open" : "" }}"><a href="/membership/sponsor-ship"><i class="zmdi zmdi-accounts"></i><span>Sponsorship</span></a></li> --}}
            <li class="{{ (Request::url()==url("/membership/pohon-jaringan") ) ? "active open" : "" }}"><a href="/membership/pohon-jaringan"><i class="zmdi zmdi-accounts"></i><span>Invite Member</span></a></li>
            {{-- <li class="{{ (Request::url()==url("/membership/mutasi-bonus" )) ? "active open" : "" }}"><a href="/membership/mutasi-bonus"><i class="zmdi zmdi-money"></i><span>Mutasi Bonus</span></a></li> --}}
            {{-- <li class="{{ (Request::url()==url("/membership/withdrawal" )) ? "active open" : "" }}"><a href="/membership/withdrawal"><i class="zmdi zmdi-balance"></i><span>Withdrawal</span></a></li> --}}
            {{-- <li class="{{ (Request::url()==url("/membership/history-topup" )
                    || Request::url()==url("/membership/bonus-generasi" ) || Request::url()==url("/membership/reward-history" )) ? "active open" : "" }}"><a href="#" class="menu-toggle"><i class="zmdi zmdi-assignment-account"></i><span>History</span></a> --}}
                {{-- <ul class="ml-menu"> --}}
{{--                    <li class="{{ (Request::url()==url("/membership/history-topup") ) ? "active" : "" }}"><a href="/membership/history-topup">TopUp</a></li>--}}
{{--                    <li class="{{ (Request::url()==url("/membership/polis") ) ? "active" : "" }}"><a href="/membership/polis">Polis</a></li>--}}
                    {{--<li><a href="#">With Draw</a></li>--}}
                    {{-- <li class="{{ (Request::url()==url("/membership/bonus-generasi") ) ? "active" : "" }}"><a href="/membership/bonus-generasi">Bonus Generasi</a></li> --}}
                    {{--<li><a href="#">Premi</a></li>--}}
{{--                    <li class="{{ (Request::url()==url("/membership/reward-history") ) ? "active" : "" }}"><a href="/membership/reward-history">Rewards</a></li>--}}
                {{-- </ul> --}}
            {{-- </li> --}}
{{--            <li  class="{{ (Request::url()==url("/membership/topup") ) ? "active open" : "" }}"><a href="/membership/topup"><i class="zmdi zmdi-balance-wallet"></i><span>TopUp</span></a></li>--}}
            {{--<li><a href="#"><i class="zmdi zmdi-balance-wallet"></i><span>With Draw</span></a></li>--}}
            <li><a href="/membership/logout" onclick="return confirm('Apakah anda mau logOut?');"><i class="zmdi zmdi-power"></i><span>LogOut</span></a></li>
        </ul>
    </div>

</aside>
    @else
    <aside id="leftsidebar" class="sidebar">
        <div class="navbar-brand">
            <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
            <a href="{{url("membership/home")}}"><img src="/img/logos/logo2.png" width="25" alt="Logo"><span class="m-l-10">Member Area</span></a>
        </div>
        <div class="menu">
            <ul class="list">
                <li>
                    <div class="user-info">
                        {{--                    <a class="image" href="{{url("/membership/profile")}}"><img src="{{URL::asset("img/profile/".Auth::guard('member')->user()->member_photo)}}" class="img" alt="User"></a>--}}
                        <div class="detail">
                            <h4 class="center no-padding" style="margin: 0px;"><a class="image" href="{{url("/membership/profile")}}">{{Auth::guard('member')->user()->member_name}}</a></h4>
                            {{--                        <h5 class=" no-padding" style="margin: 0px;">{{strtoupper(Auth::guard('member')->user()->member_type)}}</h5>--}}
                        </div>
                    </div>
                </li>
                <li class="{{ (Request::url()== url("/membership/home") ) ? "active open" : "" }}"><a href="/membership/home"><i class="zmdi zmdi-home"></i><span>Dashboard </span></a></li>
                {{--<li class="{{ (Request::url()== url("/membership/registrasi/nasabah") ) ? "active open" : "" }}"><a href="/membership/registrasi/nasabah"><i class="zmdi zmdi-home"></i><span>Pendaftaran Nasabah</span></a></li>--}}
                {{--<li><a href="/membership/news"><i class="zmdi zmdi-archive"></i><span>News</span></a></li>--}}
                <li class="{{ (Request::url()==url("/membership/profile") ) ? "active open" : "" }}"><a href="/membership/profile"><i class="zmdi zmdi-account"></i><span>Profile</span></a></li>
                {{--<li class="{{ (Request::url()==url("/membership/token" )) ? "active open" : "" }}"><a href="/membership/token"><i class="zmdi zmdi-money"></i><span>Token</span></a></li>--}}
                <li class="{{ (Request::url()==url("/membership/beli-polis" )
                    || Request::url()==url("/membership/polis" )) ? "active open" : "" }}"><a href="#" class="menu-toggle"><i class="zmdi zmdi-graphic-eq"></i><span>Polis</span></a>
                    <ul class="ml-menu">
                        <li class="{{ (Request::url()==url("/membership/polis") ) ? "active" : "" }}"><a href="/membership/polis">Daftar Polis</a></li>
                        <li class="{{ (Request::url()==url("/membership/beli-polis" )) ? "active" : "" }}"><a href="/membership/beli-polis">Beli Polis</a></li>

                    </ul>
                </li>           
                
                {{--<li class="{{ (Request::url()==url("/membership/sponsor-ship") ) ? "active open" : "" }}"><a href="/membership/sponsor-ship"><i class="zmdi zmdi-accounts"></i><span>Sponsorship</span></a></li>--}}
                {{--<li class="{{ (Request::url()==url("/membership/pohon-jaringan") ) ? "active open" : "" }}"><a href="/membership/pohon-jaringan"><i class="zmdi zmdi-accounts"></i><span>Pohon Jaringan</span></a></li>--}}
                {{--<li class="{{ (Request::url()==url("/membership/mutasi-bonus" )) ? "active open" : "" }}"><a href="/membership/mutasi-bonus"><i class="zmdi zmdi-money"></i><span>Mutasi Bonus</span></a></li>--}}
                {{--<li class="{{ (Request::url()==url("/membership/withdrawal" )) ? "active open" : "" }}"><a href="/membership/withdrawal"><i class="zmdi zmdi-balance"></i><span>Withdrawal</span></a></li>--}}
                {{--<li class="{{ (Request::url()==url("/membership/history-topup" )--}}
                    {{--|| Request::url()==url("/membership/bonus-generasi" ) || Request::url()==url("/membership/reward-history" )) ? "active open" : "" }}"><a href="#" class="menu-toggle"><i class="zmdi zmdi-assignment-account"></i><span>History</span></a>--}}
                    {{--<ul class="ml-menu">--}}
                        {{--<li class="{{ (Request::url()==url("/membership/history-topup") ) ? "active" : "" }}"><a href="/membership/history-topup">TopUp</a></li>--}}
                        {{--<li><a href="#">With Draw</a></li>--}}
{{--                        <li class="{{ (Request::url()==url("/membership/bonus-generasi") ) ? "active" : "" }}"><a href="/membership/bonus-generasi">Bonus Generasi</a></li>--}}
                        {{--<li><a href="#">Premi</a></li>--}}
                        {{--                    <li class="{{ (Request::url()==url("/membership/reward-history") ) ? "active" : "" }}"><a href="/membership/reward-history">Rewards</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
{{--                <li  class="{{ (Request::url()==url("/membership/topup") ) ? "active open" : "" }}"><a href="/membership/topup"><i class="zmdi zmdi-balance-wallet"></i><span>TopUp</span></a></li>--}}
                {{--<li><a href="#"><i class="zmdi zmdi-balance-wallet"></i><span>With Draw</span></a></li>--}}
                <li><a href="/membership/logout" onclick="return confirm('Apakah anda mau logOut?');"><i class="zmdi zmdi-power"></i><span>LogOut</span></a></li>
            </ul>
        </div>

    </aside>

@endif
