<!doctype html>
<html class="no-js " lang="en">

<!-- Mirrored from thememakker.com/templates/aero/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Jan 2019 06:46:56 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="">
    <title>:: Membership :: @yield('title')</title>
    <link rel="shortcut icon" href="/img/logos/favicon.png" />
    <link rel="stylesheet" href="{{URL::asset('/assets/plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{URL::asset('/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css')}}"/>
{{--    <link rel="stylesheet" href="{{url("/fancybox/jquery.fancybox-1.3.4.css")}}" type="text/css" media="screen" />--}}
    <link rel="stylesheet" href="{{URL::asset('/assets/plugins/charts-c3/plugin.css')}}"/>
    <link rel="stylesheet" href="{{URL::asset('/assets/plugins/summernote/dist/summernote.css')}}"/>
    <link rel="stylesheet" href="{{URL::asset('/assets/plugins/morrisjs/morris.min.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('/assets/plugins/bootstrap-select/css/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{URL::asset('/assets/css/style.min.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
</head>

<BODY class="theme-blush">

{{--@if(Session::get('username')!=NULL)--}}

    @include('memberarea.master.sidebar')

    @yield('content')

{{--@else--}}

{{--    <script>--}}
{{--        alert('Kamu harus Login!!!');--}}
{{--        window.location.href = "/membership"--}}
{{--    </script>--}}
{{--@endif--}}


<!-- Jquery Core Js -->
<script src="{{URL::asset('/assets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{URL::asset('/assets/bundles/vendorscripts.bundle.js')}}"></script>
{{--<script src="{{URL::asset('/assets/bundles/jvectormap.bundle.js')}}"></script>--}}
{{--<script src="{{URL::asset('/assets/bundles/sparkline.bundle.js')}}"></script>--}}
<script src="{{URL::asset('/assets/bundles/c3.bundle.js')}}"></script>
<script src="{{URL::asset('/assets/bundles/mainscripts.bundle.js')}}"></script>
{{--<script src="{{URL::asset('/assets/js/pages/index.js')}}"></script>--}}

<script src="{{URL::asset('/assets/plugins/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('/assets/plugins/jquery-steps/jquery.steps.js')}}"></script>
<script src="{{URL::asset('/assets/js/pages/forms/form-validation.js')}}"></script>

<script type="text/javascript" src="{{url("/fancybox/jquery.fancybox-1.3.4.js")}}"></script>
<script type="text/javascript" src="{{url("/fancybox/jquery.mousewheel-3.0.4.pack.js")}}"></script>
<script type="text/javascript" src="{{url("/fancybox/jquery.easing-1.3.pack.js")}}"></script>
<script src="{{URL::asset('/assets/plugins/nestable/jquery.nestable.js')}}"></script> <!-- Jquery Validation Plugin Css -->
<script src="{{URL::asset('/assets/js/pages/ui/sortable-nestable.js')}}"></script> <!-- Jquery Validation Plugin Css -->
@yield('javascript')
{{--<script type="text/javascript" src="{{url("/fancybox/jquery.easing-1.4.pack.js")}}"></script>--}}
</BODY>
</html>
