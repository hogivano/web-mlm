@extends('memberarea.master.masterblade')

@section('title','Mutasi Bonus')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Mutasi Bonus</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Mutasi Bonus</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            @php
            $balance = 0;
            @endphp
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card w_data_1">
                            <div class="body">
                                <div class="w_icon blue"><i class="zmdi zmdi-card"></i></div>
                                <h4 class="mt-3">Rp. {{number_format($total_debit,0,',','.')}}</h4>
                                <span class="text-muted">Debit</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card w_data_1">
                            <div class="body">
                                <div class="w_icon dark"><i class="zmdi zmdi-card"></i></div>
                                <h4 class="mt-3">Rp. {{number_format(abs($total_credit),0,',','.')}}</h4>
                                <span class="text-muted">Kredit</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card w_data_1">
                            <div class="body">
                                <div class="w_icon indigo"><i class="zmdi zmdi-balance-wallet"></i></div>
                                <h4 class="mt-3">Rp. {{number_format(abs($total_debit+$total_credit),0,',','.')}}</h4>
                                <span class="text-muted">Balance</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                @if(count($mutation_list)==0)
                                    <div class="alert alert-danger">
                                        <center>Belum ada data Mutasi Bonus.</center>
                                    </div>
                                    @else
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Deskripsi</th>
                                            <th>Debit</th>
                                            <th>Credit</th>
                                            <th>Balance</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($mutation_list as $mutation_item)
                                                <tr>
                                                    <td>{{date('D, d M Y H:i:s',strtotime($mutation_item->created_at))}}</td>
                                                    <td>{{$mutation_item->mutation_desc}}</td>
                                                    <td>Rp. {{number_format($mutation_item->mutation_debit,0,',','.')}}</td>
                                                    <td>Rp. {{number_format(abs($mutation_item->mutation_credit),0,',','.')}}</td>
                                                    @php
                                                        $balance = ($balance + $mutation_item->mutation_debit+$mutation_item->mutation_credit);
                                                    @endphp
                                                    <td>Rp. {{number_format($balance,0,',','.')}}</td>
                                                    @if($mutation_item->mutation_type === 'premi' || $mutation_item->mutation_type === 'topup' || $mutation_item->mutation_type === 'closingdate')
                                                        <td>-</td>
                                                    @else
                                                        <td width="75px"><a href="/membership/detail-mutasi/{{$mutation_item->mutation_id}}"><span class="zmdi zmdi-eye"></span> </a> </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>



@endsection

