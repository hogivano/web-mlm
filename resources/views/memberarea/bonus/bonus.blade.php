@extends('memberarea.master.masterblade')

@section('title','History Bonus Generasi')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Bonus Generasi History</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Bonus Generasi History</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Dari</th>
                                            <th>Bonus</th>
                                            <th>Tipe</th>
                                            <th>Deskripsi</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($bonus_list as $bonus_item)
                                           <tr>
                                               <td>{{$no++}}</td>
                                               <td>{{$bonus_item->memberFrom->member_name}}</td>
                                               <td>Rp. {{number_format($bonus_item->bonus_amount,0,',','.')}}</td>
                                               @if($bonus_item->bonus_type === 'network')
                                                   <td>Jaringan</td>
                                                   @else
                                                   <td>Generasi</td>
                                               @endif
                                               <td>{{$bonus_item->bonus_desc}}</td>
                                               @if($bonus_item->bonus_status === 'paid')
                                                    <td><span class="badge badge-success">Terkonfirmasi</span> </td>
                                                    @else
                                                   <td><span class="badge badge-primary">Baru</span> </td>
                                               @endif
                                               <td>{{$bonus_item->created_at}}</td>
                                           </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{$bonus_list->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>





@endsection
