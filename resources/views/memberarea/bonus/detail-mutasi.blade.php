@extends('memberarea.master.masterblade')

@section('title','Detail Miutasi Bonus')

@section('content')
    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Mutasi Bonus</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/membership/home"><i class="zmdi zmdi-home"></i> MLM</a></li>
                            <li class="breadcrumb-item active">Detail Mutasi Bonus</li>
                            <li class="breadcrumb-item active">Detail</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responve">
                                    <table class="table table-striped">
                                        <tr>
                                            <td>Jenis Mutasi</td>
                                            <td>:</td>
                                            <td>{{$detail->mutation_desc}}</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Uang</td>
                                            <td>:</td>
                                            <td>Rp. {{number_format($detail->amount,2,',','.')}}</td>
                                        </tr>
                                        <tr>
                                            <td>PPN</td>
                                            <td>:</td>
                                            <td>{{$detail->ppn}}%</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Uang PPN</td>
                                            <td>:</td>
                                            <td>Rp. {{number_format($detail->ppn_amount,2,',','.')}}</td>
                                        </tr>
                                        <tr>
                                            <td>PPH</td>
                                            <td>:</td>
                                            <td>{{$detail->pph}}%</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Uang PPH</td>
                                            <td>:</td>
                                            <td>Rp. {{number_format($detail->pph_amount,2,',','.')}}</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>:</td>
                                            <td>@if($detail->withdrawal_status === 'pending')
                                                    <span class="badge badge-warning">{{$detail->withdrawal_status}}</span>
                                                    @elseif($detail->withdrawal_status === 'process')
                                                    <span class="badge badge-primary">{{$detail->withdrawal_status}}</span>
                                                    @else
                                                    <span class="badge badge-success">{{$detail->withdrawal_status}}</span>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Total Uang Diterima</td>
                                            <td>:</td>
                                            <td>Rp. {{number_format($detail->amount - $detail->ppn_amount - $detail->pph_amount,2,',','.')}}</td>
                                        </tr>
                                    </table>
                                    {{--<a id="single_image" href="/img/topup/{{$topup->bukti_transfer}}"><img src="/img/topup/{{$topup->bukti_transfer}}" alt=""/></a>--}}
                                    {{--<a id="inline" href="#data">This shows content of element who has id="data"</a>--}}

                                    {{--<div style="display:none"><div id="data">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div></div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

