@extends('memberarea.master.masterblade')

@section('title','Home')
<style>
    .pagination {
        display: inline-block;
    }

    .pagination a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    .pagination a.active {
        background-color: #4CAF50;
        color: white;
    }

    .pagination a:hover:not(.active) {background-color: #ddd;}
</style>
@section('content')
@php
$no=1;
@endphp
    <section class="content">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="header">
                                <h2><strong><i class="zmdi zmdi-money"></i> Character List</strong></h2>
                            </div>
                            <div class="body mb-2">
                                <div class="row clearfix">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Picture</th>
                                                <th>Character Name</th>
                                                <th>Profit</th>
                                                <th>Price</th>
                                                <th>Goldbar</th>
                                                <th>Time</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($characters as $character)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td><img src="{{'/storage/'.$character->picture}}" width="100px" height="100px"></td>
                                                    <td>{{$character->name}}</td>
                                                    <td>{{$character->profit_for}}% - {{$character->profit_to}}%</td>
                                                    <td>${{$character->price_to}} - ${{$character->price_to}}</td>
                                                    <td>{{$character->goldbar}}</td>
                                                    <td>{{$character->time}} AM</td>
                                                    <td><span class="badge badge-success">Active</span></td>
                                                    <td><a data-toggle="modal" data-id="{{$character->character_id}}" title="Add this item" class="open-AddBookDialog btn btn-primary" href="#addBookDialog">Buy</a></td>
                                                </tr>
    
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Modal -->
                                     {{-- Modals --}}
                                     <div class="modal fade" id="addBookDialog" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="title" id="largeModalLabel">Hire Confirmation</h4>
                                                </div>
                                                <div class="modal-body">
                                                <form action="#" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="charId" id="charId" value=""/>
                                                    <div class="row clearfix" >
                                                        <div class="col-lg-2 col-md-2 col-sm-2 form-control-label">
                                                            <label>Jumlah</label>
                                                        </div>
                                                        <div class="col-lg-10 col-md-10 col-sm-8">
                                                            <div class="form-group">
                                                                <input type="number" min="1" class="form-control amounts" name="jumlah" placeholder="Masukan jumlah Barang">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                               <div class="modal-footer">
                                                <button class="btn btn-primary waves-effect">Kirim</button>
                                                <button class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                                            </div>
                                           </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@section("javascript")
    <script type="text/javascript">
    $(document).on("click", ".open-AddBookDialog", function () {
     var charId = $(this).data('id');
     $(".modal-body #charId").val( charId );
     // As pointed out in comments, 
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});
    </script>
@endsection

