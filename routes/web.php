<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');
Route::get('/template', 'PageController@template');
Route::get('/forget-password', function(){
  return view('frontend.login.forget-password');
})->name('forget-password');

Route::get('/referal-code/{usr}', 'PageController@referalLink')->name('referal');
// Route::get('/profile', 'PageController@profile');
// Route::get('/marketing-plan', 'PageController@marketingPlan');
// Route::get('/insurance', 'PageController@insurance');
// Route::get('/blog-news', 'PageController@blogNews');
// Route::get('/detail/{slug}', 'PageController@detail');
// Route::get('/contact-us', 'PageController@contact');
// Route::get('/test', 'PageController@test');
// Route::get('/stockist', 'PageController@stockist');
// Route::get('/album', 'PageController@album');
// Route::get('/gallery/{slug}', 'PageController@gallery');

// Route::get('/ilham/{id}', 'TestController@testIlham');
// Route::get('/test', 'TestController@test');
// Route::get('/closingDate', 'TestController@closingDate');
// Route::get('/autoWithdraw', 'TestController@autoWithdraw');
// Route::get('/checkMember', 'TestController@checkMember');

Route::prefix('admin')->name('admin.')->group(function () {
    // Route::get('/', 'Admin\LoginController')
    // Voyager::routes();
    Route::get('/login', 'Admin\LoginController@login')->name('login');
    Route::post('/login', 'Admin\LoginController@loginSubmit')->name('login.submit');
    Route::get('/logout', 'Admin\HomeController@logout')->name('logout');
    ROute::get('/test', 'Admin\HomeController@test')->name('test');

    Route::get('/clear-cache', function() {
        $exitCode = Artisan::call('config:clear');
        $exitCode = Artisan::call('cache:clear');
        $exitCode = Artisan::call('config:cache');
        return 'DONE';
    });

    Route::get('/', 'Admin\HomeController@index')->name('dashboard');
    Route::get('/fix-member-data', 'Admin\HomeController@fixMemberData')->name('fix_member_data');

    Route::prefix('blacklist-status')->name('blacklist_status')->group(function(){
      Route::get('/', 'Admin\BlacklistStatusController@index');
      Route::get('/new', 'Admin\BlacklistStatusController@new')->name('.new');
      Route::post('/new', 'Admin\BlacklistStatusController@create')->name('.create');
      Route::get('/edit/{id}', 'Admin\BlacklistStatusController@edit')->name('.edit');
      Route::post('/edit/{id}', 'Admin\BlacklistStatusController@update')->name('.update');
      Route::post('/delete', 'Admin\BlacklistStatusController@delete')->name('.delete');
    });

    Route::prefix('blacklist-member')->name('blacklist_member')->group(function(){
      Route::get('/', 'Admin\BlacklistMemberController@index');
      Route::get('/new', 'Admin\BlacklistMemberController@new')->name('.new');
      Route::post('/new', 'Admin\BlacklistMemberController@create')->name('.create');
      Route::post('/change-status', 'Admin\BlacklistMemberController@changeStatus')->name('.change_status');
    });

    Route::prefix('country')->name('country')->group(function(){
      Route::get('/', 'Admin\CountryController@index');
      Route::get('/new', 'Admin\CountryController@new')->name('.new');
      Route::post('/new', 'Admin\CountryController@create')->name('.create');
      Route::get('/edit/{id}', 'Admin\CountryController@edit')->name('.edit');
      Route::post('/edit/{id}', 'Admin\CountryController@update')->name('.update');
      Route::post('/delete', 'Admin\CountryController@delete')->name('.delete');
    });

    Route::prefix('character')->name('character.')->group(function(){
      Route::get('/', 'Admin\CharacterController@index');
      Route::get('/new', 'Admin\CharacterController@new')->name('new');
      Route::post('/new', 'Admin\CharacterController@create')->name('create');
      Route::get('/edit/{id}', 'Admin\CharacterController@edit')->name('edit');
      Route::post('/edit/{id}', 'Admin\CharacterController@update')->name('update');
      Route::post('/delete', 'Admin\CharacterController@delete')->name('delete');

      Route::prefix('show')->name('show')->group(function(){
        Route::get('download-template-order', 'Admin\CharacterController@downloadFormatTemplateOrder')->name('.download_template_order');
        Route::get('download-template-transaction', 'Admin\CharacterController@downloadFormatTemplateTransaction')->name('.download_template_transaction');
        Route::get('/{id}', 'Admin\CharacterController@show');
        Route::post('/{id}', 'Admin\CharacterController@showPost')->name('post');

        Route::post('order/set_status', 'Admin\CharacterController@charactersOrderStatus')->name('.order.set_status');

        Route::post('/{id}/export-order', 'Admin\CharacterController@exportOrder')->name('.export-order');
        Route::post('/{id}/export-seller', 'Admin\CharacterController@exportSeller')->name('.export-seller');
        Route::post('/{id}/import-order', 'Admin\CharacterController@importOrder')->name('.import-order');
        Route::post('/{id}/import-seller', 'Admin\CharacterController@importSeller')->name('.import-seller');
        Route::post('/{id}/import-transaction', 'Admin\CharacterController@importTransaction')->name('.import-transaction');
      });

      Route::prefix('transaction')->name('transaction')->group(function(){
        Route::get('/show/{id}', 'Admin\CharacterTransactionController@show')->name('.show');
        Route::get('/edit/{id}', 'Admin\CharacterTransactionController@edit')->name('.edit');
        Route::post('/edit/{id}', 'Admin\CharacterTransactionController@update')->name('.update');
        Route::get('/payment/show/{id}', 'Admin\CharacterTransactionController@showPayment')->name('.payment.show');
        Route::post('/set_status', 'Admin\CharacterTransactionController@changeStatus')->name('.set_status');
      });

      Route::get('/picture/{id}', 'Admin\CharacterController@showPicture')->name('picture.show');
    });

    Route::prefix('bank-company')->name('bank_company')->group(function(){
      Route::get('/', 'Admin\BankCompanyController@index');
      Route::get('/new', 'Admin\BankCompanyController@new')->name('.new');
      Route::post('/create', 'Admin\BankCompanyController@create')->name('.create');
      Route::get('/edit/{id}', 'Admin\BankCompanyController@edit')->name('.edit');
      Route::post('/edit/{id}', 'Admin\BankCompanyController@update')->name('.update');
      Route::post('/delete', 'Admin\BankCompanyController@delete')->name('.delete');
    });

    Route::prefix('bank')->name('bank')->group(function(){
      Route::get('/', 'Admin\BankController@index');
      Route::get('/new', 'Admin\BankController@new')->name('.new');
      Route::post('/create', 'Admin\BankController@create')->name('.create');
      Route::get('/edit/{id}', 'Admin\BankController@edit')->name('.edit');
      Route::post('/edit/{id}', 'Admin\BankController@update')->name('.update');
      Route::post('/delete', 'Admin\BankController@delete')->name('.delete');
    });

    Route::prefix('member')->name('member')->group(function(){
      Route::get('/', 'Admin\MemberController@index');
      Route::get('/show/{id}', 'Admin\MemberController@show')->name('.show');
      Route::get('/new', 'Admin\MemberController@new')->name('.new');
      Route::post('/new', 'Admin\MemberController@create')->name('.create');
      Route::get('/edit/{id}', 'Admin\MemberController@edit')->name('.edit');
      Route::post('/edit/{id}', 'Admin\MemberController@update')->name('.update');
      Route::post('/delete', 'Admin\MemberController@delete')->name('.delete');

      Route::post('/status', 'Admin\MemberController@setStatus')->name('.set_status');
    });

    Route::prefix('network')->name('network')->group(function(){
      Route::get('/', 'Admin\NetworkController@index');
    });

    Route::prefix('soul')->name('soul')->group(function(){
      Route::get('/', 'Admin\WalletController@soul');

      Route::prefix('buy')->name('.buy')->group(function(){
        Route::get('/', 'Admin\WalletController@soulBuy');
        Route::post('status', 'Admin\WalletController@soulSetStatus')->name('.set_status');
      });

      Route::prefix('transfer')->name('.transfer')->group(function(){
        Route::get('/', 'Admin\WalletController@soulTransfer');
        Route::get('/new', 'Admin\WalletController@soulTransferNew')->name('.new');
        Route::post('/create', 'Admin\WalletController@soulTransfer')->name('.create');
      });

      Route::get('/proof_of_payment/{id}', 'Admin\WalletController@imageProofOfPayment')->name('.image.proof_of_payment');
      Route::post('/proof_of_payment/upload', 'Admin\WalletController@uploadProofOfPayment')->name('.proof_of_payment.upload');
    });

    Route::prefix('setting')->name('setting')->group(function(){
      Route::get('/', 'Admin\SettingController@index');
      // Route::get('/new', 'Admin\SettingController@new')->name('.new');
      // Route::post('/new', 'Admin\SettingController@create')->name('.create');
      Route::get('/edit/{id}', 'Admin\SettingController@edit')->name('.edit');
      Route::post('/edit/{id}', 'Admin\SettingController@update')->name('.update');
    });

    Route::get('/profile', 'Admin\HomeController@profile')->name('profile');

    Route::get('jaringan', 'MemberController@index');
    Route::get('matching-member','Admin\MembersController@matching');
    Route::get('reset-match','Admin\MembersController@reset');
    Route::get('matching','Admin\MembersController@proses_matching');
    Route::get('get-username','Admin\TokenTransactionController@getUsername');
    Route::post('insert-token','Admin\TokenTransactionController@insertToken');
    Route::get('export-excel', 'Admin\AdminWithdrawalController@export')->name('export');
    Route::get('export-polish', 'Admin\AdminPolishPaymentController@export')->name('export');
    Route::get('networks','Admin\NetworkController@index');
//    Route::get('/withdrawals?status=pending','AdminWithdrawalController@index');
    Route::get('networks/{id}','Admin\NetworkController@networks');
    //Route::get('/','Admin\HomeController@index'   );
    Route::get('/withdrawal-proses','Admin\AdminWithdrawalController@updateProses');
    Route::get('/withdrawal-sukses','Admin\AdminWithdrawalController@updateSukses');
    Route::get('/topup-done/{id}','Admin\AdminTopupController@topupDone');
    Route::get('/topup-reject/{id}','Admin\AdminTopupController@topupReject');

    Route::get('/report-polish','Admin\AdminReportController@polish');
    Route::get('/report-bonus','Admin\AdminReportController@bonus');
    Route::get('/detail-report-bonus','Admin\AdminReportController@detailBonus');
    Route::get('/export-bonus','Admin\AdminReportController@exportBonus');

    //Admin
    Route::get('/detail-bonus','Admin\AdminBonusController@detail');
});



Route::prefix('/membership')->name('membership.')->namespace('Member')->group(function(){

    Route::get('/', function () {
        return redirect('/membership/home');
    });

    Route::get('/register', 'RegistrationController@register')->name('register');
    Route::post('/register', 'RegistrationController@registerSubmit')->name('register.submit');
    Route::post('/register-account', 'RegistrationController@registerAccountSubmit')->name('register.account.submit');

    Route::get('/register-information', 'RegistrationController@registerInformation')->name('register.information');
    Route::post('/register-information', 'RegistrationController@registerInformationSubmit')->name('register.information.submit');

      //Route::get('/','LoginController@showLoginForm');
    Route::get('/login','LoginController@showLoginForm');
    Route::post('/login','LoginController@login')->name('login');
    Route::get('/logout','LoginController@logout')->name('logout');

    Route::get('/complete-account', 'RegistrationController@completeAccount')->middleware('auth:member')->name('complete_account');
    Route::group(['middleware' => 'check-status'], function(){
      Route::get('/home','HomeController@index')->name('home');
      Route::post('/auto-hire', 'HomeController@autoHire')->name('auto_hire');

      Route::prefix('/character')->name('character.')->group(function(){
        Route::get('/picture/{id}', 'HomeController@showPictureCharacter')->name('picture');
        Route::post('/order', 'CharacterController@buySubmit')->name('order.submit');
        Route::post('/set_status', 'CharacterController@charactersTransactionsStatus')->name('set_status');
        Route::prefix('/payment')->name('payment')->group(function(){
          Route::post('/upload-payment', 'CharacterController@uploadPayment')->name('.submit');
          Route::get('/show/{id}', 'CharacterController@showPayment')->name('.show');
        });
      });

      Route::get('/invite', 'MemberController@invite')->name('invite');
      Route::get('/security', 'MembershipController@security')->name('security');

      Route::post('/account-password', 'MembershipController@passwordUpdate')->name('security.account.submit');
      Route::post('/wallet-password', 'MembershipController@walletUpdate')->name('security.wallet.submit');

      Route::get('/bank-account', 'MembershipController@bank')->name('bank');
      Route::post('/bank-account', 'MembershipController@bankSubmit')->name('bank.submit');

      Route::get('/inventory', 'MembershipController@inventory')->name('inventory');

      Route::get('/registrasi/{type}','RegistrationController@index')->name('registrasi_nasabah');
      Route::post('/registrasi/{type}','MembershipController@registrasiMemberNasabah')->name('post_registrasi_nasabah');

      Route::get('/detail/{id}','MembershipController@test');
      Route::post('/registrasi/{id}/{kaki}','MembershipController@showRegistrationForm');
      Route::get('/registrasi/{id}/{kaki}','MembershipController@showRegistrationForm');
      Route::post('/register-member','MembershipController@registerMember');
      Route::match(array("GET","POST"),'/edit-profile/','MemberController@edit');
      Route::get('/autocomplete', 'MembershipController@autocomplete')->name('autocomplete');
      Route::get('/get-city-list/{province_id}','MembershipController@getCityList');
      Route::get('/test/{id}/{id2}','MembershipController@test');

      Route::get('/pohon-jaringan/{memberId?}','ThreeController@index');
      Route::get('/pohon-jaringan2/{memberId?}','ThreeController@index2');
      Route::post('/search-network','ThreeController@searchNetwork');

      //Route::get('/news','NewsController@index');

      Route::get('/sponsor-ship','SponsorController@index');
      Route::get('/detail-member/{id}','SponsorController@detail');

      Route::get('/profile','MemberController@profile')->name('profile');
      Route::get('/edit-profile/{id}','MemberController@edit');
      Route::post('/proses-update','MemberController@prosesEdit');
      Route::get('/ubah-password','MemberController@ubahPassword');
      Route::post('/proses-update-password','MemberController@prosesUpdatePassword');

  //    Route::post('/proses-input-topup','TopupController@prosesInputTopup');
  //    Route::post('/konfirmasi-topup','PolisController@konfirmasiTopup');
  //    Route::get('/topup','TopupController@topup');
  //    Route::get('history-topup','TopupController@history');

      Route::get('/detail-history-topup/{topupId}','TopupController@detail');
      Route::get('/get-username','TokenController@getNameList');
      Route::post('/input-soul','TokenController@prosesInput');

      Route::prefix('soul')->name('soul')->group(function(){
        Route::get('/','TokenController@index');

        Route::get('/buy', 'TokenController@soulBuy')->name('.buy');
        Route::post('/buy', 'TokenController@soulBuySubmit')->name('.buy.submit');
        Route::get('/transfer', 'TokenController@soulTransfer')->name('.transfer');
        Route::post('/transfer', 'TokenController@soulTransferSubmit')->name('.transfer.submit');

        Route::get('/buyback', 'TokenController@soulBuyback')->name('.buyback');
        Route::get('/proof_of_payment/{id}', 'TokenController@proofOfPaymentSoul')->name('.proof_of_payment');
        Route::post('/buyback', 'TokenController@soulBuybackSubmit')->name('.buyback.submit');
      });

      Route::prefix('power')->name('power')->group(function(){
        Route::get('/', 'PowerController@index');
      });

      Route::prefix('mind')->name('mind')->group(function(){
        Route::get('/', 'ProfitController@index');
      });

      Route::prefix('sub-account')->name('sub_account')->group(function(){
        Route::get('/', 'SubAccountController@index');
        Route::get('/new', 'SubAccountController@new')->name('.new');
        Route::post('/update', 'SubAccountController@create')->name('.create');
        Route::post('/change-account', 'SubAccountController@changeAccount')->name('.change_account');
      });


      Route::get('/buyin','PolisController@index');
      Route::get('/sellout','PolisController@beliPolis');
      // Route::post('/insert-polis','PolisController@insertPolish');

      Route::get('/bonus-generasi','BonusController@index');

      Route::get('/reward-history','RewardController@index');
      Route::get('/mutasi-bonus','BonusController@mutasi');
      Route::get('/detail-mutasi/{mutasiId}','BonusController@detail');
      Route::get('/withdrawal','WithdrawalController@index');
      Route::get('member-detail/{uplineId}','ThreeController@memberDetail');
      Route::get('/get-network','ThreeController@getNetwork');

      //Route Custom
      Route::get('/hire','HireController@index')->name('hire');
    });
});

//Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/pajak/{sponsor}/{jaringan}','PajakController@pajak');
