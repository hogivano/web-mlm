<?php

namespace App;

use App\Member;
use Illuminate\Database\Eloquent\Model;

class MatchingMember extends Model
{
    const TABLE_NAME = 'matching_member';
    public $timestamps = false;
    protected $table = 'matching_member';
    protected $primaryKey = 'match_id';

    public function NewMember(){
        return $this->belongsTo(Member::class,'member_new','member_id');
    }

    public function OldMember(){
        return $this->belongsTo(Member::class,'member_old','member_id');
    }
}
