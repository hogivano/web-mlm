<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mutation extends Model
{
    const TABLE_NAME = 'mutations';
    protected $table = 'mutations';
    protected $primaryKey = 'mutation_id';
    public $timestamps    = true;
}
