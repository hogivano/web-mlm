<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Network
 * @property integer network_id
 * @property integer member_id
 * @property integer sponsor_id
 * @property integer upline_id
 * @property integer network_level
 * @property integer downline_count
 * @property integer kaki
 * @property string network_status
 * @property string created_at
 * @package app
 */
class Network extends Model {
    const TABLE_NAME = 'networks';
    public $timestamps = false;
    protected $table = 'networks';
    protected $primaryKey = 'network_id';

    /**
     * Get the Childs.
     *
     * @return string
     */
    public function downlines() {
        return $this->hasMany(self::class, 'upline_id', 'member_id')
            ->leftJoin('members','members.member_id','=','networks.member_id')
            ->select('networks.*','member_name','member_username','member_type')
            ->orderBy('kaki','ASC');
    }

    public function member() {
        return $this->belongsTo(Member::class, 'member_id', 'member_id');
    }

    public function isQualified() {
        return $this->belongsTo(Member::class, 'member_id', 'member_id')
            ->select("member_qualified");
    }

    public function networkUpline() {
        return $this->belongsTo(self::class, 'upline_id', 'member_id');
    }
    public function networkSponsor() {
        return $this->belongsTo(self::class, 'member_id', 'sponsor_id');
    }


    public function recursiveUpline() {
        return $this->networkUpline()->with('recursiveUpline');
    }

//    public function recursiveDownline() {
//        return $this->downlines()->with('recursiveDownline');
//    }

    public function upline() {
        return $this->belongsTo(Member::class, 'upline_id', 'member_id');
    }

    public function sponsor() {
        return $this->belongsTo(Member::class, 'sponsor_id', 'member_id');
    }

    public function scopeOwner($query)
    {
        return $query->select('member_name','member_username','member_type','networks.*')->join('members','members.member_id','=','networks.member_id');
    }

    public function isOneNetwork($parentId){
        $parent = Network::where("member_id",$parentId);

        return $parent;
    }

    public static function nestable($network) {
        $network->downline_count = $network->downline_count + 1;
        $network->update();

        if (!empty($network->networkUpline)) {
            $network->network_upline = self::nestable($network->networkUpline);
        }

        return $network;
    }
}
