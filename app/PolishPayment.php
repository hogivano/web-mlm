<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PolishPayment
 * @package App
 * @property integer pp_id
 * @property integer member_id
 * @property integer nominal
 * @property string polis_date
 * @property string pp_type
 * @property string pp_status
 * @property string created_at
 * @property string updated_at
 */
class PolishPayment extends Model {
    const TABLE_NAME = 'polish_payments';
    public $timestamps = true;
    protected $table = 'polish_payments';
    protected $primaryKey = 'pp_id';
    protected $appends = ['fpolish_date'];
    protected $perPage = 50;




    public function memberFilterName($filter) {
        return $this->hasOne(Member::class, 'member_id', 'member_id')
            ->whereMemberNameOrMemberUsername($filter);
    }

    public function withMember(){
        return $this->belongsTo(Member::class,'member_id','member_id');
    }
    public function member() {
        return $this->hasOne(Member::class, 'member_id', 'member_id');
    }

    public function getFpolishDateAttribute() {
//        return date('M Y',strtotime($this->attributes['polis_date']));
        return Carbon::create($this->attributes['polis_date'])->format('M Y');
//        return 'test';
    }

    public function scopeReportList($query){

        $query->select('*', DB::raw('count(*) as total'))
            ->leftjoin('polis_fee',DB::raw('EXTRACT( YEAR_MONTH FROM polish_payment.polis_date )'),'=',
                DB::raw('EXTRACT( YEAR_MONTH FROM polis_fee.periode_date )'))
            ->groupBy(DB::raw('EXTRACT( YEAR_MONTH FROM polis_date )'));
    }
    /**
     * @param $query
     * @param $keywords
     * filter polish berdasarkan nama/username member
     */
    public function scopeWithFilterMember($query, $keywords) {
        $query->whereHas("member", function ($q) use ($keywords) {
            $q->where(function ($q2) use ($keywords){
                $q2->orWhere('member_name','like','%'.$keywords.'%')
                    ->orWhere('member_username','like','%'.$keywords.'%');
            });
        }
        );
    }

    public function scopeWithStatusPayment($query, $status) {
        $query->whereHas("member", function ($q) use ($status) {
            $q->where('pp_status', $status);
        });
    }

    public function scopeWithFilterStartDate($query, $start_date){
        $query->whereHas("member", function ($q) use ($start_date){
           $q->whereBetween('polis_date',[$start_date,NOW()]);
        });
    }

    public function scopeWithFilterDate($query, $start_date , $end_date){
        $query->whereHas("member", function ($q) use ($start_date,$end_date){
            $q->whereBetween('polis_date',[$start_date,$end_date]);
        });
    }

    public function scopeWithFilterRequest($query, $start_date , $end_date){
        $query->whereHas("member", function ($q) use ($start_date,$end_date){
            $q->whereBetween('polis_date',[$start_date,$end_date])
                ->where('pp_status','success');
        });
    }



}

