<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlacklistStatus extends Model
{
    //
    protected $table = 'blacklist_status';
    public $timestamps = false;

    protected $fillable = [
      'name', 'color'
    ];

    public function blacklistMember(){
      return $this->hasMany('App\BlacklistMember', 'blacklist_status_id', 'id');
    }
}
