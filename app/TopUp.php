<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopUp extends Model
{
    const TABLE_NAME = 'topups';
    protected $table = 'topups';
    protected $primaryKey = 'topup_id';
    public $timestamps    = false;

    public function Member(){
        return $this->belongsTo(Member::class ,'member_id','member_id');
    }

    public function Bank(){
        return $this->belongsTo(Bank::class,'bank_id','bank_id');
    }
}
