<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharactersTransactions extends Model
{
    //
    protected $table = 'characters_transactions';
    protected $fillable = [
      'character_id', 'characters_order_id', 'characters_seller_id', 'soul', 'extend_of_payment', 'price_of_sale',
      'profit', 'start_of_payment', 'end_of_sale', 'start_of_sale', 'estimate_hour', 'prediction_profit', 'prediction_price_for_buyer', 'end_of_payment', 'method_payment', 'proof_of_payment', 'status',
      'last_change_id', 'created_at', 'updated_at'
    ];

    public function charactersOrder(){
      return $this->belongsTo('App\CharactersOrder', 'characters_order_id', 'id');
    }

    public function charactersSeller(){
      return $this->belongsTo('App\CharactersSeller', 'characters_seller_id', 'id');
    }

    public function character(){
      return $this->belongsTo('App\Character', 'character_id', 'character_id');
    }
}
