<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Class TokenTransaction
 * @package App
 * @property Integer tt_id
 * @property Integer transaction_from
 * @property Integer transaction_to
 * @property Integer token_amount
 * @property String transaction_type
 * @property String method_payment
 * @property String transaction_status
 * @property String token_type
 * @property String proof_of_payment
 * @property String created_at
 * @property String updated_at
 */
class TokenTransaction extends Model {
    const TABLE_NAME = 'token_transactions';
    public $timestamps = true;
    protected $table = 'token_transactions';
    protected $primaryKey = 'tt_id';

    protected $fillable = [
      'transaction_from', 'transaction_to', 'token_amount', 'transaction_type', 'method_payment', 'transaction_status',
      'token_type', 'proof_of_payment', 'created_at', 'updated_at'
    ];
    public function memberFrom() {
        return $this->belongsTo(Member::class, 'transaction_from', 'id');
    }

    public function memberTo() {
        return $this->belongsTo(Member::class, 'transaction_to', 'id');
    }


}
