<?php

namespace App;

use App\CharactersTransactions;
use Illuminate\Database\Eloquent\Model;

class CharactersSeller extends Model
{
    //
    protected $table = 'characters_seller';
    protected $fillable = [
      'character_id', 'member_id', 'start_of_sale', 'end_of_sale', 'date_of_sale', 'price', 'prediction_profit', 'prediction_price', 'extend_of_sale', 'status',
      'last_change_id', 'created_at', 'updated_at'
    ];

    public function character(){
      return $this->belongsTo('App\Character', 'character_id', 'character_id');
    }

    public function member(){
      return $this->belongsTo('App\Member', 'member_id', 'id');
    }

    public function charactersTransactions(){
      return $this->hasOne(CharactersTransactions::class, 'characters_seller_id', 'id')
          ->where('status', '!=', 'failed');
    }

    public function charactersTransactionsAll(){
      return $this->hasMany('App\CharactersTransactions', 'characters_seller_id', 'id');
    }
}
