<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * Class Withdrawal
 * @package App\Withdrawal
 * @property integer withdrawal_id
 * @property integer mutation_id
 * @property integer amount
 * @property integer ppn
 * @property integer ppn_amount
 * @property integer pph
 * @property integer pph_amount
 * @property string withdrawal_status
 * @property string done_at
 * @property string processed_at
 * @property string created_at
 * @property string updated_at
 */
class Withdrawal extends Model
{
    const TABLE_NAME = 'withdrawals';
    protected $table = 'withdrawals';
    protected $primaryKey = 'withdrawal_id';
    public $timestamps = true;

    public function mutasi() {
        return $this->belongsTo(Mutation::class,'mutation_id','mutation_id');
    }
}
