<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    const TABLE_NAME = 'galleries';
    protected $table = 'galleries';
    protected $primaryKey = 'gallery_id';
}
