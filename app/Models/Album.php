<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    const TABLE_NAME = 'albums';
    protected $table = 'albums';
    protected $primaryKey = 'album_id';
    public $timestamps    = false;
}
