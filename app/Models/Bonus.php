<?php

namespace App\Models;

use App\Member;
use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bonus
 * @package App
 * @property integer bonus_id
 * @property integer bonus_to
 * @property integer bonus_from
 * @property integer bonus_amount
 * @property string bonus_type
 * @property  string bonus_desc
 * @property string bonus_status
 * @property  string paid_at
 * @property string updated_at
 * @property string created_at
 */
class Bonus extends Model
{
    const TABLE_NAME = 'bonuses';
    protected $table = 'bonuses';
    protected $primaryKey = 'bonus_id';
    public $timestamps    = true;
    protected $perPage = 50;


    public function getBonusAmountAttribute(){
       return number_format($this->attributes['bonus_amount']);
    }

    public function getPayAtAttribute(){
        if($this->attributes['paid_at'] === NULL){
            return '';
        }else{
            return date('D, d M Y H:i:s', strtotime($this->attributes['paid_at']));
        }
    }
//
    public function getCreatedAtAttribute(){
        if($this->attributes['created_at']===NULL){
            return '';
        }else{
            return date('D, d M Y H:i:s', strtotime($this->attributes['created_at']));
        }
    }

    public function memberFrom() {
        return $this->belongsTo(Member::class, 'bonus_from', 'member_id');
    }

    public function memberTo() {
        return $this->belongsTo(Member::class, 'bonus_to', 'member_id');
    }

    public function member(){
        return $this->hasOne(Member::class, 'bonus_to', 'member_id');
    }

    public function scopeSearchPending($query, $name, $status) {
        $query->whereHas('member',function ($q) use ($name,$status) {
            $q->where('bonus_to', $name)
                ->where('bonus_status',$status);
        });
    }
}
