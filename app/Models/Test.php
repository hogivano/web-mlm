<?php


namespace App\Models;


use App\Member;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class Test implements ShouldAutoSize, FromQuery,WithMapping, WithHeadings {
    use Exportable;

    public function query() {
        return Member::query();
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array {
        return [
            $row->member_id,
            $row->member_name,
            $row->member_username,
            $row->member_email,
        ];
    }

    /**
     * @return array
     */
    public function headings(): array {
        return [
            'ID',
            'NAMA',
            'USERNAME',
            'EMAIL',
        ];
    }
}