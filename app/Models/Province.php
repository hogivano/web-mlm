<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    const TABLE_NAME = 'province';
    protected $table = 'province';
    protected $primaryKey = 'propinsi_id';
    public $timestamps    = false;
    function cities(){
        return $this->hasMany(City::class, 'province_id', $this->primaryKey);
    }
}
