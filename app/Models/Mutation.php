<?php

namespace App\Models;

use App\Member;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Mutation
 * @package App\Models
 * @property integer mutation_id
 * @property integer member_id
 * @property string mutation_type
 * @property integer mutation_debit
 * @property integer mutation_credit
 * @property string mutation_desc
 * @property string mutation_status
 * @property string created_at
 * @property string updated_at
 */
class Mutation extends Model {
    const TABLE_NAME = 'mutations';
    protected $perPage = 50;
    public $timestamps = true;
    protected $table = 'mutations';
    protected $primaryKey = 'mutation_id';

    public function member() {
        return $this->belongsTo(Member::class, 'member_id', 'member_id');
    }

    public function mutationsSuccess() {
        return $this->whereMutationStatus('success');
    }

    public function getTotalDebitAttribute() {
        return $this->mutationsSuccess->where('mutation_credit',0)->sum('mutation_debit');
    }

    public function  getTotalCreditAttribute() {
        return $this->mutationsSuccess->where('mutation_debit',0)->sum('mutation_credit');
    }

    public function mutationsPending() {
        return $this->mutations()->whereMutationStatus('pending');
    }
}
