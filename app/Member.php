<?php

namespace App;

use App\Models\City;
use DateTime;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\MatchingMember;

class Member extends Authenticatable {
    use Notifiable;

    protected $guard = 'member';
    protected $sisa = 0;
    protected $table = 'member';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [
    //    'member_password','remember_token',
    //];
    protected $fillable = [
        'sponsor_id',
        'parent_id',
        'member_data_id',
        'username',
        'email',
        'password',
        'wallet_password',
        'reg_date',
        'type',
        'status',
        'identity_number',
        'qualified',
        'remember_token',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token', 'wallet_password'
    ];

    public function getAuthPassword() {
        return $this->password;
    }

    public function memberData(){
      return $this->belongsTo('App\MemberData', 'member_data_id', 'id');
    }

    public function getSisaTokenAttribute($params) {
        return $this->tokenTransactionTo($params)->sum('token_amount') - $this->tokenTransactionFrom($params)->sum('token_amount');
    }

    public function tokenTransactionTo($params) {
        return $this->hasMany(TokenTransaction::class, 'transaction_to', 'id')
            ->where('token_transactions.transaction_status', 'sukses')
            ->where('token_transactions.transaction_type', 'transfer')
            ->where('token_type', $params);
    }

    public function tokenTransactionFrom($params) {
        return $this->hasMany(TokenTransaction::class, 'transaction_from', 'id')
            ->where('token_transactions.transaction_status', 'sukses')
            ->where('token_type', $params);
    }

    public function getSisaTokenNasabahAttribute() {
        return $this->tokenTransactionTo("nasabah")->sum('token_amount') - $this->tokenTransactionFrom("nasabah")->sum('token_amount');
    }

    public function getSisaTokenMembershipAttribute() {
        return $this->tokenTransactionTo("membership")->sum('token_amount') - $this->tokenTransactionFrom("membership")->sum('token_amount');
    }

    public function network() {
        return $this->hasOne('App\Network', 'member_id', 'id');
    }

    public function city(){
        return $this->belongsTo(City::class,'city_id','city_id');
    }

    public function sponsored() {
        return $this->hasMany('App\Network', 'sponsor_id', 'id');
    }

    public function uplined() {
        return $this->hasMany('App\Network', 'upline_id', 'id');
    }

    public function mutationsSuccessDebit() {
        return $this->mutations()->whereMutationStatus('success')->where('mutation_credit', 0);
    }

    public function mutations() {
        return $this->hasMany('App\Mutation', 'member_id', 'id');
    }

    public function mutationsPending() {
        return $this->mutations()->whereMutationStatus('pending');
    }

    public function lastPremi() {
        return $this->premis()->orderBy('polis_date', 'DESC')->take(1);
    }

    public function premis() {
        return $this->hasMany(PolishPayment::class, 'member_id', 'id');
    }

    public  function recursivePremis(){
        return $this->premis()->with('recursivePremis');
    }

    public function scopeRegisteredMoreThan($query, $months) {
        return $query->where('member_reg_date', '<', $this->freshTimestamp()->subMonths($months));
    }

    public function scopeQualifiedToWithdraw($query) {
        return $query->where('member_status', '=', 'active');
    }

    public function scopeSponsoredMoreThan($query, $months) {
        return $query->where('member_reg_date', '<', $this->freshTimestamp()->subMonths($months));
    }

    public function getRegisterAgeAttribute() {
        return (new DateTime)->diff(new DateTime($this->attributes['member_reg_date']))->m;
    }

    public function getBalanceAttribute() {
        return $this->mutationsSuccess()->sum('mutation_debit') + $this->mutationsSuccess()->sum('mutation_credit');
    }

    public function mutationsSuccess() {
        return $this->mutations()->where(function ($q) {
            $q->whereMutationStatus('success')->orWhere('mutation_status', 'pending');
        });
    }

    public function scopeSearchKeyMember($query, $keywords) {
        $query->where(function ($q2) use ($keywords) {
            $q2->orWhere('member_name', 'like', '%' . $keywords . '%')
                ->orWhere('member_username', 'like', '%' . $keywords . '%')
                ->orWhere('member_email', 'like', '%' . $keywords . '%');
        });
    }

    public function scopeWithStartDate($query, $start_date) {
        $query->whereBetween('created_at', [$start_date, Now()]);
    }

    public function scopeSearchDate($query, $start_date, $end_date) {
        $query->whereBetween('created_at', [$start_date, $end_date]);
    }

    public function scopeWithType($query, $member_type) {
        $query->where('member_type', $member_type);
    }

    public function getMemberNameAttribute($member){
        return ucwords($member);
    }

    public function NewMemberMatching(){
        return $this->hasMany(MatchingMember::class,'member_new','id');
    }

    public function OldMemberMatching(){
        return $this->hasMany(MatchingMember::class,'member_old','id');
    }

    public function wallet(){
      return $this->hasOne('App\Wallet', 'member_id', 'id');
    }

    public function charactersOrder(){
      return $this->hasMany('App\CharactersOrder', 'member_id', 'id');
    }

    public function charactersSeller(){
      return $this->hasMany('App\CharactersSeller', 'member_id', 'id');
    }

    public function blacklistMember(){
      return $this->hasMany('App\BlacklistMember', 'member_id', 'id');
    }

    public function parent(){
      return $this->belongsTo('App\Member', 'parent_id', 'id');
    }
}
