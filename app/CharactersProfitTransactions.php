<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharactersProfitTransactions extends Model
{
    //
    protected $table = 'characters_profit_transactions';
    protected $fillable = [
      'characters_seller_id', 'profit', 'status', 'created_at', 'updated_at'
    ];

    public function charactersSeller(){
      return $this->belongsTo('App\CharactersSeller', 'characters_seller_id', 'id');
    }
}
