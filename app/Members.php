<?php

namespace App;

use App\Models\City;
use DateTime;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\MatchingMember;

class Members extends Authenticatable {
    use Notifiable;
    const TABLE_NAME = 'members';
    public $timestamps = false;
    protected $guard = 'member';
    protected $sisa = 0;
    protected $table = 'members';
    protected $primaryKey = 'member_id';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [
    //    'member_password','remember_token',
    //];
    protected $fillable = [
        'member_name',
        'member_username',
        'member_password',
        'member_wallet_password',
        'member_is_login',
        'member_reg_date',
        'member_ip',
        'member_type',
        'member_status',
        'member_email',
        'member_mobile',
        'member_photo',
        'member_gender',
        'member_work',
        'member_dob',
        'member_pob',
        'member_identity',
        'member_identity_number',
        'member_waris',
        'member_hub',
        'member_address',
        'member_postcode',
        'member_city',
        'member_country',
        'member_bank',
        'member_bank_account',
        'member_bank_number',
        'member_bank_branch',
        'member_qualified',
        'sponsor_id',
        'created_at',
        'updated_at'
    ];

    public function getAuthPassword() {
        return $this->member_password;
    }

    public function getSisaTokenAttribute($params) {
        return $this->tokenTransactionTo($params)->sum('token_amount') - $this->tokenTransactionFrom($params)->sum('token_amount');
    }

    public function tokenTransactionTo($params) {
        return $this->hasMany(TokenTransaction::class, 'transaction_to', 'member_id')
            ->where('token_transactions.transaction_status', 'sukses')
            ->where('token_transactions.transaction_type', 'transfer')
            ->where('token_type', $params);
    }

    public function tokenTransactionFrom($params) {
        return $this->hasMany(TokenTransaction::class, 'transaction_from', 'member_id')
            ->where('token_transactions.transaction_status', 'sukses')
            ->where('token_type', $params);
    }

    public function getSisaTokenNasabahAttribute() {
        return $this->tokenTransactionTo("nasabah")->sum('token_amount') - $this->tokenTransactionFrom("nasabah")->sum('token_amount');
    }

    public function getSisaTokenMembershipAttribute() {
        return $this->tokenTransactionTo("membership")->sum('token_amount') - $this->tokenTransactionFrom("membership")->sum('token_amount');
    }

    public function network() {
        return $this->hasOne('App\Network', 'member_id', 'member_id');
    }

    public function city(){
        return $this->belongsTo(City::class,'city_id','city_id');
    }

    public function sponsored() {
        return $this->hasMany('App\Network', 'sponsor_id', 'member_id');
    }

    public function uplined() {
        return $this->hasMany('App\Network', 'upline_id', 'member_id');
    }

    public function mutationsSuccessDebit() {
        return $this->mutations()->whereMutationStatus('success')->where('mutation_credit', 0);
    }

    public function mutations() {
        return $this->hasMany('App\Mutation', 'member_id', 'member_id');
    }

    public function mutationsPending() {
        return $this->mutations()->whereMutationStatus('pending');
    }

    public function lastPremi() {
        return $this->premis()->orderBy('polis_date', 'DESC')->take(1);
    }

    public function premis() {
        return $this->hasMany(PolishPayment::class, 'member_id', 'member_id');
    }

    public  function recursivePremis(){
        return $this->premis()->with('recursivePremis');
    }

    public function scopeRegisteredMoreThan($query, $months) {
        return $query->where('member_reg_date', '<', $this->freshTimestamp()->subMonths($months));
    }

    public function scopeQualifiedToWithdraw($query) {
        return $query->where('member_status', '=', 'active');
    }

    public function scopeSponsoredMoreThan($query, $months) {
        return $query->where('member_reg_date', '<', $this->freshTimestamp()->subMonths($months));
    }

    public function getRegisterAgeAttribute() {
        return (new DateTime)->diff(new DateTime($this->attributes['member_reg_date']))->m;
    }

    public function getBalanceAttribute() {
        return $this->mutationsSuccess()->sum('mutation_debit') + $this->mutationsSuccess()->sum('mutation_credit');
    }

    public function mutationsSuccess() {
        return $this->mutations()->where(function ($q) {
            $q->whereMutationStatus('success')->orWhere('mutation_status', 'pending');
        });
    }

    public function scopeSearchKeyMember($query, $keywords) {
        $query->where(function ($q2) use ($keywords) {
            $q2->orWhere('member_name', 'like', '%' . $keywords . '%')
                ->orWhere('member_username', 'like', '%' . $keywords . '%')
                ->orWhere('member_email', 'like', '%' . $keywords . '%');
        });
    }

    public function scopeWithStartDate($query, $start_date) {
        $query->whereBetween('created_at', [$start_date, Now()]);
    }

    public function scopeSearchDate($query, $start_date, $end_date) {
        $query->whereBetween('created_at', [$start_date, $end_date]);
    }

    public function scopeWithType($query, $member_type) {
        $query->where('member_type', $member_type);
    }

    public function getMemberNameAttribute($member){
        return ucwords($member);
    }

    public function NewMemberMatching(){
        return $this->hasMany(MatchingMember::class,'member_new','member_id');
    }

    public function OldMemberMatching(){
        return $this->hasMany(MatchingMember::class,'member_old','member_id');
    }
}
