<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ExportAction extends AbstractAction
{
    public function getTitle()
    {
        return "Export";
    }

    public function getIcon()
    {
        return 'voyager-external';
    }

    public function getPolicy()
    {
        return 'export';
    }

    public function getAttributes()
    {
        return [
            'class'   => 'btn btn-sm btn-danger pull-right delete',
            'data-id' => $this->data->{$this->data->getKeyName()},
            'id'      => 'delete-'.$this->data->{$this->data->getKeyName()},
        ];
    }

    public function getDefaultRoute()
    {
        return 'javascript:;';
    }

    public function shouldActionDisplayOnDataType()
    {
        //return $this->dataType->slug === 'token-transactions';
        $model = $this->data->getModel();
        if ($model && in_array(\Illuminate\Database\Eloquent\SoftDeletes::class, class_uses($model)) && $this->data->deleted_at) {
            return false;
        }

        return parent::shouldActionDisplayOnDataType();
    }
}
