<?php

namespace App\Console\Commands;

use App\CharactersOrder;
use App\Character;
use Illuminate\Console\Command;

class AutoResetOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Character Order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // cronjob
        // */5 * * * *
        $all = Character::with(['charactersOrder' => function($q){
          $q->where('status', 'request');
        }])->get();
        $now = date('Y-m-d H:i:s');
        $count = 0;
        foreach ($all as $i) {
          // code...
          $start_order_time = $i->start_order_time;
          $start_date = date('Y-m-d H:i:s', strtotime($start_order_time));
          $now10 = date('Y-m-d H:i:s', strtotime('-15 minutes', strtotime($start_order_time)));

          if(strtotime($now) <= strtotime($start_date) && strtotime($now) >= strtotime($now10)){
            foreach ($i->charactersOrder as $order) {
              // code...
              $count++;
              $update = CharactersOrder::where('id', $order->id)->update([
                'status' => 'unsuccess'
              ]);
            }
          }
        }
        echo 'success update ' . $count . ' characters_order';
    }
}
