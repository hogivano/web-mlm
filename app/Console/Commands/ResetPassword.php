<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Facades\Hash;

class ResetPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tries:reset-password {username} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'reset password to bcrypt (php artisan tries:reset-password username password)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('username');
        $password = $this->argument('password');

        $admin_list =  DB::table('members')->where('member_username',$username)->count();
        if($admin_list > 0){
            $query = DB::table('members')->where('member_username',$username)
                ->update(['member_password'=>Hash::make($password)]);
            if($query){
                $this->info('password berhasil diubah');
            }else{
                $this->error('username tidak ada');
            }
        }else{
            $this->error('username tidak ada');
        }


    }
}
