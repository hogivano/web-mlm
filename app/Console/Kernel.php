<?php

namespace App\Console;

use App\Bonus;
use App\Member;
use App\Models\Mutation;
use App\Models\Withdrawal;
use App\PolishPayment;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        //Cron closing date tgl 16 jam 00.01
        $schedule->call(function () {
            $this->closingDate();
            $this->checkMember();
        })->monthlyOn(16, '00:01');

        //Cron closing date start of month jam 00.01
        $schedule->call(function () {
            $this->closingDate();
            $this->checkMember();
        })->monthlyOn(1, '00:01');


        //Auto withdrawal tgl 17 jam 00.01
        $schedule->call(function () {
            $this->autoWithdraw();
        })->monthlyOn(17, '00:01');

        //Auto withdrawal tgl 2 jam 00.01
        $schedule->call(function () {
            $this->autoWithdraw();
        })->monthlyOn(2, '00:01');
    }

    protected function closingDate() {
        //AUTO MAINTAIN
        //Bonus::whereBonusStatus('paid')->update(['bonus_status'=>'new']);
        $bonuses = Bonus::whereBonusStatus('new')->groupBy('bonus_to')->selectRaw('bonus_to,sum(bonus_amount) as total')->get();
        //$memberPremi = PolishPayment::whereMemberId($bonuses[1]->bonus_to)->get()->last();
        //return $bonuses;
        $mutations = [];
        $premis    = [];
        foreach ($bonuses as $bonus) {

            $mutations[] = [
                'member_id'       => $bonus->bonus_to,
                'mutation_type'   => 'closingdate',
                'mutation_debit'  => $bonus->total,
                'mutation_credit' => 0,
                'mutation_desc'   => 'Auto Maintain Bonus',
                'mutation_status' => 'success',
                'created_at'      => Carbon::now()->toDateTimeString(),
                'updated_at'      => Carbon::now()->toDateTimeString()
            ];

            $memberPremi = PolishPayment::whereMemberId($bonus->bonus_to)->get()->last();

            //echo json_encode($bonus);
            //dd($memberPremi);
            //return $memberPremi;
            //jika total bonus lebih besar dari premi dan memiliki premi
            if ($bonus->total > env("PREMI_AMOUNT", 150000) && $memberPremi != null) {
                $countCanPay = floor($bonus->total / 150000);
                //return $countCanPay;

                $diffMonth = (Carbon::parse($memberPremi->polis_date)->month) - date('n');
                //echo $diffMonth;
                $sisaBonus = $bonus->total;
                if ($diffMonth < 0 && abs($diffMonth) != 1) {
                    $iMax = abs($diffMonth);
                    //jika bonus hanya cukup untuk membayar premi menunggak
                    if ($countCanPay < $iMax) {
                        $iMax = $countCanPay;
                    } elseif ($countCanPay == ($iMax + 1)) {
                        $iMax = $countCanPay + 1;
                    } else {
                        $iMax += 1;
                    }

                    //return $iMax;
                    //jika nominal bonus cukup untuk menutup kekurangan premi + premi satu bulan kedepan
                    for ($i = 0; $i < $iMax; $i++) {
                        $mutations[] = [
                            'member_id'       => $bonus->bonus_to,
                            'mutation_type'   => 'premi',
                            'mutation_debit'  => 0,
                            'mutation_credit' => -150000,
                            'mutation_desc'   => 'Pembayaran Premi ' . Carbon::parse($memberPremi->polis_date)->addMonths($i + 1)->format('M Y'),
                            'mutation_status' => 'success',
                            'created_at'      => Carbon::now()->toDateTimeString(),
                            'updated_at'      => Carbon::now()->toDateTimeString()
                        ];
                        $premis[]    = [
                            'member_id'  => $bonus->bonus_to,
                            'nominal'    => env("PREMI_AMOUNT", 150000),
                            'polis_date' => Carbon::parse($memberPremi->polis_date)->addMonths($i + 1),
                            'pp_type'    => 'automaintain',
                            'pp_status'  => 'pending',
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ];
                        $sisaBonus   = $bonus->total - 150000;
                    }

                } else {
                    $sisaBonus   = $bonus->total - 150000;
                    $mutations[] = [
                        'member_id'       => $bonus->bonus_to,
                        'mutation_type'   => 'premi',
                        'mutation_debit'  => 0,
                        'mutation_credit' => -150000,
                        'mutation_desc'   => 'Pembayaran Premi ' . Carbon::parse($memberPremi->polis_date)->addMonth()->format('M Y'),
                        'mutation_status' => 'success',
                        'created_at'      => Carbon::now()->toDateTimeString(),
                        'updated_at'      => Carbon::now()->toDateTimeString()
                    ];
                    $premis[]    = [
                        'member_id'  => $bonus->bonus_to,
                        'nominal'    => env("PREMI_AMOUNT", 150000),
                        'polis_date' => Carbon::parse($memberPremi->polis_date)->addMonth(),
                        'pp_type'    => 'automaintain',
                        'pp_status'  => 'pending',
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];
                }

                //insert withdrawal???
                //if ($sisaBonus > 120000){
                //    Withdrawal::insert([
                //
                //    ]);
                //}

            }
        }
        $a = Mutation::insert($mutations);
        $b = Bonus::whereBonusStatus('new')->where('created_at', '<=', Carbon::now())->update(['bonus_status' => 'paid', 'paid_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        $c = PolishPayment::insert($premis);

        return [$a, $b, $c];
    }

    protected function checkMember() {
        //Freeze member syarat :
        //- Tidak sponsori dalam waktu 2 bulan
        //- Tidak membayar premi di bulan ke 3
        $members = Member::withCount("sponsored", 'premis')->except()->registeredMoreThan(2)->having("sponsored_count", '<', 3)->get();
        $members->map(function ($member) {
            $lateCount = Carbon::create($member->lastPremi[0]->polis_date)->diffInMonths(Carbon::now(), false);
            if ($lateCount == 3) {
                $member->network->network_status = 'freeze';
                $member->network->save();
                $member->member_status = 'freeze';
                $member->save();
            }
        });
    }

    protected function autoWithdraw() {
        //Auto withdrawal
        //$withdrawals = [];
        $members = Member::with('mutationsSuccess')
            ->whereHas('mutations')->get();
        $members->map(function ($member) {
            $member->balance = $member->mutationsSuccess->sum(function ($m) {
                    return $m->mutation_debit + $m->mutation_credit;
                }) . "<br/>";
        });

        //return $members;
        $members->where('balance', '>', 120000)->map(function ($member) {
            $mutation       = [
                'member_id'       => $member->member_id,
                'mutation_type'   => 'withdraw',
                'mutation_debit'  => 0,
                'mutation_credit' => -$member->balance,
                'mutation_desc'   => 'Auto Withdraw',
                'mutation_status' => 'pending',
                'created_at'      => Carbon::now()->toDateTimeString(),
                'updated_at'      => Carbon::now()->toDateTimeString()
            ];
            $memberBalance  = $member->balance;
            //$lastMutationId = "0";
            $memBalance = $member->balance;
            $ppn_amount     = $memBalance * 10 / 100;

            if ($memberBalance <= 50000000){
                $pph = 5;
            }elseif ($memberBalance > 50000000 && $memberBalance <= 250000000 ){
                $pph = 15;
            }elseif ($memberBalance > 250000000 && $memberBalance <= 500000000 ){
                $pph = 25;
            }elseif ($memberBalance > 500000000){
                $pph = 30;
            }

            $pph_amount     = $memBalance * $pph / 100;

            $lastMutationId = Mutation::insertGetId($mutation);
            $withdrawals    = [
                'mutation_id'       => $lastMutationId,
                'amount'            => $memberBalance,
                'ppn'               => 10,
                'ppn_amount'        => $ppn_amount,
                'pph'               => $pph,
                'pph_amount'        => $pph_amount,
                'withdrawal_status' => 'pending',
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ];

            //echo json_encode($withdrawals);
            Withdrawal::insert($withdrawals);
        });

    }

    //protected function closingDate() {
    //    //AUTO MAINTAIN
    //    //Bonus::whereBonusStatus('paid')->update(['bonus_status'=>'new']);
    //    $bonuses = Bonus::whereBonusStatus('new')->groupBy('bonus_to')->selectRaw('bonus_to,sum(bonus_amount) as total')->get();
    //    //$memberPremi = PolishPayment::whereMemberId($bonuses[1]->bonus_to)->get()->last();
    //    $mutations = [];
    //    $premis    = [];
    //    foreach ($bonuses as $bonus) {
    //
    //        $mutations[] = [
    //            'member_id'       => $bonus->bonus_to,
    //            'mutation_type'   => 'closingdate',
    //            'mutation_debit'  => $bonus->total,
    //            'mutation_credit' => 0,
    //            'mutation_desc'   => 'Auto Maintain Bonus',
    //            'mutation_status' => 'success',
    //            'created_at'      => Carbon::now()->toDateTimeString(),
    //            'updated_at'      => Carbon::now()->toDateTimeString()
    //        ];
    //        //jika total bonus lebih besar dari premi
    //        if ($bonus->total > env("PREMI_AMOUNT", 150000)) {
    //            $countCanPay = floor($bonus->total / 150000);
    //            //return $countCanPay;
    //            $memberPremi = PolishPayment::whereMemberId($bonus->bonus_to)->get()->last();
    //            //return $memberPremi;
    //            $diffMonth = (Carbon::parse($memberPremi->polis_date)->month) - date('n');
    //            //echo $diffMonth;
    //            $sisaBonus = $bonus->total;
    //            if ($diffMonth < 0 && abs($diffMonth) != 1) {
    //                $iMax = abs($diffMonth);
    //                //jika bonus hanya cukup untuk membayar premi menunggak
    //                if ($countCanPay < $iMax) {
    //                    $iMax = $countCanPay;
    //                } elseif ($countCanPay == ($iMax + 1)) {
    //                    $iMax = $countCanPay + 1;
    //                } else {
    //                    $iMax += 1;
    //                }
    //
    //                //return $iMax;
    //                //jika nominal bonus cukup untuk menutup kekurangan premi + premi satu bulan kedepan
    //                for ($i = 0; $i < $iMax; $i++) {
    //                    $mutations[] = [
    //                        'member_id'       => $bonus->bonus_to,
    //                        'mutation_type'   => 'premi',
    //                        'mutation_debit'  => 0,
    //                        'mutation_credit' => -150000,
    //                        'mutation_desc'   => 'Pembayaran Premi ' . Carbon::parse($memberPremi->polis_date)->addMonths($i + 1)->format('M Y'),
    //                        'mutation_status' => 'success',
    //                        'created_at'      => Carbon::now()->toDateTimeString(),
    //                        'updated_at'      => Carbon::now()->toDateTimeString()
    //                    ];
    //                    $premis[]    = [
    //                        'member_id'  => $bonus->bonus_to,
    //                        'nominal'    => env("PREMI_AMOUNT", 150000),
    //                        'polis_date' => Carbon::parse($memberPremi->polis_date)->addMonths($i + 1),
    //                        'pp_type'    => 'automaintain',
    //                        'pp_status'  => 'success',
    //                        'created_at' => Carbon::now()->toDateTimeString(),
    //                        'updated_at' => Carbon::now()->toDateTimeString()
    //                    ];
    //                    $sisaBonus   = $bonus->total - 150000;
    //                }
    //
    //            } else {
    //                $sisaBonus   = $bonus->total - 150000;
    //                $mutations[] = [
    //                    'member_id'       => $bonus->bonus_to,
    //                    'mutation_type'   => 'premi',
    //                    'mutation_debit'  => 0,
    //                    'mutation_credit' => -150000,
    //                    'mutation_desc'   => 'Pembayaran Premi ' . Carbon::parse($memberPremi->polis_date)->addMonth()->format('M Y'),
    //                    'mutation_status' => 'success',
    //                    'created_at'      => Carbon::now()->toDateTimeString(),
    //                    'updated_at'      => Carbon::now()->toDateTimeString()
    //                ];
    //                $premis[]    = [
    //                    'member_id'  => $bonus->bonus_to,
    //                    'nominal'    => env("PREMI_AMOUNT", 150000),
    //                    'polis_date' => Carbon::parse($memberPremi->polis_date)->addMonth(),
    //                    'pp_type'    => 'automaintain',
    //                    'pp_status'  => 'success',
    //                    'created_at' => Carbon::now()->toDateTimeString(),
    //                    'updated_at' => Carbon::now()->toDateTimeString()
    //                ];
    //            }
    //
    //            //insert withdrawal???
    //            //if ($sisaBonus > 120000){
    //            //    Withdrawal::insert([
    //            //
    //            //    ]);
    //            //}
    //
    //        }
    //    }
    //    $a = Mutation::insert($mutations);
    //    $b = Bonus::whereBonusStatus('new')->where('created_at', '<=', Carbon::now())->update(['bonus_status' => 'paid', 'paid_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
    //    $c = PolishPayment::insert($premis);
    //
    //    return [$a, $b, $c];
    //}
    //
    //protected function checkMember() {
    //    //Freeze member syarat :
    //    //- Tidak sponsori dalam waktu 2 bulan
    //    //- Tidak membayar premi di bulan ke 3
    //    $members = Member::withCount("sponsored", 'premis')->registeredMoreThan(2)->having("sponsored_count", '<', 3)->get();
    //    //return $members;
    //    $members->map(function ($member) {
    //        $lateCount = Carbon::create($member->lastPremi[0]->polis_date)->diffInMonths(Carbon::now(), false);
    //        if ($lateCount == 3) {
    //            $member->network->network_status = 'freeze';
    //            $member->network->save();
    //            $member->member_status = 'freeze';
    //            $member->save();
    //        }
    //    });
    //}
    //
    //protected function autoWithdraw() {
    //    //Auto withdrawal
    //    //$withdrawals = [];
    //    $members = Member::with('mutationsSuccess')
    //        ->whereHas('mutations')->get();
    //    $members->map(function ($member) {
    //        $member->balance = $member->mutationsSuccess->sum(function ($m) {
    //                return $m->mutation_debit + $m->mutation_credit;
    //            }) . "<br/>";
    //    });
    //
    //    //return $members;
    //    $members->where('balance', '>', 120000)->map(function ($member) {
    //        $mutation       = [
    //            'member_id'       => $member->member_id,
    //            'mutation_type'   => 'withdraw',
    //            'mutation_debit'  => 0,
    //            'mutation_credit' => -$member->balance,
    //            'mutation_desc'   => 'Auto Withdraw',
    //            'mutation_status' => 'pending',
    //            'created_at'      => Carbon::now()->toDateTimeString(),
    //            'updated_at'      => Carbon::now()->toDateTimeString()
    //        ];
    //        $memberBalance  = $member->balance;
    //        $lastMutationId = Mutation::insertGetId($mutation);
    //        $ppn_amount     = $member->balance * 10 / 100;
    //
    //        if ($memberBalance <= 50000000){
    //            $pph = 5;
    //        }elseif ($memberBalance > 50000000 && $memberBalance <= 250000000 ){
    //            $pph = 15;
    //        }elseif ($memberBalance > 250000000 && $memberBalance <= 500000000 ){
    //            $pph = 25;
    //        }elseif ($memberBalance > 500000000){
    //            $pph = 30;
    //        }
    //
    //        $pph_amount     = $member->balance * $pph / 100;
    //        $withdrawals    = [
    //            'mutation_id'       => $lastMutationId,
    //            'amount'            => $memberBalance,
    //            'ppn'               => 10,
    //            'ppn_amount'        => $ppn_amount,
    //            'pph'               => $pph,
    //            'pph_amount'        => $pph_amount,
    //            'withdrawal_status' => 'pending',
    //            'created_at'        => Carbon::now()->toDateTimeString(),
    //            'updated_at'        => Carbon::now()->toDateTimeString()
    //        ];
    //        Withdrawal::insert($withdrawals);
    //    });
    //
    //}

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
