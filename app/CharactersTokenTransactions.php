<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharactersTokenTransactions extends Model
{
    //
    protected $table = 'characters_token_transactions';
    protected $fillable = [
      'characters_order_id', 'soul', 'status', 'created_at', 'updated_at'
    ];

    public function charactersOrder(){
      return $this->belongsTo('App\CharactersOrder', 'characters_order_id', 'id');
    }
}
