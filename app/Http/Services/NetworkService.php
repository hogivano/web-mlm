<?php

namespace App\Http\Services;

use App\Bonus;
use App\Network;
use Carbon\Carbon;

class NetworkService implements NetworkInterface {

    protected $bonus = [10000, 10000, 5000, 5000, 5000, 5000, 10000, 10000];

    public function recursiveUpline($uplineId, $deep = null) {
        //$net = Network::whereMemberId($uplineId)->with('recursiveUpline')->get();
        $net       = Network::whereMemberId($uplineId)->first();
        $uplines   = [];
        $uplines[] = $net->fresh();
        $current   = $net;
        while ($current != null) {
            $parent = Network::whereMemberId($current->upline_id)->first();
            if ($current->networkUpline != null) {
                $uplines[] = $parent;
            }

            if ($deep != null && count($uplines) == $deep) {
                $current = null;
            } else {
                $current = $current->networkUpline;
            }
        }
        return $uplines;
    }

    public function prosesTambahBonus($fromId, $uplineId) {
        //$member_type = Auth::guard('member')->user()->member_type;

        //Get uplines recursive
        $networks = $this->recursiveUpline($uplineId, 8);
        $bonuses  = [];
        $iMax     = count($this->bonus);
        for ($i = 0; $i < $iMax; $i++) {

            //Default bonus to ADMIN
            $bonusToId = 1;
            $bonusDesc = "(Unqualified Member) Bonus generasi level ke " . ($i + 1) . ".";
            $bonusType = "unqualified";
            if (isset($networks[$i])) {
                $network = $networks[$i];

                $unqualifiedFromId = $network->member->member_id;
                if ($network->member->member_qualified == 8) {
                    goto updatebonus;
                } elseif ($network->member->member_qualified == 5 && $i < 5) {
                    goto updatebonus;
                } elseif ($network->member->member_qualified == 3 && $i == 2) {
                    goto updatebonus;
                } elseif ($network->member->member_qualified == 3 && $i < 2) {
                    goto updatebonus;
                } elseif ($network->member->member_qualified < 3 && $i < 2) {
                    goto updatebonus;
                }
                goto defaultbonus;
            } else {
                goto defaultbonus;
            }

            updatebonus:
            $bonusToId         = $network->member_id;
            $bonusDesc         = "Bonus generasi level ke " . ($i + 1) . ".";
            $bonusType         = "generasi";
            $unqualifiedFromId = 0;

            defaultbonus:

            $bonuses[] = [
                "bonus_to"         => $bonusToId,
                "bonus_from"       => $fromId,
                "unqualified_from" => $unqualifiedFromId,
                "bonus_amount"     => $this->bonus[$i],
                "bonus_type"       => $bonusType,
                "bonus_desc"       => $bonusDesc,
                "bonus_status"     => "new",
                "created_at"       => Carbon::now()->toDateTimeString()
            ];
        }
        Bonus::insert($bonuses);
    }
}