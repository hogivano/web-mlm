<?php

/**
 * Created by PhpStorm.
 * User: j3p0n
 * Date: 7/25/2019
 * Time: 2:40 PM
 */

namespace App\Http\Services;


interface NetworkInterface {

    public function recursiveUpline($uplineId, $deep = null);

    public function prosesTambahBonus($fromId, $uplineId);
}