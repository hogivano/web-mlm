<?php

namespace App\Http\Controllers;

use App\Network;
use Illuminate\Support\Facades\DB;

use App\Member;
use App\Models\Stockist;
use App\Models\Gallery;

class PageController extends Controller
{
    public function index(){

    return redirect('/membership');
        $data["new"] = DB::table('posts')
            ->where('featured', '1')
            ->orderBy("created_at","ASC")
            ->limit(1)
            ->first();

        $data["news"] = DB::table('posts')
            ->orderBy("created_at","ASC")
            ->offset(1)
            ->limit(3)
            ->get();

        return view('web-pages.pages.home', $data);
    }

    public function index2(){
        $data["new"] = DB::table('posts')
            ->where('featured', '1')
            ->orderBy("created_at","ASC")
            ->limit(1)
            ->first();

        $data["news"] = DB::table('posts')
            ->orderBy("created_at","ASC")
            ->offset(1)
            ->limit(3)
            ->get();
        return view('web-pages.pages.home2', $data);
    }

    public function profile(){
        $data = DB::table('pages')->where('slug', 'profile')->first();

        return view('web-pages.pages.about', ['data' => $data]);
    }

    public function marketingPlan(){
        return view('web-pages.pages.marketing-plan');
    }

    public function insurance(){
        return view('web-pages.pages.asuransi');
    }

    public function blogNews(){
        $posts = DB::table('posts')->orderBy('created_at', 'desc')->get();
        return view('web-pages.pages.blog-news', ['posts' => $posts]);
    }
    public function detail($slug){
        $data = DB::table('posts')->where('slug', $slug)->first();

        return view('web-pages.pages.detail', ['data' => $data]);
    }

    public function stockist(){
        $data['stockists'] = Stockist::get();
        return view('web-pages.pages.stockist', $data);
    }

    public function album(){
        $data['albums'] = Gallery::join('albums', 'galleries.album_id', '=', 'albums.album_id')
            ->whereCover(1)
            ->get();
        return view('web-pages.pages.album', $data);
    }

    public function gallery($slug){
        $data['gallery'] = Gallery::join('albums', 'galleries.album_id', '=', 'albums.album_id')
            ->whereAlbumSlug($slug)
            ->get();
        $data['title'] = $data['gallery'][0]['album_name'];
        return view('web-pages.pages.gallery', $data);
    }

    public function template(){
        return view('home');
    }


    public function referalLink($usr){
      if ($usr){
        $member = Member::where('username', $usr)->first();
        if($member->parent_id == null || $member->parent_id == 0 || !$member->parent_id){
          if ($member){
            return redirect()->route('membership.register', compact('usr'));
          } else {
            flash('link referal tidak ditemukan');
            return redirect()->route('membership.login');
          }
        } else {
          flash('link referal tidak ditemukan');
          return redirect()->route('membership.login');
        }
      } else {
        return redirect()->back();
      }
    }
}
