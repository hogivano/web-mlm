<?php

namespace App\Http\Controllers\Voyager;

use App\Bonus;
use App\Member;
use App\Mutation;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;

class VoyagerController extends BaseVoyagerController
{
    public function index()
    {
        $jml_nasabah = Member::where('type','nasabah')->count();
        $jml_member = Member::where(function ($where){
            $where->where('type','member')
                ->orWhere('type','leader');
        })->count();

        $month = request('month');

        $kalender = CAL_GREGORIAN;
        if($month != NULL){
            $bulan = date('m',strtotime($month));
        }else{
            $bulan = date('m');
        }
        $tahun = date('Y');

        $hari = cal_days_in_month($kalender,$bulan,$tahun);

//        return $hari;

//        return date('m');


//        return $bulan;

        if($month !=NULL){
            $nasabah = Member::selectraw("count(*) as total,DATE(created_at) as tanggal")
                ->where('type','nasabah')
                ->whereMonth('created_at',$bulan)
                ->groupBy('tanggal')
                ->get();

            $member = Member::selectraw("count(*) as total_member,DATE(created_at) as tanggal_member")
                ->where(function ($where){
                    $where->where('type','member')
                        ->orWhere('type','leader');
                })
                ->whereMonth('created_at',$bulan)
                ->groupBy('tanggal_member')
                ->get();
        }else{
            $nasabah = Member::selectraw("count(*) as total,DATE(created_at) as tanggal")
                ->where('type','nasabah')
                ->whereMonth('created_at',date('m'))
                ->groupBy('tanggal')
                ->get();

            $member = Member::selectraw("count(*) as total_member,DATE(created_at) as tanggal_member")
                ->where(function ($where){
                    $where->where('type','member')
                        ->orWhere('type','leader');
                })
                ->whereMonth('created_at',date('m'))
                ->groupBy('tanggal_member')
                ->get();
        }

//        return $member;

        $bonus = Bonus::where('bonus_status','new')->sum('bonus_amount');

        $debit = Mutation::where('mutation_status','success')->sum('mutation_debit');
        $kredit = Mutation::where('mutation_status','success')->sum('mutation_credit');

        $balance = ($debit + $kredit);

//        return $bonus;



        return view("vendor.voyager.index",compact(
            'jml_nasabah',
            'jml_member',
            'nasabah',
            'member',
            'hari',
            'bonus',
            'balance'
        ));
    }
}
