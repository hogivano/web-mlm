<?php


namespace App\Http\Controllers;


use App\Bonus;
use App\Http\Services\NetworkInterface;
use App\Http\Services\NetworkService;
use App\Member;
use App\Models\Mutation;
use App\Models\Withdrawal;
use App\PolishPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestController extends Controller {
    const LEVEL_1 = 50000000;
    const LEVEL_2 = 200000000;
    const LEVEL_3 = 250000000;
    const LEVEL_4 = 500000000;
    protected $network;
    protected $bonus = [10000, 10000, 5000, 5000, 5000, 5000, 10000, 10000];
    protected $countQualified = 0;

    //public function __construct(NetworkService $network) {
    //    $this->network = $network;
    //
    //}

    public function test(Request $request, NetworkService $network) {
        //$a = Network::with("recursiveDownline")->find(143);
        //
        //return $a->recursiveDownline->first()->recursiveDownline->first()->recursiveDownline->first();
        //foreach ($a->downlines as $downline){
        //    echo "<pre>";
        ////    //print_r($downline->with("member")->get());
        //    $m = $downline;
        //    print_r ($m->networkDownline()->get()->toArray());
        //}
        //return $a->downlines;
        return $network->recursiveUpline(41,8);
        //return $diffMonth = (Carbon::parse("2019-08-01")->month) - date('n');
    }


    protected function closingDate() {
        //AUTO MAINTAIN
        //Bonus::whereBonusStatus('paid')->update(['bonus_status'=>'new']);
        $bonuses = Bonus::whereBonusStatus('new')->groupBy('bonus_to')->selectRaw('bonus_to,sum(bonus_amount) as total')->get();
        //$memberPremi = PolishPayment::whereMemberId($bonuses[1]->bonus_to)->get()->last();
        //foreach ($bonuses as $bonus) {
        //    echo $bonus->memberTo->balance."<br/>";
        //}
        //return $bonuses->first()->memberTo->balance;
        $mutations = [];
        $premis    = [];
        foreach ($bonuses as $bonus) {

            $mutations[] = [
                'member_id'       => $bonus->bonus_to,
                'mutation_type'   => 'closingdate',
                'mutation_debit'  => $bonus->total,
                'mutation_credit' => 0,
                'mutation_desc'   => 'Auto Maintain Bonus',
                'mutation_status' => 'success',
                'created_at'      => Carbon::now()->toDateTimeString(),
                'updated_at'      => Carbon::now()->toDateTimeString()
            ];

            $memberPremi = PolishPayment::whereMemberId($bonus->bonus_to)->get()->last();


            //jika total bonus lebih besar dari premi dan memiliki premi
            $balance = $bonus->total + $bonus->memberTo->balance;
            if ($balance >= env("PREMI_AMOUNT", 150000) && $memberPremi != null) {
                $countCanPay = floor($bonus->total / 150000);
                //return $countCanPay;

                // -1 bulan lalu, 0 bulan sekarang, 1 bulan depan
                $diffMonth = (Carbon::parse($memberPremi->polis_date)->month) - date('n');

                //Belikan polis yang belum beli bulan terdahulu/bulan ini
                if ($diffMonth <= 0) {

                    //fix hanya sekali saja pemotongan
                    $addMonths = 1; //default penambahan bulan pembelian berikutnya
                    //jika lebih dari 1 bulan terlambat, pembelian default bulan ini
                    if ($diffMonth < -1){
                        $addMonths = abs($diffMonth);
                    }

                    $mutations[] = [
                        'member_id'       => $bonus->bonus_to,
                        'mutation_type'   => 'premi',
                        'mutation_debit'  => 0,
                        'mutation_credit' => -150000,
                        'mutation_desc'   => 'Pembayaran Premi ' . Carbon::parse($memberPremi->polis_date)->addMonths($addMonths)->format('M Y'),
                        'mutation_status' => 'success',
                        'created_at'      => Carbon::now()->toDateTimeString(),
                        'updated_at'      => Carbon::now()->toDateTimeString()
                    ];

                    $premis[]  = [
                        'member_id'  => $bonus->bonus_to,
                        'nominal'    => env("PREMI_AMOUNT", 150000),
                        'polis_date' => Carbon::parse($memberPremi->polis_date)->addMonths($addMonths),
                        'polis_exp_date' => Carbon::parse($memberPremi->polis_date)->addMonths($addMonths + 12),
                        'pp_type'    => 'automaintain',
                        'pp_status'  => 'pending',
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];

                }

            }
        }
        $a = Mutation::insert($mutations);
        $b = Bonus::whereBonusStatus('new')->where('created_at', '<=', Carbon::now())->update(['bonus_status' => 'paid', 'paid_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        $c = PolishPayment::insert($premis);

        return [$a, $b, $c];
    }

    protected function checkMember() {
        //Freeze member syarat :
        //- Tidak sponsori dalam waktu 2 bulan
        //- Tidak membayar premi di bulan ke 3
        $members = Member::withCount("sponsored", 'premis')->except()->registeredMoreThan(2)->having("sponsored_count", '<', 3)->get();
        $members->map(function ($member) {
            $lateCount = Carbon::create($member->lastPremi[0]->polis_date)->diffInMonths(Carbon::now(), false);
            if ($lateCount == 3) {
                $member->network->network_status = 'freeze';
                $member->network->save();
                $member->member_status = 'freeze';
                $member->save();
            }
        });
    }

    protected function autoWithdraw() {
        //Auto withdrawal
        //$withdrawals = [];
        $members = Member::with('mutationsSuccess')
            ->whereHas('mutations')->get();
        $members->map(function ($member) {
            $member->balance = $member->mutationsSuccess->sum(function ($m) {
                    return $m->mutation_debit + $m->mutation_credit;
                });
        });

        //return $members;
        //balance member diatas 150rb akan di withdraw
        $members->where('balance', '>', 150000)->map(function ($member) {
            $mutation      = [
                'member_id'       => $member->member_id,
                'mutation_type'   => 'withdraw',
                'mutation_debit'  => 0,
                'mutation_credit' => -$member->balance,
                'mutation_desc'   => 'Auto Withdraw',
                'mutation_status' => 'pending',
                'created_at'      => Carbon::now()->toDateTimeString(),
                'updated_at'      => Carbon::now()->toDateTimeString()
            ];
            $memberBalance = $member->balance;
            //$lastMutationId = "0";
            $memBalance = $member->balance;
            $ppn_amount = $memBalance * 10 / 100;

            if ($ppn_amount > 100000) {
                $ppn_amount = 100000;
            }

            $pph_amount = $this->hitungPPH($memBalance);

            $lastMutationId = Mutation::insertGetId($mutation);
            $withdrawals    = [
                'mutation_id'       => $lastMutationId,
                'amount'            => $memberBalance,
                'ppn'               => 10,
                'ppn_amount'        => $ppn_amount,
                'pph'               => 0,
                'pph_amount'        => $pph_amount,
                'withdrawal_status' => 'pending',
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ];

            //echo json_encode($withdrawals);
            Withdrawal::insert($withdrawals);
        });

    }

    public function hitungPPH($amount) {
        $marginPTKP = 4500000 / 2; //perbulan 4,5 jt. karena sebulan 2 kali, jadi dibagi 2
        $norma      = 50 / 100;
        $bonus      = $amount;
        $totalPajak = 0;

        //neto kena pajak
        $jmlNeto = $bonus * $norma;
        if ($jmlNeto < $marginPTKP) {
            return 0;
        }

        $jmlNeto = $jmlNeto - $marginPTKP;

        //tier 1
        if ($jmlNeto >= self::LEVEL_1) {
            $totalPajak += 2500000;
        } else {
            $totalPajak += ($jmlNeto * 5 / 100);
            return $totalPajak;
        }

        //tier 2
        if ($jmlNeto >= self::LEVEL_3) {
            $totalPajak += 3000000;
        } else {
            $totalPajak += (($jmlNeto - self::LEVEL_1) * 15 / 100);
            return $totalPajak;
        }

        //tier 3
        if ($jmlNeto >= self::LEVEL_4) {
            $totalPajak += 125000000;
        } else {
            $totalPajak += (($jmlNeto - self::LEVEL_3) * 25 / 100);
            return $totalPajak;
        }

        //tier 4
        if ($jmlNeto - self::LEVEL_4 > 0) {
            $totalPajak += (($jmlNeto - self::LEVEL_4) * 30 / 100);
            return $totalPajak;
        }

        return $totalPajak;
    }
}