<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Wallet;
use App\Character;
use App\CharactersOrder;
use App\CharactersSeller;
use App\CharactersTokenTransactions;
use App\CharactersTransactions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;
use App\Exports\CharactersOrderExport;
use App\Exports\CharactersSellerExport;
use App\Imports\CharactersOrderImport;
use App\Imports\CharactersSellerImport;
use App\Imports\CharactersTransactionsImport;
use Maatwebsite\Excel\Facades\Excel;

class CharacterController extends Controller
{
    //
    public function __construct(){
      $this->middleware('auth:web');
    }

    public function index(){
      $data = Character::orderBy('characters_number', 'ASC')->get();
      $charactersOrder = CharactersOrder::with('character', 'member')->orderBy('date_order', 'DESC')->paginate(10, ['*'] ,'page-order')->appends(request()->query());
      $charactersTransactions = CharactersTransactions::with('charactersOrder.member', 'charactersSeller.member', 'character')->paginate(10, ['*'] ,'page-tf')->appends(request()->query());
      $charactersSeller = CharactersSeller::with('character', 'member')->where('status', 'ready')->paginate(10, ['*'] ,'page-seller')->appends(request()->query());

      return view('admin.character.index', compact('data', 'charactersOrder', 'charactersTransactions', 'charactersSeller'));
    }

    public function new(){
      $character = Character::orderBy('characters_number', 'ASC')->get();
      return view('admin.character.new', compact('character'));
    }

    public function create(Request $req){
      $rules = [
        'picture' => 'mimes:jpg,jpeg,png|max:10240',
      ];

      $validation = Validator::make($req->all(), $rules);
      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return redirect()->back()->withInput();
      }

      $c = explode('.', $req->soul);
      if(sizeof($c) == 2){
        if(strlen($c[1]) > 2){
          flash('Sorry, soul just support 2 decimal. Ex: x.xx')->error();
          return redirect()->back()->withInput();
        }
      }

      $belowChar = Character::where('character_id', $req->below_character)->first();

      $changeStep = Character::orderBy('characters_number', 'ASC')->get();
      foreach ($changeStep as $i) {
        // code...
        if ($i->characters_number > $belowChar->characters_number){
          // $update = Character::find($i->character_id);
          $i->characters_number = $i->characters_number + 1;
          $i->save();
        }
      }

      $new = new Character();
      $new->name = $req->name;
      $new->soul = $req->soul;
      $new->price_for = $req->price_for;
      $new->price_to = $req->price_to;
      $new->profit_for = $req->profit_for;
      $new->profit_to = $req->profit_to;
      $new->start_order_time = $req->start_order_time;
      $new->end_order_time = $req->end_order_time;
      $new->length_of_sale = $req->length_of_sale;
      $new->time = $req->time;
      $new->characters_number = $belowChar->characters_number + 1;
      if($req->is_sequence){
        $new->is_sequence = 1;
      }
      $new->save();

      if ($req->hasFile('picture')){
        $nama_foto = $new->character_id . 'picture.' . $req->file('picture')->getClientOriginalExtension();
        $img = Image::make($req->file('picture'));
        $pathImg = 'public/character' . '/' . $new->character_id . '/picture';

        $path = Storage::put($pathImg , $req->file('picture'));
        $new->picture = $path;
        $new->save();
      }

      flash('new character has saved');
      return redirect()->route('admin.character.');
    }

    public function edit($id){
      $character = Character::orderBy('characters_number', 'ASC')->get();
      $data = Character::where('character_id', $id)->first();
      return view('admin.character.edit', compact('data', 'character'));
    }

    public function update(Request $req, $id){
      $rules = [
        'picture' => 'mimes:jpg,jpeg,png|max:10240',
      ];

      $validation = Validator::make($req->all(), $rules);
      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return redirect()->back()->withInput();
      }

      $c = explode('.', $req->soul);
      if(sizeof($c) == 2){
        if(strlen($c[1]) > 2){
          flash('Sorry, soul just support 2 decimal. Ex: x.xx')->error();
          return redirect()->back()->withInput();
        }
      }

      if (!$req->below_character){
        $changeStep = Character::orderBy('characters_number', 'ASC')->get();
        $characters_number = 1;
        $count = 2;

        foreach ($changeStep as $i) {
          // code...
          if($i->character_id != $id){
            $i->characters_number = $count;
            $i->save();
            $count = $count + 1;
          }
        }

        $new = Character::find($id);
        $new->name = $req->name;
        $new->soul = $req->soul;
        $new->price_for = $req->price_for;
        $new->price_to = $req->price_to;
        $new->profit_for = $req->profit_for;
        $new->profit_to = $req->profit_to;
        $new->start_order_time = $req->start_order_time;
        $new->end_order_time = $req->end_order_time;
        $new->length_of_sale = $req->length_of_sale;
        $new->time = $req->time;
        $new->characters_number = $characters_number;
        if ($req->is_sequence){
          $new->is_sequence = 1;
        }
        $new->save();
      } else {
        $belowChar = Character::where('character_id', $req->below_character)->first();
        if ($belowChar){
          $changeStep = Character::orderBy('characters_number', 'ASC')->get();
          foreach ($changeStep as $i) {
            // code...
            if ($i->characters_number > $belowChar->characters_number){
              // $update = Character::find($i->character_id);
              $i->characters_number = $i->characters_number + 1;
              $i->save();
            }
          }
        } else {
          flash('sorry below characters not define')->error();
          return redirect()->back()->withInput();
        }


        $new = Character::find($id);
        $new->name = $req->name;
        $new->soul = $req->soul;
        $new->price_for = $req->price_for;
        $new->price_to = $req->price_to;
        $new->profit_for = $req->profit_for;
        $new->profit_to = $req->profit_to;
        $new->start_order_time = $req->start_order_time;
        $new->end_order_time = $req->end_order_time;
        $new->length_of_sale = $req->length_of_sale;
        $new->time = $req->time;
        $new->characters_number = $belowChar->characters_number + 1;
        if ($req->is_sequence){
          $new->is_sequence = 1;
        }
        $new->save();
      }

      if ($req->hasFile('picture')){
        $nama_foto = $new->character_id . 'picture.' . $req->file('picture')->getClientOriginalExtension();
        $img = Image::make($req->file('picture'));
        $pathImg = 'public/character' . '/' . $new->character_id . '/picture';

        $path = Storage::put($pathImg , $req->file('picture'));
        $new->picture = $path;
        $new->save();
      }

      flash('update character has saved');
      return redirect()->route('admin.character.');
    }

    public function delete(Request $req){
        $del = Character::where('character_id', $req->id)->delete();
        return redirect()->route('admin.character.');
    }

    public function show(Request $req, $id){
      // dd($req->all());
      $data = Character::where('character_id', $id)->first();
      $charactersOrder = CharactersOrder::where('character_id', $id)->with(['member.blacklistMember' => function($q){
        $q->where('status', 'on')->with('blacklistStatus');
      }])->orderBy('date_order', 'DESC')->orderBy('status', 'ASC')->paginate(10, ['*'] ,'page-order')->appends(request()->query());
      $charactersSeller = CharactersSeller::where('character_id', $id)->with('member')->paginate(10, ['*'] ,'page-seller')->appends(request()->query());
      $charactersTransactions = CharactersTransactions::where('character_id', $id)->with(['charactersOrder.member' => function($q){
        $q->with(['blacklistMember' => function($r){
          $r->where('status', 'on')->with('blacklistStatus');
        }]);
      },'charactersSeller.member' => function($q){
        $q->with(['blacklistMember' => function($r){
          $r->where('status', 'on')->with('blacklistStatus');
        }]);
      }])->paginate(10, ['*'] ,'page-tr')->appends(request()->query());
      // $test = Character::paginate(1, ['*'], 'page-test')->appends(request()->query());
      return view('admin.character.show', compact('data', 'charactersOrder', 'charactersSeller', 'charactersTransactions'));
    }

    public function changeGainStatusCharactersOrder($characters_order_id){
      $updae = CharactersOrder::where('id', $characters_order_id)->update([
        'status' => 'gain'
      ]);

      if ($update){
        return true;
      }

      return false;
    }

    public function changeFailedStatusCharactersOrder($characters_order_id){
      $update = CharactersOrder::where('id', $characters_order_id)->update([
        'status' => 'failed'
      ]);

      if ($update){
        return true;
      }

      return false;
    }

    public function exportOrder(Request $req, $id){
      $character = Character::where('character_id', $id)->first();
      // return response()->json($character);
      $startTime = strtotime('00:00:00');
      $endTime = strtotime('23:59:59');

      $start_date_time = $req->start_date . ' ' . $character->start_order_time;
      $end_date_time = $req->end_date . ' ' . $character->end_order_time;

      $oneDay = false;
      if (strtotime($character->start_order_time) < strtotime($character->end_order_time)){
        $oneDay = true;
        if (strtotime($req->start_date) != strtotime($req->end_date)){
          flash('sorry, you must set start date and end date at the same date')->error();
          return redirect()->back();
        }
      } else {
        if (strtotime($start_date_time) >= strtotime($end_date_time)){
          flash('sorry, end date must greater than start date')->error();
          return redirect()->back();
        }
      }

      if($oneDay){
        $startDate = date('Y-m-d', strtotime('-1 day', strtotime($req->start_date)));
        $start_date_time = $startDate . ' ' . $character->end_order_time;
        return (new CharactersOrderExport($start_date_time, $end_date_time, $id))->download('Characters_order.xlsx');
      } else {
        $start_date_time = $req->start_date . ' ' . $character->end_order_time;
        return (new CharactersOrderExport($start_date_time, $end_date_time, $id))->download('Characters_order.xlsx');
      }
    }

    public function exportSeller(Request $req, $id = null){
      return Excel::download(new CharactersSellerExport($req->status, $id), 'characters_seller '. date('d-m-Y') . '.xlsx');
      return response()->json($req->all());
    }

    public function importOrder(Request $req, $id){

    }

    public function importSeller(Request $req, $id){
      $validation = Validator::make(
        [
            'file_excel'      => $req->file_excel,
            'extension' => strtolower($req->file_excel->getClientOriginalExtension()),
        ],
        [
            'file_excel'          => 'required|max:10240',
            'extension'      => 'required|in:xlsx,xls',
        ]
      );
      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return redirect()->back();
      }
      Excel::import(new CharactersSellerImport, $req->file_excel);
    }

    public function importTransaction(Request $req, $id){
      $validation = Validator::make(
        [
            'file_excel'      => $req->file_excel,
            'extension' => strtolower($req->file_excel->getClientOriginalExtension()),
        ],
        [
            'file_excel'          => 'required|max:10240',
            'extension'      => 'required|in:xlsx,xls',
        ]
      );
      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return redirect()->back();
      }

      $sub_account = false;
      if($req->sub_account){
        $sub_account = true;
      }

      $import = new CharactersTransactionsImport($id, $sub_account);
      Excel::import($import, $req->file_excel);

      if ($import->getStatus()['error']){
        flash($import->getStatus()['message'])->error();
        return redirect()->back();
      }

      flash($import->getStatus()['message'])->success();
      return redirect()->back();
    }

    public function showPicture($id){
      $cek = Character::where('character_id', $id)->first();
      if ($cek->picture == null){
        return null;
      }

      $contents = Storage::get($cek->picture);
      if (!$contents){
        return null;
      }

      $file_extension = strtolower(substr(strrchr($cek->picture,"."),1));

      switch( $file_extension ) {
          case "gif": $ctype="image/gif"; break;
          case "png": $ctype="image/png"; break;
          case "jpeg": $ctype="image/jpeg"; break;
          case "jpg": $ctype="image/jpeg"; break;
          default:
      }
      ob_clean();
      header("Content-type: " . $ctype);
      $response = Image::make($contents)->response($ctype);
      // $response->header('Content-Type', $ctype);
      return $response;
    }

    public function exportCharactersOrder(Request $req){
      return Excel::download(new CharactersOrderExport, 'characters_order'. $req->start_date . '_' . $req->end_date . '.xlsx');
      return response()->json($req->all());
    }

    public function importCharactersSeller(Request $req){
      return response()->json($req->all());
    }

    public function charactersOrderStatus(Request $req){
      if($req->status == 'cancel' || $req->status == 'unsucceess'){

        $charactersOrder = CharactersOrder::where('id', $req->id)->first();
        $wallet = Wallet::where('member_id', $req->member_id)->first();

        if($charactersOrder && $wallet){
          $cek = CharactersTokenTransactions::where('characters_order_id', $charactersOrder->id)->where('status', 'sent')->first();
          if($cek){
            $new = CharactersTokenTransactions::create([
              'characters_order_id' => $charactersOrder->id,
              'soul' => $cek->soul,
              'status' => 'back'
            ]);

            $wallet->soul = $wallet->soul + $cek->soul;
            $wallet->save();

            $charactersOrder->status = $req->status;
            $charactersOrder->save();

            flash('Success, change status member')->success();
            return redirect()->back();
          }
        }
      }
      flash('Sorry, failed change status order. Please try again')->error();
      return redirect()->back();
    }

    public function downloadFormatTemplateOrder(){
      return response()->download(storage_path("app/document/excel/format_characters_order.xlsx"));
    }

    public function downloadFormatTemplateTransaction(){
      return response()->download(storage_path("app/document/excel/format_transaction.xlsx"));
    }
}
