<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\TokenTransaction;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use App\Member;

class TokenTransactionController extends VoyagerBaseController {

    public function index(Request $request) {

        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
//        return json_encode($dataType);
        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
//        return json_encode($searchable);
        //return $searchable;
        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + 1;
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) && app('VoyagerAuth')->user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Query sum

        $sum_tf_membership = TokenTransaction::where('transaction_type','transfer')->where('transaction_from',0)->where('token_type','membership')->sum('token_amount');
        $sum_re_membership = TokenTransaction::where('transaction_type','registrasi')->where('token_type','membership')->sum('token_amount');

        $sum_tf_nasabah = TokenTransaction::where('transaction_type','transfer')->where('transaction_from',0)->where('token_type','nasabah')->sum('token_amount');
        $sum_re_nasabah = TokenTransaction::where('transaction_type','registrasi')->where('token_type','nasabah')->sum('token_amount');
        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        //return $dataTypeContent;
        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchable',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'sum_tf_membership',
            'sum_re_membership',
            'sum_tf_nasabah',
            'sum_re_nasabah'
        ));
    }

    public function edit(Request $request, $id) {
        return parent::edit($request, $id); // TODO: Change the autogenerated stub
    }

    public function show(Request $request, $id) {
        return parent::show($request, $id); // TODO: Change the autogenerated stub
    }

    public function destroy(Request $request, $id) {
        return parent::destroy($request, $id); // TODO: Change the autogenerated stub
    }

    public function getUsername(Request $request){
        $term    = $request['search'];
        $name    = Member::where('member_username', 'LIKE', '%' . $term . '%')
            ->get();
        $results = array();
        foreach ($name as $item) {
            $results[] = ["value" => $item->member_username, "label" => $item->member_name,"member_id" =>$item->member_id];
        }
        return response()->json($results);
    }

    public function insertToken(Request $request){
        $name = $request['nama'];
        $jumlah_token = $request['jumlah_token'];
        $token_type = $request['token_type'];
        $password = $request['password'];
        $rules = [
            'nama' => 'required|string|max:100',
            'jumlah_token' => 'required|digits_between:1,16',
            'password' => 'required|string|max:100',
        ];

        //error Message
        $errorMsg   = [
            'nama.required' => 'Maaf Member dengan nama tersebut tidak ditemukan.',
            'jumlah_token.digit_between' => 'Maaf digit kurang memenuhi syarat.',
            'jumlah_token.required' => 'Maaf Nominal Harus diisi.',
            'password.required' => 'Maaf Password Harus diisi.'
        ];
        $validation = Validator::make($request->all(), $rules, $errorMsg);
        if ($validation->fails()) {
            return redirect()->back()->with([
                'error' => $validation->errors()->first(),
            ])->withInput();
        }
        //validasi password
        if (!Hash::check($request['password'], Auth::user()->password)) {
            return redirect()->back()->with([
                'error' => "Password Anda salah"

            ])->withInput();
        }

        //pengecekan username aktif
        $user_item = Member::where('member_username',$name)->first();
        if(empty($user_item)){
            return redirect()->back()->with("error","Maaf Member tidak ditemukan");
        }else{

            //insert ke Table

            $token_transaction                     = new TokenTransaction();
            $token_transaction->transaction_from   = 0;
            $token_transaction->transaction_to     = $user_item->member_id;
            $token_transaction->token_amount       = $jumlah_token;
            $token_transaction->transaction_type   = 'transfer';
            $token_transaction->token_type         = $token_type;
            $token_transaction->created_at         = NOW();
            $token_transaction->updated_at         = NULL;
            $token_transaction->transaction_status = 'sukses';
            $token_transaction->save();

            return redirect()->back()->with('success', 'Transaksi transfer token berhasil.');
//            $password = Auth::user()->password;
//            return $password;
        }

    }
}
