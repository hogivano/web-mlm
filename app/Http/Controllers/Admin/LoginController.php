<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    //
    public function login(){
      return view('admin.login');
    }

    public function loginSubmit(Request $request){
      $this->validator($request);
      if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])) {
          //Authentication passed...
          return redirect()->route('admin.dashboard');
          // return redirect()
          //     ->intended(route('membership.home'))
          //     ->with('status', 'Login Failed.');
      }
      //Authentication failed...
      //Redirect the admin...
      return $this->loginFailed();
    }

    private function validator(Request $request) {
        //validation rules.
        $rules = [
            'email' => 'required|exists:users,email|min:6|max:50',
            'password' => 'required|string|min:6|max:255',
        ];
        //custom validation error messages.
        $messages = [
            '*' => 'Login Failed.'
        ];
        //validate the request.
        $request->validate($rules, $messages);
    }

    private function loginFailed() {
        //Login failed...
        flash('email or password not found')->error();
        return redirect()
            ->back()
            ->withInput()
            ->with('error', 'email or password not found');
    }
}
