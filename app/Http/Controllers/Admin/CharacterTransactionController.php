<?php

namespace App\Http\Controllers\Admin;


use Auth;
use App\Wallet;
use App\Character;
use App\CharactersOrder;
use App\CharactersSeller;
use App\CharactersTokenTransactions;
use App\CharactersProfitTransactions;
use App\CharactersTransactions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;
use App\Exports\CharactersOrderExport;
use App\Exports\CharactersSellerExport;
use App\Imports\CharactersOrderImport;
use App\Imports\CharactersSellerImport;
use App\Imports\CharactersTransactionsImport;
use Maatwebsite\Excel\Facades\Excel;

class CharacterTransactionController extends Controller
{
    //
    public function __construct(){
      $this->middleware('auth:web');
    }

    public function show($id){
      $data = CharactersTransactions::where('id', $id)->with('charactersOrder.member', 'charactersSeller.member')->first();
      return view('admin.character.transaction.show', compact('data'));
      return response()->json($data);
    }

    public function edit($id){
      $data = CharactersTransactions::where('id', $id)->with('charactersOrder.member', 'charactersSeller.member')->first();
      return view('admin.character.transaction.edit', compact('data'));
    }

    public function update(Request $req){

    }

    //Characters Transaction
    public function showPayment($id){
      $get = CharactersTransactions::where('id', $id)->with('charactersOrder.member', 'charactersSeller.member')->first();
      if ($get->proof_of_payment != null){
        $contents = Storage::get($get->proof_of_payment);
        if (!$contents){
          return null;
        }

        $file_extension = strtolower(substr(strrchr($get->proof_of_payment,"."),1));

        switch( $file_extension ) {
            case "gif": $ctype="image/gif"; break;
            case "png": $ctype="image/png"; break;
            case "jpeg": $ctype="image/jpeg"; break;
            case "jpg": $ctype="image/jpeg"; break;
            default:
        }
        ob_clean();
        header("Content-type: " . $ctype);
        $response = Image::make($contents)->response($ctype);
        // $response->header('Content-Type', $ctype);
        return $response;
      }
      flash('sorry you cannot open this payment')->error();
      return redirect()->back();
    }


    public function changeStatus(Request $req){
      $charactersTransactions = CharactersTransactions::where('id', $req->id)->with('character', 'charactersOrder.member', 'charactersSeller.member')->first();
      if ($charactersTransactions->character){
        if ($charactersTransactions->characters_seller_id == 0 || $charactersTransactions->characters_seller_id == null){
          if ($charactersTransactions->charactersOrder){
            if ($charactersTransactions->charactersOrder->member && $charactersTransactions->charactersOrder->status == 'success'){
              if ($req->status == 'success'){
                //success new seller
                $set_start_of_sale = null;
                $set_end_of_sale = null;

                $set_start_of_sale = date('Y-m-d H:i:s');
                $plus = ' +' . $charactersTransactions->estimate_hour . ' hour';
                $set_end_of_sale = date('Y-m-d H:i:s', strtotime($plus, strtotime($charactersTransactions->character->time)));

                $newSeller = CharactersSeller::create([
                  'character_id' => $charactersTransactions->character_id,
                  'member_id' => $charactersTransactions->charactersOrder->member->id,
                  'start_of_sale' => $set_start_of_sale,
                  'end_of_sale' => $set_end_of_sale,
                  'price' => $charactersTransactions->price_of_sale,
                  'prediction_profit' => $charactersTransactions->prediction_profit,
                  'prediction_price' => $charactersTransactions->prediction_price_for_buyer,
                  'status' => 'ready',
                  'last_change_id' => Auth::guard('web')->user()->id
                ]);

                $charactersTransactions->start_of_sale = $set_start_of_sale;
                $charactersTransactions->end_of_sale = $set_end_of_sale;
                $charactersTransactions->status = 'success';
                $charactersTransactions->save();

                flash('success update status')->success();
              } elseif ($req->status == 'failed'){
                $charactersOrder = CharactersOrder::where('id', $charactersTransactions->charactersOrder->id)->first();
                $wallet = Wallet::where('member_id', $charactersTransactions->charactersOrder->member->id)->first();

                //cek wallet member
                if ($wallet){
                  $cek = CharactersTokenTransactions::where('characters_order_id', $charactersOrder->id)->where('status', 'sent')->first();
                  $cekBack = CharactersTokenTransactions::where('characters_order_id', $charactersOrder->id)->where('status', 'back')->first();
                  if($cek && !$cekBack){
                    $new = CharactersTokenTransactions::create([
                      'characters_order_id' => $charactersOrder->id,
                      'soul' => $cek->soul,
                      'status' => 'back'
                    ]);

                    $wallet->soul = $wallet->soul + $cek->soul;
                    $wallet->save();

                    $charactersOrder->status = 'unsuccess';
                    $charactersOrder->save();

                    $charactersTransactions->status = $req->status;
                    $charactersTransactions->save();

                    flash('Success, update status')->success();
                  } else {
                    flash('Sorry, transaction soul character not found')->error();
                  }
                } else {
                  flash('Sorry, wallet buyer not found')->error();
                }
              } elseif($req->status == 'reject') {
                $charactersTransactions->status = $req->status;
                $charactersTransactions->save();
                flash('Success, update status')->success();
              } else {
                flash('Sorry, cannot proccess')->error();
              }
            } else {
              flash('sorry member order not found')->error();
            }
          } else{
            flash('sorry character order not found')->error();
          }
        } else {
          if ($charactersTransactions->charactersOrder && $charactersTransactions->charactersSeller){
            if (($charactersTransactions->charactersOrder->member && $charactersTransactions->charactersSeller->member) &&
              ($charactersTransactions->charactersOrder->status == 'success' &&
              $charactersTransactions->charactersSeller->status == 'on_payment')){
              if ($req->status == 'success'){
                $walletSeller = Wallet::where('member_id', $charactersTransactions->charactersSeller->member->id)->first();
                if ($walletSeller){
                  $updateSeller = CharactersSeller::where('id', $charactersTransactions->characters_seller_id)->update([
                    'status' => 'sold',
                    'date_of_sale' => date('Y-m-d H:i:s')
                  ]);

                  $newProfit = CharactersProfitTransactions::create([
                    'characters_seller_id' => $charactersTransactions->characters_seller_id,
                    'profit' => $charactersTransactions->profit,
                    'status' => 'success'
                  ]);

                  $walletSeller->reality = $walletSeller->reality + $charactersTransactions->profit;
                  $walletSeller->save();

                  $set_start_of_sale = null;
                  $set_end_of_sale = null;

                  $set_start_of_sale = date('Y-m-d H:i:s');
                  $plus = ' +' . $charactersTransactions->estimate_hour . ' hour';
                  $set_end_of_sale = date('Y-m-d H:i:s', strtotime($plus, strtotime($charactersTransactions->character->time)));

                  $newSeller = CharactersSeller::create([
                    'character_id' => $charactersTransactions->character_id,
                    'member_id' => $charactersTransactions->charactersOrder->member->id,
                    'start_of_sale' => $set_start_of_sale,
                    'end_of_sale' => $set_end_of_sale,
                    'price' => $charactersTransactions->price_of_sale,
                    'prediction_profit' => $charactersTransactions->prediction_profit,
                    'prediction_price' => $charactersTransactions->prediction_price_for_buyer,
                    'status' => 'ready',
                    'last_change_id' => Auth::guard('web')->user()->id
                  ]);

                  $charactersTransactions->start_of_sale = $set_start_of_sale;
                  $charactersTransactions->end_of_sale = $set_end_of_sale;
                  $charactersTransactions->status = $req->status;
                  $charactersTransactions->save();
                } else {
                  flash('Sorry, wallet member seller not found')->error();
                }
              } elseif ($req->status == 'failed') {
                $charactersOrder = CharactersOrder::where('id', $charactersTransactions->charactersOrder->id)->first();
                $wallet = Wallet::where('member_id', $charactersTransactions->charactersOrder->member->id)->first();

                //cek wallet member
                if ($wallet){
                  $cek = CharactersTokenTransactions::where('characters_order_id', $charactersOrder->id)->where('status', 'sent')->first();
                  $cekBack = CharactersTokenTransactions::where('characters_order_id', $charactersOrder->id)->where('status', 'back')->first();
                  if($cek && !$cekBack){
                    $new = CharactersTokenTransactions::create([
                      'characters_order_id' => $charactersOrder->id,
                      'soul' => $cek->soul,
                      'status' => 'back'
                    ]);

                    $wallet->soul = $wallet->soul + $cek->soul;
                    $wallet->save();

                    $charactersOrder->status = 'unsuccess';
                    $charactersOrder->save();

                    $charactersTransactions->status = $req->status;
                    $charactersTransactions->save();

                    CharactersSeller::where('id', $charactersTransactions->characters_seller_id)->update([
                      'status' => 'ready'
                    ]);

                    flash('Success, update status')->success();
                  } else {
                    flash('Sorry, transaction soul character not found')->error();
                  }
                } else {
                  flash('Sorry, wallet buyer not found')->error();
                }
              } elseif($req->status == 'reject') {
                  $charactersTransactions->status = $req->status;
                  $charactersTransactions->save();
                  flash('Success, update status')->success();
              } else {
                flash('Sorry, cannot proccess')->error();
              }
            } else {
              flash('Sorry, member seller or buyer not found')->error();
            }
          } else {
            flash('Sorry, seller and buyer not found')->error();
          }
        }
      } else {
        flash('Character not found')->error();
      }

      return redirect()->back();
    }
}
