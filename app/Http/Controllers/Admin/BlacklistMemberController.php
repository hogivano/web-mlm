<?php

namespace App\Http\Controllers\Admin;

use App\Member;
use App\BlacklistMember;
use App\BlacklistStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlacklistMemberController extends Controller
{
    //
    public function index(){
      $data = BlacklistMember::with('member')->with('blacklistStatus')->get();
      return view('admin.blacklist_member.index', compact('data'));
    }

    public function new(){
      $blacklistStatus = BlacklistStatus::all();
      $member = Member::orderBy('username', 'ASC')->get();
      return view('admin.blacklist_member.new', compact('blacklistStatus', 'member'));
    }

    public function create(Request $req){
      $cek = BlacklistMember::where('member_id', $req->member_id)->where('status', 'on')->get();
      foreach ($cek as $i) {
        // code...
        $i->status = 'off';
        $i->save();
      }

      $new = BlacklistMember::create([
        'member_id' => $req->member_id,
        'blacklist_status_id' => $req->blacklist_status_id,
        'message' => $req->message
      ]);

      flash('success create blacklist member')->success();
      return redirect()->route('admin.blacklist_member');
    }


    public function changeStatus(Request $req){
      $update = BlacklistMember::where('id', $req->id)->first();
      if($req->status == 'on'){
        $updateAll = BlacklistMember::where('member_id', $update->member_id)->get();
        foreach ($updateAll as $i) {
          // code...
          $i->status = 'off';
          $i->save();
        }
      }

      $update->status = $req->status;
      $update->save();

      flash('success update status')->success();
      return redirect()->route('admin.blacklist_member');
    }
}
