<?php

namespace App\Http\Controllers\Admin;

use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    //
    public function index(){
      $data = Member::with('memberData')->paginate(10, ['*'], 'page-data')->appends(request()->query());
      $newMember = Member::whereHas('memberData')->where('status', 'pending')->paginate(10, ['*'], 'page-new')->appends(request()->query());
      return view('admin.member.index', compact('data', 'newMember'));
    }

    public function new(){

    }

    public function edit($id){

    }

    public function setStatus(Request $req){
      $update = Member::where('id', $req->id)->update([
        'status' => $req->status
      ]);

      $user = Member::where('id', $req->id)->first();

      flash($user->username . ' success actived member');
      return redirect()->route('admin.member');
    }
}
