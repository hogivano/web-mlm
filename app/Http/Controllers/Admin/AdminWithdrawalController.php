<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\Http\Controllers\Voyager\VoyagerController;
use App\Models\Withdrawal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\WithdrawalExport;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Database\Schema\SchemaManager;

//use Maatwebsite\Excel\Concerns\WithHeadings;

class AdminWithdrawalController extends Controller
{
    public function index(Request $request)
    {
        $getUrl = $request->status;

        if($getUrl === 'pending'){
            $data['withdrawal_list'] = Withdrawal::where('withdrawal_status','pending')
                ->orWhere('withdrawal_status','process')
                ->orderBy('withdrawal_id','desc')
                ->paginate(20);

            $data['amount'] = Withdrawal::where('withdrawal_status','pending')
                ->orWhere('withdrawal_status','process')
                ->sum('amount');
            $data['ppn_amount'] = Withdrawal::where('withdrawal_status','pending')
                ->orWhere('withdrawal_status','process')
                ->sum('ppn_amount');
            $data['pph_amount'] = Withdrawal::where('withdrawal_status','pending')
                ->orWhere('withdrawal_status','process')
                ->sum('pph_amount');
        }else{
            $data['withdrawal_list'] = Withdrawal::where('withdrawal_status','done')
                ->orderBy('withdrawal_id','desc')->paginate(20);

            $data['amount'] = Withdrawal::where('withdrawal_status','done')
                ->sum('amount');
            $data['ppn_amount'] = Withdrawal::where('withdrawal_status','done')
                ->sum('ppn_amount');
            $data['pph_amount'] = Withdrawal::where('withdrawal_status','done')
                ->sum('pph_amount');
        }

        $data['pending'] = Withdrawal::where('withdrawal_status','pending')->count();
        $data['proses'] = Withdrawal::where('withdrawal_status','process')->count();

        return view('vendor.voyager.withdrawals.browse',$data);
    }


    public function export(Request $request){
        $status = $request->status;
        return (new WithdrawalExport)->forStatus($status)->download('withdrawal.xlsx');
    }

    public function updateProses(){
        Withdrawal::where('withdrawal_status','pending')
            ->update([
                'withdrawal_status'=>'process',
                'updated_at' => NOW(),
                'processed_at' => NOW()
            ]);
        return redirect()->back()->with('success','Status berhasil diupdate.');
    }

    public function updateSukses(){
        Withdrawal::where('withdrawal_status','process')
            ->update([
                'withdrawal_status'=>'done',
                'updated_at' => NOW(),
                'done_at' => NOW()
            ]);
        return redirect()->back()->with('success','Status berhasil diupdate.');
    }

}
