<?php

namespace App\Http\Controllers\Admin;

use App\Bonus;
use App\Exports\BonusExport;
use App\PolisFee;
use App\PolishPayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdminReportController extends Controller
{
    public function polish(){

        //$polis_fee = DB::table('polis_fee')->count();
        //if($polis_fee > 0){
        //    $report_polish = PolishPayment::leftjoin(DB::raw('EXTRACT( YEAR_MONTH FROM polish_payments.polis_date )'),'=',
        //            'polis_fee.periode_date')
        //        ->groupBy(DB::raw('EXTRACT( YEAR_MONTH FROM polish_payments.polis_date )'))
        //        ->where('polis_fee.fee_status','paid')
        //        ->select(DB::raw('count(*) as total'),'*')->get();
        //}else{
        //    $report_polish = PolishPayment::select('*', DB::raw('count(*) as total'),
        //        DB::raw('EXTRACT( YEAR_MONTH FROM polis_date ) as year'))
        //        ->groupBy(DB::raw('EXTRACT( YEAR_MONTH FROM polis_date )'))
        //        ->where('pp_status','pending')->get();
        //}

        $report_polish = PolishPayment::select('*', DB::raw('count(*) as total'))
            ->groupBy(DB::raw('EXTRACT( YEAR_MONTH FROM created_at )'))
            ->leftJoin('polis_fee', 'polis_fee.periode_date', '=', DB::raw('EXTRACT( YEAR_MONTH FROM created_at )'))->get();
        $data['report_polish'] = $report_polish;
        //$data['polis_fee'] = $polis_fee;
//        return $report_polish;
        return view("vendor.voyager.Report.report-polish",$data);
    }

    public function updatePolis($periode){
        $fee = new PolisFee();
        $fee->periode_date = $periode;
        $fee->fee_status   = 'paid';
        $fee->paid_date    = NOW();
        $fee->save();

        return redirect()->back()->with('success','Berhasil di Update');
    }

    public function bonus(Request $request){
        $start = $request->start_date;
        $end    = $request->end_date;

//        return date('Y-m-d');

        if($start != '' && $end == ''){
            //polis
            $data['sponsor'] = Bonus::where('bonus_type','sponsor')->whereBetween('created_at',[$start,date('Y-m-d')])->sum('bonus_amount');
            //bonus
            $data['total'] = Bonus::where('bonus_to','1')
                ->wherebetween('created_at',[$start,date('Y-m-d')])->sum('bonus_amount');
            $data['generasi'] = Bonus::where('bonus_to','1')->where('bonus_type','generasi')
                ->wherebetween('created_at',[$start,date('Y-m-d')])
                ->sum('bonus_amount');
            $data['unqualified'] = Bonus::where('bonus_to','1')
                ->where('bonus_type','unqualified')
                ->wherebetween('created_at',[$start,date('Y-m-d')])
                ->sum('bonus_amount');
            $data['freeze'] = Bonus::where('bonus_to','1')
                ->where('bonus_type','freeze')
                ->wherebetween('created_at',[$start,date('Y-m-d')])
                ->sum('bonus_amount');
        }elseif($start != NULL && $end != NULL){
            //Premi
            $data['sponsor'] = Bonus::where('bonus_type','sponsor')->whereBetween('created_at',[$start,$end])->sum('bonus_amount');
            //Bonus
            $data['total'] = Bonus::where('bonus_to','1')
                ->wherebetween(DB::raw('DATE(created_at)'),[$start,$end])->sum('bonus_amount');
            $data['generasi'] = Bonus::where('bonus_to','1')->where('bonus_type','generasi')
                ->wherebetween('created_at',[$start,date('Y-m-d')])
                ->sum('bonus_amount');
            $data['unqualified'] = Bonus::where('bonus_to','1')
                ->where('bonus_type','unqualified')
                ->wherebetween('created_at',[$start,date('Y-m-d')])
                ->sum('bonus_amount');
            $data['freeze'] = Bonus::where('bonus_to','1')
                ->where('bonus_type','freeze')
                ->wherebetween('created_at',[$start,date('Y-m-d')])
                ->sum('bonus_amount');
        }elseif($start === NULL && $end != NULL){
            return redirect()->back()->with('error','Maaf Start Date tidak boleh kosong!');
        }elseif($start>$end){
            return redirect()->back()->with('error','Maaf Start Date tidak boleh lebih besar dari End Date!');
        }
        else{
            $data['sponsor'] = Bonus::where('bonus_type','sponsor')->sum('bonus_amount');
            $data['total'] = Bonus::where('bonus_to','1')->sum('bonus_amount');
            $data['generasi'] = Bonus::where('bonus_to','1')->where('bonus_type','generasi')->sum('bonus_amount');
            $data['unqualified'] = Bonus::where('bonus_to','1')->where('bonus_type','unqualified')->sum('bonus_amount');
            $data['freeze'] = Bonus::where('bonus_to','1')->where('bonus_type','freeze')->sum('bonus_amount');
        }

//        $total = Bonus::where('bonus_to','1')->sum('bonus_amount');
//        $total = Bonus::where('bonus_to','1')->sum('bonus_amount');

//        return $generasi;
        return view("vendor.voyager.Report.report-bonus",$data);
    }

    public function detailBonus(Request $request){
        $start = $request->start;
        $end    = $request->end;
        $status    = $request->status;
        $type   = $request->type;

        if($type != NULL){
            if($start != NULL && $end==NULL){
                $data['bonus'] = Bonus::where('bonus_type','sponsor')
                    ->whereBetween('created_at',[$start,date('Y-m-d')])->paginate(50);

                $data['new'] = Bonus::where('bonus_type','sponsor')
                    ->whereBetween('created_at',[$start,date('Y-m-d')])
                    ->where('bonus_status','new')->sum('bonus_amount');

                $data['paid']= Bonus::where('bonus_type','sponsor')
                    ->whereBetween('created_at',[$start,date('Y-m-d')])
                    ->where('bonus_status','paid')->sum('bonus_amount');

                $data['jumlah'] = Bonus::where('bonus_type','sponsor')
                    ->whereBetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');
            }elseif($start != NULL && $end!=NULL){
                $data['bonus'] = Bonus::where('bonus_type','sponsor')
                    ->whereBetween('created_at',[$start,$end])->paginate(50);

                $data['new'] = Bonus::where('bonus_type','sponsor')
                    ->whereBetween('created_at',[$start,$end])
                    ->where('bonus_status','new')->sum('bonus_amount');

                $data['paid']= Bonus::where('bonus_type','sponsor')
                    ->whereBetween('created_at',[$start,$end])
                    ->where('bonus_status','paid')->sum('bonus_amount');

                $data['jumlah'] = Bonus::where('bonus_type','sponsor')
                    ->whereBetween('created_at',[$start,$end])
                    ->sum('bonus_amount');
            }else{
                $data['bonus'] = Bonus::where('bonus_type','sponsor')->paginate(50);

                $data['new'] = Bonus::where('bonus_type','sponsor')
                    ->where('bonus_status','new')->sum('bonus_amount');

                $data['paid']= Bonus::where('bonus_type','sponsor')
                    ->where('bonus_status','paid')->sum('bonus_amount');

                $data['jumlah'] = Bonus::where('bonus_type','sponsor')->sum('bonus_amount');
            }
            return view("vendor.voyager.Report.detail-report-bonus",$data);
        }

        if($status === 'generasi'){
            if($start != NULL && $end == NULL){
                $data['bonus'] = Bonus::where('bonus_to','1')->where('bonus_type','generasi')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->orderBy('bonus_id','desc')
                    ->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','new')
                    ->where('bonus_type','generasi')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','paid')
                    ->where('bonus_type','generasi')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');

                $data['jumlah'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','generasi')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');
            }elseif($start != NULL && $end != NULL){
                $data['bonus'] = Bonus::where('bonus_to','1')->where('bonus_type','generasi')
                    ->wherebetween('created_at',[$start,$end])
                    ->orderBy('bonus_id','desc')
                    ->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','new')
                    ->where('bonus_type','generasi')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','paid')
                    ->where('bonus_type','generasi')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');

                $data['jumlah'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','generasi')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');
            }else{
                $data['bonus'] = Bonus::where('bonus_to','1')->where('bonus_type','generasi')
                    ->orderBy('bonus_id','desc')->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','new')
                    ->where('bonus_type','generasi')
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','paid')
                    ->where('bonus_type','generasi')
                    ->sum('bonus_amount');

                $data['jumlah'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','generasi')
                    ->sum('bonus_amount');
            }
        }elseif($status === 'unqualified'){
            if($start != NULL){
                $data['bonus'] = Bonus::where('bonus_to','1')->where('bonus_type','unqualified')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->orderBy('bonus_id','desc')
                    ->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','new')
                    ->where('bonus_type','unqualified')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','paid')
                    ->where('bonus_type','unqualified')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');

                $data['jumlah'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','unqualified')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');
            }elseif($start != NULL && $end != NULL){
                $data['bonus'] = Bonus::where('bonus_to','1')->where('bonus_type','unqualified')
                    ->wherebetween('created_at',[$start,$end])
                    ->orderBy('bonus_id','desc')
                    ->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','unqualified')
                    ->where('bonus_status','new')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','unqualified')
                    ->where('bonus_status','paid')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');

                $data['jumlah'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','unqualified')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');
            }else{
                $data['bonus'] = Bonus::where('bonus_to','1')->where('bonus_type','unqualified')
                    ->orderBy('bonus_id','desc')->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','unqualified')
                    ->where('bonus_status','new')
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','unqualified')
                    ->where('bonus_status','paid')
                    ->sum('bonus_amount');

                $data['jumlah'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','unqualified')
                    ->sum('bonus_amount');
            }
        }elseif($status === 'freeze'){
            if($start != NULL){
                $data['bonus'] = Bonus::where('bonus_to','1')->where('bonus_type','freeze')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->orderBy('bonus_id','desc')
                    ->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->where('bonus_status','new')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->where('bonus_status','paid')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');

                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');
            }elseif($start != NULL && $end != NULL){
                $data['bonus'] = Bonus::where('bonus_to','1')->where('bonus_type','freeze')
                    ->wherebetween('created_at',[$start,$end])
                    ->orderBy('bonus_id','desc')
                    ->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->where('bonus_status','new')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->where('bonus_status','paid')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');

                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');
            }else{
                $data['bonus'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->where('bonus_status','new')
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->where('bonus_status','paid')
                    ->sum('bonus_amount');

                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_type','freeze')
                    ->sum('bonus_amount');
            }
        }else{
            if($start != NULL){
                $data['bonus'] = Bonus::where('bonus_to','1')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->orderBy('bonus_id','desc')
                    ->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','new')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','paid')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');

                $data['paid'] = Bonus::where('bonus_to','1')
                    ->wherebetween('created_at',[$start,date('Y-m-d')])
                    ->sum('bonus_amount');
            }elseif($start != NULL && $end != NULL){
                $data['bonus'] = Bonus::where('bonus_to','1')
                    ->wherebetween('created_at',[$start,$end])
                    ->orderBy('bonus_id','desc')
                    ->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','new')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','paid')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');

                $data['paid'] = Bonus::where('bonus_to','1')
                    ->wherebetween('created_at',[$start,$end])
                    ->sum('bonus_amount');
            }else{

                $data['bonus'] = Bonus::where('bonus_to','1')
                    ->orderBy('bonus_id','desc')->paginate(50);

                $data['new'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','new')
                    ->sum('bonus_amount');
                $data['paid'] = Bonus::where('bonus_to','1')
                    ->where('bonus_status','paid')
                    ->sum('bonus_amount');

                $data['paid'] = Bonus::where('bonus_to','1')
                    ->sum('bonus_amount');
            }
        }



        return view("vendor.voyager.Report.detail-report-bonus",$data);
    }

    public function exportBonus(Request $request){

        $type = $request->type;
        $start = $request->start;
        $end = $request->end;
        $status = $request->status;
//        return $request->all();
        if($type != NULL) {
            return (new BonusExport)->filter($type, $start, $end, $status)->download('laporan-bonus-' .$type.date('d-M-Y-h-i-s') . '.xlsx');
        }else{
            return (new BonusExport)->filter($type, $start, $end, $status)->download('laporan-bonus-' .$status.date('d-M-Y-h-i-s') . '.xlsx');
        }
    }

//    public function detailPolis(Request $request){
//        $start = $request->start;
//        $end    = $request->end;
//        $status    = $request->status;
//    }
}
