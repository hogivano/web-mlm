<?php

namespace App\Http\Controllers\Admin;

use App\Bank;
use App\BankCompany;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankCompanyController extends Controller
{
    //
    public function index(){
      $data = BankCompany::with('bank')->get();
      return view('admin.bank_company.index', compact('data'));
    }

    public function new(){
      $bank = Bank::orderBy('name', 'ASC')->get();
      return view('admin.bank_company.new', compact('bank'));
    }

    public function create(Request $req){
      $new = BankCompany::create([
        'bank_id' => $req->bank_id,
        'bank_account' => $req->bank_account,
        'bank_number' => $req->bank_number
      ]);

      flash('data saved')->success();
      return redirect()->route('admin.bank_company');
    }

    public function edit($id){
      $bank = Bank::orderBy('name', 'ASC')->get();
      $data = BankCompany::where('id', $id)->first();
      return view('admin.bank_company.edit', compact('data', 'bank'));
    }

    public function update(Request $req, $id){
      $update = BankCompany::where('id', $id)->update([
        'bank_id' => $req->bank_id,
        'bank_account' => $req->bank_account,
        'bank_number' => $req->bank_number
      ]);

      flash('success update')->success();
      return redirect()->route('admin.bank_company');
    }

    public function delete(Request $req){
      $delete = BankCompany::where('id', $req->id)->delete();
      flash('success delete')->success();
      return redirect()->route('admin.bank_company');
    }
}
