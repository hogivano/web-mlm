<?php

namespace App\Http\Controllers\Admin;

use App\Member;
use App\Models\Bonus;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\MatchingMember;
use App\Models\Mutation;
use App\PolishPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request as FacadesRequest;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;

class MembersController extends VoyagerBaseController {

    public function verification(Request $req){
      $update = Member::where('id', $req->id);
      return view('admin.member.verification');
    }

    public function index(Request $request) { z
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search          = (object)['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable      = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $orderBy         = $request->get('order_by', $dataType->order_column);
        $sortOrder       = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;
        $orderColumn     = [];
        if ($orderBy) {
            $index       = $dataType->browseRows->where('field', $orderBy)->keys()->first() + 1;
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder   = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) && app('VoyagerAuth')->user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query           = $query->withTrashed();
                }
            }



            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value  = ($search->filter == 'equals') ? $search->value : '%' . $search->value . '%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder  = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($request->get('keyword') != '') {
                $dataTypeContent = call_user_func([$query->SearchKeyMember($request->get('keyword'))->orderBy($model->getKeyName(), 'DESC'), $getter]);
                if ($request->get('start_date') != '' && $request->get('end_date') === '') {
                    $dataTypeContent = call_user_func([$query->SearchKeyMember($request->get('start_date'))->orderBy($model->getKeyName(), 'DESC'), $getter]);
                }
                if ($request->get('start_date') != '' && $request->get('start_date') != '') {
                    $dataTypeContent = call_user_func([$query->SearchDate($request->get('start_date'), $request->get('end_date'))->orderBy($model->getKeyName(), 'DESC'), $getter]);
                }
                if ($request->member_type != '') {
                    $dataTypeContent = call_user_func([$query->WithType($request->get('member_type'))->orderBy($model->getKeyName(), 'DESC'), $getter]);
                }
            }elseif ($request->get('start_date') != '' && $request->get('end_date') === '') {
                if ($request->member_type != '') {
                    $dataTypeContent = call_user_func([$query->WithType($request->get('member_type'))->WithStartDate($request->get('start_date'))->orderBy($model->getKeyName(), 'DESC'), $getter]);
                }else{
                    $dataTypeContent = call_user_func([$query->WithStartDate($request->get('start_date'))->orderBy($model->getKeyName(), 'DESC'), $getter]);
                }
            } elseif ($request->get('start_date') != '' && $request->get('start_date') != '') {
                if ($request->member_type != '') {

                    $dataTypeContent = call_user_func([$query->WithType($request->get('member_type'))->SearchDate($request->get('start_date'), $request->get('end_date'))->orderBy($model->getKeyName(), 'DESC'), $getter]);
                } else {
                    $dataTypeContent = call_user_func([$query->SearchDate($request->get('start_date'), $request->get('end_date'))->orderBy($model->getKeyName(), 'DESC'), $getter]);
                }
            } elseif ($request->member_type != '') {
                $dataTypeContent = call_user_func([$query->WithType($request->get('member_type'))->orderBy($model->getKeyName(), 'DESC'), $getter]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model           = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }
//return $dataTypeContent->currentPage();
        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchable',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted'
        ));

    }

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }
        //counting polish
        $polish = PolishPayment::where('member_id',$id)->count();
        //bonus
        $bonusNew = Bonus::where('bonus_to',$id)->where('bonus_status','new')->sum('bonus_amount');
        $bonus = Bonus::where('bonus_to',$id)->sum('bonus_amount');
        //balance
        $member = Member::where('member_id',$id)->first();
        $balance = $member->balance;
//        return $debit;
//        return $bonus;


//        return $polish;

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'isSoftDeleted',
            'polish',
            'bonus',
            'balance',
            'bonusNew',
            'member'
        ));
    }

    public function matching(Request $request){

        $userInput = 0;
        $count_member = $request->count_member;

        // return $count_member;
        if($count_member==NULL || $count_member==0 || !isset($count_member)){
            $new_member = [];
            $last_member = [];
        }else{
            $members = Member::orderBy('member_id','desc')
                    ->where('member_status','active')
                    ->count();

            if($members >= ($count_member*2)){
                $new_member = Member::withCount('NewMemberMatching','OldMemberMatching')
                            ->has('NewMemberMatching','=','0')
                            ->orHas('OldMemberMatching','=','0')
                            ->orderBy('member_id','desc')
                            ->where('member_status','active')
                            ->limit($count_member)
                            ->get();

                $last_member = Member::withCount('NewMemberMatching','OldMemberMatching')
                            ->has('NewMemberMatching','=','0')
                            ->orHas('OldMemberMatching','=','0')
                            ->orderBy('member_id','asc')
                            ->where('member_status','active')
                            ->limit($count_member)
                            ->get();
            }else{
                return redirect()->back()->with('error','Maaf jumlah member tidak cukup.');
            }

        }
        $member_match = MatchingMember::orderBy('match_id','desc')->paginate(15);

        $data['new_member'] = $new_member;
        $data['old_member'] = $last_member;
        $data['member_match'] = $member_match;

        return view('vendor.voyager.members.member-match',$data);
    }

    public function proses_matching(Request $request){
        $new_member = $request['new_member'];
        $old_member = $request['old_member'];

        $match_member = MatchingMember::where('member_new',$new_member)->count();
        if($match_member==0){
            for($x=0;$x<count($new_member);$x++){
                MatchingMember::insert([
                    'member_new' => $new_member[$x],
                    'member_old' => $old_member[$x],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => NULL
                ]);
            }
            $result =  redirect()->back()->with('success','Member matching sukses.');
        }else{
            $result = redirect()->back()->with('error','Member hasbeen matched.');
        }

        return $result;
    }

    public function reset(){
        $table =  MatchingMember::truncate();
        return redirect()->back()->with('success','Data berhasil di reset.');
    }

}
