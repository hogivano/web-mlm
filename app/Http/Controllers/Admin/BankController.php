<?php

namespace App\Http\Controllers\Admin;

use App\Bank;
use App\BankCompany;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankController extends Controller
{
    //
    public function index(){
      $data = Bank::with('country')->get();
      return view('admin.bank.index', compact('data'));
    }

    public function new(){
      $country = Country::orderBy('name', 'ASC')->get();
      return view('admin.bank.new', compact('country'));
    }

    public function create(Request $req){
      $new = Bank::create([
        'country_id' => $req->country_id,
        'name' => strtolower($req->name)
      ]);

      flash('success add bank')->success();
      return redirect()->route('admin.bank');
    }

    public function edit($id){
      $data = Bank::where('id', $id)->with('country')->first();
      $country = Country::orderBy('name', 'ASC')->get();
      return view('admin.bank.edit', compact('data', 'country'));
    }

    public function update(Request $req, $id){
      $update = Bank::where('id', $id)->update([
        'country_id' => $req->country_id,
        'name' => strtolower($req->name)
      ]);

      flash('success update bank')->success();
      return redirect()->route('admin.bank');
    }

    public function delete(Request $req){
      $del = Bank::where('id', $req->id)->delete();
      $delCompany = BankCompany::where('bank_id', $req->id)->delete();

      flash('succes delete bank')->success();
      return redirect()->route('admin.bank');
    }
}
