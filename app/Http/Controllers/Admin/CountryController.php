<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    //
    public function index(){
      $data = Country::with('bank')->paginate(10);
      return view('admin.country.index', compact('data'));
    }

    public function new(){
      return view('admin.country.new');
    }

    public function create(Request $req){
      $new = Country::create([
        'name' => $req->name,
        'time_zone' => $req->time_zone,
        'code_number' => $req->code_number
      ]);

      flash('success created')->success();
      return redirect()->route('admin.country');
    }

    public function edit($id){
      $data = Country::where('id', $id)->first();
      return view('admin.country.edit', compact('data'));
    }

    public function update(Request $req, $id){
      $update = Country::where('id', $id)->update([
        'name' => $req->name,
        'time_zone' => $req->time_zone,
        'code_number' => $req->code_number
      ]);

      flash('success updated')->success();
      return redirect()->route('admin.country');
    }

    public function delete(Request $req){
      $delete = Country::where('id', $req->id)->delete();
      flash('success deleted')->success();
      return redirect()->route('admin.country');
    }
}
