<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Character;
use App\Member;
use App\MemberData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerController;

class HomeController extends Controller
{
    public function __construct(){
      $this->middleware('auth:web');
    }

    public function test(){
      $test = date('Y-m-d H:i:s', strtotime('+10 minutes', strtotime('00:12:00')));
      $test = Character::with(['charactersOrder' => function($q){
        $q->where('status', 'request');
      }])->get();
      return response()->json($test);
    }

    public function index()
    {
      return view('admin.dashboard');
        // return view('vendor.voyager.index');
    }

    public function fixMemberData(){
      $member = Member::all();
      $onUpdate = 0;
      foreach ($member as $i) {
        // code...
        if ($i->member_data_id == null){
          $cek = MemberData::where('member_id', $i->id)->first();
          if($cek){
            $onUpdate++;
            $i->member_data_id = $cek->id;
            $i->save();
          }
        }
      }

      return response()->json('success update total count ' . $onUpdate);
    }

    public function setting(){
      return view('admin.setting');
    }

    public function profile(){
      return view('admin.profile');
    }

    public function logout(){
      Auth::guard('web')->logout();
      return redirect()->route('admin.login');
    }
}
