<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    //
    public function index(){
      $data = Setting::orderBy('key', 'ASC')->get();
      return view('admin.setting.index', compact('data'));
    }

    public function new(){
      return view('admin.setting.new');
    }

    public function create(Request $req){

    }

    public function edit($id){
      $data = Setting::where('id', $id)->first();
      return view('admin.setting.edit', compact('data'));
    }

    public function update(Request $req, $id){
      $update = Setting::where('id', $id)->update([
        'value' => $req->value
      ]);

      flash('Success update setting')->success();
      return redirect()->route('admin.setting');
    }
}
