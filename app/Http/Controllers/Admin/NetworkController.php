<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Network;

class NetworkController extends Controller
{
    public $levelKedalaman = 10;

    public function __construct(){
      $this->middleware('auth:web');
    }

    public function index(Request $request){

        // $levelKedalaman = $this->levelKedalaman;
        //
        // $data['networks'] = $this->createTreeView(-1, $this->dataBuilder($levelKedalaman));
        // $data['uplineId'] = "";
        // $data['levelKedalaman'] = $this->levelKedalaman;
        // return view('vendor.voyager.networks.browse', $data);
    }

    public function networks($id){
        if($id == 1){
            return redirect('/admin/networks');
        }

        $data['current'] = Network::join('members', 'networks.member_id', '=', 'members.member_id')
            ->where('networks.member_id', '=', $id)
            ->first();
        $levelKedalaman = $data['current']->network_level + $this->levelKedalaman;

        $current = Network::where('member_id', '=', $id)->first();
        $data['upline'] = Network::join('members', 'networks.member_id', '=', 'members.member_id')
        ->where('networks.member_id', '=', $current['upline_id'])->first();

        $data['networks'] = $this->createTreeView($id, $this->dataBuilder($levelKedalaman));
        $data['uplineId'] = $current->upline_id;
        $data['levelKedalaman'] = $levelKedalaman;
        return view('vendor.voyager.networks.browse', $data);
    }

    public function createTreeView($parent, $menu) {
        $html = "";

        if (isset($menu['parents'][$parent])) {
            $html .= "
                <ol class='dd-list'>
            ";
            foreach ($menu['parents'][$parent] as $itemId) {
                if(!isset($menu['parents'][$itemId])) {
                    $uri = "/admin/networks/{$menu['items'][$itemId]['member_id']}";
                    $uri_member = "/admin/members/{$menu['items'][$itemId]['member_id']}";
                    $nama = ucwords($menu['items'][$itemId]['member_name']);
                    $html .= "
                    <li class='dd-item' data-id='1'>
                        <div class='dd-handle handle-null'>
                            <a href='$uri' style='padding-left: 13px;'>
                                <i class='zmdi zmdi-account' style='padding-right:5px;'></i>
                                {$nama}
                            </a>
                            <span class='pull-right'>Q{$menu['items'][$itemId]['member_qualified']} <a href='$uri_member'><i class='zmdi zmdi-eye' style='font-size: 14px; padding-right: 8px;'></i></a>  {$menu['items'][$itemId]['downline_count']} Downline
                            </span>
                        </div>
                    </li>";
                }
                if(isset($menu['parents'][$itemId])) {
                    $uri = "/admin/networks/{$menu['items'][$itemId]['member_id']}";
                    $uri_member = "/admin/members/{$menu['items'][$itemId]['member_id']}";
                    $nama = ucwords($menu['items'][$itemId]['member_name']);
                    $html .= "
                    <li class='dd-item' data-id='2'>
                        <div class='dd-handle'>

                            <a href='$uri'>
                                <i class='zmdi zmdi-account' style='padding-right:5px;'></i>
                                {$nama}
                            </a>
                            <span class='pull-right'>Q{$menu['items'][$itemId]['member_qualified']} <a href='$uri_member'><i class='zmdi zmdi-eye' style='font-size: 14px; padding-right: 8px;'></i></a>    {$menu['items'][$itemId]['downline_count']} Downline
                            </span>
                        </div>
                    ";
                    $html .= $this->createTreeView($itemId, $menu);
                    $html .= "</li>";
                }
            }
            $html .= "</ol>";
        }
        return $html;
    }

    public function dataBuilder($levelKedalaman)
    {
        $networks = Network::join('members', 'networks.member_id', '=', 'members.member_id')
            ->where('network_status', '=', 'active')
            ->where('network_level','<=',$levelKedalaman)
            ->orderBy('kaki','ASC')
            ->get();

        $menus = array(
            'items' => array(),
            'parents' => array()
        );

        foreach ($networks as $items) {
            // Create current menus item id into array
            $menus['items'][$items['member_id']] = $items;
            // Creates list of all items with children
            $menus['parents'][$items['upline_id']][] = $items['member_id'];
        }

        return $menus;
    }
}
