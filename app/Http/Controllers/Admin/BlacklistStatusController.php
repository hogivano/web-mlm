<?php

namespace App\Http\Controllers\Admin;

use App\BlacklistStatus;
use App\BlacklistMember;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlacklistStatusController extends Controller
{
    //
    public function index(){
      $data = BlacklistStatus::with(['blacklistMember' => function($q){
        $q->where('status', 'on');
      }, 'blacklistMember' => function($q){
        $q->where('status', 'off');
      }])->get();

      return view('admin.blacklist_status.index', compact('data'));
    }

    public function new(){
      return view('admin.blacklist_status.new');
    }

    public function create(Request $req){
      if (strlen($req->color) == 7 || strlen($req->color) == 4){
        if(substr($req->color, 0, 1) != "#"){
          flash('Sorry format color #xxx or #xxxxxx')->error();
          return redirect()->back()->withInput();
        }

        $pattern = "/^[-_a-zA-Z0-9.]+$/";
        if (!preg_match($pattern, substr($req->color, 1, strlen($req->color)))){
          flash('Sorry format color just support word and number')->error();
          return redirect()->back()->withInput();
        }

        $new = BlacklistStatus::create([
          'name' => $req->name,
          'color' => $req->color
        ]);

        flash('Success created blacklist status')->success();
        return redirect()->route('admin.blacklist_status');
      } else {
        flash('Sorry format color #xxx or #xxxxxx')->error();
        return redirect()->back()->withInput();
      }
    }

    public function edit($id){
      $data = BlacklistStatus::where('id', $id)->first();
      return view('admin.blacklist_status.edit', compact('data'));
    }

    public function update(Request $req, $id){
      if (strlen($req->color) == 7 || strlen($req->color) == 4){
        if(substr($req->color, 0, 1) != "#"){
          flash('Sorry format color #xxx or #xxxxxx')->error();
          return redirect()->back()->withInput();
        }

        $pattern = "/^[-_a-zA-Z0-9.]+$/";
        if (!preg_match($pattern, substr($req->color, 1, strlen($req->color)))){
          flash('Sorry format color just support word and number')->error();
          return redirect()->back()->withInput();
        }

        $update = BlacklistStatus::where('id', $id)->update([
          'name' => $req->name,
          'color' => $req->color
        ]);

        flash('Success created blacklist status')->success();
        return redirect()->route('admin.blacklist_status');
      } else {
        flash('Sorry format color #xxx or #xxxxxx')->error();
        return redirect()->back()->withInput();
      }
    }

    public function delete(Request $req){
      $del = BlacklistStatus::where('id', $req->id)->delete();
      $del = BlacklistMember::where('blacklist_status_id', $req->id)->delete();
      flash('success delete blacklist status')->success();

      return redirect()->route('admin.blacklist_status');
    }
}
