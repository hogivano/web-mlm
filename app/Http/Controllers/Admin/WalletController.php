<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Wallet;
use App\Member;
use App\TokenTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image as Image;

class WalletController extends Controller
{
    //
    public function __construct(){
      $this->middleware('auth:web');
    }

    public function soul(){
      $requestBuy = TokenTransaction::where('transaction_status', 'request')->with('memberTo', 'memberFrom')->get();
      $historiesBuy = TokenTransaction::where('transaction_type', 'buy')->orWhere('transaction_type', 'buyback')->with('memberTo', 'memberFrom')->paginate(10, ['*'] ,'page-buy')->appends(request()->query());
      $historiesTransfer = TokenTransaction::where('transaction_type', 'transfer')->with('memberTo', 'memberFrom')->paginate(10, ['*'] ,'page-tf')->appends(request()->query());

      return view('admin.wallet.soul.index', compact('requestBuy', 'historiesBuy', 'historiesTransfer'));
    }

    public function soulSetStatus(Request $req){
      if ($req->transaction_type == 'buy'){

        $update = TokenTransaction::find($req->tt_id);
        if(!$update){
          flash('token transaction not found')->error();
          return redirect()->back();
        }

        if ($req->status == 'success'){
          $wallet = Wallet::where('member_id', $update->transaction_to)->with('member')->first();
          if (!$wallet){
            flash('wallet not found')->error();
            return redirect()->back();
          }

          if ($wallet->soul - $update->token_amount < 10){
            flash('Sorry, min 10 soul in this account')->error();
            return redirect()->back();
          }

          $wallet->soul = $wallet->soul + $update->token_amount;
          $wallet->save();
        }

        $update->transaction_status = $req->status;
        $update->save();

        flash('successful update status ' . $wallet->member->username)->success();
        return redirect()->back();
        // $wallet =
      } elseif($req->transaction_type == 'buyback') {
        $update = TokenTransaction::find($req->tt_id);
        if (!$update){
          flash('soul transaction not found')->error();
          return redirect()->back();
        }

        $wallet = Wallet::where('member_id', $update->transaction_to)->with('member')->first();
        if ($req->status == 'success'){
          if ($wallet->soul < $update->token_amount){
            flash('cannot change buyback status. Because soul ' . $wallet->member->username . ' not enough')->error();
            return redirect()->back();
          }
          $wallet->soul = $wallet->soul - $update->token_amount;
          $wallet->save();
        }

        $update->transaction_status = $req->status;
        $update->save();

        flash('successful update buyback status ' . $wallet->member->username)->success();
        return redirect()->back();
      } else {
        return response()->json($req->all());
      }
    }

    public function soulBuy(){
      return view('admin.wallet.soul.buy.index');
    }

    public function soulBuySubmit(Request $req){

    }

    public function imageProofOfPayment($id){
      $cek = TokenTransaction::where('tt_id', $id)->first();
      if ($cek->proof_of_payment == null){
        flash('sorry this member not upload the proof of payment')->error();
        return redirect()->back();
      }

      $contents = Storage::get($cek->proof_of_payment);
      if (!$contents){
        flash('sorry this member not upload the proof of payment')->error();
        return redirect()->back();
      }

      $file_extension = strtolower(substr(strrchr($cek->proof_of_payment,"."),1));

      switch( $file_extension ) {
          case "gif": $ctype="image/gif"; break;
          case "png": $ctype="image/png"; break;
          case "jpeg": $ctype="image/jpeg"; break;
          case "jpg": $ctype="image/jpeg"; break;
          default:
      }
      ob_clean();
      header("Content-type: " . $ctype);
      $response = Image::make($contents)->response($ctype);
      // $response->header('Content-Type', $ctype);
      return $response;
    }

    public function uploadProofOfPayment(Request $req){
      $rules = [
        'proof_of_payment' => 'mimes:jpg,jpeg,png|max:10240',
      ];

      $validation = Validator::make($req->all(), $rules);
      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return redirect()->back()->withInput();
      }

      $tokenTransaction = TokenTransaction::where('tt_id', $req->tt_id)->first();

      if ($req->hasFile('proof_of_payment')){
        $saveImg = Storage::put('public/token_transaction' . '/' . $tokenTransaction->tt_id , $req->file('proof_of_payment'));
        $tokenTransaction->proof_of_payment = $saveImg;
        $tokenTransaction->save();

        flash('success upload payment')->success();
      } else {
        flash('file payment not found')->error();
      }

      return redirect()->back();
    }
}
