<?php

namespace App\Http\Controllers;

use App\Models\Bonus;
use App\Models\Withdrawal;
use App\Mutation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BonusController extends Controller
{
    public function index(){
        $currentId = Session::get('user_item')->member_id;
        $bonus = Bonus::where('bonus_to',$currentId)
                ->orderBy('bonus_id','asc')->paginate(50);
//       return $bonus[0]->memberFrom->member_name;
        return view('memberarea.bonus.bonus',[
           'bonus_list' => $bonus,
           'no'         => 1
        ]);
    }

    public function mutasi(){
        $currentId = Session::get('user_item')->member_id;
        $mutation_list = Mutation::where('member_id',$currentId)->orderBy('mutation_id','asc')->get();
        $total_debit = Mutation::where('member_id',$currentId)->sum('mutation_debit');
        $total_credit = Mutation::where('member_id',$currentId)->sum('mutation_credit');

        $data['mutation_list'] = $mutation_list;
        $data['total_debit'] = $total_debit;
        $data['total_credit'] = $total_credit;

//        return $mutation_list;
        return view('memberarea.bonus.mutasi-bonus',$data);
    }

    public function detail($mutasiId){
        $detail['detail'] = Withdrawal::join('mutations','withdrawals.mutation_id','=','mutations.mutation_id')
                        ->where('mutations.mutation_id',$mutasiId)
                        ->where('mutations.member_id',Session::get('user_item')->member_id)
                        ->select('withdrawals.*','mutations.mutation_desc')
                        ->first();
//        return json_encode($detail);
        return view('memberarea.bonus.detail-mutasi',$detail);
    }
}
