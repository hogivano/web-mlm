<?php

/**
 * Created by PhpStorm.
 * User: j3p0n
 * Date: 5/4/2019
 * Time: 10:56 PM
 */

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Member;
//use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SponsorController extends Controller {

    public function __construct() {
        $this->middleware('auth:member');
    }

    public function index(Request $request) {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }

        $keyword = $request->get('keyword');
        $type    = $request->get('type');
//       return $type;
        $query = Member::join('networks', 'members.member_id', '=', 'networks.member_id')
            ->where('networks.sponsor_id', Auth::guard('member')->user()->member_id);

        if($keyword != ''){
            $query = $query->where(function ($q) use ($keyword){
               $q->orwhere('member_username','like','%'.$keyword.'%')
                 ->orwhere('member_name','like','%'.$keyword.'%')
                 ->orwhere('member_email','like','%'.$keyword.'%');
            });
        }

        if($type != ''){
            $query = $query->where('member_type',$type);
        }

        $sponsor_list = $query->select('members.*')->orderBy('members.member_id', 'desc')->get();

//        return $sponsor_list;
        return view('memberarea.sponsor.sponsor', [
            'sponsor_list' => $sponsor_list,
            'no' => 1,
            'member_type' => Auth::guard('member')->user()->member_type
        ]);
//        return $sponsor_list;
    }

    public function detail($id) {
        $id = (int)$id;
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }
        $member_item['member_item'] = Member::select('*')
            ->selectraw("(SELECT members.member_name FROM members JOIN networks ON members.member_id = networks.sponsor_id WHERE networks.member_id = " . $id . ") as sponsor ")
            ->selectraw("(SELECT networks.sponsor_id FROM members JOIN networks ON members.member_id = networks.member_id WHERE networks.member_id = " . $id . ") as sponsor_id ")
            ->selectraw("(SELECT members.member_name FROM members JOIN networks ON members.member_id = networks.upline_id WHERE networks.member_id = " . $id . ") as upline ")
            ->selectraw("(SELECT city.city_province FROM city JOIN members ON city.city_id = members.member_city WHERE members.member_id = '" . $id . "') as province")
            ->selectraw("(SELECT city.city_name FROM city JOIN members ON city.city_id = members.member_city WHERE members.member_id = '" . $id . "') as city")
            ->where('members.member_id', $id)
            ->first();

//        return json_encode($member_item);
        //cek sponsor dia
        if($member_item['member_item'] === NULL){
            return redirect('/membership/sponsor-ship')->with('error','Maaf data bukan dalam satu jaringan.');
        }

        if($member_item['member_item']->sponsor_id != Auth::guard('member')->user()->member_id){
            return redirect('/membership/sponsor-ship')->with('error','Maaf data bukan dalam satu jaringan.');
        }

       $member_item['member_type'] = Auth::guard('member')->user()->member_type;
//        return json_encode($member_item);
        return view('memberarea.sponsor.detail', $member_item);
    }
}
