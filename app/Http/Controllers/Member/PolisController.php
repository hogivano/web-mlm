<?php

namespace App\Http\Controllers\Member;

use App\Bonus;
use App\Http\Controllers\Controller;
use App\Http\Services\NetworkService;
use App\PolishPayment;
use App\Member;
use App\TokenTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class PolisController extends Controller {

    public function __construct() {
        $this->middleware('auth:member');
    }

    public function index() {
        $member_id   = Auth::guard('member')->user()->member_id;
        $polish_list = PolishPayment::where('member_id', $member_id)->get();
        return view('memberarea.polis.polis', [
            'polish_list' => $polish_list,
            'no' => 1,
            'member_type' => Auth::guard('member')->user()->member_type
        ]);
    }

    public function beliPolis(){
        $currentUserId   = Auth::guard('member')->user()->member_id;
        $sisa_nasabah    = Member::find($currentUserId)->getSisaTokenAttribute('nasabah');

        $polis_aktif = PolishPayment::where('member_id',$currentUserId)->count();
//        $polis_aktif = 0;
        $polis_pasif = 12-$polis_aktif;
        //if($polis_aktif==0){
        //    $polis_pasif = 12;
        //}
        return view('memberarea.polis.beli-polish',[
            'token_nasabah' => $sisa_nasabah,
            'polis_aktif' => $polis_aktif,
            'polis_pasif' => $polis_pasif
        ]);
    }

    public function insertPolish(Request $request, NetworkService $networkService){
        $currentUser   = Auth::guard('member')->user();
        $currentUserId   = $currentUser->member_id;
        $sisa_nasabah    = Member::find($currentUserId)->getSisaTokenAttribute('nasabah');
        //Validasi Inputan
        $rules = [
            'periode' => 'required|string|max:12',
            'password'       => 'required|string|max:100',
        ];

        //error Message
        $errorMsg   = [
            'periode.required'  => 'Periode tidak boleh kosong.',
            'password.required'        => 'Maaf Password Harus diisi.'
        ];
        $validation = Validator::make($request->all(), $rules, $errorMsg);
        if ($validation->fails()) {
            return redirect()->back()->with([
                'error' => $validation->errors()->first(),
            ])->withInput();
        }

        //validasi max
        if($request->periode > 12){
            return redirect()->back()->with('error','Periode tidak boleh lebih dari 12');
        }

        //validasi password
        if (!Hash::check($request['password'], Auth::guard('member')->user()->member_password)) {
            return redirect()->back()->with([
                'error' => "Password Anda salah"

            ])->withInput();
        }
        //validasi Token
        if($sisa_nasabah==0){
            return redirect()->back()->with('error','Jumlah Token tidak mencukupi!');
        }


        $bonusTo = $currentUserId;
        $bonusFrom = $currentUserId;

        if ($currentUser->member_type === "nasabah"){
            $bonusTo = $currentUser->sponsor_id;
        }

        $periode = $request->periode;
        for($i=0;$i<$periode;$i++){
            $polis = new PolishPayment();
            $polis->member_id = $currentUserId;
            $polis->nominal = 150000;
            $polis->polis_date = NOW();
            $polis->polis_exp_date = Carbon::parse($polis->polis_date)->addMonths(12);
            $polis->nomor_polis_induk = '';
            $polis->nomor_ringkasan_jaminan = '';
            $polis->file_polish = '';
            $polis->pp_type = 'purchase';
            $polis->pp_status = 'pending';
            $polis->created_at = NOW();
            $polis->updated_at = Now();
            $polis->save();

            //insert bonus sponsor
            Bonus::insert([
                "bonus_to"     => $bonusTo,
                "bonus_from"   => $bonusFrom,
                "bonus_amount" => env("BONUS_SPONSOR_NASABAH", 35000),
                "bonus_type"   => 'sponsor',
                "bonus_desc"   => 'Keuntungan penjualan langsung',
                "bonus_status" => "new",
                "created_at"   => Carbon::now()->toDateTimeString()
            ]);

            //insert bonus level generasi sampe 8 level upline
            //$memberNetworks = $this->nestable($request['upline_id']);
            $networkService->prosesTambahBonus($bonusFrom, $bonusTo);
        }

        $token = new TokenTransaction();
        $token->transaction_from = $currentUserId;
        $token->transaction_to = $currentUserId;
        $token->token_amount = $periode;
        $token->transaction_type = 'purchase';
        $token->token_type = 'nasabah';
        $token->transaction_status = 'sukses';
        $token->created_at = NOW();
        $token->updated_at = NOW();
        $token->save();

//        for ($i=0;$i<$periode;$i++){
//            $polis->save();
//        }

        return redirect()->back()->with('success','Pembelian polish anda berhasil.');


    }
}
