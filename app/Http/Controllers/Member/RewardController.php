<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Network;
use App\Reward;
use Illuminate\Support\Facades\Auth;

class RewardController extends Controller
{
    public function __construct() {
        $this->middleware('auth:member');
    }

    public function index(){
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }
        $currentId = Auth::guard('member')->user()->member_id;
        $reward_list = Reward::get();

        $network_list = Network::where('upline_id',$currentId)->orderBy('downline_count','desc')->get();
        $sum = 0;
        $xMax = count($network_list);
        for($x=1; $x< $xMax; $x++){
            $sum = ($sum + $network_list[$x]->downline_count);
        }
        $data['reward_list'] = $reward_list;
        $data['no'] = 1;
        $data['downline'] = $network_list[0]->downline_count;
        $data['sum'] = $sum;
        $data["member_type"]     = Auth::guard('member')->user()->member_type;
//        return $sum;
//        return $rew1ard_list[]->RewardId[0]->rh_status;
        return view('memberarea.rewards.reward',$data);
    }
}
