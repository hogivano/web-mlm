<?php

namespace App\Http\Controllers\Member;

use Auth;
use App\Member;
use App\TokenTransaction;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class SubAccountController extends Controller
{
    //
    public function __construct(){
      $this->middleware('auth:member');
    }

    public function index(){
      $user = Auth::guard('member')->user();
      $cek = Member::where('id', $user->id)->with('memberData', 'wallet')->first();
      $data = [$cek];
      if ($cek->parent_id == null || $cek->parent_id == '' || !$cek->parent_id || $cek->parent_id == 0){
        $child = Member::where('parent_id', $user->id)->with('wallet')->get();
        foreach ($child as $i) {
          // code...
          array_push($data, $i);
        }
      } else {
        $parent = Member::where('id', $cek->parent_id)->with('wallet')->first();
        array_push($data, $parent);
        $child = Member::where('parent_id', $cek->parent_id)->with('wallet')->get();
        foreach ($child as $i) {
          // code...
          if($cek->id != $i->id){
            array_push($data, $i);
          }
        }
      }
      return view('frontend.sub_account.index', compact('data'));
    }

    public function new(){
      return view('frontend.sub_account.new');
    }

    public function create(Request $req){
      $user = Auth::guard('member')->user();
      if (!Hash::check($req->password, $user->password)) {
        flash('your current password is wrong')->error();
        return redirect()->back()->withInput();
      }

      $cek = Member::where('username', strtolower($req->username))->first();
      if($cek){
        flash('Sorry, username already exists')->error();
        return redirect()->back()->withInput($req->all());
      }

      $cekWallet = Wallet::where('member_id', $user->id)->first();

      if (!$cekWallet){
        flash('Your wallet not found')->error();
        return redirect()->back()->withInput($req->all());
      }

      if($cekWallet->soul < 10 || $cekWallet->soul - 10 < 10){
        flash('Your soul minimum 10 or change another account to add new sub account')->error();
        return redirect()->back()->withInput($req->all());
      }

      $new = Member::create([
        'username' => strtolower($req->username),
        'password' => $user->password,
        'wallet_password' => $user->wallet_password,
        'member_data_id' => $user->member_data_id,
        'parent_id' => ($user->parent_id == null || $user->parent_id == '' || $user->parent_id == 0) ? $user->id : $user->parent_id,
        'type' => 'member',
        'status' => 'active',
        'reg_date' => date('Y-m-d H:i:s')
      ]);

      $newWallet = Wallet::create([
        'member_id' => $new->id,
        'soul' => 10
      ]);

      $cekWallet->soul = $cekWallet->soul - 10;
      $cekWallet->save();

      $newTransaction = TokenTransaction::create([
        'transaction_from' => $user->id,
        'transaction_to' => $new->id,
        'token_amount' => 10,
        'transaction_type' => 'transfer',
        'transaction_status' => 'success',
        'token_type' => 'membership'
      ]);

      flash('success add sub account')->success();
      return redirect()->route('membership.sub_account');
    }

    public function edit($id){

    }

    public function update(Request $req, $id){

    }

    public function changeAccount(Request $req){
      $user = Auth::guard('member')->user();

      $newLogin = Member::where('id', $req->id)->first();

      $cekSub = false;
      if ($user->parent_id == $newLogin->id) {
        $cekSub = true;
      } else if ($user->id == $newLogin->parent_id) {
        $cekSub = true;
      } else if (($user->parent_id == $newLogin->parent_id) &&
        ($user->parent_id != null || $user->parent_id != 0) && ($newLogin->parent_id != null || $newLogin->parent_id != 0)) {
        $cekSub = true;
      }

      if ($cekSub){
        $login = Auth::login($newLogin);
        flash('Success change account')->success();
        return redirect()->route('membership.sub_account');
      } else {
        flash('Sorry is not your account')->error();
        return redirect()->back();
      }
    }
}
