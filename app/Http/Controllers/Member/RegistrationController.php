<?php

namespace App\Http\Controllers\Member;

use Redirect;
use App\Http\Controllers\Controller;
use App\Country;
use App\Wallet;
use App\Member;
use App\MemberData;
use App\Setting;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class RegistrationController extends Controller {

    public function index(Request $request,$type) {
        if (Auth::user()){
          $member_type = Auth::guard('member')->user()->member_type;

          if($member_type === 'nasabah'){
            return redirect('/membership/home');
          }
        }


        if ($type !== "nasabah"){
            return redirect()->back(302);
        }


        $data = Province::get();
        return view('memberarea.member.register-nasabah', [
            'data' => $data,
            'member_type' => Auth::guard('member')->user()->member_type
        ]);
    }

    public function register(){
      $setting = Setting::where('key', 'site.term')->first();
      return view('frontend.register.registrasi', compact('setting'));
    }

    public function registerInfo(){
      return view('frontend.register.registrasi-info');
    }

    public function registerAccountSubmit(Request $req){
      $introducer = $req->username_introducer;
      $cekIntroducer = Member::where('username', strtolower($introducer))->first();
      if (!$cekIntroducer){
        flash('Username introducer not found')->error();
        return Redirect::back()->withInput($req->all());
      }

      if ($cekIntroducer->parent_id == null || $cekIntroducer->parent_id == 0 || !$cekIntroducer->parent_id){
      } else {
        flash('Username introducer not found')->error();
        return Redirect::back()->withInput($req->all());
      }

      if ($introducer == $req->username){
        flash('The introducer username cannot be the same as the registered username')->error();
        return Redirect::back()->withInput($req->all());
      }

      $rules  = [
          'username'            => 'required|string|alpha_dash|regex:/^\S*$/u|min:6|max:50|unique:member,username',
          'email'               => 'required|string|email|max:100',
          'password'            => 'required|string|min:6|confirmed',
          'wallet_password'     => 'required|string|min:6',
      ];

      $validation = Validator::make($req->all(), $rules);

      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return Redirect::back()->withInput($req->all());
      }

      if ($req->wallet_password != $req->wallet_password_confirmation ){
        flash('Wallet password is not the same as the wallet confirmation')->error();
        return Redirect::back()->withInput($req->all());
      }

      $new = Member::create([
        'username' => strtolower($req->username),
        'email' => strtolower($req->email),
        'password' => Hash::make($req->password),
        'wallet_password' => Hash::make($req->wallet_password),
        'sponsor_id' => $cekIntroducer->id,
        'type' => 'member',
        'status' => 'pending',
        'reg_date' => date('Y-m-d H:i:s')
      ]);

      if (Auth::guard('member')->attempt(['username' => $req->username, 'password' => $req->password])) {
        return redirect()->route('membership.register.information');
      } else {
        flash('Sorry you must login first')->error();
        return redirect()->route('membership.login');
      }
    }

    public function registerInformation(){
      $cek = Auth::guard('member')->user();
      $country = Country::with('bank')->orderBy('name', 'ASC')->get();
      if ($cek){
        if ($cek->status != 'pending'){
          return redirect()->route('membership.home');
        } elseif (MemberData::where('id', $cek->member_data_id)->first() && $cek->status == 'pending'){
          return redirect()->route('membership.complete_account');
        }
        return view('frontend.register.registrasi-info', compact('country'));
      } else {
        flash('Sorry, you must login first')->error();
        return redirect()->route('membership.login');
      }
    }

    public function registerInformationSubmit(Request $req){
      $rules  = [
          'full_name'         => 'required|string',
          'number_phone'      => 'required|digits_between:9,14',
          'gender'            => 'required',
          'country'           => 'required',
          'bank'              => 'required',
          'bank_account'      => 'required',
          'bank_number'       => 'required',
      ];

      $validation = Validator::make($req->all(), $rules);

      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return Redirect::back()->withInput($req->all());
      }

      $user = Auth::guard('member')->user();
      $new = MemberData::create([
        'full_name' => $req->full_name,
        'member_id' => $user->id,
        'number_phone' => strval($req->code_number) . strval($req->number_phone),
        'gender' => $req->gender,
        'country_id' => $req->country,
        'bank' => ($req->bank == 'custom') ? $req->bank_name : $req->bank,
        'bank_account' => $req->bank_account,
        'bank_number' => $req->bank_number,
        'swift_code' => $req->swift_code
      ]);

      $update = Member::where('id', $user->id)->update([
        'member_data_id' => $new->id
      ]);

      $newWallet = Wallet::create([
        'member_id' => $user->id,
      ]);

      //Set default status member user
      // $status = Member::where('id', $user->id)->update([
      //   'status' => 'active'
      // ]);

      flash('Data saved');
      return redirect()->route('membership.home');
    }

    public function completeAccount(){
      $user = Auth::guard('member')->user();
      if ($user->status == 'active'){
        return redirect()->route('membership.home');
      }
      return view('frontend.register.complete-account');
    }
}
