<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Member;
use App\Wallet;
use App\TokenTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image as Image;

class TokenController extends Controller {

    public function __construct() {
        $this->middleware('auth:member');
    }

    public function index() {
        return view('frontend.soul.index', [

        ]);
    }

    public function prosesInput(Request $request) {
        $member_type = Auth::guard('member')->user()->member_type;
        if ($member_type === 'nasabah') {
            return redirect('/membership/home');
        }
        //Validasi Inputan
        $rules = [
            'transaction_to' => 'required|string|max:100',
            'jml_amount'     => 'required|digits_between:1,16',
            'password'       => 'required|string|max:100',
        ];

        //error Message
        $errorMsg   = [
            'transaction_to.required'  => 'Maaf Member dengan nama tersebut tidak ditemukan.',
            'jml_amount.digit_between' => 'Maaf digit kurang memenuhi syarat.',
            'jml_amount.required'      => 'Maaf Nominal Harus diisi.',
            'password.required'        => 'Maaf Password Harus diisi.'
        ];
        $validation = Validator::make($request->all(), $rules, $errorMsg);
        if ($validation->fails()) {
            return redirect()->back()->with([
                'error' => $validation->errors()->first(),
            ])->withInput();
        }

        //validasi password
        if (!Hash::check($request['password'], Auth::guard('member')->user()->member_password)) {
            return redirect()->back()->with([
                'error' => "Password Anda salah"

            ])->withInput();
        }
//      validasi username
        $member_item = Member::where('member_username', $request['transaction_to'])
            ->first();
        if ($member_item == NULL) {
          flash('username tidak ditemukan')->error();
            return redirect()->back()->with('error', 'Username tidak ditemukan.');
        }


//        $username           = Session::get('user_item')->member_username;
        $transaction_from = Auth::guard('member')->user()->member_id;
        $transaction_to   = $member_item->member_id;
        $token_amount     = $request['jml_amount'];
        $token_type       = $request['type'];

        //validasi sisa Token

        $token = Member::find($transaction_from)->getSisaTokenAttribute($token_type);
        if ($token < $token_amount) {
          flash('username tidak ditemukan')->error();
            return redirect()->back()->with('error', 'Token Anda tidak cukup.');
        }

        //validate member login
        if ($member_item->member_id == Auth::guard('member')->user()->member_id) {
            return redirect()->back()->with('error', 'Transfer token gagal.');
        }
        //insert ke Table

        $token_transaction                     = new TokenTransaction();
        $token_transaction->transaction_from   = $transaction_from;
        $token_transaction->transaction_to     = $transaction_to;
        $token_transaction->token_amount       = $token_amount;
        $token_transaction->token_type         = $token_type;
        $token_transaction->transaction_type   = 'transfer';
        $token_transaction->created_at         = NOW();
        $token_transaction->updated_at         = NULL;
        $token_transaction->transaction_status = 'sukses';
        $token_transaction->save();

        return redirect()->back()->with('success', 'Transaksi anda berhasil.');
    }

    public function getNameList(Request $request) {
        $member_type = Auth::guard('member')->user()->member_type;
        if ($member_type === 'nasabah') {
            return redirect('/membership/home');
        }
        $term    = $request['search'];
        $name    = Member::where('member_username', 'LIKE', '%' . $term . '%')
            ->get();
        $results = array();
        foreach ($name as $item) {
            $results[] = ["value" => $item->member_username, "label" => $item->member_name, "member_id" => $item->member_id];
        }
        return response()->json($results);
    }

    public function soulTransferSubmit(Request $req){
      $rules = [
          'transaction_to' => 'required|string|max:100',
          'amount'     => 'required|numeric|min:1',
          'wallet_password'       => 'required|string|max:100',
      ];

      $count = explode('.', $req->amount);
      if(sizeof($count) > 1){
        flash('Sorry, format must numeric')->error();
        return redirect()->back()->withInput();
      }

      $validation = Validator::make($req->all(), $rules);
      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return redirect()->back()->withInput();
      }

      $cekTo = Member::where('username', $req->transaction_to)->with('wallet', 'memberData')->first();
      if (!$cekTo){
        flash('username not found')->error();
        return redirect()->back()->withInput();
      } elseif (!$cekTo->wallet || !$cekTo->memberData) {
        flash('this username is not active')->error();
        return redirect()->back()->withInput();
      }

      $user = Auth::guard('member')->user();
      if (!Hash::check($req->wallet_password, $user->wallet_password)) {
        flash('your wallet password is wrong')->error();
        return redirect()->back()->withInput();
      }

      $walletFrom = Wallet::where('member_id', $user->id)->first();
      if (!$walletFrom){
        flash('you must complete the register')->error();
        return redirect()->back()->withInput();
      }

      if ($walletFrom->soul < $req->amount){
        flash('your soul is not enough')->error();
        return redirect()->back()->withInput();
      } else if ($req->amount < 1){
        flash('minimum send 1 soul')->error();
        return redirect()->back()->withInput();
      } else if ($req->transaction_to == $user->username){
        flash('cannot send in same username')->error();
        return redirect()->back()->withInput();
      }

      $cekParent = Member::where('sponsor_id', $user->id)->where('id', $cekTo->id)->first();
      $cekChild = false;
      if($user->parent_id == null || $user->parent_id == 0){
        if ($cekTo->parent_id == $user->id){
          $cekChild = true;
        }
      } elseif ($user->parent_id == $cekTo->id) {
        $cekChild = true;
      } elseif ($user->parent_id == $cekTo->parent_id){
        $cekChild = true;
      }

      if (!$cekParent){
        if (!$cekChild){
          flash('you just can send soul in 1 generation member')->error();
          return redirect()->back()->withInput();
        }
      }

      if($cekTo->status == 'pending' && $req->amount < 10){
        flash('Sorry, min transfer 10 soul to activate ' . $cekTo->username)->error();
        return redirect()->back()->withInput();
      } else {
        $cekTo->status = 'active';
        $cekTo->save();
      }

      if($walletFrom->soul-$req->amount < 10 && ($user->parent_id == null || $user->parent_id == 0 || !$user->parent_id)){
        flash('Sorry, main account min 10 soul in your account')->error();
        return redirect()->back()->withInput();
      }

      if (($user->parent_id != null && $user->parent_id != 0 && $user->parent_id) && $walletFrom->soul-$req->amount < 5) {
        flash('Sorry, sub account min 5 soul in your account')->error();
        return redirect()->back()->withInput();
      }

      if($cekTo->sponsor_id == $user->id && $req->amount < 20){
        flash('Sorry, min transfer 20 soul in your downline')->error();
        return redirect()->back()->withInput();
      }

      $updateUserWallet = Wallet::where('member_id', $user->id)->update([
        'soul' => $walletFrom->soul-$req->amount
      ]);

      $updateUserTo = Wallet::where('member_id', $cekTo->id)->update([
        'soul' => $cekTo->wallet->soul + $req->amount
      ]);

      $newTransaction = TokenTransaction::create([
        'transaction_from' => $user->id,
        'transaction_to' => $cekTo->id,
        'token_amount' => $req->amount,
        'transaction_type' => 'transfer',
        'transaction_status' => 'success',
        'token_type' => 'membership'
      ]);

      flash('success send soul to ' . $cekTo->username)->success();
      return redirect()->route('membership.soul.transfer');
    }

    public function soulTransfer(){
      $user = Auth::guard('member')->user();
      $wallet = Wallet::where('member_id', $user->id)->first();
      $tokenTransaction = TokenTransaction::where('transaction_from', $user->id)->orWhere('transaction_to', $user->id)->where('transaction_type', 'transfer')->with('memberFrom', 'memberTo')->orderBy('created_at', 'DESC')->paginate(10);
      return view('frontend.soul.transfer', compact('wallet', 'tokenTransaction'));
    }

    public function soulBuy(){
      $user = Auth::guard('member')->user();
      $wallet = Wallet::where('member_id', $user->id)->first();
      $tokenTransaction = TokenTransaction::where('transaction_to', $user->id)->where('transaction_type', 'buy')->orderBy('created_at', 'DESC')->paginate(10);
      return view('frontend.soul.buy', compact('wallet', 'tokenTransaction'));
    }

    public function soulBuySubmit(Request $req){
      $rules = [
         'quantity' => 'required|digits_between:1,16',
         'custom_quantity' => 'digits_between:1,16',
         'wallet_password' => 'required',
         'proof_of_payment' => 'required|mimes:jpg,jpeg,png|max:10240',
      ];

      $count = explode('.', $req->quantity);
      if(sizeof($count) > 1){
        flash('Sorry, format must numeric')->error();
        return redirect()->back()->withInput();
      }

      $validation = Validator::make($req->all(), $rules);
      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return redirect()->back()->withInput();
      }

      $user = Auth::guard('member')->user();

      if (!Hash::check($req->wallet_password, $user->wallet_password)) {
        flash('your wallet password is wrong')->error();
        return redirect()->back()->withInput();
      }

      $quantity = $req->quantity;
      if($req->quantity == 0){
        $quantity = $req->custom_quantity;
      }

      if($quantity < 10){
        flash('Sorry, min buy 10 soul')->error();
        return redirect()->back()->withInput();
      }

      $wallet = Wallet::where('member_id', $user->id)->first();
      if (!$wallet){
        flash('Your wallet not found')->error();
        return redirect()->back()->withInput();
      }

      $total = $wallet->soul + $quantity;
      if ($total > 9999999999999999){
        flash('Sorry, max 16 digit number soul in your wallet')->error();
        return redirect()->back()->withInput();
      }

      $save = new TokenTransaction();
      $save->transaction_from = 0;
      $save->transaction_to = $user->id;
      $save->token_amount = $quantity;
      $save->transaction_status = 'request';
      $save->transaction_type = 'buy';
      $save->token_type = 'membership';
      $save->save();

      if ($req->hasFile('proof_of_payment')){
        $saveImg = Storage::put('public/token_transaction' . '/' . $save->tt_id , $req->file('proof_of_payment'));
        $save->proof_of_payment = $saveImg;
        $save->save();
      }


      flash('Success, Waiting system process max. 3 hours');
      return redirect()->route('membership.soul.buy');
    }

    public function soulBuyBack(){
        $user = Auth::guard('member')->user();
        $wallet = Wallet::where('member_id', $user->id)->first();
        $tokenTransaction = TokenTransaction::where('transaction_to', $user->id)->where('transaction_type', 'buyback')->orderBy('created_at', 'DESC')->paginate(10);
        return view('frontend.soul.buyback', compact('wallet', 'tokenTransaction'));
    }

    public function soulBuybackSubmit(Request $req){
      $rules = [
         'quantity' => 'required|numeric|min:1',
         'wallet_password' => 'required',
      ];


      $count = explode('.', $req->quantity);
      if(sizeof($count) > 1){
        flash('Sorry, format must numeric')->error();
        return redirect()->back()->withInput();
      }

      $validation = Validator::make($req->all(), $rules);
      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return redirect()->back()->withInput();
      }

      $user = Auth::guard('member')->user();

      if (!Hash::check($req->wallet_password, $user->wallet_password)) {
        flash('your wallet password is wrong')->error();
        return redirect()->back()->withInput();
      }

      if($req->quantity < 1){
        flash('Sorry, min buyback 1 soul')->error();
        return redirect()->route()->withInput();
      }

      $wallet = Wallet::where('member_id', $user->id)->first();
      if($wallet->soul < $req->quantity){
        flash('your soul is not enough')->error();
        return redirect()->back()->withInput();
      }

      if($wallet->soul - $req->quantity < 10){
          flash('Sorry, min 10 soul in your account')->error();
          return redirect()->back()->withInput();
      }

      $save = new TokenTransaction();
      $save->transaction_from = 0;
      $save->transaction_to = $user->id;
      $save->token_amount = $req->quantity;
      $save->transaction_status = 'request';
      $save->transaction_type = 'buyback';
      $save->token_type = 'membership';
      $save->save();


      flash('Success, Waiting system process max. 3 hours')->success();
      return redirect()->route('membership.soul.buyback');
    }

    public function proofOfPaymentSoul($id){
      $cek = TokenTransaction::where('tt_id', $id)->first();

      if($cek){
        $user = Auth::guard('member')->user();
        if ($cek->transaction_to == $user->id){

          $contents = Storage::get($cek->proof_of_payment);
          if (!$contents){
            flash('sorry proof of payment not uploaded')->error();
            return null;
          }

          $file_extension = strtolower(substr(strrchr($cek->proof_of_payment,"."),1));

          switch( $file_extension ) {
              case "gif": $ctype="image/gif"; break;
              case "png": $ctype="image/png"; break;
              case "jpeg": $ctype="image/jpeg"; break;
              case "jpg": $ctype="image/jpeg"; break;
              default:
          }
          ob_clean();
          header("Content-type: " . $ctype);
          $response = Image::make($contents)->response($ctype);
          // $response->header('Content-Type', $ctype);
          return $response;
        }
      }

      return null;
    }
}
