<?php

namespace App\Http\Controllers\Member;

use Auth;
use App\Character;
use App\CharactersOrder;
use App\CharactersSeller;
use App\CharactersProfitTransactions;
use App\CharactersTokenTransactions;
use App\CharactersTransactions;
use App\Wallet;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class CharacterController extends Controller
{
    //
    public function __construct(){
      $this->middleware('auth:member');
    }

    public function buySubmit(Request $req){
      $user = Auth::guard('member')->user();
      $wallet = Wallet::where('member_id', $user->id)->first();

      $characterAll = Character::with(['charactersOrder' => function($q) use ($user) {
        $q->where('member_id', $user->id)->where('status', 'request');
      }, 'charactersSeller' => function($q) use ($user){
        $q->where('member_id', $user->id)->where('status', '!=', 'sold');
      }])->orderBy('characters_number', 'ASC')->get();

      $cek = true;
      $cekSeller = true;
      foreach ($characterAll as $i) {
        // code...
        if ($i->character_id == $req->character_id){
          $character = $i;
          break;
        }

        if (sizeof($i->charactersOrder) == 0){
          $cek = false;
        }

        if(sizeof($i->charactersSeller) == 0){
          $cekSeller = false;
        }
      }

      if (!$cek && $character->is_sequence == 1){
        flash('sorry you must buy in sequence order')->error();
        return redirect()->back();
      }

      if (sizeof($character->charactersOrder) == 1){
        flash('Sorry you cannot buy ' . $character->name . ', because you already requested')->error();
        return redirect()->back();
      }

      if (sizeof($character->charactersSeller) == 1){
        flash('Sorry you have ' . $character->name . ' on proccess sale')->error();
        return redirect()->back();
      }

      if ($wallet->soul < $character->soul){
        flash('Sorry your soul is not enough')->error();
        return redirect()->back();
      }

      $newOrder = CharactersOrder::create([
        'member_id' => $user->id,
        'character_id' => $character->character_id,
        'date_order' => date('Y-m-d H:i:s'),
        'status' => 'request'
      ]);

      $newCharactersTokenTransaction = CharactersTokenTransactions::create([
        'characters_order_id' => $newOrder->id,
        'soul' => $character->soul,
        'status' => 'sent'
      ]);

      $wallet->soul = $wallet->soul - $character->soul;
      $wallet->save();

      flash('Order ' . $character->name . ' is success')->success();
      return redirect()->back();
    }

    public function uploadPayment(Request $req){
      $rules = [
        'proof_of_payment' => 'mimes:jpg,jpeg,png|max:10240',
      ];

      $validation = Validator::make($req->all(), $rules);
      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return redirect()->back();
      }

      $user = Auth::user();
      if ($req->hasFile('proof_of_payment')){
        $cek = CharactersTransactions::where('id', $req->characters_transactions_id)->with('charactersOrder')->where('status', 'on_payment')->orWhere('status', 'reject')->first();
        if ($cek){
          if ($cek->charactersOrder->member_id == $user->id){
            $pathImg = 'public/characters_transactions' . '/' . $req->characters_transactions_id . '/proof_of_payment';
            $path = Storage::put($pathImg , $req->file('proof_of_payment'));

            $update = CharactersTransactions::where('id', $req->characters_transactions_id)->update([
              'method_payment' => $req->method_payment,
              'proof_of_payment' => $path,
              'status' => 'on_payment'
            ]);

            flash('success upload payment')->success();
          } else {
            flash('sorry request order not found')->error();
          }
        } else {
          flash('sorry your transaction not found')->error();
        }
      } else {
        flash('sorry you must upload your payment')->error();
      }

      return redirect()->back();
    }

    public function showPayment($id){
      $user = Auth::guard('member')->user();
      $get = CharactersTransactions::where('id', $id)->with('charactersOrder.member', 'charactersSeller.member')->first();

      if ($get->proof_of_payment != null){
        if ($get->charactersOrder){
          $order = $get->charactersOrder;
          if ($order->member){
            if ($order->member->id == $user->id){
              //get image proof of payment
              $contents = Storage::get($get->proof_of_payment);
              if (!$contents){
                return null;
              }

              $file_extension = strtolower(substr(strrchr($get->proof_of_payment,"."),1));

              switch( $file_extension ) {
                  case "gif": $ctype="image/gif"; break;
                  case "png": $ctype="image/png"; break;
                  case "jpeg": $ctype="image/jpeg"; break;
                  case "jpg": $ctype="image/jpeg"; break;
                  default:
              }
              ob_clean();
              header("Content-type: " . $ctype);
              $response = Image::make($contents)->response($ctype);
              // $response->header('Content-Type', $ctype);
              return $response;
            }
          }
        }
        if ($get->charactersSeller){
          $seller = $get->charactersSeller;
          if ($seller->member) {
            if ($seller->member->id == $user->id){
              //get image proof of payment
              $contents = Storage::get($get->proof_of_payment);
              if (!$contents){
                return null;
              }

              $file_extension = strtolower(substr(strrchr($get->proof_of_payment,"."),1));

              switch( $file_extension ) {
                  case "gif": $ctype="image/gif"; break;
                  case "png": $ctype="image/png"; break;
                  case "jpeg": $ctype="image/jpeg"; break;
                  case "jpg": $ctype="image/jpeg"; break;
                  default:
              }
              ob_clean();
              header("Content-type: " . $ctype);
              $response = Image::make($contents)->response($ctype);
              // $response->header('Content-Type', $ctype);
              return $response;
            }
          }
        }
      }
      flash('sorry you cannot open this payment')->error();
      return redirect()->back();
    }

    public function charactersTransactionsStatus(Request $req){
      $user = Auth::guard('member')->user();

      $charactersTransactions = CharactersTransactions::where('id', $req->id)->with('character', 'charactersOrder.member', 'charactersSeller.member')->first();
      if ($charactersTransactions->characters_seller_id == 0 || $charactersTransactions->characters_seller_id == null){
        flash('sorry you cannot change status')->error();
      } else {
        if($charactersTransactions->charactersSeller && $charactersTransactions->charactersOrder){
          if ($charactersTransactions->charactersSeller->member && $charactersTransactions->charactersOrder->member){
            if ($charactersTransactions->charactersSeller->member->id == $user->id){
              if ($req->status == 'success'){
                $walletSeller = Wallet::where('member_id', $charactersTransactions->charactersSeller->member->id)->first();
                if ($walletSeller){
                  $updateSeller = CharactersSeller::where('id', $charactersTransactions->characters_seller_id)->update([
                    'status' => 'sold',
                    'date_of_sale' => date('Y-m-d H:i:s')
                  ]);

                  $cekProfit = CharactersProfitTransactions::where('characters_seller_id', $charactersTransactions->characters_seller_id)
                    ->where('status', 'success')->orWhere('status', 'failed')->first();

                  if(!$cekProfit){
                    $newProfit = CharactersProfitTransactions::create([
                      'characters_seller_id' => $charactersTransactions->characters_seller_id,
                      'profit' => $charactersTransactions->profit,
                      'status' => 'success'
                    ]);

                    $walletSeller->reality = $walletSeller->reality + $charactersTransactions->profit;
                    $walletSeller->save();
                  }

                  $set_start_of_sale = null;
                  $set_end_of_sale = null;

                  $set_start_of_sale = date('Y-m-d H:i:s');
                  $plus = ' +' . $charactersTransactions->estimate_hour . ' hour';
                  $set_end_of_sale = date('Y-m-d H:i:s', strtotime($plus, strtotime($charactersTransactions->character->time)));

                  $newSeller = CharactersSeller::create([
                    'character_id' => $charactersTransactions->character_id,
                    'member_id' => $charactersTransactions->charactersOrder->member->id,
                    'start_of_sale' => $set_start_of_sale,
                    'end_of_sale' => $set_end_of_sale,
                    'price' => $charactersTransactions->price_of_sale,
                    'prediction_profit' => $charactersTransactions->prediction_profit,
                    'prediction_price' => $charactersTransactions->prediction_price_for_buyer,
                    'status' => 'ready'
                  ]);

                  $charactersTransactions->start_of_sale = $set_start_of_sale;
                  $charactersTransactions->end_of_sale = $set_end_of_sale;
                  $charactersTransactions->status = $req->status;
                  $charactersTransactions->save();
                } else {
                  flash('Sorry, wallet your wallet not found')->error();
                }
              } elseif($req->status == 'reject') {
                $charactersTransactions->status = $req->status;
                $charactersTransactions->save();
                flash('success update status')->success();
              } else {
                flash('Sorry, cannot proccess')->error();
              }
            } else {
              flash('Sorry, cannot proccess')->error();
            }
          } else {
              flash('Sorry, member seller or order not found')->error();
          }
        } else {
          flash('Sorry, member seller or order not found')->error();
        }
      }
      return redirect()->back();
    }
}
