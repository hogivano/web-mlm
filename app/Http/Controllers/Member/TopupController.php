<?php

namespace App\Http\Controllers\Member;

use App\Bank;
use App\Http\Controllers\Controller;
use App\TopUp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;

class TopupController extends Controller {

    public function __construct() {
        $this->middleware('auth:member');
    }

    public function topup() {

        $topup_count = TopUp::where('member_id', Auth::guard('member')->user()->member_id)
            ->where(function ($where) {
                $where->where('topup_status', 'new')
                    ->Orwhere('topup_status', 'process');
            })->count();
//        return $topup_item;
        $bank_list = Bank::all();

//        return $request->file('bukti_transfer');
        return view('memberarea.topup.topup', [
            'bank_list' => $bank_list,
            'topup_count' => $topup_count,
            'member_type' => Auth::guard('member')->user()->member_type

        ]);
    }

    public function prosesInputTopup(Request $request) {
        //request Inputan
        $bank           = $request['bank'];
        $bank_name      = $request['bank_name'];
        $bank_number    = $request['bank_number'];
        $bank_account   = $request['bank_account'];
        $jumlah_premi   = 1;
        $member_id      = Auth::guard('member')->user()->member_id;
        $jumlah_nominal = $request['jumlah_nominal'];
        $bukti_transfer = $request->file('bukti_transfer');
        $topup_unique   = rand(10, 999);
        $topup_total    = $jumlah_nominal . $topup_unique;


        //Validasi jumlah TopUp
        $topup_count = TopUp::where('member_id', Auth::guard('member')->user()->member_id)
            ->where(function ($where) {
                $where->where('topup_status', 'new')
                    ->Orwhere('topup_status', 'process');
            })->count();

        if ($topup_count > 0) {
            return redirect()->back()->with('error', 'Maaf saat ini anda tidak dapat melakukan topup dikarenakan topup anda sebelumnya sedang kami proses, terimakasih.');
        }


        //Validasi gambar harus berupa Image
        $ext = $bukti_transfer->getMimeType();
        $ext = substr($ext, 0, 5);
        if ($ext != 'image') {
            return redirect()->back()->with('error', 'inputan harus berupa image');
        }


        //Proses Input
        $filename = time() . $topup_unique . $bukti_transfer->getClientOriginalName();

        $canvas = Image::canvas(600, 250);

        $resizeImage = Image::make($bukti_transfer)->resize(600, 250, function ($constraint) {
            $constraint->aspectRatio();
        });
        $canvas->insert($resizeImage, 'center');
        $canvas->save(public_path('/img/topup/') . $filename);

        $topup                    = new TopUp();
        $topup->member_id         = $member_id;
        $topup->bank_id           = $bank;
        $topup->topup_nominal     = $jumlah_nominal;
        $topup->topup_unique      = $topup_unique;
        $topup->topup_total       = $topup_total;
        $topup->premi_qty         = $jumlah_premi;
        $topup->create_at         = NOW();
        $topup->updated_at        = NULL;
        $topup->topup_desc        = NULL;
        $topup->payment_method    = 'transfer';
        $topup->topup_bank_from   = $bank_name;
        $topup->topup_rek_no_from = $bank_number;
        $topup->topup_name_from   = $bank_account;
        $topup->bukti_transfer    = $filename;
        $topup->topup_key         = md5(md5($topup_unique . $member_id . $topup_total));
        $topup->topup_status      = 'new';
        $topup->save();

        return redirect()->back()->with('success', 'Terimakasih, Request topup anda sedang diproses.');

//        return $topup_total;


//        return $request->file('bukti_transfer');
    }

    public function history() {
        $topup_list = TopUp::where('member_id', Auth::guard('member')->user()->member_id)->get();
        return view('memberarea.topup.history', [
            'no' => 1,
            'topup_list' => $topup_list,
            'member_type' => Auth::guard('member')->user()->member_type
        ]);
    }

    public function detail($topupId) {
        //Select TopUp By Id
        $data['topup'] = TopUp::where('topup_id', $topupId)->first();

//        $data["member_type"]     = Auth::guard('member')->user()->member_type;

        return view('memberarea.topup.detail', $data);
    }
}

