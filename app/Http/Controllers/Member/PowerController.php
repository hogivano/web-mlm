<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PowerController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth:member');
    }
    public function index(){
      return view('frontend.power.index');
    }
}
