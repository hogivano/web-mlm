<?php

namespace App\Http\Controllers\Member;

use Redirect;
use App\Bonus;
use App\Http\Controllers\Controller;
use App\Member;
use App\MemberData;
use App\Models\City;
use App\Models\Mutation;
use App\Models\Province;
use App\Models\Withdrawal;
use App\Network;
use App\PolishPayment;
use App\TokenTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class MembershipController extends Controller {
    protected $networks;
    protected $bonus = [10000, 10000, 5000, 5000, 5000, 5000, 10000, 10000];
    protected $countQualified = 0;

    public function __construct() {
        $this->middleware('auth:member');
    }

    public function inventory(){
      return view('frontend.inventory.index');
    }

    public function showRegistrationForm($uplineId, $kaki, Request $request) {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }

        //cek kaki maksimal 5
        if ($kaki > 5) {
            return redirect("membership/pohon-jaringan")->with('error', 'Maaf, posisi kaki tidak valid');
        }

        //cek token apakah mencukupi
        $memberId = Auth::guard('member')->user()->member_id;

        $sponsor = Member::whereMemberId($memberId)->first();
        if ($sponsor->sisaTokenMembership < 1) {
            Network::whereUplineIdAndKakiAndMemberId($uplineId, $kaki,0)->delete();
            return redirect("membership/pohon-jaringan")->with('error', 'Maaf, jumlah token Anda tidak mencukupi');
        }

        //cek apakah ada booking jaringan sebelumnya
        //$networkBefore = Network::whereSponsorIdAndMemberId($memberId, 0)->where('kaki', '!=', $kaki)->where('upline_id','!=',$uplineId);
        $networkBefore = Network::whereSponsorIdAndMemberId($memberId, 0);
        if ($networkBefore->get()) {
            $networkBefore->delete();
        }

        $upline      = Member::where('member_id', $uplineId)->first();
        $jmlDownline = Network::where('upline_id', $uplineId)->where('member_id','0')->count();
        $sponsorId   = $memberId;

        //validasi Jumlah Kaki
        if ($upline->member_type === 'leader' && $jmlDownline >= 10) {
            return redirect()->back()->with('error', 'Maaf, jumlah kaki sudah penuh');
        }
        //echo $jmlDownline;exit;
        if ($upline->member_type === 'member' && $jmlDownline >= 5) {
            return redirect()->back()->with('error', 'Maaf, jumlah kaki sudah penuh');
        }

        //validasi upline apakah satu jaringan dengan sponsor
        if (!$this->isOneNetwork($uplineId, $sponsorId)) {
            return redirect()->back()->with('error', 'Maaf, Upline tidak dalam satu generasi jaringan dengan Anda');
        }

        //Validasi apakah kaki sudah terisi
        $newNetwork = Network::where([['upline_id', $uplineId], ['kaki', $kaki]])->first();

        if ($newNetwork) {
            //Validasi limit booking kaki 5 menit
            $lastInsertDate = Carbon::parse($newNetwork->created_at)->diffInMinutes(Carbon::now()->toDateTimeString());
            if ($lastInsertDate > 120) {
                $newNetwork->delete();

            }

            if ($newNetwork->member_id == 0) {
                if ($newNetwork->sponsor_id == $sponsorId) {
                    goto showForm;
                }
                return redirect()->back()->with('error', 'Maaf, posisi kaki ' . $kaki . ' sedang dalam proses input');

            }
            return redirect()->back()->with('error', 'Maaf, posisi kaki ' . $kaki . ' sudah terisi');
        }


        //lanjutkan proses registrasi
        //Insert network dengan member_id 0 untuk lock kaki
        $insert                 = new Network();
        $insert->member_id      = 0;
        $insert->sponsor_id     = $sponsorId;
        $insert->upline_id      = $uplineId;
        $insert->kaki           = $kaki;
        $insert->network_level  = $upline->network->network_level + 1;
        $insert->downline_count = 0;
        $insert->network_status = "booking";
        $insert->created_at     = Carbon::now()->toDateTimeString();

        $insert->save();

        showForm:
        $data = Province::get();
        return view('memberarea.member.register-member', [
            'member_item' => $upline,
            'data'        => $data,
            'upline_id'   => $uplineId,
            'kaki'        => $kaki,
            'member_type' => Auth::guard('member')->user()->member_type
        ]);


    }

    public function isOneNetwork($uplineId, $registrantId) {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }
        //dapatkan jaringan user ini
        if ($uplineId == $registrantId) {
            return true;
        }

        $network = Network::where("member_id", $uplineId)->first();

        if ($network->upline_id == $registrantId) {
            return true;
        }

        $temp = $network;
        for ($i = $network->network_level; 0 < $i; $i--) {
            if ($temp->sponsor_id == $registrantId) {
                return true;
            }
            if ($temp->upline_id == $registrantId) {
                return true;
            }

            $upline = Network::where("member_id", $temp->upline_id)->first();

            if ($upline != null) {
                $temp = $upline;
            }

        }
        return false;
    }

    public function registerMember(Request $request) {

        // validasi user type
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }

        $sponsorId = Auth::guard('member')->user()->member_id;
        //$newNetwork = Network::whereUplineIdAndKakiAndSponsorIdAndMemberId($request['upline_id'], $request['kaki'], $sponsorId, 0)->first();
        $newNetwork = Network::where([
            ['upline_id', '=', $request['upline_id']],
            ['kaki', '=', $request['kaki']],
            ['sponsor_id', '=', $sponsorId],
            ['member_id', '=', 0],
        ])->first();

        if (!$newNetwork) {
            return redirect()->back()->with('error', 'Maaf, jaringan yang anda input tidak valid')->withInput();
        }

        //cek token apakah mencukupi
        $sponsorMember = Member::whereMemberId($sponsorId)->first();
        if ($sponsorMember->sisaTokenMembership < 1) {
            return redirect()->back()->with('error', 'Maaf, jumlah token Anda tidak mencukupi');
        }

        //validasi form
        //$upline = Member::where('member_id', $request['upline_id'])->first();
        $rules  = [
            'username'            => 'required|string|alpha_dash|min:6|max:50|unique:members,member_username',
            'name'                => 'required|string|max:100',
            'member_pob'          => 'required|string|max:100',
            'dob'                 => 'required|date',
            'gender'              => 'required|string|max:10',
            'member_work'         => 'required|string|max:100',
            'member_waris'        => 'required|string|max:100',
            'member_hub'          => 'required|string|max:100',
            'city'                => 'required|string',
            'alamat'              => 'required|string|max:250',
            'postal_code'         => 'required|string|max:7',
            'member_identity'     => 'required|string|max:5',
            'number_identity'     => 'required|digits_between:12,16|unique:members,member_identity_number',
            'no_hp'               => 'required|regex:/(08)[0-9]/|digits_between:9,14',
            'email'               => 'required|string|email|max:100|unique:members,member_email',
            'password'            => 'required|string|min:8|confirmed',
            'member_bank'         => 'required|string|max:100',
            'member_bank_number'  => 'required||digits_between:9,16',
            'member_bank_account' => 'required|string|max:100',
            'ktp_picture'         => 'mimes:jpeg,jpg,png,gif|required|max:10000'
        ];

        $errorMessages = [
            'username'                          => [
                'required' => "Silahkan masukkan Username",
                'unique'   => "Username sudah pernah digunakan",
            ],
            'username.unique'                   => "Username sudah pernah digunakan",
            'username.min'                      => "Username minimal 6 karakter",
            'username.alpha_dash'               => "Username tidak boleh mengandung spasi dan titik",
            'name.required'                     => "Silahkan masukkan Nama",
            'member_pob.required'               => "Silahkan masukkan Tempat Lahir",
            'dob.required'                      => "Silahkan masukkan Tanggal Lahir",
            'member_work.required'              => "Silahkan masukkan Pekerjaan",
            'member_waris.required'             => "Silahkan masukkan Ahli Waris",
            'member_hub.required'               => "Silahkan masukkan Hubungan dengan Ahli Waris",
            'city.required'                     => "Silahkan pilih Kota/Kabupaten",
            'alamat.required'                   => "Silahkan masukkan Alamat",
            'postal_code.required'              => "Silahkan masukkan Kode Pos",
            'postal_code.max'                   => "Kode Pos tidak valid",
            'member_identity.required'          => "Silahkan pilih Jenis Identitas",
            'number_identity.required'          => "Silahkan masukkan Nomor Identitas",
            'number_identity.digits_between'    => "Nomor Identitas tidak valid",
            'number_identity.unique'            => "Nomor Identitas sudah pernah digunakan",
            'no_hp.required'                    => "Silahkan masukkan Nomor HP",
            'no_hp.digits_between'              => "Format Nomor HP tidak valid",
            'email.required'                    => "Silahkan masukkan Email",
            'email.email'                       => "Format Email tidak valid",
            'password.required'                 => "Silahkan masukkan Password baru",
            'password.min'                      => "Password minimal 8 karakter",
            'password.confirmed'                => "Konfirmasi Password tidak cocok",
            'member_bank.required'              => "Silahkan masukkan Nama Bank",
            'member_bank_number.required'       => "Silahkan masukkan Nomor Rekening",
            'member_bank_number.digits_between' => "Nomor Rekening tidak valid",
            'member_bank_account.required'      => "Silahkan masukkan Nama Pemilik Rekening",
            'ktp_picture.required'              => "Silahkan tambahkan foto KTP calon member",
            'ktp_picture.mimes'                 => "File harus berupa gambar.",
        ];

        $validation = Validator::make($request->all(), $rules, $errorMessages);
        $cities     = City::where('province_id', $request['province'])->get();
        if ($validation->fails()) {
            return redirect()->back()->with([
                'error'  => $validation->errors()->first(),
                'cities' => $cities

            ])->withInput();
        }

        //cek password sponsor
        if (!Hash::check($request['sponsor_password'], Auth::guard('member')->user()->member_password)) {
            return redirect()->back()->with([
                'error'  => "Password Anda salah",
                'cities' => $cities

            ])->withInput();
        }

        $upline_id           = $request['upline_id'];
        $kaki                = $request['kaki'];
        $username            = $request['username'];
        $name                = $request['name'];
        $member_pob          = $request['member_pob'];
        $dob                 = $request['dob'];
        $gender              = $request['gender'];
        $member_work         = $request['member_work'];
        $member_waris        = $request['member_waris'];
        $member_hub          = $request['member_hub'];
        $country             = $request['country'];
        $city                = $request['city'];
        $alamat              = $request['alamat'];
        $postal_code         = $request['postal_code'];
        $identity            = $request['member_identity'];
        $no_identity         = $request['number_identity'];
        $phone_number        = $request['no_hp'];
        $email               = $request['email'];
        $password            = $request['password'];
        $nama_bank           = $request['member_bank'];
        $member_bank_number  = $request['member_bank_number'];
        $member_bank_branch  = $request['member_bank_branch'];
        $member_bank_account = $request['member_bank_account'];
        $filename            = time() . $request->file('ktp_picture')->getClientOriginalName();


        if (date('Ymdhis', strtotime($dob)) > date('Ymdhis')) {
            return redirect()->back()->with('error', 'Maaf Tanggal lahir tidak boleh melebihi hari ini!');
        } else {
            //Store data

            $newMember                         = new Member();
            $newMember->member_name            = $name;
            $newMember->member_username        = $username;
            $newMember->member_password        = Hash::make($password);
            $newMember->member_is_login        = NULL;
            $newMember->member_reg_date        = NOW();
            $newMember->member_ip              = $_SERVER['REMOTE_ADDR'];
            $newMember->member_type            = 'member';
            $newMember->member_status          = 'active';
            $newMember->member_waris           = $member_waris;
            $newMember->member_hub             = $member_hub;
            $newMember->member_email           = $email;
            $newMember->member_mobile          = $phone_number;
            $newMember->member_photo           = NULL;
            $newMember->member_gender          = $gender;
            $newMember->member_work            = $member_work;
            $newMember->member_dob             = $dob;
            $newMember->member_pob             = $member_pob;
            $newMember->member_identity        = $identity;
            $newMember->member_identity_number = $no_identity;
            $newMember->member_address         = $alamat;
            $newMember->member_postcode        = $postal_code;
            $newMember->member_city            = $city;
            $newMember->member_country         = $country;
            $newMember->member_bank            = $nama_bank;
            $newMember->member_bank_account    = $member_bank_account;
            $newMember->member_bank_number     = $member_bank_number;
            $newMember->member_bank_branch     = $member_bank_branch;
            $newMember->member_qualified       = 0;
            $newMember->created_at             = NOW();
            $newMember->updated_at             = NULL;
            $newMember->sponsor_id             = $sponsorId;
            $newMember->ktp_image              = $filename;
            $newMember->save();



            if ($newMember) {
                //update network member_id with inserted new member
                //Jika type nasabah hapus jaringan
                $newNetwork->member_id      = $newMember->member_id;
                $newNetwork->network_status = "active";
                $newNetwork->update();

                //Insert transaksi token
                $tokenTransaction                     = new TokenTransaction();
                $tokenTransaction->transaction_from   = $sponsorId;
                $tokenTransaction->transaction_to     = $newMember->member_id;
                $tokenTransaction->token_amount       = 1;
                $tokenTransaction->transaction_type   = "registrasi";
                $tokenTransaction->transaction_status = "sukses";
                $tokenTransaction->token_type         = "membership";
                $tokenTransaction->save();

                //increment jumlah downline tiap2 uplines sampe level 1
                // Network::nestable(Network::where("member_id", $request['upline_id'])->first());

                //update qualifikasi sponsor
                // $sponsoredCount = Network::whereSponsorId($sponsorId)->count();
                // switch ($sponsoredCount) {
                //     case 0:
                //         $sponsorMember->member_qualified = 0;
                //         break;
                //     case 1:
                //         $sponsorMember->member_qualified = 1;
                //         break;
                //     case 2:
                //         $sponsorMember->member_qualified = 2;
                //         break;
                //     case 3:
                //         $sponsorMember->member_qualified = 3;
                //         break;
                //     case 4:
                //         $sponsorMember->member_qualified = 5;
                //         break;
                //     case 5:
                //         $sponsorMember->member_qualified = 8;
                //         break;
                // }
                // $sponsorMember->save();

                //insert bonus sponsor
                // Bonus::insert([
                //     "bonus_to"     => $sponsorId,
                //     "bonus_from"   => $newMember->member_id,
                //     "bonus_amount" => env("BONUS_SPONSOR", 50000),
                //     "bonus_type"   => 'sponsor',
                //     "bonus_desc"   => 'Keuntungan penjualan langsung',
                //     "bonus_status" => "new",
                //     "created_at"   => Carbon::now()->toDateTimeString(),
                //     "updated_at"   => NULL
                // ]);

                //insert bonus level generasi sampe 8 level upline
                // $memberNetworks = $this->nestable($request['upline_id']);
                // $this->prosesTambahBonus($newMember->member_id, $request['upline_id']);
            }
        }

        return redirect('/membership/pohon-jaringan')->with('success', 'Proses registrasi berhasil');
    }

    public function prosesTambahBonus($fromId, $uplineId) {
        $member_type = Auth::guard('member')->user()->member_type;

        //Get uplines recursive
        $networks = $this->recursiveUpline($uplineId, 8);
        $bonuses  = [];
        $iMax     = count($this->bonus);
        for ($i = 0; $i < $iMax; $i++) {

            //Default bonus to ADMIN
            $bonusToId = 1;
            $bonusDesc = "(Unqualified Member) Bonus generasi level ke " . ($i + 1) . ".";
            $bonusType = "unqualified";
            if (isset($networks[$i])) {
                $network = $networks[$i];

                $unqualifiedFromId = $network->member->member_id;
                if ($network->member->member_qualified == 8) {
                    goto updatebonus;
                } elseif ($network->member->member_qualified == 5 && $i < 5) {
                    goto updatebonus;
                } elseif ($network->member->member_qualified == 3 && $i == 2) {
                    goto updatebonus;
                } elseif ($network->member->member_qualified == 3 && $i < 2) {
                    goto updatebonus;
                } elseif ($network->member->member_qualified < 3 && $i < 2) {
                    goto updatebonus;
                }
                goto defaultbonus;
            } else {
                goto defaultbonus;
            }

            updatebonus:
            $bonusToId         = $network->member_id;
            $bonusDesc         = "Bonus generasi level ke " . ($i + 1) . ".";
            $bonusType         = "generasi";
            $unqualifiedFromId = 0;

            defaultbonus:

            $bonuses[] = [
                "bonus_to"         => $bonusToId,
                "bonus_from"       => $fromId,
                "unqualified_from" => $unqualifiedFromId,
                "bonus_amount"     => $this->bonus[$i],
                "bonus_type"       => $bonusType,
                "bonus_desc"       => $bonusDesc,
                "bonus_status"     => "new",
                "created_at"       => Carbon::now()->toDateTimeString()
            ];
        }
        Bonus::insert($bonuses);
    }

    public function recursiveUpline($uplineId, $deep = null) {

        //$net = Network::whereMemberId($uplineId)->with('recursiveUpline')->get();
        $net       = Network::whereMemberId($uplineId)->first();
        $uplines   = [];
        $uplines[] = $net->fresh();
        $current   = $net;
        while ($current != null) {
            $parent = Network::whereMemberId($current->upline_id)->first();
            if ($current->networkUpline != null) {
                $uplines[] = $parent;
            }

            if ($deep != null && count($uplines) == $deep) {
                $current = null;
            } else {
                $current = $current->networkUpline;
            }
        }
        return $uplines;
    }

    public function registrasiMemberNasabah(Request $request, $type) {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }

        if ($type !== "nasabah") {
            return redirect()->back(302);
        }

        //cek token apakah mencukupi
        $sponsorId     = Auth::guard('member')->user()->member_id;
        $sponsorMember = Member::whereMemberId($sponsorId)->first();
        if ($sponsorMember->sisaTokenNasabah < 1) {
            return redirect()->back()->with('error', 'Maaf, jumlah token Anda tidak mencukupi');
        }

        if ($request->register_nasabah != "") {

            //validasi new registered and not activated network
            $newNetwork = Network::whereUplineIdAndKakiAndSponsorIdAndMemberId($request['upline_id'], $request['kaki'],$sponsorId,0)->first();
            if (!$newNetwork){
               return redirect()->back()->with('error', 'Maaf, jaringan yang anda input tidak valid');
            }

           // cek token apakah mencukupi
            $sponsorMember = Member::find($sponsorId)->first();

            $sisa_token = Member::find($sponsorId)->getSisaTokenAttribute('nasabah');
            if ($sisa_token < 1) {
                return redirect()->back()->with('error', 'Maaf, jumlah token Anda tidak mencukupi');
            }

            //validasi form
            //$upline = Member::where('member_id', $request['upline_id'])->first();
            $rules = [
                'ktp_picture'         => 'image|mimes:jpeg,jpg,png,gif|required|max:5000',
                'username'            => 'required|string|min:6|max:50|unique:members,member_username',
                'name'                => 'required|string|max:100',
                'member_pob'          => 'required|string|max:100',
                'dob'                 => 'required|date',
                'gender'              => 'required|string|max:10',
                'member_work'         => 'required|string|max:100',
                'member_waris'        => 'required|string|max:100',
                'member_hub'          => 'required|string|max:100',
                'city'                => 'required|string',
                'alamat'              => 'required|string|max:250',
                'postal_code'         => 'required|string|max:7',
                'member_identity'     => 'required|string|max:5',
                'number_identity'     => 'required|digits_between:12,16',
                'no_hp'               => 'required|regex:/(08)[0-9]/|digits_between:9,14',
                'email'               => 'required|string|email|max:100|unique:members,member_email',
                'password'            => 'required|string|min:8|confirmed',
                'member_bank'         => 'required|string|max:100',
                'member_bank_number'  => 'required||digits_between:9,16',
                'member_bank_account' => 'required|string|max:100',
            ];

            $errorMessages = [
                'username'                          => [
                    'required' => "Silahkan masukkan Username",
                    'unique'   => "Username sudah pernah digunakan",
                ],
                'username.unique'                   => "Username sudah pernah digunakan",
                'username.min'                      => "Username minimal 6 karakter",
                'name.required'                     => "Silahkan masukkan Nama",
                'member_pob.required'               => "Silahkan masukkan Tempat Lahir",
                'dob.required'                      => "Silahkan masukkan Tanggal Lahir",
                'member_work.required'              => "Silahkan masukkan Pekerjaan",
                'member_waris.required'             => "Silahkan masukkan Ahli Waris",
                'member_hub.required'               => "Silahkan masukkan Hubungan dengan Ahli Waris",
                'city.required'                     => "Silahkan pilih Kota/Kabupaten",
                'alamat.required'                   => "Silahkan masukkan Alamat",
                'postal_code.required'              => "Silahkan masukkan Kode Pos",
                'postal_code.max'                   => "Kode Pos tidak valid",
                'member_identity.required'          => "Silahkan pilih Jenis Identitas",
                'number_identity.required'          => "Silahkan masukkan Nomor Identitas",
                'number_identity.digits_between'    => "Nomor Identitas tidak valid",
                'no_hp.required'                    => "Silahkan masukkan Nomor HP",
                'no_hp.digits_between'              => "Format Nomor HP tidak valid",
                'email.required'                    => "Silahkan masukkan Email",
                'email.email'                       => "Format Email tidak valid",
                'password.required'                 => "Silahkan masukkan Password baru",
                'password.min'                      => "Password minimal 8 karakter",
                'password.confirmed'                => "Konfirmasi Password tidak cocok",
                'member_bank.required'              => "Silahkan masukkan Nama Bank",
                'member_bank_number.required'       => "Silahkan masukkan Nomor Rekening",
                'member_bank_number.digits_between' => "Nomor Rekening tidak valid",
                'member_bank_account.required'      => "Silahkan masukkan Nama Pemilik Rekening",
                'ktp_picture.required'              => "Silahkan tambahkan foto KTP calon member",
                'ktp_picture.mimes'                 => "Format yang diijinkan .jpg, .jpeg, .png",
                'ktp_picture.image'                 => "File harus berupa gambar",
            ];

            $validation = Validator::make($request->all(), $rules, $errorMessages);
            $cities     = City::where('province_id', $request['province'])->get();
            if ($validation->fails()) {
                return redirect()->back()->with([
                    'error'  => $validation->errors()->first(),
                    'cities' => $cities

                ])->withInput();
            }

            //cek password sponsor
            if (!Hash::check($request['sponsor_password'], Auth::guard('member')->user()->member_password)) {
                return redirect()->back()->with([
                    'error'  => "Password Anda salah",
                    'cities' => $cities

                ])->withInput();
            }

            $upline_id           = $request['upline_id'];
            $kaki                = $request['kaki'];
            $username            = $request['username'];
            $name                = $request['name'];
            $member_pob          = $request['member_pob'];
            $dob                 = $request['dob'];
            $gender              = $request['gender'];
            $member_work         = $request['member_work'];
            $member_waris        = $request['member_waris'];
            $member_hub          = $request['member_hub'];
            $country             = $request['country'];
            $city                = $request['city'];
            $alamat              = $request['alamat'];
            $postal_code         = $request['postal_code'];
            $identity            = $request['member_identity'];
            $no_identity         = $request['number_identity'];
            $phone_number        = $request['no_hp'];
            $email               = $request['email'];
            $password            = $request['password'];
            $nama_bank           = $request['member_bank'];
            $member_bank_number  = $request['member_bank_number'];
            $member_bank_branch  = $request['member_bank_branch'];
            $member_bank_account = $request['member_bank_account'];
            $filename            = time() . $request->file('ktp_picture')->getClientOriginalName();


            if (date('Ymdhis', strtotime($dob)) > date('Ymdhis')) {
                return redirect()->back()->with('error', 'Maaf Tanggal lahir tidak boleh melebihi hari ini!');
            } else {
                //Store data


                $newMember                         = new Member();
                $newMember->member_name            = $name;
                $newMember->member_username        = $username;
                $newMember->member_password        = Hash::make($password);
                $newMember->member_is_login        = NULL;
                $newMember->member_reg_date        = NOW();
                $newMember->member_ip              = $_SERVER['REMOTE_ADDR'];
                $newMember->member_type            = $request['member_type'];
                $newMember->member_status          = 'active';
                $newMember->member_waris           = $member_waris;
                $newMember->member_hub             = $member_hub;
                $newMember->member_email           = $email;
                $newMember->member_mobile          = $phone_number;
                $newMember->member_photo           = NULL;
                $newMember->member_gender          = $gender;
                $newMember->member_work            = $member_work;
                $newMember->member_dob             = $dob;
                $newMember->member_pob             = $member_pob;
                $newMember->member_identity        = $identity;
                $newMember->member_identity_number = $no_identity;
                $newMember->member_address         = $alamat;
                $newMember->member_postcode        = $postal_code;
                $newMember->member_city            = $city;
                $newMember->member_country         = $country;
                $newMember->member_bank            = $nama_bank;
                $newMember->member_bank_account    = $member_bank_account;
                $newMember->member_bank_number     = $member_bank_number;
                $newMember->member_bank_branch     = $member_bank_branch;
                $newMember->member_qualified       = 0;
                $newMember->created_at             = NOW();
                $newMember->updated_at             = NULL;
                $newMember->sponsor_id             = $sponsorId;
                $newMember->ktp_image              = $filename;
                $newMember->save();

                if ($newMember) {

                    //Jika type nasabah
                    $newNetwork                 = new Network();
                    $newNetwork->member_id      = $newMember->member_id;
                    $newNetwork->sponsor_id     = $sponsorId;
                    $newNetwork->upline_id      = -1;
                    $newNetwork->kaki           = -1;
                    $newNetwork->network_level  = -1;
                    $newNetwork->downline_count = -1;
                    $newNetwork->network_status = "freeze";
                    $newNetwork->created_at     = Carbon::now()->toDateTimeString();
                    $newNetwork->save();

                    //Insert transaksi token
                    $tokenTransaction                     = new TokenTransaction();
                    $tokenTransaction->transaction_from   = $sponsorId;
                    $tokenTransaction->transaction_to     = $newMember->member_id;
                    $tokenTransaction->token_amount       = 1;
                    $tokenTransaction->transaction_type   = "registrasi";
                    $tokenTransaction->transaction_status = "sukses";
                    $tokenTransaction->token_type         = "nasabah";
                    $tokenTransaction->save();

                    //insert polis payment
                    $polis             = new PolishPayment();
                    $polis->member_id  = $newMember->member_id;
                    $polis->nominal    = 150000;
                    $polis->polis_date = Carbon::now()->toDateString();
                    $polis->polis_exp_date = Carbon::parse($polis->polis_date)->addMonths(12);
                    $polis->pp_status  = "pending";
                    $polis->pp_type    = "register";
                    $polis->save();

                    //increment jumlah downline tiap2 uplines sampe level 1
                    //Network::nestable(Network::where("member_id", $sponsorId)->first());


                    //update qualifikasi sponsor
                    $sponsoredCount = Network::whereSponsorId($sponsorId)->count();
                    switch ($sponsoredCount) {
                        case 1:
                            $sponsorMember->member_qualified = 1;
                            break;
                        case 2:
                            $sponsorMember->member_qualified = 2;
                            break;
                        case 3:
                            $sponsorMember->member_qualified = 3;
                            break;
                        case 4:
                            $sponsorMember->member_qualified = 5;
                            break;
                        case 5:
                            $sponsorMember->member_qualified = 8;
                            break;
                    }
                    $sponsorMember->save;

                    //insert bonus sponsor
                    Bonus::insert([
                        "bonus_to"     => $sponsorId,
                        "bonus_from"   => $newMember->member_id,
                        "bonus_amount" => env("BONUS_SPONSOR", 35000),
                        "bonus_type"   => 'sponsor',
                        "bonus_desc"   => 'Keuntungan penjualan langsung',
                        "bonus_status" => "new",
                        "created_at"   => Carbon::now()->toDateTimeString()
                    ]);

                    //insert bonus level generasi sampe 8 level upline
                    //$memberNetworks = $this->nestable($request['upline_id']);
                    $this->prosesTambahBonus($newMember->member_id, $sponsorId);
                }
                return redirect('/membership/sponsor-ship')->with('success',
                    'Proses registrasi berhasil');

            }
        }

        $data = Province::get();
        return view('memberarea.member.register-nasabah', [
            'data' => $data,
        ]);
    }

    public function getCityList($province_id) {
        //$city = DB::table('city')->where('province_id', $province_id)->get();
        $city = City::whereProvinceId($province_id)->get();
        return response()->json($city);
    }

    public function test($uplineId, $fromId) {
        $newNetwork = Network::where([
            ['upline_id', '=', 94],
            ['kaki', '=', 1],
            ['sponsor_id', '=', 89],
            ['member_id', '=', 96],
        ])->first();
        return $newNetwork;
        ////Auto withdrawal

        //$members = Member::with('mutationsSuccess')
        //    ->whereHas('mutations')->get();
        //$members->map(function ($member) {
        //    $member->balance = $member->mutationsSuccess->sum(function ($m) {
        //            return $m->mutation_debit + $m->mutation_credit;
        //        }) . "<br/>";
        //});
        //
        ////return $members;
        //$members->where('balance', '>', 120000)->map(function ($member) {
        //    $mutation       = [
        //        'member_id'       => $member->member_id,
        //        'mutation_type'   => 'withdraw',
        //        'mutation_debit'  => 0,
        //        'mutation_credit' => -$member->balance,
        //        'mutation_desc'   => 'Auto Withdraw',
        //        'mutation_status' => 'pending',
        //        'created_at'      => Carbon::now()->toDateTimeString(),
        //        'updated_at'      => Carbon::now()->toDateTimeString()
        //    ];
        //    $memberBalance  = $member->balance;
        //    $lastMutationId = Mutation::insertGetId($mutation);
        //    $ppn_amount     = $memberBalance * 10 / 100;
        //    $pph_amount     = $memberBalance * 5 / 100;
        //    $withdrawals    = [
        //        'mutation_id'       => $lastMutationId,
        //        'amount'            => $memberBalance,
        //        'ppn'               => 10,
        //        'ppn_amount'        => $ppn_amount,
        //        'pph'               => 5,
        //        'pph_amount'        => $pph_amount,
        //        'withdrawal_status' => 'pending',
        //        'created_at'        => Carbon::now()->toDateTimeString(),
        //        'updated_at'        => Carbon::now()->toDateTimeString()
        //    ];
        //    Withdrawal::insert($withdrawals);
        //});


        ////Freeze member syarat :

        ////- Tidak sponsori dalam waktu 2 bulan
        ////- Tidak membayar premi di bulan ke 3
        //$members = Member::withCount("sponsored",'premis')->registeredMoreThan(2)->having("sponsored_count",'<',3)->get();
        ////return $members;
        ////    return $members[0]->lastPremi;
        //$members->map(function ($member){
        //    $lateCount = Carbon::create($member->lastPremi[0]->polis_date)->diffInMonths(Carbon::now(),false);
        //    echo $lateCount;exit;
        //    if ($lateCount == 3){
        //        $member->network->network_status = 'freeze';
        //        $member->network->save();
        //    }
        //});


        //AUTO MAINTAIN

        Bonus::whereBonusStatus('paid')->update(['bonus_status'=>'new']);
        $bonuses   = Bonus::whereBonusStatus('new')->groupBy('bonus_to')->selectRaw('bonus_to,sum(bonus_amount) as total')->get();
        //$memberPremi = PolishPayment::whereMemberId($bonuses[1]->bonus_to)->get()->last();
        ////return Carbon::parse($memberPremi->polis_date)->month ;
        //$diffMonth   = (Carbon::parse($memberPremi->polis_date)->month) - date('n');
        //return $diffMonth;
        //return $bonuses;
        $mutations = [];
        $premis    = [];
        foreach ($bonuses as $bonus) {

            $mutations[] = [
                'member_id' => $bonus->bonus_to,
                'mutation_type' => 'closingdate',
                'mutation_debit' => $bonus->total,
                'mutation_credit' => 0,
                'mutation_desc' => 'Auto Maintain Bonus',
                'mutation_status' => 'success',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ];
            //jika total bonus lebih besar dari premi
            if ($bonus->total > env("PREMI_AMOUNT", 150000)) {
                $countCanPay = floor($bonus->total / 150000);
                //return $countCanPay;
                $memberPremi = PolishPayment::whereMemberId($bonus->bonus_to)->get()->last();
                //return $memberPremi;
                $diffMonth   = (Carbon::parse($memberPremi->polis_date)->month) - date('n');
                //echo $diffMonth;
                $sisaBonus = $bonus->total;
                if ($diffMonth < 0 && abs($diffMonth) != 1) {
                    $iMax = abs($diffMonth);

                    //jika bonus hanya cukup untuk membayar premi menunggak
                    if ($countCanPay < $iMax){
                        $iMax = $countCanPay;
                    }elseif ($countCanPay == ($iMax + 1)){
                        $iMax = $countCanPay + 1;
                    }else{
                        $iMax += 1;
                    }

                    //return $iMax;
                    //jika nominal bonus cukup untuk menutup kekurangan premi + premi satu bulan kedepan
                    for ($i = 0; $i < $iMax; $i++) {
                        $mutations[] = [
                            'member_id' => $bonus->bonus_to,
                            'mutation_type' => 'premi',
                            'mutation_debit' => 0,
                            'mutation_credit' => -150000,
                            'mutation_desc' => 'Pembayaran Premi '.Carbon::parse($memberPremi->polis_date)->addMonths($i+1)->format('M Y'),
                            'mutation_status' => 'success',
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ];
                        $premis[]    = [
                            'member_id' => $bonus->bonus_to,
                            'nominal' => env("PREMI_AMOUNT", 150000),
                            'polis_date' => Carbon::parse($memberPremi->polis_date)->addMonths($i+1),
                            'pp_type' => 'automaintain',
                            'pp_status' => 'success',
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ];
                        $sisaBonus = $bonus->total-150000;
                    }

                } else {
                    $sisaBonus = $bonus->total-150000;
                    $mutations[] = [
                        'member_id' => $bonus->bonus_to,
                        'mutation_type' => 'premi',
                        'mutation_debit' => 0,
                        'mutation_credit' => -150000,
                        'mutation_desc' => 'Pembayaran Premi '.Carbon::parse($memberPremi->polis_date)->addMonth()->format('M Y'),
                        'mutation_status' => 'success',
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];
                    $premis[]    = [
                        'member_id' => $bonus->bonus_to,
                        'nominal' => env("PREMI_AMOUNT", 150000),
                        'polis_date' => Carbon::parse($memberPremi->polis_date)->addMonth(),
                        'pp_type' => 'automaintain',
                        'pp_status' => 'success',
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];
                }

                //insert withdrawal???
                //if ($sisaBonus > 120000){
                //    Withdrawal::insert([
                //
                //    ]);
                //}

            }
        }
        $a= Mutation::insert($mutations);
        $b = Bonus::whereBonusStatus('new')->where('created_at', '<=', Carbon::now()->toDateTimeString())->update(['bonus_status' => 'paid','paid_at'=>Carbon::now()->toDateTimeString(),'updated_at' => Carbon::now()->toDateTimeString()]);
        $c = PolishPayment::insert($premis);

        return [$a,$b,$c];


        ////BONUS GENERASI LEVEL

        //$networks = $this->recursiveUpline($uplineId, 8);
        //return $networks;
        //$bonuses  = [];
        //$iMax = count($this->bonus);
        //for ($i = 0; $i < $iMax; $i++) {
        //
        //    //Default bonus to ADMIN
        //    $bonusToId = 1;
        //    $bonusDesc = "(Unqualified Member) Bonus generasi level ke " . ($i + 1) . ".";
        //    $bonusType = "unqualified";
        //    if (isset($networks[$i])) {
        //        $network = $networks[$i];
        //
        //        $unqualifiedFromId = $network->member->member_id;
        //
        //        //Jika member dalam status FREEZE
        //        if($network->member->member_status === "freeze" || $network->member->member_status === "block"){
        //            goto defaultbonus;
        //        }
        //
        //        //Jika member dalam status ACTIVE
        //        if ($network->member->member_qualified == 8){
        //            goto updatebonus;
        //        }elseif ($network->member->member_qualified == 5 && $i < 5){
        //            goto updatebonus;
        //        }elseif ($network->member->member_qualified == 3 && $i == 2){
        //            goto updatebonus;
        //        }elseif ($network->member->member_qualified == 3 && $i < 2){
        //            goto updatebonus;
        //        }elseif ($network->member->member_qualified < 3 && $i < 2){
        //            goto updatebonus;
        //        }
        //        goto defaultbonus;
        //    }else{
        //        goto defaultbonus;
        //    }
        //
        //    updatebonus:
        //    $bonusToId = $network->member_id;
        //    $bonusDesc = "Bonus generasi level ke " . ($i + 1) . ".";
        //    $bonusType = "generasi";
        //    $unqualifiedFromId = 0;
        //
        //    defaultbonus:
        //
        //    $bonuses[] = [
        //        "bonus_to" => $bonusToId,
        //        "bonus_from" => $fromId,
        //        "unqualified_from" => $unqualifiedFromId,
        //        "bonus_amount" => $this->bonus[$i],
        //        "bonus_type" => $bonusType,
        //        "bonus_desc" => $bonusDesc,
        //        "bonus_status" => "new"
        //    ];
        //}
        //Bonus::insert($bonuses);
    }

    //public function nestable($uplineId) {
    //    $network = Network::where("member_id", $uplineId)->first();
    //    return $network->isQualified;
    //    //Proses input bonus
    //
    //    if ($network->networkUpline != null) {
    //        $this->nestable($network->upline_id);
    //    }
    //
    //}

    public function bank(){
      $user = Auth::guard('member')->user();
      $member = Member::where('id', $user->id)->with('memberData')->first();
      $data = $member->memberData;
      return view('frontend.profile.bank-account', compact('user', 'data'));
    }

    public function bankSubmit(Request $req){
      $user = Auth::guard('member')->user();
      $member = MemberData::where('id', $user->member_data_id)->update([
        'bank_account' => $req->bank_account,
        'bank_number' => $req->bank_number,
        'bank' => strtolower($req->bank)
      ]);

      flash('Bank account successful updated')->success();
      return redirect()->route('membership.bank');
    }

    public function security(){
      return view('frontend.profile.security');
    }

    public function passwordUpdate(Request $req){
      $user = Auth::user();

      $rules  = [
          'new_password'            => 'required|string|min:6',
      ];

      $validation = Validator::make($req->all(), $rules);

      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return Redirect::back()->withInput($req->all());
      }

      if(Hash::check($req->old_password, Auth::guard('member')->user()->password)){
        if ($req->new_password == $req->confirmation_new_password){

          $new_password = Hash::make($req->new_password);
          if ($user->parent_id == null || $user->parent_id == '' || !$user->parent_id || $user->parent_id == 0){
            $child = Member::where('parent_id', $user->id)->get();
            foreach ($child as $i) {
              // code...
              $i->password =$new_password;
              $i->save();
            }
          } else {
            $parent = Member::where('id', $user->parent_id)->update([
              'password' => $new_password
            ]);

            $child = Member::where('parent_id', $user->parent_id)->get();
            foreach ($child as $i) {
              // code...
              if($user->id != $i->id){
                $i->password =$new_password;
                $i->save();
              }
            }
          }

          $update = Member::where('id', $user->id)->update([
            'password' => $new_password
          ]);
          flash('New Password successful updated')->success();
        } else {
          flash('Confirmation password incorrect')->error();
        }
      } else {
        flash('Old password is wrong')->error();
      }

      return redirect()->route('membership.security');
    }

    public function walletUpdate(Request $req){
      $user = Auth::user();
      $rules  = [
          'new_wallet_password'            => 'required|string|min:6',
      ];

      $validation = Validator::make($req->all(), $rules);

      if ($validation->fails()) {
        flash($validation->errors()->first())->error();
        return Redirect::back()->withInput($req->all());
      }

      if(Hash::check($req->old_wallet_password, Auth::guard('member')->user()->wallet_password)){
        if ($req->new_wallet_password == $req->confirmation_new_wallet_password){
          $new_password = Hash::make($req->new_wallet_password);

          if ($user->parent_id == null || $user->parent_id == '' || !$user->parent_id || $user->parent_id == 0){
            $child = Member::where('parent_id', $user->id)->get();
            foreach ($child as $i) {
              // code...
              $i->wallet_password =$new_password;
              $i->save();
            }
          } else {
            $parent = Member::where('id', $user->parent_id)->update([
              'wallet_password' => $new_password
            ]);

            $child = Member::where('parent_id', $user->parent_id)->get();
            foreach ($child as $i) {
              // code...
              if($user->id != $i->id){
                $i->wallet_password =$new_password;
                $i->save();
              }
            }
          }

          $update = Member::where('id', $user->id)->update([
            'wallet_password' => $new_password
          ]);
          flash('The wallet password is updated')->success();
        } else {
          flash('Confirmation wallet password not correct')->error();
        }
      } else {
        flash('Your old wallet password is wrong')->error();
      }

      return redirect()->route('membership.security');
    }

    public function qrCode(){

    }
}
