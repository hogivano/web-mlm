<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Http\Models\Members;
use App\Wallet;
use App\Member;
use App\MemberData;
use App\Models\City;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class MemberController extends Controller {

    public function __construct() {
        $this->middleware('auth:member');

    }

    public function index() {
        $users = DB::table('members')->select('member_name', 'member_username', 'member_id', 'member_network')->get();
        $users = json_decode($users, true);
        return view('vendor.voyager.members.jaringan', ['users' => $users]);
    }

    public function profile() {
      $user = Auth::guard('member')->user();
      $member = Member::where('id', $user->id)->with('memberData', 'wallet')->first();
      $data = $member->memberData;
      $wallet = $member->wallet;
      return view('frontend.profile.profile', compact('data', 'user', 'wallet'));
    }

    public function edit(Request $request) {
        $userId              = Auth::guard('member')->user()->member_id;

        if ($request->has("member_name")) {
            $rules = [
                'member_name' => 'required|string|max:100',
                'member_pob' => 'required|string|max:100',
                'member_dob' => 'required|date',
                'gender' => 'required|string|max:10',
                'member_work' => 'required|string|max:100',
                'member_waris' => 'required|string|max:100',
                'member_hub' => 'required|string|max:100',
                'city' => 'required|string',
                'member_address' => 'required|string|max:250',
                'postal_code' => 'required|string|max:7',
//                'member_identity' => 'required|string|max:5',
//                'number_identity' => 'required|digits_between:12,16',
//                'no_hp' => 'required|regex:/(08)[0-9]/|digits_between:9,14',
//                'email' => 'required|string|email|max:100|unique:members,member_email',
                'member_bank' => 'required|string|max:100',
                'member_bank_number' => 'required||digits_between:9,16',
                'member_bank_account' => 'required|string|max:100',
            ];

            $errorMessages = [
                'member_name.required' => "Silahkan masukkan Nama",
                'member_pob.required' => "Silahkan masukkan Tempat Lahir",
                'member_dob.required' => "Silahkan masukkan Tanggal Lahir",
                'member_work.required' => "Silahkan masukkan Pekerjaan",
                'member_waris.required' => "Silahkan masukkan Ahli Waris",
                'member_hub.required' => "Silahkan masukkan Hubungan dengan Ahli Waris",
                'city.required' => "Silahkan pilih Kota/Kabupaten",
                'member_address.required' => "Silahkan masukkan Alamat",
                'postal_code.required' => "Silahkan masukkan Kode Pos",
                'postal_code.max' => "Kode Pos tidak valid",
                'member_identity.required' => "Silahkan pilih Jenis Identitas",
                'number_identity.required' => "Silahkan masukkan Nomor Identitas",
                'number_identity.digits_between' => "Nomor Identitas tidak valid",
                'no_hp.required' => "Silahkan masukkan Nomor HP",
                'no_hp.digits_between' => "Format Nomor HP tidak valid",
                'email.required' => "Silahkan masukkan Email",
                'email.email' => "Format Email tidak valid",
                'member_bank.required' => "Silahkan masukkan Nama Bank",
                'member_bank_number.required' => "Silahkan masukkan Nomor Rekening",
                'member_bank_number.digits_between' => "Nomor Rekening tidak valid",
                'member_bank_account.required' => "Silahkan masukkan Nama Pemilik Rekening",
            ];

            $validation = Validator::make($request->all(), $rules, $errorMessages);
            $cities     = City::where('province_id', $request['province'])->get();
            if ($validation->fails()) {
                return redirect()->back()->with([
                    'error' => $validation->errors()->first(),
                    'cities' => $cities

                ])->withInput();
            } else {
//                if ($request->has("member_photo")){
//                    die("PHOTo");
//
//                }
                return $this->prosesEdit($request);
            }

        }
        $data['member_item'] = Member::select('*')
            ->leftJoin("city", "city.city_id", "=", "member_city")
            ->where('members.member_id', $userId)
            ->first();
        $data["province"]    = Province::select()->with("cities")->get();
        $data["cities"]      = City::select()->where("province_id", "=", $data['member_item']->province_id)->get();
//        $data["member_type"]     = Auth::guard('member')->user()->member_type;

        return view('memberarea.profile.edit', $data);
    }

    public function prosesEdit(Request $request) {
        $member_photos = $request['member_photos'];
        $member_id     = Auth::guard('member')->user()->member_id;
//        $member_date = $request['member_date'];
        $member_photo = $request->file('member_photo');
        $member_name  = $request['member_name'];
//        $member_username = $request['member_username'];
//        $member_email = $request['member_email'];
        $member_waris = $request['member_waris'];
        $member_hub   = $request['member_hub'];
//        $member_mobile = $request['member_mobile'];
        $member_gender  = $request['gender'];
        $member_work    = $request['member_work'];
        $member_pob     = $request['member_pob'];
        $member_bank    = $request['member_bank'];
        $member_bank_account = $request['member_bank_account'];
        $member_bank_number = $request['member_bank_number'];
        $member_bank_branch = $request['member_bank_branch'];
        $member_dob     = $request['member_dob'];
        $member_address = $request['member_address'];
        $member_status  = $request['member_status'];

        if ($member_photo === NULL) {
            $update   = Member::where('member_id', $member_id)
                ->update([
                    'member_photo' => $member_photos,
                    'member_name' => $member_name,
//                    'member_username'=>$member_username,
//                    'member_email'=>$member_email,
                    'member_waris' => $member_waris,
                    'member_hub' => $member_hub,
//                    'member_mobile' => $member_mobile,
                    'member_gender' => $member_gender,
                    'member_work' => $member_work,
                    'member_pob' => $member_pob,
                    'member_dob' => $member_dob,
                    'member_address' => $member_address,
                    'member_city' => $request->input("city"),
                    'member_postcode' => $request->input("postal_code"),
                    'member_bank' => $member_bank,
                    'member_bank_account' => $member_bank_account,
                    'member_bank_number' => $member_bank_number,
                    'member_bank_branch' => $member_bank_branch
                ]);
            $response = redirect('/membership/profile')->with('success', 'Data Profile berhasil diubah.');
        } else {
            $ext = $member_photo->getMimeType();
            $ext = substr($ext, 0, 5);
            if ($ext != 'image') {
                return redirect()->back()->with('error', 'inputan harus berupa image');
            } else {


//                $logo->move(public_path('/asset/customer/'),$logo->getClientOriginalName());
                $filename = time() . $member_id . $member_photo->getClientOriginalName();

                $canvas = Image::canvas(600, 250);

                $resizeImage = Image::make($member_photo)->resize(600, 250, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $canvas->insert($resizeImage, 'center');
                $canvas->save(public_path('/img/profile/') . $filename);

            }
            $update = Member::where('member_id', $member_id)
                ->update([
                    'member_photo' => $filename,
                    'member_name' => $member_name,
//                    'member_username'=>$member_username,
//                    'member_email'=>$member_email,
                    'member_waris' => $member_waris,
                    'member_hub' => $member_hub,
//                    'member_mobile' => $member_mobile,
                    'member_gender' => $member_gender,
                    'member_work' => $member_work,
                    'member_pob' => $member_pob,
                    'member_dob' => $member_dob,
                    'member_address' => $member_address,
                    'member_city' => $request->input("city"),
                    'member_postcode' => $request->input("postal_code"),
                    'member_bank' => $member_bank,
                    'member_bank_account' => $member_bank_account,
                    'member_bank_number' => $member_bank_number,
                    'member_bank_branch' => $member_bank_branch
                ]);

            $response = redirect('/membership/profile')->with('success', 'Data Profile berhasil diubah.');
        }

        return $response;

//        return $member_photo;


    }

    public function ubahPassword() {

        return view('memberarea.profile.ubah-password');
    }

    public function prosesUpdatePassword(Request $request) {

        if (!Hash::check($request['password_lama'], Auth::guard('member')->user()->member_password)) {
            return redirect()->back()->with([
                'error'  => "Password Anda salah"
            ])->withInput();
        }

        $rules = [
            "password" => "required|string|min:6|confirmed",
        ];

        $errorMessages = [
            'password.required' => "Silahkan masukkan Password baru",
            'password.min' => "Password minimal 6 karakter",
            'password.confirmed' => "Konfirmasi Password tidak cocok"
        ];
        $validation    = Validator::make($request->all(), $rules, $errorMessages);
        if ($validation->fails()) {
            return redirect()->back()->with([
                'error' => $validation->errors()->first()
            ])->withInput();
        }

        $password = $request['password'];
        Member::where('member_id',Auth::guard('member')->user()->member_id)
            ->update(['member_password' => Hash::make($password)]);
        return redirect('/membership/profile')->with('success', 'Password berhasil diubah.');
//        return "TEST";
    }

    public function invite(){
      $username = Auth::guard('member')->user()->username;
      return view('frontend.invite.index', compact('username'));
    }
}
