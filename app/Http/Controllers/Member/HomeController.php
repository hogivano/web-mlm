<?php

namespace App\Http\Controllers\Member;

use App\Bonus;
use App\Http\Controllers\Controller;
use App\Member;
use App\Network;
use App\PolishPayment;
use App\TokenTransaction;
use App\TopUp;
use App\Character;
use App\CharactersOrder;
use App\CharactersTokenTransactions;
use App\CharactersTransactions;
use App\Wallet;
//use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;


class HomeController extends Controller {
    public function __construct() {
        $this->middleware('auth:member');
    }

    public function index(Request $request) {
      $user = Auth::guard('member')->user();
      $data = Character::with(['charactersOrder' => function($q) use ($user){
        $q->where('status', 'request')->where('member_id', $user->id);
      }, 'charactersSeller' => function($q) use ($user){
        $q->where('member_id', $user->id)->where('status', '!=', 'sold');
      }])->orderBy('characters_number', 'ASC')->get();

      // return response()->json($data);

      return view('frontend.home', compact('data'));
    }

    public function autoHire(Request $req){
      // return response()->json($req->checkbox);
      if(!$req->checkbox){
        flash("you must order min 1 character for auto hire")->error();
        return redirect()->back();
      }

      $user = Auth::guard('member')->user();
      $cekChild = [];
      if ($user->parent_id != null || $user->parent_id != 0){
        $cekChild  = Member::where('parent_id', $user->id)->orWhere('parent_id', $user->parent_id)->orWhere('id', $user->parent_id)->with('wallet')->get();
      } else {
        $cekChild  = Member::where('parent_id', $user->id)->orWhere('id', $user->parent_id)->with('wallet')->get()->toArray();
        $thisAccount = Member::where('id', $user->id)->with('wallet')->first();
        array_push($cekChild, $thisAccount);
      }

      $arrTotalSoul = [];

      foreach ($cekChild as $user) {
        // code...
        $characterAll = Character::with(['charactersOrder' => function($q) use ($user) {
          $q->where('member_id', $user->id)->where('status', 'request');
        }, 'charactersSeller' => function($q) use ($user){
          $q->where('member_id', $user->id)->where('status', '!=', 'sold');
        }])->orderBy('characters_number', 'ASC')->get();

        $cekChar = true;
        $cekBox = true;
        $total_soul = 0;
        foreach ($characterAll as $all) {
          // code...
          $cekPos = false;
          foreach ($req->checkbox as $key => $value) {
            if ($key == $all->character_id){
              $cekPos = true;
              $total_soul = $total_soul + $all->soul;
              break;
            }
          }

          if ($cekPos){
            if (sizeof($all->charactersOrder) > 0 || sizeof($all->charactersSeller) > 0){
              $cekBox = false;
            }
          }

          if ($all->is_sequence == 1 && !$cekPos){
            if (sizeof($all->charactersOrder) == 0 && sizeof($all->charactersSeller) == 0){
              $cekChar = false;
            }
          }

          if(!$cekChar || !$cekBox){
            flash('Sorry auto hire not accept, please order again')->error();
            return redirect()->back();
            break;
          }
        }

        if (!$user->wallet){
          flash('sorry wallet account ' . $user->username . ' is not found')->error();
          return redirect()->back();
        }

        if($user->wallet->soul < $total_soul){
          flash('sorry soul in ' . $user->username . ' is not enough')->error();
          return redirect()->back();
        }

        $arrTotalSoul[$user->id] = $total_soul;
      }

      foreach ($cekChild as $user) {
        // code...
        $wallet = Wallet::where('member_id', $user->id)->first();

        foreach ($req->checkbox as $key => $value) {
          // code...
          $character = Character::where('character_id', $key)->first();

          $newOrder = CharactersOrder::create([
            'member_id' => $user->id,
            'character_id' => $character->character_id,
            'date_order' => date('Y-m-d H:i:s'),
            'status' => 'request'
          ]);

          $newCharactersTokenTransaction = CharactersTokenTransactions::create([
            'characters_order_id' => $newOrder->id,
            'soul' => $character->soul,
            'status' => 'sent'
          ]);
        }

        $wallet->soul = $wallet->soul - $arrTotalSoul[$user->id];
        $wallet->save();
      }

      flash('success auto hire character')->success();
      return redirect()->route('membership.home');
    }

    public function showPictureCharacter($id){
      $cek = Character::where('character_id', $id)->first();
      if ($cek->picture == null){
        return null;
      }

      $contents = Storage::get($cek->picture);
      if (!$contents){
        return null;
      }

      $file_extension = strtolower(substr(strrchr($cek->picture,"."),1));

      switch( $file_extension ) {
          case "gif": $ctype="image/gif"; break;
          case "png": $ctype="image/png"; break;
          case "jpeg": $ctype="image/jpeg"; break;
          case "jpg": $ctype="image/jpeg"; break;
          default:
      }
      ob_clean();
      header("Content-type: " . $ctype);
      $response = Image::make($contents)->response($ctype);
      // $response->header('Content-Type', $ctype);
      return $response;
    }

}
