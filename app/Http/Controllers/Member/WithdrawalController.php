<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Withdrawal;
use Illuminate\Support\Facades\Auth;

class WithdrawalController extends Controller {

    public function __construct() {
        $this->middleware('auth:member');
    }

    public function index() {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }

        $memberId                = Auth::guard('member')->user()->member_id;
        $withdrawal              = Withdrawal::join('mutations', 'withdrawals.mutation_id', '=', 'mutations.mutation_id')
            ->where('mutations.member_id', $memberId)
            ->orderBy('withdrawals.withdrawal_id', 'desc')
            ->select('withdrawals.*')
            ->paginate(20);
        $pending                 = Withdrawal::join('mutations', 'withdrawals.mutation_id', '=', 'mutations.mutation_id')
            ->where('mutations.member_id', $memberId)
            ->where('withdrawals.withdrawal_status', 'pending')
            ->orderBy('withdrawals.withdrawal_id', 'desc')
            ->select('withdrawals.*')
            ->sum('amount');
        $process                 = Withdrawal::join('mutations', 'withdrawals.mutation_id', '=', 'mutations.mutation_id')
            ->where('mutations.member_id', $memberId)
            ->where('withdrawals.withdrawal_status', 'process')
            ->orderBy('withdrawals.withdrawal_id', 'desc')
            ->select('withdrawals.*')
            ->sum('amount');
        $done                    = Withdrawal::join('mutations', 'withdrawals.mutation_id', '=', 'mutations.mutation_id')
            ->where('mutations.member_id', $memberId)
            ->where('withdrawals.withdrawal_status', 'done')
            ->orderBy('withdrawals.withdrawal_id', 'desc')
            ->select('withdrawals.*')
            ->sum('amount');
        $cancel                  = Withdrawal::join('mutations', 'withdrawals.mutation_id', '=', 'mutations.mutation_id')
            ->where('mutations.member_id', $memberId)
            ->where('withdrawals.withdrawal_status', 'cancel')
            ->orderBy('withdrawals.withdrawal_id', 'desc')
            ->select('withdrawals.*')
            ->sum('amount');
        $data['withdrawal_list'] = $withdrawal;
        $data['no']              = 1;
        $data['pending']         = $pending;
        $data['process']         = $process;
        $data['done']            = $done;
        $data['cancel']          = $cancel;

        $data['member_type'] = Auth::guard('member')->user()->member_type;
//        return $pending." - ".$process;
//        return $withdrawal;
        return view('memberarea.withdrawal.withdrawal-list', $data);
    }
}
