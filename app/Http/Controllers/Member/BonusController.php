<?php

/**
 * Created by PhpStorm.
 * User: j3p0n
 * Date: 5/4/2019
 * Time: 10:55 PM
 */

namespace App\Http\Controllers\Member;

use App\Bonus;
use App\Http\Controllers\Controller;
use App\Models\Mutation;
use App\Models\Withdrawal;
use Illuminate\Support\Facades\Auth;

class BonusController extends Controller {

    public function __construct() {
        $this->middleware('auth:member');
    }

    public function index() {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }

        $currentId = Auth::guard('member')->user()->member_id;
        $bonus     = Bonus::where('bonus_to', $currentId)
            ->orderBy('bonus_id', 'desc')->paginate(50);
//       return $bonus[0]->memberFrom->member_name;
        return view('memberarea.bonus.bonus', [
            'bonus_list' => $bonus,
            'no' => 1,
            'member_type' => Auth::guard('member')->user()->member_type
        ]);
    }

    public function mutasi() {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }
        $currentId     = Auth::guard('member')->user()->member_id;
        $mutation_list = Mutation::where('member_id', $currentId)->orderBy('mutation_id', 'asc')->get();
        $total_debit   = Mutation::where('member_id', $currentId)->sum('mutation_debit');
        $total_credit  = Mutation::where('member_id', $currentId)->sum('mutation_credit');

        $data['mutation_list'] = $mutation_list;
        $data['total_debit']   = $total_debit;
        $data['total_credit']  = $total_credit;
//        $data["member_type"]     = Auth::guard('member')->user()->member_type;

//        return $mutation_list;
        return view('memberarea.bonus.mutasi-bonus', $data);
    }

    public function detail($mutasiId) {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }
        $detail['detail'] = Withdrawal::join('mutations', 'withdrawals.mutation_id', '=', 'mutations.mutation_id')
            ->where('mutations.mutation_id', $mutasiId)
            ->where('mutations.member_id', Auth::guard('member')->user()->member_id)
            ->select('withdrawals.*', 'mutations.mutation_desc')
            ->first();
//        return json_encode($detail);
//        $detail["member_type"]     = Auth::guard('member')->user()->member_type;
        return view('memberarea.bonus.detail-mutasi', $detail);
    }
}
