<?php

/**
 * Created by PhpStorm.
 * User: j3p0n
 * Date: 5/4/2019
 * Time: 10:56 PM
 */

namespace App\Http\Controllers\Member;

//use App\Http\Controllers\Auth\LoginController as Controller;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Member;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

//use App\Http\Controllers\Controller;

class LoginController extends Controller {

    protected $redirectTo = '/membership/home';

    public function __construct() {
        //parent::__construct();
        //
        // $this->middleware('guest:member')->except('logout');
    }

    /**
     * Show the login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm() {
        if (Auth::guard('member')->check()){
          return redirect()->route('membership.home');
        }

        return view('frontend.login.login', [
            'title' => 'Member Login',
            'loginRoute' => 'membership.login',
        ]);
    }

    /**
     * Login the member.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request) {
        //Validation...
        // return response()->json($request->all());
        $this->validator($request);

        // Login the admin...
        // $user = Member::where('username', $request->member_username)->first();
        // if (!empty($user)){
        //   if (Hash::check($request->member_password, $user->password)){
        //
        //   }
        // }

        $user = Member::where('username', $request->member_username)->first();
        if ($user){
          if ($user->parent_id == null || $user->parent_id == 0){
            if (Auth::guard('member')->attempt(['username' => strtolower($request->member_username), 'password' => $request->member_password])) {
              //Authentication passed...
              return redirect()
              ->intended(route('membership.home'))
              ->with('status', 'Login Success');
            }
          }
        }
        //Authentication failed...
        //Redirect the admin...
        return $this->loginFailed();
    }

    /**
     * Validate the form data.
     *
     * @param \Illuminate\Http\Request $request
     * @return
     */
    private function validator(Request $request) {
        //validation rules.
        $rules = [
            'member_username' => 'required|exists:member,username|min:6|max:50',
            'member_password' => 'required|string|min:6|max:255',
        ];
        //custom validation error messages.
        $messages = [
            '*' => 'Login failed.'
        ];
        //validate the request.
        $request->validate($rules, $messages);
    }

    /**
     * Redirect back after a failed login.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    private function loginFailed() {
        //Login failed...

        return redirect()
            ->back()
            ->withInput()
            ->with('error', 'username or password not found');
    }

    /**
     * Logout the admin.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request) {
        //logout the admin...
//        Session::flush();
        Auth::guard('member')->logout();
        return redirect()
            ->route('membership.login')
            ->with('status', 'Member has been logged out!');
    }

    public function username() {
        return 'member_username';
    }

    protected function guard() {
        return Auth::guard('member');
    }
}
