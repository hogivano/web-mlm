<?php

namespace App\Http\Controllers\Member;

use Auth;
use App\Member;
use App\MemberData;
use App\BankCompany;
use App\Character;
use App\CharactersOrder;
use App\CharactersSeller;
use App\CharacterTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HireController extends Controller
{
  public function __construct(){
    $this->middleware('auth:member');
  }

  public function index(){
    $user = Auth::guard('member')->user();
    $data = Character::with(['charactersOrder' => function($q) use ($user){
      $q->where('member_id', $user->id);
    }])->orderBy('characters_number', 'ASC')->get();

    $charactersOrder = CharactersOrder::where('member_id', $user->id)->with(['character', 'charactersTransactions.charactersSeller' => function($q){
      $q->with(['member.memberData' => function ($r){
        $r->with('country');
      }]);
    }])->where('status', '!=', 'unsuccess')->where('status', '!=', 'cancel')->orderBy('date_order', 'DESC')->get();

    // return response()->json($charactersOrder);

    $charactersSeller = CharactersSeller::where('member_id', $user->id)->with(['character', 'charactersTransactions.charactersOrder' => function($q){
      $q->with(['member.memberData' => function ($r){
        $r->with('country');
      }]);
    }])->orderBy('created_at', 'DESC');
    $charactersSeller = $charactersSeller->where('status', '!=', 'sold')->where('status', '!=', 'expired')->get();

    $soldSeller = CharactersSeller::where('member_id', $user->id)->with(['character', 'charactersTransactions.charactersOrder' => function($q){
      $q->with(['member.memberData' => function ($r){
        $r->with('country');
      }]);
    }])->orderBy('created_at', 'DESC')->paginate(5, ['*'], 'page-sell')->appends(request()->query());

    $historyOrder = CharactersOrder::where('member_id', $user->id)->with(['character', 'charactersTransactions.charactersSeller' => function($q){
      $q->with(['member.memberData' => function ($r){
        $r->with('country');
      }]);
    }])->orderBy('date_order', 'DESC')->paginate(5, ['*'], 'page-order')->appends(request()->query());

    $member = Member::where('id', $user->id)->with('memberData')->first();
    $memberData = $member->memberData;
    $bankCompany = BankCompany::whereHas('bank', function($q) use ($memberData){
      $q->where('country_id', $memberData->country_id);
    })->with('bank')->get();
    return view('frontend.hire.index', compact('data', 'charactersOrder', 'charactersSeller', 'bankCompany', 'soldSeller', 'historyOrder'));
  }
}
