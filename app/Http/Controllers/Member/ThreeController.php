<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Member;
use App\Network;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ThreeController extends Controller {
    public function __construct() {
        $this->middleware('auth:member');
    }

    public function index($memberId = 0) {

        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }

        if (empty($memberId)) {
            $sponsorId = Auth::guard('member')->user()->member_id;
        } else {
            //validasi upline apakah satu jaringan dengan sponsor
            if (!$this->isOneNetwork($memberId, Auth::guard('member')->user()->member_id)) {
                return redirect()->back()->with('error', 'Member tidak ditemukan.');
            }
            
            $cek = Network::owner()->where("networks.member_id",$memberId)->first();
            $aku = Network::owner()->where("networks.member_id",Auth::guard('member')->user()->member_id)->first();

            if (empty($cek)) {
                $sponsorId = Auth::guard('member')->user()->member_id;
            } else {
                if ($cek->network_level < $aku->network_level) {
                    $sponsorId = Auth::guard('member')->user()->member_id;
                } else {
                    $sponsorId = $memberId;
                }
            }

//            return $cek;

        }
        $me      = Network::owner()->where("networks.member_id",Auth::guard('member')->user()->member_id)->first();
        $network = Network::owner()->where("networks.member_id",$sponsorId)->first();
        $upline  = Network::owner()->where("networks.member_id",$network->upline_id)->first();

        //dd(array_key_exists(3,$network->downlines[0]->downlines));
        //return $network->downlines->where('kaki',(4+1))->first();
        return view('memberarea.pohon-jaringan.pohon', [
            'me'      => $me,
            'network' => $network,
            'upline'  => $upline
        ]);

    }

    public function searchNetwork(Request $request) {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }
        $username = $request['username'];
        if ($username === NULL) {
            return redirect()->back()->with('error', 'Member tidak ditemukan.');
        } else {
            $member = Member::where('member_username', $username)->first();
            if (empty($member)) {
                return redirect()->back()->with('error', 'Member tidak ditemukan.');
            }
            $cek = Network::owner()->where("networks.member_id",$member->member_id)->first();
            $aku = Network::owner()->where("networks.member_id",Auth::guard('member')->user()->member_id)->first();

            //validasi upline apakah satu jaringan dengan sponsor
            if (!$this->isOneNetwork($member->member_id, Auth::guard('member')->user()->member_id)) {
                return redirect()->back()->with('error', 'Member tidak ditemukan.');
            }
            if ($cek->network_level < $aku->network_level) {
                return redirect()->back()->with('error', 'Member tidak ditemukan.');
            }

            return redirect('/membership/pohon-jaringan/' . $member->member_id);
        }
    }

    public function isOneNetwork($uplineId, $registrantId) {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }
        //dapatkan jaringan user ini
        if ($uplineId == $registrantId) {
            return true;
        }

        $network = Network::where("member_id", $uplineId)->first();

        if ($network->upline_id == $registrantId) {
            return true;
        }

        $temp = $network;
        for ($i = $network->network_level; 0 < $i; $i--) {
            if ($temp->sponsor_id == $registrantId) {
                return true;
            }
            if ($temp->upline_id == $registrantId) {
                return true;
            }

            $upline = Network::where("member_id", $temp->upline_id)->first();

            if ($upline != null) {
                $temp = $upline;
            }

        }
        return false;
    }

    public function memberDetail($uplineId) {
        $member_type = Auth::guard('member')->user()->member_type;
        if($member_type === 'nasabah'){
            return redirect('/membership/home');
        }
        $member_item = Network::withCount('networkSponsor')->with('member.lastPremi')->whereMemberId($uplineId)->first();
        return $member_item;
    }



//    public function getNetwork(Request $request){
//        $term    = $request['search'];
//        $name    = Network::where('upline_id', 'LIKE', '%' . $term . '%')
//            ->get();
//        $results = array();
//        foreach ($name as $item) {
//            $results[] = ["value" => $item->member_username, "label" => $item->member_name];
//        }
//        return response()->json($results);
//    }
}
