<?php

namespace App\Http\Middleware;

use App\MemberData;
use Closure;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      $user = $request->user();
      $memberData = MemberData::where('id', $user->member_data_id)->first();
      if (!$memberData){
        return redirect()->route('membership.register.information');
      }else {
        if($request->route()->getActionName() == 'App\Http\Controllers\Member\SubAccountController@changeAccount'){
          return $next($request);
        }
        if ($user->status == 'pending'){
          return redirect()->route('membership.complete_account');
        }
        return $next($request);
      }
    }
}
