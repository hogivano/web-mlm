<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    protected $table = 'settings';
    public $timestamps = false;
    
    protected $fillable = [
      'key',
      'display_name',
      'value',
      'details',
      'type',
      'order',
      'group'
    ];
}
