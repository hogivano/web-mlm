<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Reward extends Model
{
    const TABLE_NAME = 'rewards';
    public $timestamps = true;
    protected $table = 'rewards';
    protected $primaryKey = 'reward_id';

    public function rewardId(){
        $currentId = Auth::guard('member')->user()->member_id;
        return $this->hasMany(self::class,'reward_id','reward_id')
            ->leftJoin('reward_histories','reward_histories.reward_id','=','rewards.reward_id')
            ->where('reward_histories.member_id',$currentId)
            ->select('rewards.reward_name','rewards.reward_target','reward_histories.rh_status','reward_histories.created_at');
    }

    public function scopeWithKeyword($query,$keywords){
        $query->where('reward_name','like','%'.$keywords.'%');
    }

    public function scopeWithStartDate($query,$startDate){
        $query->whereBetween('created_at',[$startDate,Now()]);
    }

    public function scopeWithDate($query,$startDate,$endDate){

    }
}
