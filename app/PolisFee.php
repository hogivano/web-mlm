<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolisFee extends Model
{
    const TABLE_NAME = 'polis_fee';
    public $timestamps = false;
    protected $table = 'polis_fee';
    protected $primaryKey = 'fee_id';
}
