<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankCompany extends Model
{
    //
    protected $table = 'bank_company';
    protected $fillable = [
      'bank_id', 'bank_account', 'bank_number', 'token', 'created_at', 'updated_at'
    ];

    public function bank(){
      return $this->belongsTo('App\Bank', 'bank_id', 'id');
    }
}
