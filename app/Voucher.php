<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    const TABLE_NAME = 'voucher_req';
    protected $table = 'voucher_req';
    protected $primary_key = 'voucher_id';
    public $timestamps    = false;
}
