<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    const TABLE_NAME = 'bank';
    protected $table = 'bank';
    public $timestamps    = false;

    protected $fillable = [
      'country_id', 'name'
    ];

    public function country(){
      return $this->belongsTo('App\Country', 'country_id', 'id');
    }
}
