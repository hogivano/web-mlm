<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    const TABLE_NAME = 'characters';
    public $timestamps = true;
    protected $table = 'characters';
    protected $primaryKey = 'character_id';

    protected $fillable = [
      'name', 'picture', 'profit_for', 'price_for', 'soul', 'time',
      'start_order_time', 'end_order_time', 'length_of_sale', 'profit_to', 'price_to', 'characters_number', 'status',
      'is_sequence', 'last_change_id', 'created_at', 'updated_at'
    ];

    public function charactersOrder(){
      return $this->hasMany('App\CharactersOrder', 'character_id', 'character_id');
    }

    public function charactersSeller(){
      return $this->hasMany('App\CharactersSeller', 'character_id', 'character_id');
    }

    public function lastChange(){
      return $this->belongsTo('App\User', 'id', 'last_change_id');
    }
}
