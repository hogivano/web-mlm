<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $table = 'country';
    public $timestamps = false;
    protected $fillable = [
      'name', 'time_zone', 'code_number'
    ];

    public function bank(){
      return $this->hasMany('App\Bank', 'country_id', 'id');
    }
}
