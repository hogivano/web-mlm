<?php

namespace App\Imports;

use App\CharactersSeller;
use App\CharactersOrder;
use App\CharactersTransactions;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CharactersSellerImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    // array header
    // [character_id, seller_id, buyer_id, start_of_sale, end_of_sale, price, status]
    public function collection(Collection $rows)
    {

      foreach ($rows as $row) {
        echo 'oke ' . $row['character_id'];
      }
    }
}
