<?php

namespace App\Imports;

use Auth;
use App\CharactersOrder;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class CharactersOrderImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
        foreach ($collection as $i) {
          // code...
          $update = CharactersOrder::where('id', 'characters_order_id')->updaet([
            'status' => $i['status'],
            'last_change_id' => Auth::guard('web')->user()->id
          ]);
        }
    }
}
