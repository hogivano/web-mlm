<?php

namespace App\Imports;

use Auth;
use App\Member;
use App\Character;
use App\CharactersSeller;
use App\CharactersOrder;
use App\CharactersTransactions;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CharactersTransactionsImport implements ToCollection, WithHeadingRow
{
    public function __construct($id, $sub_account){
      $this->id = $id;
      $this->sub_account = $sub_account;
    }
    /**
    * @param Collection $collection
    */
    public $status = [];

    public function collection(Collection $collection)
    {
        $saveData = [];
        $cekError = false;
        $cekCharacter = false;
        $cekOrder = false;
        $cekSeller = false;
        $cekTransaction = false;
        foreach ($collection as $i) {
          // code...
          if($i['character_id'] != $this->id){
            $this->status = [
              'error' => true,
              'message' => 'Sorry all character_id in excel must ' . $this->id
            ];
            break;
          } else {
            $character = Character::where('character_id', $i['character_id'])->first();
            if(!$character){
              $this->status = [
                'error' => true,
                'message' => 'sorry character_id ('. $i['character_id'] .') not found, check again your excel'
              ];
            }
            $cekSeller = null;
            if ($i['seller_id'] == 0){
              $characters_seller_id = null;
            } else {
              $cekSeller = CharactersSeller::where('member_id', $i['seller_id'])->where('character_id', $i['character_id'])
                ->where('status', 'ready')->first();
              if (!$cekSeller){
                $this->status = [
                  'error' => true,
                  'message' =>'sorry seller_id (' . $i['seller_id'] .') not found, check again your excel'
                ];
              } else {
                $characters_seller_id = $cekSeller->id;
              }
            }

            $cekOrder = CharactersOrder::where('member_id', $i['buyer_id'])->where('character_id', $i['character_id'])->where('status', 'request')->first();
            if (!$cekOrder){
              $this->status = [
                'error' => true,
                'message' =>'sorry buyer_id (' . $i['buyer_id'] .') not found, check again your excel'
              ];
            }

            $buyerMember = Member::where('id', $i['buyer_id'])->first();
            $sellerMember = Member::where('id', $i['seller_id'])->first();

            if($buyerMember && $sellerMember){
              if(!$this->sub_account){
                if($buyerMember->id == $sellerMember->parent_id){
                  $this->status = [
                    'error' => true,
                    'message' =>'Sorry, buyer('. $buyerMember->username .') and seller('. $sellerMember->username .') at the same sub account cannot proccess'
                  ];
                } elseif ($sellerMember->id == $buyerMember->parent_id){
                  $this->status = [
                    'error' => true,
                    'message' =>'Sorry, buyer('. $buyerMember->username .') and seller('. $sellerMember->username .') at the same sub account cannot proccess'
                  ];
                } elseif ($sellerMember->parent_id == $buyerMember->parent_id &&
                  ($sellerMember->parent_id != null && $buyerMember->parent_id != null)) {
                  $this->status = [
                    'error' => true,
                    'message' =>'Sorry, buyer('. $buyerMember->username .') and seller('. $sellerMember->username .') at the same sub account cannot proccess'
                  ];
                }
              }
            } else {
              if($i['seller_id'] != 0){
                $this->status = [
                  'error' => true,
                  'message' =>'Seller not found'
                ];
              }elseif($i['buyer_id'] == 0){
                $this->status = [
                  'error' => true,
                  'message' =>'Buyer not found'
                ];
              }
            }

            $i['pre_price_of_sale_buyer'] = str_replace(".", "", $i['pre_price_of_sale_buyer']);
            $i['price_of_sale_seller'] = str_replace(".", "", $i['price_of_sale_seller']);
            $i['profit_seller'] = str_replace(".", "", $i['profit_seller']);

            if (!$this->checkDate($i['end_of_payment']) || !$this->checkDate($i['start_of_payment']) ){
              $this->status = [
                'error' => true,
                'message' =>'Format start_of_payment or end_of_payment must (YYYY-MM-DD HH:MM:SS)'
              ];
            } else {
              $i['start_of_payment'] = date('Y-m-d H:i:s', strtotime($i['start_of_payment']));
              $i['end_of_payment'] = date('Y-m-d H:i:s', strtotime($i['end_of_payment']));
            }

            $set_start_of_sale = null;
            $set_end_of_sale = null;
            if (!is_numeric($i['estimate_hour']) ||
              !is_numeric($i['pre_price_of_sale_buyer']) || !is_numeric($i['profit_seller']) || !is_numeric($i['price_of_sale_seller'])){
                $this->status = [
                  'error' => true,
                  'message' =>'Sorry, format estimate_hour, pre_price_of_sale_buyer, profit_seller, price_of_sale_seller must numeric'
                ];
            } elseif (($i['estimate_hour'] < 2 || $i['estimate_hour'] > 720) ||
                ($i['profit_seller'] < 1 || strlen((string) $i['profit_seller']) > 16 ) ||
                ($i['pre_price_of_sale_buyer'] < 1 || strlen((string) $i['pre_price_of_sale_buyer']) > 16) ||
                ($i['price_of_sale_seller'] < 1 || strlen((string) $i['price_of_sale_seller']) > 16) ||
                ($i['capital_seller'] < 1 || strlen((string) $i['capital_seller']) > 16)){
                  $this->status = [
                    'error' => true,
                    'message' =>'Sorry, format capital_seller, estimate_hour, pre_price_of_sale_buyer, profit_seller, price_of_sale_seller error'
                  ];
            } elseif($i['capital_seller'] + $i['profit_seller'] != $i['price_of_sale_seller']) {
                $this->status = [
                  'error' => true,
                  'message' =>'Sorry, transaction buyer_id('. $i['buyer_id'] .') and seller_id(' . $i['seller_id'] . ') capital_seller + profit_seller not equals price_of_sale_seller'
                ];
            } else {
              $set_start_of_sale = date('Y-m-d H:i:s');
              $plus = ' +' . $i['estimate_hour'] . ' day';
              $set_end_of_sale = date('Y-m-d H:i:s', strtotime($plus));
            }


            if ($cekOrder && sizeof($this->status) == 0){
              $new = new \stdClass();
              $new->character_id = $i['character_id'];
              $new->characters_order_id = $cekOrder->id;
              $new->characters_seller_id = $characters_seller_id;
              $new->soul = $character->soul;
              $new->price_of_sale = $i['price_of_sale_seller'];
              $new->profit = $i['profit_seller'];
              $new->start_of_payment = $i['start_of_payment'];
              $new->end_of_payment = $i['end_of_payment'];
              $new->status_transaction = 'on_payment';
              $new->status_seller = 'on_payment';
              $new->estimate_hour = $i['estimate_hour'];
              $new->prediction_price_for_buyer = $i['pre_price_of_sale_buyer'];

              array_push($saveData, $new);
            }
          }
        }

        if (sizeof($this->status) == 0){
          foreach ($saveData as $i) {
            // code...
            $cek = CharactersTransactions::where('character_id', $i->character_id)
              ->where('characters_order_id', $i->characters_order_id)->where('characters_seller_id', $i->characters_seller_id)
              ->where('status', 'on_payment')->first();

            if (!$cek) {
              $newTransaction = new CharactersTransactions();
              $newTransaction->character_id = $i->character_id;
              $newTransaction->characters_order_id = $i->characters_order_id;
              $newTransaction->characters_seller_id = $i->characters_seller_id;
              $newTransaction->soul = $i->soul;
              $newTransaction->price_of_sale = $i->price_of_sale;
              $newTransaction->profit = $i->profit;
              $newTransaction->start_of_payment = $i->start_of_payment;
              $newTransaction->end_of_payment = $i->end_of_payment;
              $newTransaction->status = $i->status_transaction;
              $newTransaction->estimate_hour = $i->estimate_hour;
              $newTransaction->prediction_price_for_buyer = $i->prediction_price_for_buyer;
              $newTransaction->last_change_id = Auth::guard('web')->user()->id;
              $newTransaction->save();

              $changeOrder = CharactersOrder::where('id', $i->characters_order_id)->where('character_id', $i->character_id)->update([
                'status' => 'success',
                'last_change_id' => Auth::guard('web')->user()->id
              ]);

              if ($i->characters_seller_id != null || $i->characters_seller_id != 0){
                $updateSeller = CharactersSeller::where('id', $i->characters_seller_id)->update([
                  'status' => 'on_payment',
                  'last_change_id' => Auth::guard('web')->user()->id
                ]);
              }

              $this->status = [
                'error' => false,
                'message' => 'success imported'
              ];
            }else {
              $this->status = [
                'error' => true,
                'message' => 'transaction ('. $cek->id .') on progress'
              ];
              break;
            }
          }
        }
    }

    public function getStatus(){
      return $this->status;
    }

    public function checkDate($date){
      $cek = date_parse($date);
      if ($cek['error_count'] > 0){
        return false;
      } else {
        return true;
      }
    }
}
