<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bonus
 * @package App
 * @property integer bonus_id
 * @property integer bonus_to
 * @property integer bonus_from
 * @property integer bonus_amount
 * @property string bonus_type
 * @property  string bonus_desc
 * @property string bonus_status
 * @property  string paid_at
 * @property string updated_at
 * @property string created_at
 */
class Bonus extends Model {
    const TABLE_NAME = 'bonuses';
    public $timestamps = true;
    protected $table = 'bonuses';
    protected $primary_key = 'bonus_id';

    public function memberFrom() {
        return $this->belongsTo(Member::class, 'bonus_from', 'member_id');
    }
    public function memberTo() {
        return $this->belongsTo(Member::class, 'bonus_to', 'member_id');
    }
}
