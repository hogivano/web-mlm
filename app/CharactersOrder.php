<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharactersOrder extends Model
{
    //
    protected $table = 'characters_order';
    protected $fillable = [
      'character_id', 'member_id', 'date_order', 'status', 'last_change_id', 'created_at', 'updated_at'
    ];

    public function member(){
      return $this->belongsTo('App\Member', 'member_id', 'id');
    }

    public function character(){
      return $this->belongsTo('App\Character', 'character_id', 'character_id');
    }

    public function charactersTransactions(){
      return $this->hasOne('App\CharactersTransactions', 'characters_order_id', 'id');
    }

    public function charactersTokenTransactions(){
      return $this->hasMany('App\CharactersTokenTransactions', 'characters_order_id', 'id');
    }
}
