<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //
    protected $table = 'wallet';
    protected $fillable = [
      'member_id', 'reality', 'soul', 'power', 'created_at', 'updated_at'
    ];

    public function member(){
      return $this->belongsTo('App\Member', 'member_id', 'id');
    }
}
