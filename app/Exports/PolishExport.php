<?php

namespace App\Exports;

use App\PolishPayment;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PolishExport implements FromQuery,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function filter($keyword,$start_date,$end_date,$status){
    $this->keyword = $keyword;
    $this->start_date = $start_date;
    $this->end_date = $end_date;
    $this->status   = $status;

    return $this;
    }
    public function query()
    {
        if($this->keyword != NULL){
           $polish = PolishPayment::query()->join('members','polish_payments.member_id','members.member_id')
               ->where(function ($where) {
               $where->where('members.member_name','like','%'.$this->keyword.'%')
                   ->orWhere('members.member_username','like','%'.$this->keyword.'%');
            });

            if($this->start_date != NULL && $this->end_date === NULL){
                $polish = PolishPayment::query()->whereBetween('polis_date',[$this->start_date,NOW()]);
            }

            if($this->start_date != NULL && $this->end_date != NULL){
                $polish = PolishPayment::query()->whereBetween('polis_date',[$this->start_date,$this->end_date]);
            }

            if($this->status != NULL){
                $polish = PolishPayment::query()->where('pp_status',$this->status);
            }
//            return $polish;
        }elseif($this->start_date != NULL && $this->end_date === NULL){
            $polish = PolishPayment::query()->whereBetween('polis_date',[$this->start_date,NOW()]);
//            return $polish;
        }elseif($this->start_date != NULL && $this->end_date != NULL){
            $polish = PolishPayment::whereBetween('polis_date',[$this->start_date,$this->end_date]);
//            return $polish;
        }
        elseif($this->status != NULL){
            $polish = PolishPayment::query()->where('pp_status',$this->status);
        }
        else{
            $polish = PolishPayment::query();
        }

        return $polish;
    }

    public function map($polish): array
    {
        return [ $polish->withMember->member_name,
            $polish->withMember->member_username,
            $polish->withMember->member_email,
            $polish->withMember->member_mobile,
            $polish->withMember->member_gender,
            $polish->withMember->member_work,
            $polish->withMember->member_identity_number,
            $polish->withMember->member_waris,
            $polish->withMember->member_hub	,
            $polish->withMember->member_address,
            $polish->withMember->member_city,
            $polish->withMember->member_postcode,
            $polish->nominal,
            $polish->polis_date,
            $polish->pp_status
        ];

    }

    public function headings(): array
    {
        return [
            ['Nama','Username','Email','Phone','Jenis Kelamin','Pekerjaan','No KTP','Member Waris','Member Hubungan','Alamat','Kota','Kode Pos',
                'Nominal','Tanggal Polis','Status']
        ];
    }
}
