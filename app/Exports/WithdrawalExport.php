<?php

namespace App\Exports;

use App\Models\Withdrawal;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class WithdrawalExport implements FromQuery,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function forStatus($params){
        $this->status = $params;

        return $this;
    }
    public function query()
    {
        if($this->status == 'pending'){
            $withdrawal = Withdrawal::where(function ($where){
                $where->where('withdrawal_status','pending')
                    ->orWhere('withdrawal_status','process');
            });
        }else{
            $withdrawal = Withdrawal::query();
        }

        return $withdrawal;
    }
    public function map($withdrawal): array
    {
        return [
            $withdrawal->mutasi->member->member_name,
            number_format($withdrawal->amount),
            $withdrawal->ppn.'%',
            number_format($withdrawal->ppn_amount),
            $withdrawal->pph.'%',
            number_format($withdrawal->pph_amount),
            number_format($withdrawal->amount - $withdrawal->ppn_amount - $withdrawal->pph_amount),
            $withdrawal->withdrawal_status,
            date('D, d M Y H:i:s',strtotime($withdrawal->created_at)),
            $withdrawal->mutasi->member->member_bank_account,
            $withdrawal->mutasi->member->member_bank_number,
            $withdrawal->mutasi->member->member_bank,
        ];
    }
    public function headings(): array
    {
        return [
            ['Member Name','Amount', 'PPN','PPN Amount','PPH','PPH Amount','Jumlah Transfer','Withdrawal Status','Date',
                'Nama Rekening','Nomor Rekening','Nama Bank'],
        ];
    }
    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'D' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'F' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'I' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }
}
