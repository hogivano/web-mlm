<?php

namespace App\Exports;

use App\CharactersSeller;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CharactersSellerExport implements FromCollection, WithHeadings, WithMapping
{

    public function __construct($status = null, $character_id = null){
      $this->status = $status;
      $this->character_id = $character_id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $get = CharactersSeller::with('member','character');
        if ($this->status){
          $get->where('status', $this->status);
        }

        if($this->character_id){
          $get->where('character_id', $this->character_id);
        }

        return $get->get();
    }

    public function headings(): array {
      return [
        'character_id',
        'character_name',
        'member_id',
        'member_username',
        'start_of_sale',
        'end_of_sale',
        'capital_price',
        'prediction_price_of_sale',
        'extend_of_sale',
        'status',
      ];
    }

    public function map($charactersSeller):array {
      return [
        $charactersSeller->character->character_id,
        $charactersSeller->character->name,
        $charactersSeller->member_id,
        $charactersSeller->member->username,
        $charactersSeller->start_of_sale,
        $charactersSeller->end_of_sale,
        $charactersSeller->price,
        $charactersSeller->prediction_price,
        $charactersSeller->extend_of_sale,
        $charactersSeller->status
      ];
    }
}
