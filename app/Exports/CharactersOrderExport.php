<?php

namespace App\Exports;

use App\Member;
use App\CharactersOrder;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CharactersOrderExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    public function __construct($start_date, $end_date, $character_id){
      $this->start_date = $start_date;
      $this->end_date = $end_date;
      $this->character_id = $character_id;
    }

    public function headings(): array
    {
        return [
            'id_order',
            'character_id',
            'character_name',
            'member_id',
            'member_username',
            'main_account',
            'member_status',
            'date_order',
            'status',
        ];
    }

    public function collection(){
      return CharactersOrder::where('character_id', $this->character_id)->where('status', 'request')
        ->whereBetween('date_order', [$this->start_date, $this->end_date])->with(['member' => function($q){
          $q->with(['blacklistMember' => function($r){
            $r->with('blacklistStatus')->where('status', 'on');
          }, 'parent']);
        },'character'])->get();
    }

    public function map($charactersOrder): array {
      // $arr = [];
      // foreach ($charactersOrder as $i) {
      //   array_push($arr, );
      // }
      $status = '';
      if(sizeof($charactersOrder->member->blacklistMember) > 0){
        $status = $charactersOrder->member->blacklistMember[0]->blacklistStatus->name;
      }

      return [
        $charactersOrder->id,
        $charactersOrder->character_id,
        $charactersOrder->character->name,
        $charactersOrder->member_id,
        $charactersOrder->member->username,
        ($charactersOrder->member->parent) ? $charactersOrder->member->parent->username : '',
        $status,
        $charactersOrder->date_order,
        $charactersOrder->status
      ];
    }
}
