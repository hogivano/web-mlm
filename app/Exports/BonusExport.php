<?php

namespace App\Exports;

use App\Models\Bonus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class BonusExport implements  FromQuery,WithMapping,WithHeadings
{
    use Exportable;
    public function filter($type,$start,$end,$status){
        $this->type = $type;
        $this->start = $start;
        $this->end = $end;
        $this->status = $status;

        return $this;
    }

    public function query(){
        if($this->status != NULL){
            if($this->start != NULL && $this->end === NULL){
                $bonus = Bonus::query()->where('bonus_type',$this->status)
                                    ->orderBy('bonus_id','desc')
                                    ->whereBetween('created_at',[$this->start,date('Y-m-d')]);
            }elseif($this->start != NULL && $this->end != NULL){
                $bonus = Bonus::query()->where('bonus_type',$this->status)
                    ->orderBy('bonus_id','desc')
                    ->whereBetween('created_at',[$this->start,$this->end]);
            }else{
                $bonus = Bonus::query()->orderBy('bonus_id','desc')
                    ->where('bonus_type',$this->status);
            }
        }
        if($this->type != NULL){
            if($this->start != NULL && $this->end === NULL){
                $bonus = Bonus::query()->where('bonus_type',$this->type)
                    ->orderBy('bonus_id','desc')
                    ->whereBetween('created_at',[$this->start,date('Y-m-d')]);
            }elseif($this->start != NULL && $this->end != NULL){
                $bonus = Bonus::query()->where('bonus_type',$this->type)
                    ->orderBy('bonus_id','desc')
                    ->whereBetween('created_at',[$this->start,$this->end]);
            }else{
                $bonus = Bonus::query()->orderBy('bonus_id','desc')
                    ->where('bonus_type',$this->type);
            }
        }
        return $bonus;
    }

    public function map($bonus):array {
        return[
            $bonus->memberFrom->member_name,
            $bonus->memberTo->member_name,
            $bonus->bonus_type,
            $bonus->bonus_desc,
            $bonus->bonus_status,
            $bonus->created_at
            ];
    }

    public function headings():array {
        return ['Dari','Untuk','Tipe Bonus','Status','Deskripsi','Tanggal'

        ];
    }
}
