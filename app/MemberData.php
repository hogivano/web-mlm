<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberData extends Model
{
    //
    protected $table = 'member_data';
    protected $fillable = [
      'member_id',
      'full_name',
      'wechat_id',
      'number_phone',
      'photo',
      'gender',
      'country_id',
      'bank',
      'bank_account',
      'bank_number',
      'bank_branch',
      'swift_code',
      'updated_at',
      'created_at'
    ];

    public function member(){
      return $this->hasOne('App\Member', 'member_data_id', 'id');
    }

    public function country(){
      return $this->hasOne('App\Country', 'id', 'country_id');
    }
}
