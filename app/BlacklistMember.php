<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlacklistMember extends Model
{
    protected $table = 'blacklist_member';
    public $timestamps = true;

    //status = on/off
    protected $fillable = [
      'member_id', 'blacklist_status_id', 'message', 'status', 'create_at', 'updated_at'
    ];

    public function blacklistStatus(){
      return $this->belongsTo('App\BlacklistStatus', 'blacklist_status_id', 'id');
    }

    public function member(){
      return $this->belongsTo('App\Member', 'member_id', 'id');
    }
}
