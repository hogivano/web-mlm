<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardHistory extends Model
{
    const TABLE_NAME = 'reward_histories';
    public $timestamps = true;
    protected $table = 'reward_histories';
    protected $primaryKey = 'rh_id';



    public function rewards() {
        return $this->hasMany(Reward::class,'reward_id','reward_id')
                ->leftJoin('reward_histories','rewards.reward_id','=','reward_histories.reward_id')
                ->select('reward_name','reward_target','reward_histories.rh_status','reward_histories.created_at');
    }
}
